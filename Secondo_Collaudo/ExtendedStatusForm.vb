﻿Imports SecondoCollaudo.CMClass

Public Class ExtendedStatusForm

    Public Sub New()

        ' La chiamata è richiesta dalla finestra di progettazione.
        InitializeComponent()

        ' Aggiungere le eventuali istruzioni di inizializzazione dopo la chiamata a InitializeComponent().

    End Sub
    Public Sub New(answer As String)

        ' La chiamata è richiesta dalla finestra di progettazione.
        InitializeComponent()

        ' Aggiungere le eventuali istruzioni di inizializzazione dopo la chiamata a InitializeComponent().

    End Sub
    Private Sub ExtendedStatusForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dgExtendedStatus.Rows.Clear()
        With dgExtendedStatus.Rows
            .Add("Reply", myMacchina.Status.Reply)
            .Add("Feeder", myMacchina.Status.Feeder, myMacchina.Status.MsgFeeder)
            .Add("Controller", myMacchina.Status.Controller, myMacchina.Status.MsgController)
            .Add("Reader", myMacchina.Status.Reader, myMacchina.Status.MsgReader)
            .Add("Safe", myMacchina.Status.Safe, myMacchina.Status.MsgSafe)
            .Add("Cassette A", myMacchina.Status.CassetteA, myMacchina.Status.MsgCassetteA)
            .Add("Cassette B", myMacchina.Status.CassetteB, myMacchina.Status.MsgCassetteB)
            .Add("Cassette C", myMacchina.Status.CassetteC, myMacchina.Status.MsgCassetteC)
            .Add("Cassette D", myMacchina.Status.CassetteD, myMacchina.Status.MsgCassetteD)
            .Add("Cassette E", myMacchina.Status.CassetteE, myMacchina.Status.MsgCassetteE)
            .Add("Cassette F", myMacchina.Status.CassetteF, myMacchina.Status.MsgCassetteF)
            If myMacchina.Status.CassetteG <> "" Then .Add("Cassette G", myMacchina.Status.CassetteG, myMacchina.Status.MsgCassetteG)
            If myMacchina.Status.CassetteH <> "" Then .Add("Cassette H", myMacchina.Status.CassetteH, myMacchina.Status.MsgCassetteH)
            If myMacchina.Status.CassetteI <> "" Then .Add("Cassette I", myMacchina.Status.CassetteI, myMacchina.Status.MsgCassetteI)
            If myMacchina.Status.CassetteJ <> "" Then .Add("Cassette J", myMacchina.Status.CassetteJ, myMacchina.Status.MsgCassetteJ)
            If myMacchina.Status.CassetteK <> "" Then .Add("Cassette K", myMacchina.Status.CassetteK, myMacchina.Status.MsgCassetteK)
            If myMacchina.Status.CassetteL <> "" Then .Add("Cassette L", myMacchina.Status.CassetteL, myMacchina.Status.MsgCassetteL)
            If myMacchina.Status.EscrowA <> "" Then .Add("Escrow A", myMacchina.Status.EscrowA, myMacchina.Status.MsgEscrowA)
            If myMacchina.Status.DepositA <> "" Then .Add("Deposit A", myMacchina.Status.DepositA, myMacchina.Status.MsgDepositA)
            If myMacchina.Status.EscrowB <> "" Then .Add("Escrow B", myMacchina.Status.EscrowB, myMacchina.Status.MsgEscrowB)
            If myMacchina.Status.DepositB <> "" Then .Add("Deposit B", myMacchina.Status.DepositB, myMacchina.Status.MsgDepositB)
            .Add("Prot Cass Num", myMacchina.Status.ProtocolCasseteNum)
            .Add("Prot Dep Num", myMacchina.Status.ProtocoloDepositNum)
        End With
        dgExtendedStatus.Width = dgExtendedStatus.Columns(0).Width + dgExtendedStatus.Columns(1).Width + dgExtendedStatus.Columns(2).Width + 3
        Me.Width = dgExtendedStatus.Width + 40
        btnClose.Width = dgExtendedStatus.Width
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class