﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class NotesClassification
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.lblNoteId = New System.Windows.Forms.Label()
        Me.txtNoteId = New System.Windows.Forms.NumericUpDown()
        Me.dgvDetail = New System.Windows.Forms.DataGridView()
        Me.txtAnswer = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.colReason = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colValue = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.txtNoteId, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblNoteId
        '
        Me.lblNoteId.AutoSize = True
        Me.lblNoteId.Location = New System.Drawing.Point(42, 9)
        Me.lblNoteId.Name = "lblNoteId"
        Me.lblNoteId.Size = New System.Drawing.Size(76, 13)
        Me.lblNoteId.TabIndex = 0
        Me.lblNoteId.Text = "Detail for note:"
        '
        'txtNoteId
        '
        Me.txtNoteId.Location = New System.Drawing.Point(124, 7)
        Me.txtNoteId.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtNoteId.Name = "txtNoteId"
        Me.txtNoteId.Size = New System.Drawing.Size(59, 20)
        Me.txtNoteId.TabIndex = 4
        Me.txtNoteId.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'dgvDetail
        '
        Me.dgvDetail.AllowUserToAddRows = False
        Me.dgvDetail.AllowUserToDeleteRows = False
        Me.dgvDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetail.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colReason, Me.colValue})
        Me.dgvDetail.Location = New System.Drawing.Point(12, 33)
        Me.dgvDetail.Name = "dgvDetail"
        Me.dgvDetail.ReadOnly = True
        Me.dgvDetail.RowHeadersVisible = False
        Me.dgvDetail.Size = New System.Drawing.Size(300, 318)
        Me.dgvDetail.TabIndex = 30
        '
        'txtAnswer
        '
        Me.txtAnswer.Location = New System.Drawing.Point(63, 357)
        Me.txtAnswer.Name = "txtAnswer"
        Me.txtAnswer.Size = New System.Drawing.Size(249, 20)
        Me.txtAnswer.TabIndex = 31
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 360)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 13)
        Me.Label1.TabIndex = 32
        Me.Label1.Text = "Answer:"
        '
        'colReason
        '
        Me.colReason.HeaderText = "REASON"
        Me.colReason.Name = "colReason"
        Me.colReason.ReadOnly = True
        Me.colReason.Width = 195
        '
        'colValue
        '
        Me.colValue.HeaderText = "VALUE"
        Me.colValue.Name = "colValue"
        Me.colValue.ReadOnly = True
        '
        'NotesClassification
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(324, 396)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtAnswer)
        Me.Controls.Add(Me.dgvDetail)
        Me.Controls.Add(Me.txtNoteId)
        Me.Controls.Add(Me.lblNoteId)
        Me.Name = "NotesClassification"
        Me.Text = "Get detail of unit notes classification "
        CType(Me.txtNoteId, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblNoteId As Label
    Friend WithEvents txtNoteId As NumericUpDown
    Friend WithEvents dgvDetail As DataGridView
    Friend WithEvents txtAnswer As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents colReason As DataGridViewTextBoxColumn
    Friend WithEvents colValue As DataGridViewTextBoxColumn
End Class
