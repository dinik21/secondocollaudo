﻿Imports System.Data.OleDb
Imports System
Imports System.IO
Imports System.IO.Ports
Imports System.Collections
Imports Microsoft.VisualBasic.FileIO
Imports System.Text
Imports System.Net.Sockets
Imports System.Text.UTF8Encoding
Imports System.Text.StringBuilder
Imports SecondoCollaudo.CMClass
Imports SecondoCollaudo.INIFile
Imports System.Threading
Imports XmlDll

Public Class frmPrincipale
    Public myLog As XMLFile
    Public comScopeThread As Thread
    Public ultimoReplyCode As String
    Public statusCassette(12) As String
    Public statusFeeder, statusController, statusReader, statusSafe, statusEscrowA, statusDepositA, statusEscrowB, statusDepositB As String
    Public serialiVerificati As Integer
    Public serialCassetto(12) As String
    Public comunicationError As Boolean = False
    Public side As String = "L"
    Public formatoSeriale As Boolean = False
    Public stato As String = ""
    Public configIni As New INIFile(localFolder & "\setup.ini")
    Public lingua As String = configIni.ReadValue("option", "language")
    Public tipoConnessione As String = configIni.ReadValue("Connection", "Connection")
    Public comPort As String = configIni.ReadValue("RS232", "Port")
    Public comBaudRate As Integer = configIni.ReadValue("RS232", "Baudrate")
    Public comStopbits As Integer = configIni.ReadValue("RS232", "Stopbit")
    Public comParity As String = configIni.ReadValue("RS232", "Parity")
    Public comDataBits As Integer = configIni.ReadValue("RS232", "Databits")
    Public comProtocol As String = configIni.ReadValue("RS232", "Protocol")
    Public pswUserL As String = configIni.ReadValue("Pwd", "UserL")
    Public pswUserR As String = configIni.ReadValue("Pwd", "UserR")
    Public lanSSL As String = configIni.ReadValue("TCP-IP", "SSL")
    Public lanIPAddress As String = configIni.ReadValue("TCP-IP", "IPAddress")
    Public lanPort As String = configIni.ReadValue("TCP-IP", "Port")
    Public lanProtocol As String = configIni.ReadValue("TCP-IP", "Protocol")
    Public lanDevice As String = configIni.ReadValue("TCP-IP", "Device")
    Public usbSimplified As String = configIni.ReadValue("USB", "Simplified")
    Public usbProtocol As String = configIni.ReadValue("USB", "Protocol")
    Public usbDevice As String = configIni.ReadValue("USB", "Device")
    Public nPrelievoDistribuito As Integer = CInt(configIni.ReadValue("Test", "NumCicliPrelievoDistribuito"))
    Public nCicliPrelievoSingolaBn As Integer = CInt(configIni.ReadValue("Test", "NumCicliPrelievoSingolaBn"))
    Public logFolder As String = configIni.ReadValue("option", "LogFolder")
    Public inProgess As New INIFile(localFolder & "\inprogress.ini")
    Public progressSN, progressCode, progressTest As String
    Public myToolTip As New ToolTip
    Public cashData(12) As Integer
    Public totBnDep, totBnRej As Integer
    Public numeroCassetti As String = ""
    Public pulsantePremuto As Boolean = False

    Public Enum EXECUTION_STATE As Integer
        ES_CONTINUOUS = &H80000000
        ES_SYSTEM_REQUIRED = &H1
        ES_DISPLAY_REQUIRED = &H2
        ES_AWAYMODE_REQUIRED = &H40
    End Enum

    Public Declare Function SetThreadExecutionState Lib "kernel32" (ByVal esFlags As Long) As Long

    'Private Sub AggiornaComScope()
    '    Do
    '        If comScopeSend <> comCmdSend Then
    '            comScopeSend = comCmdSend
    '            MsgBox(frmComScope.lstComScope.Items.Add("SND: " & comScopeSend))
    '        End If

    '        If comScopeRec <> comCmdRec Then
    '            comScopeRec = comCmdRec
    '            MsgBox(frmComScope.lstComScope.Items.Add("SND: " & comScopeRec))
    '        End If
    '        Aspetta(100)
    '    Loop
    'End Sub


    Function ConnectionSetup() As String
        ConnectionSetup = ""
impostaTipoConnessione:
        Select Case tipoConnessione
            Case "RS232"
                With ConnectionParam
                    .ConnectionMode = DLINKMODESERIAL
                    .RsConf.baudrate = comBaudRate
                    .RsConf.parity = IIf(comParity = "NONE", False, True)
                    .RsConf.car = comDataBits
                    .RsConf.stopbit = comStopbits
                    .RsConf.dtr = False
                    .RsConf.device = comPort
                End With
            Case "RS232S"
                With ConnectionParam
                    .ConnectionMode = DLINKMODESERIALEASY
                    .RsConf.baudrate = comBaudRate
                    .RsConf.parity = IIf(comParity = "NONE", False, True)
                    .RsConf.car = comDataBits
                    .RsConf.stopbit = comStopbits
                    .RsConf.dtr = False
                    .RsConf.device = comPort
                End With
            Case "USB"
                ConnectionParam.ConnectionMode = DLINKMODEUSB
            Case "USBS"
                ConnectionParam.ConnectionMode = DLINKMODEUSBEASY
            Case "LAN"
                ConnectionParam.ConnectionMode = DLINKMODETCPIP
                ConnectionParam.TcpIpPar.clientIpAddr = lanIPAddress
                ConnectionParam.TcpIpPar.portNumber = lanPort
            Case Else
                ScriviMessaggio(Messaggio(239), lblMessaggio, ArcaColor.errore) 'Connessione non configurata correttamente nel file ini. Premere un tasto per procedere con la RS232, ESC per uscire.
                Select Case AttendiTasto()
                    Case "ESC"
                        ConnectionSetup = "KO"
                        Exit Function
                    Case Else
                        tipoConnessione = "RS232"
                        GoTo impostaTipoConnessione
                End Select
                'errore
        End Select

    End Function

    Function TestSecondoCollaudo() As Boolean
        TestSecondoCollaudo = False
        If myMacchina.Info.Categoria = "CM18SOLO" Or myMacchina.Info.Categoria = "CM18SOLOT" Or myMacchina.Info.Categoria = "OM61" Then
            ScriviMessaggio(String.Format(Messaggio(51), myMacchina.Info.Categoria), lblMessaggio) 'La categoria della macchina e' {0}. Collegare il cavo USB e premere un tasto per continuare.
            AttendiTasto()
            tipoConnessione = "USB"
        End If

        If configIni.ReadValue("ForceConnectionType", myMacchina.Info.Codice) = "USB" Then
            tipoConnessione = "USB"
        End If

Serial:
        If InserisciSeriale() = False Then
            TestSecondoCollaudo = False
            ScriviMessaggio(Messaggio(209), lblMessaggio) 'Inserimento Serial number non riuscito correttamente, premere un tasto per continuare, ESC per uscire
            Select Case AttendiTasto(, Me)
                Case "ESC"
                    TestSecondoCollaudo = False
                    Exit Function
                Case Else
                    txtMatricola.Text = ""
                    txtMatricola.Focus()
                    GoTo Serial
            End Select
        End If


        myLog.AddProduct(myMacchina.Serials.System, "dateStart", Now.ToString("d"), "timeStart", Now.ToString("T"), "family", myMacchina.Info.Categoria, "code", myMacchina.Info.Codice,
                        "cliente", myMacchina.Info.Cliente.Nome, "result", "false")
        myMacchina.Test.DataInizio = Now.ToString("d")
        myMacchina.Test.OraInizio = Now.ToString("T")


connessione:
        ConnectionSetup()

        'UnitConfiguration()
        'ImpostaConnessione()
        Dim iConnessione As Integer = 1
aperturaConnessione:
        If ApriConnessione() = False Then
            ScriviMessaggio(Messaggio(240), lblMessaggio, ArcaColor.errore) 'Connessione tramite {0} non riuscita. Premere un tasto per ripetere, 'A' per tentare l'autoconnessione, ESC per uscire
            Select Case AttendiTasto()
                Case "ESC"
                    TestSecondoCollaudo = False
                    Exit Function
                Case Else
                    GoTo aperturaConnessione
            End Select
        End If
        grpExtendedStatus.Visible = True

Open:
        'OPEN L
        side = Chr(lpLEFTSIDE)
        ScriviMessaggio(String.Format(Messaggio(10), side), lblMessaggio)
        'ultimoReplyCode = cmdOpen(side)
        ultimoReplyCode = CMOpen(side)
        myLog.AddTest("open", "result", ultimoReplyCode, "side", side)
        Select Case ultimoReplyCode
            Case "OK", "101", "1"
                'segnalare l'avvenuta operazione
            Case "BUSY", "102", "4"
                Aspetta(1000)
                GoTo Open
            Case "WRONG SIDE", "67", "213"
                ScriviMessaggio(String.Format(Messaggio(220), side, ultimoReplyCode), lblMessaggio, ArcaColor.errore) '=Comando OPEN {0} non riuscito, {1}. Premere un tasto per provare a chiudere l'altro lato e riprovare {0}, ESC per uscire.
                Select Case AttendiTasto(, Me)
                    Case "ESC"
                        TestSecondoCollaudo = False
                    Case Else
                        If side = Chr(lpLEFTSIDE) Then
                            'cmdClose(Chr(lpRIGHTSIDE))
                            CMClose(Chr(lpRIGHTSIDE))
                        Else
                            'cmdClose(Chr(lpLEFTSIDE))
                            CMClose(Chr(lpLEFTSIDE))
                        End If
                        GoTo Open
                End Select
            Case Else
                ScriviMessaggio(String.Format(Messaggio(202), myMacchina.Open.rc), lblMessaggio) 'Test Secondo Collaudo FALLITO, premere un tasto per continuare
                Select Case AttendiTasto(, Me)
                    Case "ESC"
                        TestSecondoCollaudo = False
                        Exit Function
                    Case Else
                        GoTo Open
                End Select
                Exit Function
        End Select
richiestaStato1:
        'ultimoReplyCode = CmdExtendedStatus()
        ultimoReplyCode = CMExtendedStatus()
        AggiornaStati()
        myLog.AddTest("state", "result", ultimoReplyCode, "feeder", myMacchina.Status.Feeder, "controller", myMacchina.Status.Controller, "reader", myMacchina.Status.Reader,
                       "cassetteA", myMacchina.Status.CassetteA, "cassetteB", myMacchina.Status.CassetteB, "cassetteC", myMacchina.Status.CassetteC,
                      "cassetteD", myMacchina.Status.CassetteD, "cassetteE", myMacchina.Status.CassetteE, "cassetteF", myMacchina.Status.CassetteF)
        If (myMacchina.Status.Safe <> "") Then
            myLog.AddTestAttribute("state", "safe", myMacchina.Status.Safe)
        End If
        If (myMacchina.Status.CassetteG <> "") Then
            myLog.AddTestAttribute("state", "cassetteG", myMacchina.Status.CassetteG)
            myLog.AddTestAttribute("state", "cassetteH", myMacchina.Status.CassetteH)
        End If
        If (myMacchina.Status.CassetteI <> "") Then
            myLog.AddTestAttribute("state", "cassetteI", myMacchina.Status.CassetteI)
            myLog.AddTestAttribute("state", "cassetteJ", myMacchina.Status.CassetteJ)
        End If
        If (myMacchina.Status.CassetteK <> "") Then
            myLog.AddTestAttribute("state", "cassetteK", myMacchina.Status.CassetteK)
            myLog.AddTestAttribute("state", "cassetteL", myMacchina.Status.CassetteL)
        End If
        If ultimoReplyCode <> "OK" Then
            TestSecondoCollaudo = False
            ScriviMessaggio(String.Format(Messaggio(203), myMacchina.Status.MsgController), lblMessaggio) 'Comando EXTENDED STATUS non riuscito, risposta: {0}. Premere un tasto per ripetere, ESC per uscire.
            Select Case AttendiTasto(, Me)
                Case "ESC"
                    Exit Function
                Case Else
                    GoTo richiestaStato1
            End Select
        End If
        If myMacchina.Status.MsgController <> "OK" Then
            ScriviMessaggio(String.Format(Messaggio(204), myMacchina.Status.MsgController), lblMessaggio) 'Non e' possibile proseguire, macchina in stato: {0}. Premere un tasto per ripetere, ESC per uscire.
            Select Case AttendiTasto(, Me)
                Case "ESC"
                    TestSecondoCollaudo = False
                    Exit Function
                Case Else
                    GoTo richiestaStato1
            End Select
        End If

serialelettore:
        'SERIALE LETTORE
        ultimoReplyCode = LeggiSerialeLettore()
        myLog.AddModule("Reader", "serial", myMacchina.Serials.Reader)
        If ultimoReplyCode <> "OK" Then
            TestSecondoCollaudo = False
            ScriviMessaggio(Messaggio(205), lblMessaggio) 'Non e' stato possibile leggere il seral number del lettore. Premere un tasto per ripetere, ESC per uscire.
            Select Case AttendiTasto(, Me)
                Case "ESC"
                    TestSecondoCollaudo = False
                    Exit Function
                Case Else
                    GoTo serialelettore
            End Select
        End If


macAddress:
        'MAC ADDRESS
        ultimoReplyCode = LeggiMACAddress()
        myLog.AddTest("macAddress","result",ultimoReplyCode,"value",myMacchina.Info.MACAddress)
        If ultimoReplyCode <> "OK" Then
            TestSecondoCollaudo = False
            ScriviMessaggio(Messaggio(206), lblMessaggio) 'Non e' stato possibile leggere il MAC address. Premere un tasto per ripetere, ESC per uscire
            Select Case AttendiTasto(, Me)
                Case "ESC"
                    TestSecondoCollaudo = False
                    Exit Function
                Case Else
                    GoTo macAddress
            End Select
        End If

        'CASSETTE NUMBER
leggiNumeroCassetti:
        'TEST
        ultimoReplyCode = LeggiCassetteNumber()
        myLog.AddTest("getCassetteNumber", "result", ultimoReplyCode, "cassette", myMacchina.Info.MachineCassetteNumber, "cd80", myMacchina.Info.CD80, "bag", myMacchina.Info.Bag)
        If ultimoReplyCode <> "OK" Then
            TestSecondoCollaudo = False
            ScriviMessaggio(String.Format(Messaggio(207), ultimoReplyCode), lblMessaggio) 'Non e' stato possibile leggere il numero di cassetti impostato, macchina in stato {0}. Premere un tasto per ripetere, ESC per uscire
            Select Case AttendiTasto(, Me)
                Case "ESC"
                    TestSecondoCollaudo = False
                    Exit Function
                Case Else
                    GoTo leggiNumeroCassetti
            End Select

        End If

impostaNumeroCassetti:
        ScriviMessaggio(Messaggio(52), lblMessaggio) 'Selezione il numero di cassetti installati in macchina o premere INVIO.
        SelezionaNumeroCassetti()

        'ScriviMessaggio(Messaggio(53), lblMessaggio)
        '        ultimoReplyCode = SetCassetteNumber(numeroCassetti)
        '        If ultimoReplyCode <> "OK" Then
        '            TestSecondoCollaudo = False
        '            ScriviMessaggio(String.Format(Messaggio(257), ultimoReplyCode), lblMessaggio) 'Non e' stato possibile impostare il numero di cassetti, macchina in stato {0}. Premere un tasto per ripetere, ESC per uscire
        '            Select Case AttendiTasto(, Me)
        '                Case "ESC"
        '                    TestSecondoCollaudo = False
        '                    Exit Function
        '                Case Else
        '                    GoTo impostaNumeroCassetti
        '            End Select

        '        End If

suite:
        'SUITE
        ultimoReplyCode = LeggiSuiteFileName()
        myLog.AddTest("getSuiteFileName", "result", ultimoReplyCode, "name", myMacchina.Firmware.Suite.Code)
        If ultimoReplyCode <> "OK" And ultimoReplyCode <> "SW ERROR" Then
            TestSecondoCollaudo = False
            ScriviMessaggio(String.Format(Messaggio(208), ultimoReplyCode), lblMessaggio) 'Non e' stato possibile leggere il nome della SUITE. Premere un tasto per riprovare, ESC per uscire
            Select Case AttendiTasto(, Me)
                Case "ESC"
                    TestSecondoCollaudo = False
                    Exit Function
                Case Else
                    GoTo suite
            End Select
        End If



        progressSN = inProgess.ReadValue("TestInProgress", "SerialNumber")
        If progressSN = myMacchina.Serials.System Then
            progressTest = inProgess.ReadValue("testinprogress", "testname")
            ScriviMessaggio("Il serial number " & progressSN & " non ha portato a termine il collaudo. Il test " & progressTest & " è rimasto in sospeso. Premere R per riprendere il test o qualsiasi tasto per continuare", lblMessaggio)
            If AttendiTasto(, Me) = "R" Then

                For i = 1 To myMacchina.Info.MachineCassetteNumber
                    ScriviMessaggio("Lettura del serial number del cassetto " & Chr(i + 64), lblMessaggio)
                    Try
                        cmdComandoSingolo("T,1," & Chr(i + 64) & ",13", 4, 5000)
                        If comandoSingoloRisposta.Length > 8 Then
                            Select Case i
                                Case 1
                                    myMacchina.Serials.CassettoA = comandoSingoloRisposta(8)
                                Case 2
                                    myMacchina.Serials.CassettoB = comandoSingoloRisposta(8)
                                Case 3
                                    myMacchina.Serials.CassettoC = comandoSingoloRisposta(8)
                                Case 4
                                    myMacchina.Serials.CassettoD = comandoSingoloRisposta(8)
                                Case 5
                                    myMacchina.Serials.CassettoE = comandoSingoloRisposta(8)
                                Case 6
                                    myMacchina.Serials.CassettoF = comandoSingoloRisposta(8)
                                Case 7
                                    myMacchina.Serials.CassettoG = comandoSingoloRisposta(8)
                                Case 8
                                    myMacchina.Serials.CassettoH = comandoSingoloRisposta(8)
                                Case 9
                                    myMacchina.Serials.CassettoI = comandoSingoloRisposta(8)
                                Case 10
                                    myMacchina.Serials.CassettoJ = comandoSingoloRisposta(8)
                                Case 11
                                    myMacchina.Serials.CassettoK = comandoSingoloRisposta(8)
                                Case 12
                                    myMacchina.Serials.CassettoL = comandoSingoloRisposta(8)
                            End Select
                        End If
                        If myMacchina.Info.Categoria = "CM18B" Or myMacchina.Info.Categoria = "OM61" Then
                            cmdComandoSingolo("T,1,a,13", 4, 5000)
                            If comandoSingoloRisposta.Length > 8 Then myMacchina.Serials.Bag = comandoSingoloRisposta(8)
                        End If
                    Catch ex As Exception

                    End Try
                Next
                Select Case progressTest.ToUpper
                    Case "INSERIMENTOSERIALI"
                        GoTo inserimentoSeriali
                    Case "TRANSFER"
                        GoTo transfer
                    Case "SVUOTAULTIMICASSETTI"
                        GoTo SvuotaUltimiCassetti
                    Case "CICLOPRELIEVO"
                        GoTo cicloPrelievo
                    Case "PRELIEVODISTRIBUITOINIZIO"
                        GoTo prelievoDistribuitoInizio
                    Case "DEPOSITO4MAZZETTE"
                        GoTo deposito4mazzette
                    Case "BAGTRANSFER"
                        GoTo bagTransfer
                    Case "PRELIEVOFINALE"
                        GoTo prelievoFinale
                    Case "INITCASSETTE"
                        GoTo initCassette
                End Select
            End If
        End If
        inProgess.WriteValue("TestInProgress", "SerialNumber", myMacchina.Serials.System)

        'Deposito completo
deposito:
        ultimoReplyCode = DepositoCompleto()
        myLog.AddTest("depositoCompleto", "result", ultimoReplyCode, "totDep", myMacchina.CashData.totCassetti)
        If ultimoReplyCode <> "OK" Then
            TestSecondoCollaudo = False
            ScriviMessaggio(String.Format(Messaggio(200), ultimoReplyCode), lblMessaggio) 'Deposito completo non riuscito. Macchina in stato {0}, premere un tasto per ripetere, ESC per uscire
            Select Case AttendiTasto(, Me)
                Case "ESC"
                    TestSecondoCollaudo = False
                    Exit Function
                Case Else
                    GoTo deposito
            End Select
        End If
        Call GetCashData()
        ' Log.primoDeposito = True

inserimentoSeriali:
        inProgess.WriteValue("testinprogress", "testname", "inserimentoseriali")
        'If myMacchina.Info.Categoria <> "CM18HC" Then
        serialiVerificati = 0
        If InsertSerialNumber() = False Then
            ScriviMessaggio(Messaggio(231), lblMessaggio, ArcaColor.errore) 'Inserimento Seriali non riuscito. Premere un tasto per continuare, ESC per uscire.
            Select Case AttendiTasto(, Me)
                Case "ESC"
                    TestSecondoCollaudo = False
                    Exit Function
                Case Else
                    GoTo inserimentoSeriali
            End Select
        End If
        'For i = 1 To 12
        'Log.serialNumberCassetto(i) = serialCassetto(i)
        'Next
        'End If

serialecassetti:
        ultimoReplyCode = LeggiSerialeCassetti()
        If ultimoReplyCode <> "OK" Then
            TestSecondoCollaudo = False
            ScriviMessaggio(Messaggio(205), lblMessaggio) 'Non e' stato possibile leggere il seral number del lettore. Premere un tasto per ripetere, ESC per uscire.
            Select Case AttendiTasto(, Me)
                Case "ESC"
                    TestSecondoCollaudo = False
                    Exit Function
                Case Else
                    GoTo serialecassetti
            End Select
        End If
        myLog.AddModule("cassetteA", "sn", myMacchina.Serials.CassettoA)
        myLog.AddModule("cassetteB", "sn", myMacchina.Serials.CassettoB)
        myLog.AddModule("cassetteC", "sn", myMacchina.Serials.CassettoC)
        myLog.AddModule("cassetteD", "sn", myMacchina.Serials.CassettoD)
        myLog.AddModule("cassetteE", "sn", myMacchina.Serials.CassettoE)
        myLog.AddModule("cassetteF", "sn", myMacchina.Serials.CassettoF)
        If myMacchina.Serials.CassettoG <> "" Then
            myLog.AddModule("cassetteG", "sn", myMacchina.Serials.CassettoG)
            myLog.AddModule("cassetteH", "sn", myMacchina.Serials.CassettoH)
        End If
        If myMacchina.Serials.CassettoI <> "" Then
            myLog.AddModule("cassetteI", "sn", myMacchina.Serials.CassettoI)
            myLog.AddModule("cassetteJ", "sn", myMacchina.Serials.CassettoJ)
        End If
        If myMacchina.Serials.CassettoK <> "" Then
            myLog.AddModule("cassetteK", "sn", myMacchina.Serials.CassettoK)
            myLog.AddModule("cassetteL", "sn", myMacchina.Serials.CassettoL)
        End If
        If CInt(myMacchina.Info.CD80) > 0 Then
            'Transfer 750bn miste per ogni cassetto
            'handle barcode - 2043000108 per A
            'handle barcode - 2043000109 per B
            'Svuotare cartucce manualmente
            'Limitare a 700 la capacità di A con TraspSend 940300020B0C
            'Limitare a 700 la capacità di B con TraspSend 950300020B0C
            'Riavvio
transfer:
            inProgess.WriteValue("testinprogress", "testname", "transfer")
            ultimoReplyCode = TransferCD80()
            myLog.AddTest("transferCD80", "result", ultimoReplyCode)
            If ultimoReplyCode <> "OK" Then
                ScriviMessaggio(String.Format(Messaggio(222), ultimoReplyCode), lblMessaggio, ArcaColor.errore) 'Transfer verso il CD80 non riuscito, macchina in stato {0}, premere un tasto per ripetere, ESC per uscire.
                Select Case AttendiTasto(, Me)
                    Case "ESC"
                        TestSecondoCollaudo = False
                        Exit Function
                    Case Else
                        GoTo transfer
                End Select
            End If
            'Log.transferCD80 = True

            ScriviMessaggio(Messaggio(41), lblMessaggio) 'Svuotare le cartucce e premere un tasto per continuare
            AttendiTasto(, Me)
            AttendiReset()

handlingA:
            ultimoReplyCode = HandleBarcodeCD80("A", "204300000108")
            myLog.AddTest("handleBarcodeCD80A", "result", ultimoReplyCode)
            If ultimoReplyCode <> "OK" Then
                ScriviMessaggio(String.Format(Messaggio(223), ultimoReplyCode), lblMessaggio, ArcaColor.errore) 'Handle Barcode CD80 non riuscito, macchina in stato {0}, premere un tasto per ripetere, ESC per uscire.
                Select Case AttendiTasto(, Me)
                    Case "ESC"
                        TestSecondoCollaudo = False
                        Exit Function
                    Case Else
                        GoTo handlingA
                End Select
            End If

handlingB:
            ultimoReplyCode = HandleBarcodeCD80("B", "204300000109")
            myLog.AddTest("handleBarcodeCD80B", "result", ultimoReplyCode)
            If ultimoReplyCode <> "OK" Then
                ScriviMessaggio(String.Format(Messaggio(223), ultimoReplyCode), lblMessaggio, ArcaColor.errore) 'Handle Barcode CD80 non riuscito, macchina in stato {0}, premere un tasto per ripetere, ESC per uscire.
                Select Case AttendiTasto(, Me)
                    Case "ESC"
                        TestSecondoCollaudo = False
                        Exit Function
                    Case Else
                        GoTo handlingB
                End Select
            End If



limitaA:
            ScriviMessaggio(Messaggio(50), lblMessaggio) 'Impostazione limite di capacità in corso.
            GetCashData()
            If myMacchina.CashData.A.freeCap + myMacchina.CashData.A.bnNumber <> 700 Or myMacchina.CashData.B.freeCap + myMacchina.CashData.B.bnNumber <> 700 Then
transp:
                ultimoReplyCode = cmdComandoSingolo("X,1", 2, 5000)
                If ultimoReplyCode <> "OK" Then
                    ScriviMessaggio(Messaggio(262), lblMessaggio, ArcaColor.errore) 'Transparent in non riuscito, premi un tasto per ripetere, ESC per uscire
                    If AttendiTasto(, Me) = "ESC" Then
                        TestSecondoCollaudo = False
                        Exit Function
                    End If
                    GoTo transp
                End If
                ultimoReplyCode = LimitaCD80("A", 700)
                If ultimoReplyCode <> "OK" Then
                    myLog.AddTest("limitCD80A", "result", "KO", "state", ultimoReplyCode)
                    ScriviMessaggio(String.Format(Messaggio(224), ultimoReplyCode), lblMessaggio, ArcaColor.errore) 'Limitazione capacità CD80 non riuscito, macchina in stato {0}, premere un tasto per ripetere, ESC per uscire.
                    Select Case AttendiTasto(, Me)
                        Case "ESC"
                            TestSecondoCollaudo = False
                            Exit Function
                        Case Else
                            GoTo limitaA
                    End Select
                End If
                myLog.AddTest("limitCD80A", "result", "OK")
limitaB:
                'GetCashData()
                ScriviMessaggio(Messaggio(50), lblMessaggio) 'Impostazione limite di capacità in corso.
                ultimoReplyCode = LimitaCD80("B", 700)
                If ultimoReplyCode <> "OK" Then
                    myLog.AddTest("limitCD80B", "result", "KO", "state", ultimoReplyCode)
                    ScriviMessaggio(String.Format(Messaggio(224), ultimoReplyCode), lblMessaggio, ArcaColor.errore) 'Limitazione capacità CD80 non riuscito, macchina in stato {0}, premere un tasto per ripetere, ESC per uscire.
                    Select Case AttendiTasto(, Me)
                        Case "ESC"
                            TestSecondoCollaudo = False
                            Exit Function
                        Case Else
                            GoTo limitaB
                    End Select
                End If
                myLog.AddTest("limitCD80B", "result", "OK")
transpOut:
                ultimoReplyCode = cmdComandoSingolo("Y,1", 2)
                If ultimoReplyCode <> "OK" Then
                    ScriviMessaggio(Messaggio(263), lblMessaggio, ArcaColor.errore) 'Transparent out non riuscito, premi un tasto per ripetere, ESC per uscire
                    If AttendiTasto(, Me) = "ESC" Then
                        TestSecondoCollaudo = False
                        Exit Function
                    End If
                    GoTo transpOut
                End If
                ultimoReplyCode = cmdComandoSingolo("T,1,0,7", 4)
                If ultimoReplyCode <> "OK" Then
                    ScriviMessaggio(Messaggio(264), lblMessaggio, ArcaColor.errore) 'Comando di RESET non eseguito correttamente, premi un tasto per ripetere, ESC per uscire
                    If AttendiTasto(, Me) = "ESC" Then
                        TestSecondoCollaudo = False
                        Exit Function
                    End If
                    GoTo transpOut
                End If
                AttendiReset()
            End If
            'riavvio
            'GoTo limitaA
        End If

        'Svuotamento
        '<8 svuota gli ultimi 2, >8 gli ultimi 4
SvuotaUltimiCassetti:
        inProgess.WriteValue("testinprogress", "testname", "svuotaultimicassetti")
        If myMacchina.Info.Categoria <> "CM18B" And myMacchina.Info.Categoria <> "OM61" Then
            ultimoReplyCode = SvuotaUltimi()
            myLog.AddTest("withdrawalLast", "result", ultimoReplyCode)
            If ultimoReplyCode <> "OK" Then
                ScriviMessaggio(String.Format(Messaggio(227), ultimoReplyCode), lblMessaggio, ArcaColor.errore) 'Svuotamento ultimi cassetti non riuscito, {0}. Premere un tasto per continuare, ESC per uscire.
                Select Case AttendiTasto(, Me)
                    Case "ESC"
                        TestSecondoCollaudo = False
                        Exit Function
                    Case Else
                        GoTo SvuotaUltimiCassetti
                End Select
            End If
            'Log.svuotamentoUltimi = True
            GetCashData()
            ScriviMessaggio(Messaggio(19), lblMessaggio) 'Togliere le banconote dalla bocchetta di output.
        End If

cicloPrelievo:
        inProgess.WriteValue("testinprogress", "testname", "cicloprelievo")
        'Prelievo singolo 1 * cassetto per x cicli
        ScriviMessaggio(Messaggio(46), lblMessaggio) 'Posizionare i distanziatori nella bocchetta di Output. Premere un tasto per avviare i cicli di prelievo singole banconote.
        AttendiTasto(, Me)
        ultimoReplyCode = PrelievoSingolo(15)
        myLog.AddTest("WithdrawalCycle1x15", "result", ultimoReplyCode)
        If ultimoReplyCode <> "OK" Then
            ScriviMessaggio(String.Format(Messaggio(228), ultimoReplyCode), lblMessaggio, ArcaColor.errore) 'Ciclo di prelievo singola banconota per cassetto non riuscito, {0}. Premere un tasto per continuare, ESC per uscire.
            Select Case AttendiTasto(, Me)
                Case "ESC"
                    TestSecondoCollaudo = False
                    Exit Function
                Case Else
                    GoTo cicloPrelievo
            End Select
        End If

singoloDeposito:
        'Singolo deposito
        ultimoReplyCode = DepositoSingolo()
        myLog.AddTest("depositSingle", "result", ultimoReplyCode)
        If ultimoReplyCode <> "OK" Then
            ScriviMessaggio(String.Format(Messaggio(229), ultimoReplyCode), lblMessaggio, ArcaColor.errore) 'Singolo deposito non riuscito, {0}. Premere un tasto per continuare, ESC per uscire.
            Select Case AttendiTasto(, Me)
                Case "ESC"
                    TestSecondoCollaudo = False
                    Exit Function
                Case Else
                    GoTo cicloPrelievo
            End Select
        End If
        'Log.depositoSingolo = True
        ScriviMessaggio(Messaggio(19), lblMessaggio) 'Togliere le banconote dalla bocchetta di output.

prelievoDistribuitoInizio:
        inProgess.WriteValue("testinprogress", "testname", "prelievodistribuitoinizio")
        'Prelievo distribuito
        ultimoReplyCode = PrelievoDistribuito(nPrelievoDistribuito)
        myLog.AddTest("distribuiteWithdrawal", "result", ultimoReplyCode)
        If ultimoReplyCode <> "OK" Then
            ScriviMessaggio(String.Format(Messaggio(230), ultimoReplyCode), lblMessaggio, ArcaColor.errore) 'Prelievo non portato a termine correttamente, {0}. Premere un tasto per continuare, ESC per uscire.
            Select Case AttendiTasto(, Me)
                Case "ESC"
                    TestSecondoCollaudo = False
                    Exit Function
                Case Else
                    GoTo prelievoDistribuitoInizio
            End Select
        End If



closeSide:
        'close
        ultimoReplyCode = CMClose(side)
        myLog.AddTest("close" + side, "result", ultimoReplyCode)
        Select Case ultimoReplyCode
            Case "OK"
            Case "WRONG SIDE"
                ScriviMessaggio(String.Format(Messaggio(237), side, ultimoReplyCode), lblMessaggio, ArcaColor.errore) 'Comando CLOSE {0} non riuscito, {1}. Premere un tasto per provare a chiudere l'altro lato, ESC per uscire.
                Select Case AttendiTasto(, Me)
                    Case "ESC"
                        TestSecondoCollaudo = False
                        Exit Function
                    Case Else
                        If side = Chr(lpLEFTSIDE) Then
                            side = Chr(lpRIGHTSIDE)
                        Else
                            side = Chr(lpLEFTSIDE)
                        End If
                        GoTo closeSide
                End Select
            Case Else
                ScriviMessaggio(String.Format(Messaggio(236), side, ultimoReplyCode), lblMessaggio, ArcaColor.errore) 'comando CLOSE {0} non riuscito, {1}. Premere un tasto per riprovare, ESC per uscire
                Select Case AttendiTasto(, Me)
                    Case "ESC"
                        TestSecondoCollaudo = False
                        Exit Function
                    Case Else
                        GoTo closeSide
                End Select
        End Select

        'cambio lato
        If myMacchina.Info.Categoria <> "CM18SOLO" And myMacchina.Info.Categoria <> "CM18SOLOT" And myMacchina.Info.Categoria <> "OM61" Then
            side = Chr(lpRIGHTSIDE)
        Else
            side = Chr(lpLEFTSIDE)
        End If

Open2:
        'OPEN R (o nuovamente L in caso di CM18SOLO o OM61)
        ScriviMessaggio(String.Format(Messaggio(10), side), lblMessaggio) 'OPEN {0} in corso...
        ultimoReplyCode = CMOpen(side)
        myLog.AddTest("open" + side, "result", ultimoReplyCode)
        Select Case ultimoReplyCode
            Case "OK"
                'segnalare l'avvenuta operazione
            Case "WRONG SIDE"
                ScriviMessaggio(String.Format(Messaggio(220), side, ultimoReplyCode), lblMessaggio, ArcaColor.errore) 'Comando OPEN {0} non riuscito, {1}. Premere un tasto per provare a chiudere l'altro lato e riprovare {0}, ESC per uscire.
                Select Case AttendiTasto(, Me)
                    Case "ESC"
                        TestSecondoCollaudo = False
                    Case Else
                        If side = Chr(lpLEFTSIDE) Then
                            CMClose(Chr(lpRIGHTSIDE))
                        Else
                            CMClose(Chr(lpLEFTSIDE))
                        End If
                        GoTo Open2
                End Select
            Case Else
                ScriviMessaggio(String.Format(Messaggio(202), myMacchina.Open.rc), lblMessaggio) 'Test Secondo Collaudo FALLITO, premere un tasto per continuare
                Select Case AttendiTasto(, Me)
                    Case "ESC"
                        TestSecondoCollaudo = False
                        Exit Function
                    Case Else
                        GoTo Open2
                End Select
                Exit Function
        End Select

deposito4mazzette:
        inProgess.WriteValue("testinprogress", "testname", "deposito4mazzette")
        'Deposito delle 4 mazzette prelevate prima
        ultimoReplyCode = DepositoSingolo(nPrelievoDistribuito)
        myLog.AddTest("depositLastWithdrawal", "result", ultimoReplyCode)
        If ultimoReplyCode <> "OK" Then
            ScriviMessaggio(String.Format(Messaggio(232), ultimoReplyCode), lblMessaggio, ArcaColor.errore) 'Deposito completo non riuscito, {0}. Premere un tasto per continuare, ESC per uscire.
            Select Case AttendiTasto(, Me)
                Case "ESC"
                    TestSecondoCollaudo = False
                    Exit Function
                Case Else
                    GoTo deposito4mazzette
            End Select
        End If
        'Log.depositoCompleto = True
        GetCashData()

        'Collaudo parte BUSTA
bagBarcode:
        If myMacchina.Info.Categoria = "CM18B" Or myMacchina.Info.Categoria = "OM61" Then
            ultimoReplyCode = SetBagBarcode()
            myLog.AddTest("setBagBarcode", "result", ultimoReplyCode)
            If ultimoReplyCode <> "OK" Then
                ScriviMessaggio(String.Format(Messaggio(225), ultimoReplyCode), lblMessaggio, ArcaColor.errore) 'Registrazione barcode delle buste non riuscita, {0}. Premere un tasto per riprovare, ESC per uscire.
                Select Case AttendiTasto(, Me)
                    Case "ESC"
                        TestSecondoCollaudo = False
                        Exit Function
                    Case Else
                        GoTo bagBarcode
                End Select
            End If

bagTransfer:
            inProgess.WriteValue("testinprogress", "testname", "bagtransfer")
            ultimoReplyCode = TransferBag()
            myLog.AddTest("bagTransfer", "result", ultimoReplyCode)
            If ultimoReplyCode <> "OK" Then
                ScriviMessaggio(String.Format(Messaggio(226), ultimoReplyCode), lblMessaggio, ArcaColor.errore) 'Trasferimento verso la busta non riuscito, {0}. Premere un tasto per ripetere, ESC per uscire.
                Select Case AttendiTasto(, Me)
                    Case "ESC"
                        TestSecondoCollaudo = False
                        Exit Function
                    Case Else
                        GoTo bagTransfer
                End Select
            End If
        End If

prelievoFinale:
        inProgess.WriteValue("testinprogress", "testname", "prelievofinale")
        'Prelievo totale
        ultimoReplyCode = PrelievoTotale()
        myLog.AddTest("finalWithdrawal", "result", ultimoReplyCode)
        If ultimoReplyCode <> "OK" Then
            ScriviMessaggio(String.Format(Messaggio(230), ultimoReplyCode), lblMessaggio, ArcaColor.errore) 'Prelievo non portato a termine correttamente, {0}. Premere un tasto per continuare, ESC per uscire.
            Select Case AttendiTasto(, Me)
                Case "ESC"
                    TestSecondoCollaudo = False
                    Exit Function
                Case Else
                    GoTo prelievoFinale
            End Select
        End If

initCassette:
        inProgess.WriteValue("testinprogress", "testname", "initcassette")
        ultimoReplyCode = InizializzazioneCassetti()
        myLog.AddTest("initCassette", "result", ultimoReplyCode)
        If ultimoReplyCode <> "OK" Then
            ScriviMessaggio(String.Format(Messaggio(261), ultimoReplyCode), lblMessaggio, ArcaColor.errore) 'Inizializzazione cassetti non andata a buon fine; risposta {0}, premere un tasto per ripetere, ESC per uscire
            Select Case AttendiTasto(, Me)
                Case "ESC"
                    TestSecondoCollaudo = False
                    Exit Function
                Case Else
                    GoTo initCassette
            End Select
        End If

unitConfig:
        ultimoReplyCode = UnitConfiguration()
        myLog.AddTest("setUnitConfig", "result", ultimoReplyCode)
        If ultimoReplyCode <> "OK" Then
            ScriviMessaggio(String.Format(Messaggio(266), ultimoReplyCode), lblMessaggio, ArcaColor.errore) 'Unit configuration non andata a buon fine; risposta {0}, premere un tasto per ripetere, ESC per uscire
            Select Case AttendiTasto(, Me)
                Case "ESC"
                    TestSecondoCollaudo = False
                    Exit Function
                Case Else
                    GoTo unitConfig
            End Select
        End If

rimuoviBusta:
        If myMacchina.Info.Categoria = "CM18B" Or myMacchina.Info.Categoria = "OM61" Then
            ultimoReplyCode = CambiaBusta()
            myLog.AddTest("bagChange", "result", ultimoReplyCode)
            If ultimoReplyCode <> "OK" Then
                ScriviMessaggio(String.Format(Messaggio(267), ultimoReplyCode), lblMessaggio, ArcaColor.errore) 'Rimozione busta non andata a buon fine; risposta {0}, premere un tasto per ripetere, ESC per uscire
                Select Case AttendiTasto(, Me)
                    Case "ESC"
                        TestSecondoCollaudo = False
                        Exit Function
                    Case Else
                        GoTo rimuoviBusta
                End Select
            End If
        End If

        GetCashData()
        'Analisi error log
errorlog:
        ultimoReplyCode = AnalisiErrorLog()
        myLog.AddTest("errorLogAnalisys", "result", ultimoReplyCode)
        If ultimoReplyCode <> "OK" Then
            ScriviMessaggio(Messaggio(259), lblMessaggio) 'Problemi durante l'analisi dell'error log, premere un tasto per ripetere, ESC per continuare.
            If AttendiTasto(, Me) <> "ESC" Then
                GoTo errorlog
            End If
        End If
        'close
        ultimoReplyCode = CMClose(side)
        Select Case ultimoReplyCode
            Case "OK"
            Case "WRONG SIDE"
                ScriviMessaggio(String.Format(Messaggio(237), side, ultimoReplyCode), lblMessaggio, ArcaColor.errore) 'Comando CLOSE {0} non riuscito, {1}. Premere un tasto per provare a chiudere l'altro lato, ESC per uscire.
                Select Case AttendiTasto(, Me)
                    Case "ESC"
                        TestSecondoCollaudo = False
                        Exit Function
                    Case Else
                        If side = Chr(lpLEFTSIDE) Then
                            side = Chr(lpRIGHTSIDE)
                        Else
                            side = Chr(lpLEFTSIDE)
                        End If
                        GoTo closeSide
                End Select
            Case Else
                ScriviMessaggio(String.Format(Messaggio(236), side, ultimoReplyCode), lblMessaggio, ArcaColor.errore) 'comando CLOSE {0} non riuscito, {1}. Premere un tasto per riprovare, ESC per uscire
                Select Case AttendiTasto(, Me)
                    Case "ESC"
                        TestSecondoCollaudo = False
                        Exit Function
                    Case Else
                        GoTo closeSide
                End Select
        End Select
        TestSecondoCollaudo = True
        myMacchina.Test.EsitoFinale = True
        myLog.AddProductAttribute("result", "true")
        inProgess.WriteValue("TestInProgress", "TestName", "0")
    End Function

    Function CambiaBusta()
inizio:
        ScriviMessaggio(Messaggio(70), lblMessaggio) 'Aprire la cassaforte e premere un tasto per sbloccare la maniglia
        AttendiTasto()
        CambiaBusta = cmdComandoSingolo("B,1,7,0,1", 2)
        If CambiaBusta <> "OK" And comandoSingoloRisposta(2) <> "101" And comandoSingoloRisposta(2) <> "1" Then
            Exit Function
        End If
        CambiaBusta = "OK"
        ScriviMessaggio(Messaggio(71), lblMessaggio) 'Ruotare la maniglia, alzare il maniglione e spingerlo fino a battuta per poi estrarre il supporto busta. Rimuovere la busta, richiudere tutto e premere un tasto per continuare
        AttendiTasto()
        AttendiReset()
    End Function

    Function UnitConfiguration()
        'leggi dbfw
        Dim unitConfigNumber As String = ""
        Dim cmConfig(16), cmOptionConfig(16), cmOptionOneConfig(16), cmOptionTwoConfig(32) As Boolean
        Dim cmConf, cmOptionConf, cmOptionOneConf As Integer
        Dim cmOptionTwoConf As Double
        ScriviMessaggio(Messaggio(65), lblMessaggio) 'Lettura unit configuration da DBFW in corso
        Dim Conn_String As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source =" & localDBFolder & databaseName
        'Dichiaro la nuova connessione
        Dim cn As New OleDbConnection(Conn_String)
        cn.Open()

        Dim sql As String
        sql = "SELECT DISTINCT id_nome_unit_config FROM tblman WHERE id_prodotto = '" & myMacchina.Info.Codice & "' AND id_cliente = " & myMacchina.Info.Cliente.ID & " AND id_modulo = 'FW_PRODOTTO'"

        Dim cmd As New OleDbCommand(sql, cn)

        Dim dr As OleDbDataReader = cmd.ExecuteReader
        Do While Not dr.Read() = Nothing
            unitConfigNumber = (dr(0))
        Loop

        sql = "SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " & unitConfigNumber
        cmd = New OleDbCommand(sql, cn)
        dr = cmd.ExecuteReader
        Do While Not dr.Read() = Nothing
            unitConfigNumber = dr(0) 'id
            UnitConfiguration = dr(1) 'nome
            cmConfig(0) = dr(2) '7x0001
            cmConfig(6) = dr(3) '7x0040
            cmConfig(7) = dr(4) '7x0080
            cmConfig(8) = dr(5) '7x0100
            cmConfig(10) = dr(6) '7x0400
            cmConfig(11) = dr(7) '7x0800
            cmConfig(15) = dr(8) '7x8000
            cmOptionConfig(0) = dr(9) '9x0001
            cmOptionConfig(3) = dr(10) '9x0008
            cmOptionConfig(4) = dr(11) '8x0010 Identify STD/SIMPLIFIED protocol
            cmOptionConfig(5) = dr(12) '8x0020
            cmOptionOneConfig(3) = dr(13) '9x0008
            cmOptionOneConfig(7) = dr(14) '9x0080
            cmOptionOneConfig(10) = dr(15) '9x0400
            cmOptionOneConfig(11) = dr(16) '9x0800
            cmOptionOneConfig(12) = dr(17) '9x1000
            cmOptionOneConfig(14) = dr(18) '9x4000
            cmOptionTwoConfig(0) = dr(19) '23x00000001
            cmOptionTwoConfig(1) = dr(20) '23x00000002
            cmOptionTwoConfig(2) = dr(21) '23x00000004
            cmOptionTwoConfig(3) = dr(22) '23x00000008
            cmOptionTwoConfig(4) = dr(23) '23x00000010
            cmOptionTwoConfig(5) = dr(24) '23x00000020
            cmOptionTwoConfig(6) = dr(25) '23x00000040
            cmOptionTwoConfig(7) = dr(26) '23x00000080
            cmOptionTwoConfig(8) = dr(27) '23x00000100
            cmOptionTwoConfig(9) = dr(28) '23x00000200
            cmOptionTwoConfig(10) = dr(29) '23x00000400
            cmOptionTwoConfig(11) = dr(30) '23x00000800
            cmOptionTwoConfig(12) = dr(31) '23x00001000
            cmOptionTwoConfig(13) = dr(32) '23x00002000
            cmOptionTwoConfig(14) = dr(33) '23x00004000
            cmOptionTwoConfig(15) = dr(34) '23x00008000
            cmOptionTwoConfig(16) = dr(35) '23x00010000
            cmOptionTwoConfig(17) = dr(36) '23x00020000
            cmOptionTwoConfig(18) = dr(37) '23x00040000
            cmOptionTwoConfig(19) = dr(38) '23x00080000
            cmOptionTwoConfig(20) = dr(39) '23x00100000
            cmOptionTwoConfig(21) = dr(40) '23x00200000
            cmOptionTwoConfig(22) = dr(41) '23x00400000
            cmOptionTwoConfig(23) = dr(42) '23x00800000
            cmOptionTwoConfig(24) = dr(43) '23x01000000
            cmOptionTwoConfig(25) = dr(44) '23x02000000
            cmOptionTwoConfig(26) = dr(45) '23x04000000
            cmOptionTwoConfig(27) = dr(46) '23x08000000
            cmOptionTwoConfig(28) = dr(47) '23x10000000
            cmOptionTwoConfig(29) = dr(48) '23x20000000
            cmOptionTwoConfig(30) = dr(49) '23x40000000
            cmOptionTwoConfig(31) = dr(50) '23x80000000
        Loop
        cn.Close()

        cmConf = 0
        cmOptionOneConf = 0
        cmOptionConf = 0
        cmOptionTwoConf = 0
        For i = 0 To 31
            If i < 16 Then
                cmConf += IIf(cmConfig(i) = True, 2 ^ i, 0)
                cmOptionOneConf += IIf(cmOptionOneConfig(i) = True, 2 ^ i, 0)
                cmOptionConf += IIf(cmOptionConfig(i) = True, 2 ^ i, 0)
            End If
            cmOptionTwoConf += IIf(cmOptionTwoConfig(i) = True, 2 ^ i, 0)
        Next
        myMacchina.Info.cmConfig = cmConf.ToString("X4")
        myMacchina.Info.cmOptionConfig = cmOptionConf.ToString("X4")
        myMacchina.Info.cmOptionOneConfig = cmOptionOneConf.ToString("X4")
        myMacchina.Info.cmOptionTwoConfig = DoubleToHex(cmOptionTwoConf, 8)

        ScriviMessaggio(String.Format(Messaggio(66), "F,1,7," & myMacchina.Info.cmConfig), lblMessaggio) 'Invio parametri CM CONFIG in corso ({0})
        UnitConfiguration = cmdComandoSingolo("F,1,7," & myMacchina.Info.cmConfig, 3)
        If UnitConfiguration <> "OK" Then Exit Function

        ScriviMessaggio(String.Format(Messaggio(67), "F,1,8," & myMacchina.Info.cmOptionConfig), lblMessaggio) 'Invio parametri CM OPTION CONFIG in corso ({0})
        UnitConfiguration = cmdComandoSingolo("F,1,8," & myMacchina.Info.cmOptionConfig, 3)
        If UnitConfiguration <> "OK" Then Exit Function

        ScriviMessaggio(String.Format(Messaggio(68), "F,1,9," & myMacchina.Info.cmOptionOneConfig), lblMessaggio) 'Invio parametri CM OPTION ONE CONFIG in corso ({0})
        UnitConfiguration = cmdComandoSingolo("F,1,9," & myMacchina.Info.cmOptionOneConfig, 3)
        If UnitConfiguration <> "OK" Then Exit Function

        ScriviMessaggio(String.Format(Messaggio(69), "F,1,23," & myMacchina.Info.cmOptionTwoConfig), lblMessaggio) 'Invio parametri CM OPTION TWO CONFIG in corso ({0})
        UnitConfiguration = cmdComandoSingolo("F,1,23," & myMacchina.Info.cmOptionTwoConfig, 3)
        If UnitConfiguration <> "OK" Then Exit Function

        If myMacchina.Info.Categoria = "CM18B" Or myMacchina.Info.Categoria = "OM61" Then
            ScriviMessaggio(Messaggio(74), lblMessaggio) 'Impostazione a 6 il numero di cassetti nel protocollo
            UnitConfiguration = cmdComandoSingolo("F,1,19,6", 3)

        End If

    End Function

    Function DoubleToHex(ByVal value As Double, Optional ByVal nBit As Integer = 8) As String
        Dim temp As Double = 0
        Dim rest As Int16 = 0
        temp = value
        Do Until value = 0
            If temp / 16 > 1 Then
                temp = Math.Truncate(temp / 16)
                rest = value - temp * 16
                value = temp
            Else
                rest = value
                value = 0
            End If
            DoubleToHex = rest & DoubleToHex
        Loop
        For i = DoubleToHex.Length + 1 To nBit
            DoubleToHex = "0" & DoubleToHex
        Next
    End Function
    Function CassBnNum(ByVal cass As String) As Integer

        Select Case cass
            Case "A"
                CassBnNum = myMacchina.CashData.A.bnNumber
            Case "B"
                CassBnNum = myMacchina.CashData.B.bnNumber
            Case "C"
                CassBnNum = myMacchina.CashData.C.bnNumber
            Case "D"
                CassBnNum = myMacchina.CashData.D.bnNumber
            Case "E"
                CassBnNum = myMacchina.CashData.E.bnNumber
            Case "F"
                CassBnNum = myMacchina.CashData.F.bnNumber
            Case "G"
                CassBnNum = myMacchina.CashData.G.bnNumber
            Case "H"
                CassBnNum = myMacchina.CashData.H.bnNumber
            Case "I"
                CassBnNum = myMacchina.CashData.I.bnNumber
            Case "L"
                CassBnNum = myMacchina.CashData.J.bnNumber
            Case "M"
                CassBnNum = myMacchina.CashData.K.bnNumber
            Case "N"
                CassBnNum = myMacchina.CashData.L.bnNumber
        End Select
    End Function
    Function InizializzazioneCassetti() As String
        'Empty cassettes
        'I,n,s,cas_id,code,2,bundle_num
        'reply: I,n,s,cas_id,rc,physical_nnn,logical_nnn
        'if physical<>logical goto init
        'I,1,L,AAAA,123456,2,200
        'I,1,L,BBBB,123456,2,200

        'init cassettes
        'command: I,n,s,n_id,code,note
        'reply: I,n,s,n_id,rc,message(0=OK, 6=DenNotEmpy, 4=WrongCode, 2=CassNotPresent, 1/3/5=reserved)
        'I,1,L,CPCA,123456
        'I,1,L,CPDA,123456
        Dim replyCode As New INIFile(localFolder & "\TheRCs" & myMacchina.Info.replyCodeFile & ".ini")
        Dim comando As String = ""
        Dim cassetto As String = ""
        Dim bnNumber As Integer = 0
        InizializzazioneCassetti = "OK"


        ScriviMessaggio(Messaggio(61), lblMessaggio) 'Verifica effettivo svuotamento cassetti
        For i = 1 To myMacchina.Info.MachineCassetteNumber
verifica:
            GetCashData()
            cassetto = Chr(i + 64)
            ScriviMessaggio(String.Format(Messaggio(73), cassetto), lblMessaggio) 'Verifica q.tà bn rilevate nel cassetto {0}
            If CassBnNum(cassetto) > 0 Then
                ScriviMessaggio(String.Format(Messaggio(62), cassetto), lblMessaggio) 'Il cassetto {0} non è vuoto. Svuotamento in corso
                cassetto = Chr(i + 64) & Chr(i + 64) & Chr(i + 64) & Chr(i + 64)
                comando = "I,1," & side & "," & cassetto & ",123456,2,200"
                InizializzazioneCassetti = cmdComandoSingolo(comando, 4, 112000)
                If comandoSingoloRisposta(4) <> "1" And comandoSingoloRisposta(4) <> "101" Then Exit Function
                If comandoSingoloRisposta(6) > 0 And comandoSingoloRisposta(7) > 0 And comandoSingoloRisposta(6) = comandoSingoloRisposta(7) Then GoTo verifica

                If comandoSingoloRisposta(6) <> comandoSingoloRisposta(7) Then
                    comando = "I,1," & side & "," & cassetto & ",123456"
                    InizializzazioneCassetti = cmdComandoSingolo(comando, 4, 112000)
                    Select Case comandoSingoloRisposta(5)
                        Case "0"'OK

                        Case "1", "3", "5" 'reserved

                        Case "2" 'cassette not present
                            InizializzazioneCassetti = "CASSETTE Not PRESENT"
                            Exit Function
                        Case "4" 'wrong code

                        Case "6" 'denomination not empty
                            GetCashData()
                            ScriviMessaggio(String.Format(Messaggio(62), cassetto), lblMessaggio) 'Il cassetto {0} non è vuoto. Svuotamento in corso
                            GoTo verifica
                    End Select
                    'se cassetto non vuoto, vai a verifica
                End If
            End If
        Next

    End Function


    Private Sub PremiPulsante()
        pulsantePremuto = False
        Do
            Application.DoEvents()
            If pulsantePremuto = True Then Exit Do
        Loop
    End Sub

    Private Function AnalisiErrorLog() As String
        ' Get unit life = H,n,0000 (5sec)
        ' Get log record = H,n,nnnn 


        Dim ultimoRecord As Integer = 0
        Dim recordReplyCode As String = ""
        Dim replyCode As New INIFile(localFolder & "\TheRCs" & myMacchina.Info.replyCodeFile & ".ini")
        Dim errorLog = New SErrorLog
        Dim iJam As Integer = 0
        grpCashData.Visible = False

        AnalisiErrorLog = "OK"
        lstErrorLog = New List(Of SErrorLog)
life:
        ScriviMessaggio(Messaggio(55), lblMessaggio) 'Analisi Error LOG in corso.
        AnalisiErrorLog = cmdComandoSingolo("H,1,0000", 2, 5000)
        If AnalisiErrorLog <> "OK" And AnalisiErrorLog <> "101" And AnalisiErrorLog <> "1" Then
            ScriviMessaggio(String.Format(Messaggio(268), AnalisiErrorLog), lblMessaggio) 'Errore durante la lettura dell'error log, risposta {0}. Premere un tasto per ripetere, ESC per uscire
            Select Case AttendiTasto()
                Case "ESC"
                    Exit Function
            End Select
            GoTo life
        End If
        Try
            myMacchina.ErrorLog.life = comandoSingoloRisposta(4)
        Catch
        End Try

        AnalisiErrorLog = cmdComandoSingolo("H,1,UUUU", 2, 5000)
        If AnalisiErrorLog <> "OK"  And AnalisiErrorLog <> "101" And AnalisiErrorLog <> "1"Then
            ScriviMessaggio(String.Format(Messaggio(268), AnalisiErrorLog), lblMessaggio) 'Errore durante la lettura dell'error log, risposta {0}. Premere un tasto per ripetere, ESC per uscire
            Select Case AttendiTasto()
                Case "ESC"
                    Exit Function
            End Select
            GoTo life
        End If
        Try
            ultimoRecord = comandoSingoloRisposta(3)
        Catch
        End Try

        trvErrorLog.Visible = True
        pgbErrorLog.Visible = True
        pgbErrorLog.Minimum = 0
        pgbErrorLog.Maximum = ultimoRecord
        trvErrorLog.Nodes.Clear()
        Dim newNode As TreeNode = New TreeNode
        newNode.Name = "JAM"
        newNode.Text = "JAM"
        trvErrorLog.Nodes.Add(newNode)
        For i = 0 To ultimoRecord - 1
            trvErrorLog.Nodes.Item(0).Text = "JAM (" & iJam & ")"
singoloLog:
            ScriviMessaggio(String.Format(Messaggio(72), i, ultimoRecord), lblMessaggio) 'Lettura error log in corso. Record {0} di {1}
            AnalisiErrorLog = cmdComandoSingolo("H,1," & Format(i, "0000"), 2, 8000)
            If AnalisiErrorLog <> "OK" And AnalisiErrorLog <> "101" And AnalisiErrorLog <> "1" Then
                ScriviMessaggio(String.Format(Messaggio(268), AnalisiErrorLog), lblMessaggio) 'Errore durante la lettura dell'error log, risposta {0}. Premere un tasto per ripetere, ESC per uscire
                Select Case AttendiTasto()
                    Case "ESC"
                        Exit Function
                End Select
                GoTo singoloLog
            End If
            If pgbErrorLog.Maximum >= i Then
                pgbErrorLog.Value = i
                ScriviMessaggio(String.Format(Messaggio(56), i + 1, ultimoRecord), lblMessaggio) 'Analisi Error LOG in corso. Record {0} di {1}.
            End If
            Try
                With errorLog
                    .line = comandoSingoloRisposta(3)
                    .life = comandoSingoloRisposta(4)
                    .operatorSide = comandoSingoloRisposta(5)
                    .transaction = comandoSingoloRisposta(6)
                    .replyCode = comandoSingoloRisposta(7)
                    .statusFeed = Mid(comandoSingoloRisposta(8), 2, 2)
                    .statusController = Mid(comandoSingoloRisposta(9), 2, 2)
                    .statusReader = Mid(comandoSingoloRisposta(10), 2, 2)
                    .statusSafe = Mid(comandoSingoloRisposta(11), 2, 2)
                    .statusCassA = Mid(comandoSingoloRisposta(12), 2, 2)
                    .statusCassB = Mid(comandoSingoloRisposta(13), 2, 2)
                    .statusCassC = Mid(comandoSingoloRisposta(14), 2, 2)
                    .statusCassD = Mid(comandoSingoloRisposta(15), 2, 2)
                    .statusCassE = Mid(comandoSingoloRisposta(16), 2, 2)
                    .statusCassF = Mid(comandoSingoloRisposta(17), 2, 2)
                    .statusCassG = Mid(comandoSingoloRisposta(18), 2, 2)
                    .statusCassH = Mid(comandoSingoloRisposta(19), 2, 2)
                    .statusCassI = Mid(comandoSingoloRisposta(20), 2, 2)
                    .statusCassJ = Mid(comandoSingoloRisposta(21), 2, 2)
                    .statusCassK = Mid(comandoSingoloRisposta(22), 2, 2)
                    .statusCassL = Mid(comandoSingoloRisposta(23), 2, 2)
                    .statusCassM = Mid(comandoSingoloRisposta(24), 2, 2)
                    .statusCassN = Mid(comandoSingoloRisposta(25), 2, 2)
                    .statusCassO = Mid(comandoSingoloRisposta(26), 2, 2)
                    .statusCassP = Mid(comandoSingoloRisposta(27), 2, 2)
                End With
            Catch
            End Try
            lstErrorLog.Add(errorLog)
            Try
                If ((Mid(lstErrorLog(i).replyCode, 1, 1) = "3" And lstErrorLog(i).replyCode.Length = 3) Or
                    (Mid(lstErrorLog(i).replyCode, 1, 1) = "4" And lstErrorLog(i).replyCode.Length = 3)) Then 'Or (lstErrorLog(i).replyCode.Length = 2 And CInt(lstErrorLog(i).replyCode) > 14)
                    iJam += 1
                    If lstErrorLog(i).replyCode.Length > 0 Then
                        Dim nCollection As TreeNodeCollection = trvErrorLog.Nodes
                        Dim sNode As TreeNode()
                        sNode = nCollection.Find(lstErrorLog(i).replyCode, True)
                        If sNode.Length = 0 Then
                            newNode = New TreeNode
                            newNode.Name = lstErrorLog(i).replyCode
                            newNode.Text = newNode.Name & " - " & replyCode.ReadValue("RC", lstErrorLog(i).replyCode)
                            trvErrorLog.Nodes.Item("JAM").Nodes.Add(newNode)
                        End If
                        newNode = New TreeNode
                        newNode.Name = lstErrorLog(i).line
                        newNode.Text = "line:  " & lstErrorLog(i).line & ", reply: " & lstErrorLog(i).replyCode &
                        ", Feed: " & lstErrorLog(i).statusFeed & ", Controller: " & lstErrorLog(i).statusController
                        trvErrorLog.Nodes.Item("JAM").Nodes.Item(lstErrorLog(i).replyCode).Nodes.Add(newNode)
                    End If
                End If
            Catch
            End Try
        Next

        pgbErrorLog.Visible = False
        btnEnd.Visible = True
        PremiPulsante()
        trvErrorLog.Visible = False
    End Function

    Private Sub SelezionaNumeroCassetti()
        cboCasNum.Items.Clear()

        For i = 6 To myMacchina.Info.MachineCassetteNumber Step 2
            cboCasNum.Items.Add(i)
            cboCasNum.Text = i
        Next
        numeroCassetti = ""
        cboCasNum.Visible = True
        cboCasNum.Focus()
        Do
            'Me.Text = tastoPremuto
            If numeroCassetti <> "" Then
                Exit Do
            End If
            Aspetta(50)
        Loop
        cboCasNum.Visible = False
    End Sub

    Private Sub SalvaLog()
        Dim infoMacchina As String
        Dim rigaLog As String
        Dim sep As String = " | "
        Dim intestazione As String = ""
        myMacchina.Test.DataFine = Format(Now, "dd-MM-yy")
        myMacchina.Test.OraFine = Format(Now, "HH:mm")
        intestazione = "ProductCode|ClientID|SerialNumber|DateStart|TimeStart|TestResult|SWVersion|ComputerName|" _
            & "ReaderSN|MacAddress|TestedCassNumber|MachineCassNumber|BagNumber|CD80Number|Cat2Box|SuiteCode|CD80Result|BagResult|" _
            & "SnCassetteA|SnCassetteB|SnCassetteC|SnCassetteD|SnCassetteE|SnCassetteF|SnCassetteG|SnCassetteH|SnCassetteI|" _
            & "SnCassetteJ|SnCassetteK|SnCassetteL|SnBag|TotalBnDep|TotBnRej|DateEnd|TimeEnd"

        With myMacchina
            'infoMacchina = .Info.Codice & sep & .Info.Cliente.ID & sep & .Serials.System & sep & .Test.DataInizio _
            '    & sep & .Test.OraInizio & sep & .Test.EsitoFinale & sep & Application.ProductVersion.ToString & sep _
            '    & System.Windows.Forms.SystemInformation.ComputerName & sep & .Serials.Reader & sep & .Info.MACAddress _
            '    & sep & .Info.CassetteNumber & sep & .Info.MachineCassetteNumber & sep & .Info.Bag & sep & .Info.CD80 _
            '    & sep & .Info.CRM & sep & .Firmware.Suite.Code _
            '    & sep & "-" & sep & "-" & sep & .Serials.CassettoA & sep & .Serials.CassettoB _
            '    & sep & .Serials.CassettoC & sep & .Serials.CassettoD & sep & .Serials.CassettoE & sep & .Serials.CassettoF _
            '    & sep & .Serials.CassettoG & sep & .Serials.CassettoH & sep & .Serials.CassettoI & sep & .Serials.CassettoJ _
            '    & sep & .Serials.CassettoK & sep & .Serials.CassettoL & sep & .Serials.Bag & sep & totBnDep & sep & totBnRej _
            '    & sep & .Test.DataFine & sep & .Test.OraFine
            infoMacchina = "001" & .Info.Codice + ",002" & .Info.Cliente.ID & ",004" & .Serials.System + ",300" & .Test.DataInizio _
                & ",301" & .Test.OraInizio & ",302" & .Test.EsitoFinale + ",005" & Application.ProductVersion.ToString _
                & ",006" & System.Windows.Forms.SystemInformation.ComputerName & ",007" & .Serials.Reader & ",008" & .Info.MACAddress _
                & ",009" & .Info.CassetteNumber & ",00A" & .Info.MachineCassetteNumber & ",00B" & .Info.Bag & ",00C" & .Info.CD80 _
                & ",00D" & .Info.CRM & ",100" & .Firmware.Suite.Code & ",00E" & .Serials.CassettoA & ",00F" & .Serials.CassettoB _
                & ",010" & .Serials.CassettoC & ",011" & .Serials.CassettoD & ",012" & .Serials.CassettoE & ",013" & .Serials.CassettoF _
                & ",014" & .Serials.CassettoG & ",015" & .Serials.CassettoH & ",016" & .Serials.CassettoI & ",017" & .Serials.CassettoJ _
                & ",018" & .Serials.CassettoK & ",019" & .Serials.CassettoL & ",01A" & .Serials.Bag & ",402" & totBnDep & ",403" & totBnRej _
                & ",05D" & .Info.cmConfig & ",05E" & .Info.cmOptionConfig & ",05F" & .Info.cmOptionOneConfig & ",060" & .Info.cmOptionTwoConfig _
                & ",305" & .Test.DataFine & ",306" & .Test.OraFine

        End With
        rigaLog = infoMacchina
        Try
            If Not File.Exists(localFolder & "\LogSecondoCollaudoV2.txt") Then
                Using fileLog As StreamWriter = File.CreateText(localFolder & "\LogSecondoCollaudoV2.txt")
                    'fileLog.WriteLine(intestazione)
                End Using
            End If
            If Mid(logFolder, logFolder.Length, 1) <> "\" Then logFolder &= "\"

            Using fileLog As StreamWriter = File.AppendText(localFolder & "\LogSecondoCollaudoV2.txt")
                fileLog.WriteLine(rigaLog)
            End Using
            File.Copy(localFolder & "\logSecondoCollaudoV2.txt", localFolder & "\" & myMacchina.Serials.System & ".txt")
        Catch ex As Exception
            ScriviMessaggio(ex.Message, lblMessaggio)
            Aspetta(1000)
        End Try
        SalvaLogServer(intestazione)

    End Sub

    Private Sub SalvaLogServer(ByVal intestazione As String)
        Dim lineaLetta As String = ""
        Try
            If Not File.Exists(logFolder & "logSecondoCollaudo.txt") Then
                Using serverLog As StreamWriter = File.CreateText(logFolder & "logSecondoCollaudoV2.txt")
                    serverLog.WriteLine(intestazione)
                End Using
            End If

            Dim localLog As StreamReader = New StreamReader(localFolder & "\logSecondoCollaudoV2.txt")
            Do While localLog.Peek() >= 0
                Using serverLog As StreamWriter = File.AppendText(logFolder & "logSecondoCollaudoV2.txt")
                    lineaLetta = localLog.ReadLine()
                    If Mid(lineaLetta, 1, 7) <> "Product" Then
                        serverLog.WriteLine(lineaLetta)
                    End If
                End Using
            Loop
            localLog.Close()

            File.Delete(localFolder & "\logSecondoCollaudoV2.txt")
        Catch ex As Exception
            ScriviMessaggio(ex.Message, lblMessaggio)
            Aspetta(1000)
        End Try
    End Sub
    Private Sub AggiornaStati()
        AzzeraStati()
        Aspetta(100)
        lblStatusFeeder.Text = myMacchina.Status.Feeder
        lblStatusController.Text = myMacchina.Status.Controller
        lblStatusReader.Text = myMacchina.Status.Reader
        lblStatusSafe.Text = myMacchina.Status.Safe
        lblStatusA.Text = myMacchina.Status.CassetteA
        lblStatusB.Text = myMacchina.Status.CassetteB
        lblStatusC.Text = myMacchina.Status.CassetteC
        lblStatusD.Text = myMacchina.Status.CassetteD
        lblStatusE.Text = myMacchina.Status.CassetteE
        lblStatusF.Text = myMacchina.Status.CassetteF
        lblStatusG.Text = myMacchina.Status.CassetteG
        lblStatusH.Text = myMacchina.Status.CassetteH
        lblStatusI.Text = myMacchina.Status.CassetteI
        lblStatusJ.Text = myMacchina.Status.CassetteJ
        lblStatusK.Text = myMacchina.Status.CassetteK
        lblStatusL.Text = myMacchina.Status.CassetteL

        myToolTip.SetToolTip(lblStatusFeeder, myMacchina.Status.MsgFeeder)
        myToolTip.SetToolTip(lblStatusController, myMacchina.Status.MsgController)
        myToolTip.SetToolTip(lblStatusReader, myMacchina.Status.MsgReader)
        myToolTip.SetToolTip(lblStatusSafe, myMacchina.Status.MsgSafe)
        myToolTip.SetToolTip(lblStatusA, myMacchina.Status.MsgCassetteA)
        myToolTip.SetToolTip(lblStatusB, myMacchina.Status.MsgCassetteB)
        myToolTip.SetToolTip(lblStatusC, myMacchina.Status.MsgCassetteC)
        myToolTip.SetToolTip(lblStatusD, myMacchina.Status.MsgCassetteD)
        myToolTip.SetToolTip(lblStatusE, myMacchina.Status.MsgCassetteE)
        myToolTip.SetToolTip(lblStatusF, myMacchina.Status.MsgCassetteF)
        myToolTip.SetToolTip(lblStatusG, myMacchina.Status.MsgCassetteG)
        myToolTip.SetToolTip(lblStatusH, myMacchina.Status.MsgCassetteH)
        myToolTip.SetToolTip(lblStatusI, myMacchina.Status.MsgCassetteI)
        myToolTip.SetToolTip(lblStatusJ, myMacchina.Status.MsgCassetteJ)
        myToolTip.SetToolTip(lblStatusK, myMacchina.Status.MsgCassetteK)
        myToolTip.SetToolTip(lblStatusL, myMacchina.Status.MsgCassetteL)

    End Sub



    Private Sub Principale_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Application.Exit()
    End Sub

    Private Sub Principale_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Application.Exit()
    End Sub

    Private Sub Principale_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        tastoPremuto = e.KeyCode
    End Sub

    Private Sub frmPincipale_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        Dim scala As UInteger = Me.Height / 754

        If Me.Width < 800 Then Me.Width = 800
        If Me.Height < 600 Then Me.Height = 600
        lblMessaggio.Width = Me.Width - 150
        lblMessaggio.Height = 120
        lblMessaggio.Top = 50
        lblMessaggio.Left = 25

        pgbErrorLog.Top = lblMessaggio.Top + lblMessaggio.Height + 10
        pgbErrorLog.Left = lblMessaggio.Left
        pgbErrorLog.Width = Me.Width * 0.5

        trvErrorLog.Top = pgbErrorLog.Top + pgbErrorLog.Height + 10
        trvErrorLog.Left = lblMessaggio.Left
        trvErrorLog.Width = pgbErrorLog.Width
        trvErrorLog.Height = (Me.Height - trvErrorLog.Top) / 2


        picLogo.Width = (Me.Width / 15.9)
        picLogo.Height = (Me.Width / 14.11)
        picLogo.Left = Me.Width - picLogo.Width - 25
        picLogo.Top = 40

        grpExtendedStatus.Left = 10
        grpExtendedStatus.Top = Me.Height - 180
        lblMacchina.Left = grpExtendedStatus.Width + 10
        lblMacchina.Top = grpExtendedStatus.Top + 115
        lblMacchina.Width = Me.Width - grpExtendedStatus.Width
        grpSerialNumber.Left = (Me.Width / 2) - (grpSerialNumber.Width / 2)
        grpSerialNumber.Top = (Me.Height / 2) - (grpSerialNumber.Height / 2)
        grpCashData.Left = (Me.Width / 2) - (grpCashData.Width / 2)
        grpCashData.Top = (Me.Height / 2) - (grpCashData.Height / 2) - 40

        cboIdProdotto.Width = 150 * scala
        cboCliente.Width = 400 * scala
        cboCasNum.Top = 150 * scala
        cboIdProdotto.Top = 150 * scala
        cboCliente.Top = 150 * scala
        cboCasNum.Left = (Me.Width - (cboIdProdotto.Width + cboCliente.Width)) / 2
        cboIdProdotto.Left = (Me.Width - (cboIdProdotto.Width + cboCliente.Width)) / 2
        cboCliente.Left = (cboIdProdotto.Left + cboIdProdotto.Width + 40)

        txtMatricola.Width = 300 * scala
        txtMatricola.Top = 150 * scala
        txtMatricola.Left = (Me.Width - txtMatricola.Width) / 2

        txtBagBarcode.Width = 300 * scala
        txtBagBarcode.Top = 150 * scala
        txtBagBarcode.Left = (Me.Width - txtBagBarcode.Width) / 2

        cmdPrincipale.Top = (Me.Height - cmdPrincipale.Height) / 2
        cmdPrincipale.Left = (Me.Width - cmdPrincipale.Width) / 2

        btnEnd.Top = trvErrorLog.Top
        btnEnd.Left = trvErrorLog.Left + trvErrorLog.Width + 10
        btnEnd.Width = cmdPrincipale.Width
        btnEnd.Height = cmdPrincipale.Height

    End Sub


    Function CercaComCM() As Integer

        CercaComCM = 0
        Dim myComPort As New SerialPort
        Dim inviato(4) As Byte
        Dim ricevuto As String
        Dim porteDisponibili As String() = Ports.SerialPort.GetPortNames
        If porteDisponibili Is Nothing Then
            ScriviMessaggio(Messaggio(210), lblMessaggio) 'Nessuna porta seriale disponibile. Premere un tasto per uscire
            AttendiTasto(, Me)
            End
        End If
        inviato(1) = &H5
        For i = 0 To porteDisponibili.Count - 1
            Try
                myComPort.PortName = porteDisponibili(i)
                myComPort.BaudRate = 9600
                myComPort.Parity = Parity.None
                myComPort.DataBits = 8
                myComPort.StopBits = StopBits.One
                myComPort.Close()
                myComPort.Open()
                myComPort.Write(inviato, 1, 1)
                Aspetta(100)
                ricevuto = myComPort.ReadExisting()
                If Len(ricevuto) > 0 Then
                    CercaComCM = Mid(porteDisponibili(0), 4, 1)
                    myComPort.Close()
                    Exit For
                End If
            Catch ex As Exception

            End Try
        Next
        If CercaComCM = 0 Then
            ScriviMessaggio(Messaggio(211), lblMessaggio) 'Non è possibile comunicare con la macchina
        End If
    End Function

    Private Sub Principale_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        myToolTip.AutoPopDelay = 5000
        myToolTip.InitialDelay = 1000
        myToolTip.ReshowDelay = 500
        myToolTip.ShowAlways = True
        SetThreadExecutionState(EXECUTION_STATE.ES_DISPLAY_REQUIRED Or EXECUTION_STATE.ES_CONTINUOUS)


        myLog = New XMLFile("logSecondoCollaudo.xml", TestType.SecondoCollaudo)

        'comScopeThread = New Thread(AddressOf AggiornaComScope)
        'comScopeThread.IsBackground = True
        'comScopeThread.Start()
        'LogTest = New STest
        AddHandler Me.KeyUp, AddressOf Principale_KeyUp

        'Me.Width = Screen.PrimaryScreen.Bounds.Width
        'Me.Height = Screen.PrimaryScreen.Bounds.Height
        Me.WindowState = FormWindowState.Maximized
        'Me.Width = 1024
        'Me.Height = 768
        Me.Top = (My.Computer.Screen.WorkingArea.Height \ 2) - (Me.Height \ 2)
        Me.Left = (My.Computer.Screen.WorkingArea.Width \ 2) - (Me.Width \ 2)
        Me.AcceptButton = cmdPrincipale

        'CaricaMessaggi(lingua)
        cboIdProdotto.Focus()
        Dim ip As String = String.Empty
        Dim myIps() As Net.IPAddress = Net.Dns.GetHostAddresses(Net.Dns.GetHostName)
        For Each myIp As Net.IPAddress In myIps
            If myIp.AddressFamily = AddressFamily.InterNetwork Then
                ip = myIp.ToString()
                Exit For
            End If
        Next

        Me.Text = Messaggio(1) & " - Ver. " & My.Application.Info.Version.ToString() & " TeamViewer ID: " & System.Net.Dns.GetHostName & " IpAddress: " & ip
        ScriviMessaggio(Messaggio(2), lblMessaggio)

        'Cerca aggiornamento
        Try
            Dim localVersionInfo = FileVersionInfo.GetVersionInfo(Application.StartupPath & "\Secondo_Collaudo.exe")
            Dim localVersion = localVersionInfo.ProductVersion
            Dim serverVersionInfo = FileVersionInfo.GetVersionInfo("\\arcadata\Manufacturing Italy\SWToolUpdate\SecondoCollaudo\Secondo_Collaudo.exe")
            Dim serverVersion = serverVersionInfo.ProductVersion
            If localVersion <> serverVersion Then
                MessageBox.Show("L'attuale versione è diversa da quella nel server: " & serverVersion)
                'Process.Start("Aggiorna SecondoCollaudo.bat")
            End If
        Catch
        End Try


        'TraceDirective = New STraceDirective
        'TraceDirective.infoLayer = ELayer.CMAPPLICATION
        'TraceDirective.tracePathName = "Trace.txt"
        'TraceDirective.lpComScope.hWin = IntPtr.Zero 'handle
        'TraceDirective.infoComp(ELayer.CMAPPLICATION).level = 5
        'TraceDirective.infoComp(ELayer.SIBCASH).level = 5
        'TraceDirective.infoComp(ELayer.CMCOMMAND).level = 0
        'TraceDirective.infoComp(ELayer.CMLINK).level = 5
        'TraceDirective.trcNum = 30
        'TraceDirective.trcSize = 1
        'TraceDirective.trcType = ETrcType.CM_DATE

        'Dim retTrace As Integer
        'retTrace = CMTrace_SetTraceDirective(TraceDirective)
        'MsgBox(retTrace)


    End Sub



    Private Sub Combo_Id_prodotto_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboIdProdotto.SelectedIndexChanged

        Dim Conn_String As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source =" & localDBFolder & databaseName
        'Dichiaro la nuova connessione
        Dim cn As New OleDbConnection(Conn_String)
        cn.Open()

        Dim sql As String
        sql = "SELECT distinct nome_cliente FROM clienti, tblman where tblman.id_prodotto Like '" & cboIdProdotto.Text & "' and tblman.id_cliente = clienti.id_cliente"

        Dim cmd As New OleDbCommand(sql, cn)

        cboCliente.Items.Clear()

        Dim dr As OleDbDataReader = cmd.ExecuteReader
        Do While Not dr.Read() = Nothing
            cboCliente.Items.Add(dr(0))
        Loop
        cn.Close()
        ScriviMessaggio(Messaggio(7), lblMessaggio) 'Seleziona il cliente
    End Sub

    Private Sub Combo_Id_prodotto_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboIdProdotto.TextChanged
        cboCliente.Text = ""
    End Sub

    Private Sub Principale_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown

        Crea_copia_locale()
        ScriviMessaggio(Messaggio(4), lblMessaggio)
        Riempi_combo_Id_Prodotto()
        cmdPrincipale.Text = Messaggio(5)
        cmdPrincipale.Visible = True
        btnEnd.Text = Messaggio(57)
    End Sub

    Sub Crea_copia_locale()
        My.Computer.FileSystem.CreateDirectory(localDBFolder)

        ScriviMessaggio(Messaggio(3), lblMessaggio) 'Copia del database in corso
        Application.DoEvents()
        Try
            My.Computer.FileSystem.CopyFile(serverDBFolder & databaseName, localDBFolder & databaseName, overwrite:=True)
        Catch ex As System.IO.FileNotFoundException
            ScriviMessaggio(String.Format(Messaggio(269), serverDBFolder & databaseName), lblMessaggio) 'Non e' stato possibile trovare il database {0}, verrà utilizzato il database presente in locale. Premere un tasto per continuare
            AttendiTasto()
        End Try
    End Sub

    Sub Riempi_combo_Id_Prodotto()
        Dim Conn_String As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source =" & localDBFolder & databaseName
        'Dichiaro la nuova connessione
        Dim cn As New OleDbConnection(Conn_String)
        Try
            cn.Open()

            Dim sql As String
            sql = "SELECT DISTINCT tblman.id_prodotto FROM tblman, prodotti where tblman.id_prodotto = prodotti.id_prodotto and (prodotti.categoria_prodotto like 'CM18%' OR prodotti.categoria_prodotto like 'CM20%' OR prodotti.categoria_prodotto like 'OM%')"

            Dim cmd As New OleDbCommand(sql, cn)

            Dim dr As OleDbDataReader = cmd.ExecuteReader

            Do While Not dr.Read() = Nothing
                cboIdProdotto.Items.Add(dr(0))

            Loop
            cn.Close()
        Catch ex As Exception
            ScriviMessaggio(Messaggio(270), lblMessaggio, Color.Red) 'Problemi di lettura del database, premere un tasto per uscire
            AttendiTasto()
        End Try
    End Sub

    '*************************************************************************************************************************
    '*************************************************************************************************************************
    '********************************** ALTO LIVELLO *************************************************************************
    '*************************************************************************************************************************
    '*************************************************************************************************************************

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPrincipale.Click
        cmdPrincipale.Visible = False
        ScriviMessaggio(Messaggio(6), lblMessaggio) 'Scegliere il codice macchina e il cliente e premere invio
        cboIdProdotto.Enabled = True
        cboIdProdotto.Visible = True
        cboCliente.Enabled = True
        cboCliente.Visible = True

        myMacchina.Test.DataInizio = Now.ToString("d")
        myMacchina.Test.OraInizio = Now.ToString("T")
        cboIdProdotto.Focus()

    End Sub

    Private Sub Combo_cliente_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCliente.SelectedIndexChanged
        Dim Conn_String As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source =" & localDBFolder & databaseName
        Dim cn As New OleDbConnection(Conn_String)
        Dim sql As String
        Dim fase As String = ""
        cboCliente.Enabled = False
        cboIdProdotto.Enabled = False

        myMacchina.Info.Cliente.Nome = cboCliente.Text
        myMacchina.Info.Codice = cboIdProdotto.Text

        Do
            Try
                fase = Messaggio(58)
                cn.Open()
                sql = "SELECT id_cliente FROM clienti where clienti.nome_cliente = '" & myMacchina.Info.Cliente.Nome & "'"
                Dim cmd1 As New OleDbCommand(sql, cn)
                Dim dr As OleDbDataReader = cmd1.ExecuteReader
                fase = Messaggio(59) 'Lettura database
                dr.Read()
                myMacchina.Info.Cliente.ID = dr(0)

                fase = Messaggio(58) 'Apertura database
                Dim cnn As New OleDbConnection(Conn_String)
                cnn.Open()

                sql = "SELECT DISTINCT prodotti.categoria_prodotto FROM prodotti where prodotti.id_prodotto = '" & myMacchina.Info.Codice & "'"
                Dim cmd2 As New OleDbCommand(sql, cnn)
                Dim drr As OleDbDataReader = cmd2.ExecuteReader
                fase = Messaggio(59) 'Lettura database
                drr.Read()
                myMacchina.Info.CategoriaCompleta = UCase(drr(0))
                myMacchina.Info.Categoria = myMacchina.Info.CategoriaCompleta
                Dim iCerca As String = InStr(myMacchina.Info.CategoriaCompleta, "CRM")
                myMacchina.Info.CRM = 0

                If iCerca > 0 Then
                    myMacchina.Info.CRM = 1
                    myMacchina.Info.Categoria = Mid(myMacchina.Info.CategoriaCompleta, 1, InStr(myMacchina.Info.CategoriaCompleta, " ") - 1)
                End If

                iCerca = InStr(myMacchina.Info.CategoriaCompleta, "CD80")
                myMacchina.Info.CD80 = 0
                If iCerca > 0 Then
                    myMacchina.Info.CD80 = Mid(myMacchina.Info.CategoriaCompleta, iCerca - 1, 1) * 2
                    myMacchina.Info.Categoria = Mid(myMacchina.Info.CategoriaCompleta, 1, InStr(myMacchina.Info.CategoriaCompleta, " ") - 1)
                End If
                fase = Messaggio(60) 'Chiusura database
                cnn.Close()
                fase = "OK"
            Catch ex As Exception
                ScriviMessaggio(fase & Chr(13) & ex.Message & Chr(13) & Messaggio(260), lblMessaggio, ArcaColor.errore) 'messaggio d'errore. premi un tasto per ripetere, esc per uscire
                Select Case AttendiTasto()
                    Case "ESC"
                        cboIdProdotto.Enabled = True
                        cboIdProdotto.Text = ""
                        cboCliente.Enabled = True
                        cboCliente.Items.Clear()
                        lblMacchina.Text = ""
                        Exit Sub
                End Select
            End Try
        Loop Until fase = "OK"

        lblMacchina.Visible = True
        lblMacchina.Text = myMacchina.Info.Codice & " - " & myMacchina.Info.Categoria
        If myMacchina.Info.CD80 > 0 Then lblMacchina.Text &= " - CD80=" & myMacchina.Info.CD80
        If myMacchina.Info.CRM > 0 Then lblMacchina.Text &= " - CAT2BOX "
        Select Case myMacchina.Info.Categoria
            Case "CM18", "CM18SOLO", "CM20S", "CM18EVO"
                myMacchina.Info.replyCodeFile = "CM18"
            Case "CM18T", "CM18SOLOT", "CM20T", "CM18EVOT", "CM18HC"
                myMacchina.Info.replyCodeFile = "CM18T"
            Case "CM18B", "OM61"
                myMacchina.Info.replyCodeFile = "CM18B"
            Case "CM20"
                myMacchina.Info.replyCodeFile = "CM20"
        End Select

        ScriviMessaggio(Messaggio(8), lblMessaggio)

attendi:
        Select Case AttendiTasto(, Me)
            Case "ESC"
                cboIdProdotto.Enabled = True
                cboIdProdotto.Text = ""
                cboCliente.Enabled = True
                cboCliente.Items.Clear()
                lblMacchina.Text = ""
                Exit Sub
            Case "INVIO"
                grpCashData.Visible = False
                grpExtendedStatus.Visible = False
                grpSerialNumber.Visible = False
                cboCliente.Visible = False
                cboIdProdotto.Visible = False
            Case Else
                GoTo attendi
        End Select




        ComboNascondi()
        myMacchina.Test.EsitoFinale = TestSecondoCollaudo()
        SalvaLog()
        If myMacchina.Info.Codice = "43664" Then
            ScriviMessaggio("ATTENZIONE! Prima di spegnere la macchina, eseguire la PERSONALIZZAZIONE IBM CANADA. premere 'E' per continuare", lblMessaggio, Color.LightGoldenrodYellow)
            AttendiTasto("E")
        End If
        cboCliente.Visible = True
        cboCliente.Enabled = True
        cboCliente.Items.Clear()
        cboCliente.Text = ""
        cboIdProdotto.Visible = True
        cboIdProdotto.Text = ""
        cboIdProdotto.Enabled = True
        cboIdProdotto.Focus()
        grpCashData.Visible = False
        grpExtendedStatus.Visible = False
        grpSerialNumber.Visible = False
        ScriviMessaggio(Messaggio(2), lblMessaggio) 'Scegliere il codice macchina e il cliente e premere invio
    End Sub

    Function GetCashData() As String
        CMExtendedStatus()
        AggiornaStati()
        grpCashData.Visible = True
        ScriviMessaggio(Messaggio(17), lblMessaggio) 'Get Cash Data in corso
        GetCashData = cmdComandoSingolo("G,1," & side, 3, 5000) 'Comando GETCASHDATA
        If GetCashData = "OK" Then
            grpCashData.Height = 110
            'Cassetto A
            txtCasADen.Text = comandoSingoloRisposta(4)
            myMacchina.CashData.A.name = comandoSingoloRisposta(7)
            If Len(comandoSingoloRisposta(4)) = 4 Then
                myMacchina.CashData.A.noteId = comandoSingoloRisposta(4)
                txtCasAVal.Text = Mid(comandoSingoloRisposta(4), 1, 2) & " " & ValueCode(comandoSingoloRisposta(4))
                txtCasABn.Text = CInt(comandoSingoloRisposta(5))
                myMacchina.CashData.A.bnNumber = CInt(comandoSingoloRisposta(5))
                lblCasAFree.Text = comandoSingoloRisposta(6)
                myMacchina.CashData.A.freeCap = comandoSingoloRisposta(6)
                prbCasA.Maximum = CInt(comandoSingoloRisposta(6)) + CInt(comandoSingoloRisposta(5))
                prbCasA.Value = CInt(comandoSingoloRisposta(5))
                lblCasAEnable.Text = comandoSingoloRisposta(8)
                myMacchina.CashData.A.enable = comandoSingoloRisposta(8)
                AbilitaCashData("A")
            End If

            'Cassetto B
            txtCasBDen.Text = comandoSingoloRisposta(9)
            myMacchina.CashData.B.name = comandoSingoloRisposta(12)
            If Len(comandoSingoloRisposta(9)) = 4 Then
                myMacchina.CashData.B.noteId = comandoSingoloRisposta(9)
                txtCasBVal.Text = Mid(comandoSingoloRisposta(9), 1, 2) & " " & ValueCode(comandoSingoloRisposta(9))
                txtCasBBn.Text = CInt(comandoSingoloRisposta(10))
                myMacchina.CashData.B.bnNumber = CInt(comandoSingoloRisposta(10))
                lblCasBFree.Text = comandoSingoloRisposta(11)
                myMacchina.CashData.B.freeCap = comandoSingoloRisposta(11)
                prbCasB.Maximum = CInt(comandoSingoloRisposta(11)) + CInt(comandoSingoloRisposta(10))
                prbCasB.Value = CInt(comandoSingoloRisposta(10))
                lblCasBEnable.Text = comandoSingoloRisposta(13)
                myMacchina.CashData.B.enable = comandoSingoloRisposta(13)
                AbilitaCashData("B")
            End If

            'Cassetto C
            txtCasCDen.Text = comandoSingoloRisposta(14)
            myMacchina.CashData.C.name = comandoSingoloRisposta(17)
            If Len(comandoSingoloRisposta(14)) = 4 Then
                myMacchina.CashData.C.noteId = comandoSingoloRisposta(14)
                txtCasCVal.Text = Mid(comandoSingoloRisposta(14), 1, 2) & " " & ValueCode(comandoSingoloRisposta(14))
                txtCasCBn.Text = CInt(comandoSingoloRisposta(15))
                myMacchina.CashData.C.bnNumber = CInt(comandoSingoloRisposta(15))
                lblCasCFree.Text = comandoSingoloRisposta(16)
                myMacchina.CashData.C.freeCap = comandoSingoloRisposta(16)
                prbCasC.Maximum = CInt(comandoSingoloRisposta(16)) + CInt(comandoSingoloRisposta(15))
                prbCasC.Value = CInt(comandoSingoloRisposta(15))
                lblCasCEnable.Text = comandoSingoloRisposta(18)
                myMacchina.CashData.C.enable = comandoSingoloRisposta(18)
                AbilitaCashData("C")
            End If

            'Cassetto D
            txtCasDDen.Text = comandoSingoloRisposta(19)
            myMacchina.CashData.D.name = comandoSingoloRisposta(22)
            If Len(comandoSingoloRisposta(19)) = 4 Then
                myMacchina.CashData.D.noteId = comandoSingoloRisposta(19)
                txtCasDVal.Text = Mid(comandoSingoloRisposta(19), 1, 2) & " " & ValueCode(comandoSingoloRisposta(19))
                txtCasDBn.Text = CInt(comandoSingoloRisposta(20))
                myMacchina.CashData.D.bnNumber = CInt(comandoSingoloRisposta(20))
                lblCasDFree.Text = comandoSingoloRisposta(21)
                myMacchina.CashData.D.freeCap = comandoSingoloRisposta(21)
                prbCasD.Maximum = CInt(comandoSingoloRisposta(21)) + CInt(comandoSingoloRisposta(20))
                prbCasD.Value = CInt(comandoSingoloRisposta(20))
                lblCasDEnable.Text = comandoSingoloRisposta(23)
                myMacchina.CashData.D.enable = comandoSingoloRisposta(23)
                AbilitaCashData("D")
            End If

            'Cassetto E
            txtCasEDen.Text = comandoSingoloRisposta(24)
            myMacchina.CashData.E.name = comandoSingoloRisposta(27)
            If Len(comandoSingoloRisposta(24)) = 4 Then
                myMacchina.CashData.E.noteId = comandoSingoloRisposta(24)
                txtCasEVal.Text = Mid(comandoSingoloRisposta(24), 1, 2) & " " & ValueCode(comandoSingoloRisposta(24))
                txtCasEBn.Text = CInt(comandoSingoloRisposta(25))
                myMacchina.CashData.E.bnNumber = CInt(comandoSingoloRisposta(25))
                lblCasEFree.Text = comandoSingoloRisposta(26)
                myMacchina.CashData.E.freeCap = comandoSingoloRisposta(26)
                prbCasE.Maximum = CInt(comandoSingoloRisposta(26)) + CInt(comandoSingoloRisposta(25))
                prbCasE.Value = CInt(comandoSingoloRisposta(25))
                lblCasEEnable.Text = comandoSingoloRisposta(28)
                myMacchina.CashData.E.enable = comandoSingoloRisposta(28)
                AbilitaCashData("E")
            End If

            'Cassetto F
            txtCasFDen.Text = comandoSingoloRisposta(29)
            myMacchina.CashData.F.name = comandoSingoloRisposta(32)
            If Len(comandoSingoloRisposta(29)) = 4 Then
                myMacchina.CashData.F.noteId = comandoSingoloRisposta(29)
                txtCasFVal.Text = Mid(comandoSingoloRisposta(29), 1, 2) & " " & ValueCode(comandoSingoloRisposta(29))
                txtCasFBn.Text = CInt(comandoSingoloRisposta(30))
                myMacchina.CashData.F.bnNumber = CInt(comandoSingoloRisposta(30))
                lblCasFFree.Text = comandoSingoloRisposta(31)
                myMacchina.CashData.F.freeCap = comandoSingoloRisposta(31)
                prbCasF.Maximum = CInt(comandoSingoloRisposta(31)) + CInt(comandoSingoloRisposta(30))
                prbCasF.Value = CInt(comandoSingoloRisposta(30))
                lblCasFEnable.Text = comandoSingoloRisposta(33)
                myMacchina.CashData.F.enable = comandoSingoloRisposta(33)
                AbilitaCashData("F")
            End If

            'Cassetto G
            If comandoSingoloRisposta.Length > 34 Then
                txtCasGDen.Text = comandoSingoloRisposta(34)
                myMacchina.CashData.G.name = comandoSingoloRisposta(37)
                If Len(comandoSingoloRisposta(34)) = 4 Then
                    myMacchina.CashData.G.noteId = comandoSingoloRisposta(34)
                    txtCasGVal.Text = Mid(comandoSingoloRisposta(34), 1, 2) & " " & ValueCode(comandoSingoloRisposta(34))
                    txtCasGBn.Text = CInt(comandoSingoloRisposta(35))
                    myMacchina.CashData.G.bnNumber = CInt(comandoSingoloRisposta(35))
                    lblCasGFree.Text = comandoSingoloRisposta(36)
                    myMacchina.CashData.G.freeCap = comandoSingoloRisposta(36)
                    prbCasG.Maximum = CInt(comandoSingoloRisposta(36)) + CInt(comandoSingoloRisposta(35))
                    prbCasG.Value = CInt(comandoSingoloRisposta(35))
                    lblCasGEnable.Text = comandoSingoloRisposta(38)
                    myMacchina.CashData.G.enable = comandoSingoloRisposta(38)
                    AbilitaCashData("G")
                End If
            End If

            'Cassetto H
            If comandoSingoloRisposta.Length > 39 Then
                txtCasHDen.Text = comandoSingoloRisposta(39)
                myMacchina.CashData.H.name = comandoSingoloRisposta(42)
                If Len(comandoSingoloRisposta(39)) = 4 Then
                    myMacchina.CashData.H.noteId = comandoSingoloRisposta(39)
                    txtCasHVal.Text = Mid(comandoSingoloRisposta(39), 1, 2) & " " & ValueCode(comandoSingoloRisposta(39))
                    txtCasHBn.Text = CInt(comandoSingoloRisposta(40))
                    myMacchina.CashData.H.bnNumber = CInt(comandoSingoloRisposta(40))
                    lblCasHFree.Text = comandoSingoloRisposta(41)
                    myMacchina.CashData.H.freeCap = comandoSingoloRisposta(41)
                    prbCasH.Maximum = CInt(comandoSingoloRisposta(41)) + CInt(comandoSingoloRisposta(40))
                    prbCasH.Value = CInt(comandoSingoloRisposta(40))
                    lblCasHEnable.Text = comandoSingoloRisposta(43)
                    myMacchina.CashData.H.enable = comandoSingoloRisposta(43)
                    AbilitaCashData("H")
                End If
            End If
            'Cassetto I
            If comandoSingoloRisposta.Length > 44 Then
                txtCasIDen.Text = comandoSingoloRisposta(44)
                myMacchina.CashData.I.name = comandoSingoloRisposta(47)
                If Len(comandoSingoloRisposta(44)) = 4 Then
                    myMacchina.CashData.I.noteId = comandoSingoloRisposta(44)
                    txtCasIVal.Text = Mid(comandoSingoloRisposta(44), 1, 2) & " " & ValueCode(comandoSingoloRisposta(44))
                    txtCasIBn.Text = CInt(comandoSingoloRisposta(45))
                    myMacchina.CashData.I.bnNumber = CInt(comandoSingoloRisposta(45))
                    lblCasIFree.Text = comandoSingoloRisposta(46)
                    myMacchina.CashData.I.freeCap = comandoSingoloRisposta(46)
                    prbCasI.Maximum = CInt(comandoSingoloRisposta(46)) + CInt(comandoSingoloRisposta(45))
                    prbCasI.Value = CInt(comandoSingoloRisposta(45))
                    lblCasIEnable.Text = comandoSingoloRisposta(48)
                    myMacchina.CashData.I.enable = comandoSingoloRisposta(48)
                    AbilitaCashData("I")
                End If
            End If

            'Cassetto J
            If comandoSingoloRisposta.Length > 49 Then
                txtCasJDen.Text = comandoSingoloRisposta(49)
                myMacchina.CashData.J.name = comandoSingoloRisposta(52)
                If Len(comandoSingoloRisposta(49)) = 4 Then
                    myMacchina.CashData.J.noteId = comandoSingoloRisposta(49)
                    txtCasJVal.Text = Mid(comandoSingoloRisposta(49), 1, 2) & " " & ValueCode(comandoSingoloRisposta(49))
                    txtCasJBn.Text = CInt(comandoSingoloRisposta(50))
                    myMacchina.CashData.J.bnNumber = CInt(comandoSingoloRisposta(50))
                    lblCasJFree.Text = comandoSingoloRisposta(51)
                    myMacchina.CashData.J.freeCap = comandoSingoloRisposta(51)
                    prbCasJ.Maximum = CInt(comandoSingoloRisposta(51)) + CInt(comandoSingoloRisposta(50))
                    prbCasJ.Value = CInt(comandoSingoloRisposta(50))
                    lblCasJEnable.Text = comandoSingoloRisposta(53)
                    myMacchina.CashData.J.enable = comandoSingoloRisposta(53)
                    AbilitaCashData("J")
                End If
            End If

            'Cassetto K
            If comandoSingoloRisposta.Length > 54 Then
                txtCasKDen.Text = comandoSingoloRisposta(54)
                myMacchina.CashData.K.name = comandoSingoloRisposta(57)
                If Len(comandoSingoloRisposta(54)) = 4 Then
                    myMacchina.CashData.K.noteId = comandoSingoloRisposta(54)
                    txtCasKVal.Text = Mid(comandoSingoloRisposta(54), 1, 2) & " " & ValueCode(comandoSingoloRisposta(54))
                    txtCasKBn.Text = CInt(comandoSingoloRisposta(55))
                    myMacchina.CashData.K.bnNumber = CInt(comandoSingoloRisposta(55))
                    lblCasKFree.Text = comandoSingoloRisposta(56)
                    myMacchina.CashData.K.freeCap = comandoSingoloRisposta(56)
                    prbCasK.Maximum = CInt(comandoSingoloRisposta(56)) + CInt(comandoSingoloRisposta(55))
                    prbCasK.Value = CInt(comandoSingoloRisposta(55))
                    lblCasKEnable.Text = comandoSingoloRisposta(58)
                    myMacchina.CashData.K.enable = comandoSingoloRisposta(58)
                    AbilitaCashData("K")
                End If
            End If
            'Cassetto L
            If comandoSingoloRisposta.Length > 59 Then
                txtCasLDen.Text = comandoSingoloRisposta(59)
                myMacchina.CashData.L.name = comandoSingoloRisposta(62)
                If Len(comandoSingoloRisposta(59)) = 4 Then
                    myMacchina.CashData.L.noteId = comandoSingoloRisposta(59)
                    txtCasLVal.Text = Mid(comandoSingoloRisposta(59), 1, 2) & " " & ValueCode(comandoSingoloRisposta(59))
                    txtCasLBn.Text = CInt(comandoSingoloRisposta(60))
                    myMacchina.CashData.L.bnNumber = CInt(comandoSingoloRisposta(60))
                    lblCasLFree.Text = comandoSingoloRisposta(61)
                    myMacchina.CashData.L.freeCap = comandoSingoloRisposta(61)
                    prbCasL.Maximum = CInt(comandoSingoloRisposta(61)) + CInt(comandoSingoloRisposta(60))
                    prbCasL.Value = CInt(comandoSingoloRisposta(60))
                    lblCasLEnable.Text = comandoSingoloRisposta(63)
                    myMacchina.CashData.L.enable = comandoSingoloRisposta(63)
                    AbilitaCashData("L")
                End If
            End If
        Else
            If GetCashData = "WRONG SIDE" Then
                ScriviMessaggio(String.Format(Messaggio(274), GetCashData, side), lblMessaggio) '{0}({1}). Premere un tasto per provare a cambiare lato, ESC per uscire
                If side = "L" Then side = "R" Else side = "L"
            Else
                ScriviMessaggio(String.Format(Messaggio(273), GetCashData), lblMessaggio) 'Get Cash Data non riuscito correttamente, risposta {0}. Premere un tasto per ripetere, ESC per uscire
            End If
            If AttendiTasto() = "ESC" Then
                Exit Function
            End If
        End If
        CMExtendedStatus()
        AggiornaStati()
        lblCasAStatus.ForeColor = ArcaColor.messaggio
        lblCasBStatus.ForeColor = ArcaColor.messaggio
        lblCasCStatus.ForeColor = ArcaColor.messaggio
        lblCasDStatus.ForeColor = ArcaColor.messaggio
        lblCasEStatus.ForeColor = ArcaColor.messaggio
        lblCasFStatus.ForeColor = ArcaColor.messaggio
        lblCasGStatus.ForeColor = ArcaColor.messaggio
        lblCasHStatus.ForeColor = ArcaColor.messaggio
        lblCasIStatus.ForeColor = ArcaColor.messaggio
        lblCasJStatus.ForeColor = ArcaColor.messaggio
        lblCasKStatus.ForeColor = ArcaColor.messaggio
        lblCasLStatus.ForeColor = ArcaColor.messaggio

        Select Case myMacchina.Status.CassetteA
            Case "04"
                lblCasAStatus.ForeColor = ArcaColor.attesa
                lblCasAStatus.Text = "FULL"
            Case "05"
                lblCasAStatus.ForeColor = ArcaColor.istruzione
                lblCasAStatus.Text = "EMPTY"
            Case "40"
                lblCasAStatus.Text = "ready"
            Case "07"
                lblCasAStatus.ForeColor = ArcaColor.errore
                lblCasAStatus.Text = "JAM"
            Case Else
                lblCasAStatus.Text = "-"
        End Select
        Select Case myMacchina.Status.CassetteB
            Case "04"
                lblCasBStatus.ForeColor = ArcaColor.attesa
                lblCasBStatus.Text = "FULL"
            Case "05"
                lblCasBStatus.ForeColor = ArcaColor.istruzione
                lblCasBStatus.Text = "EMPTY"
            Case "40"
                lblCasBStatus.Text = "ready"
            Case "07"
                lblCasAStatus.ForeColor = ArcaColor.errore
                lblCasAStatus.Text = "JAM"
            Case Else
                lblCasBStatus.Text = "-"
        End Select
        Select Case myMacchina.Status.CassetteC
            Case "04"
                lblCasCStatus.ForeColor = ArcaColor.attesa
                lblCasCStatus.Text = "FULL"
            Case "05"
                lblCasCStatus.ForeColor = ArcaColor.istruzione
                lblCasCStatus.Text = "EMPTY"
            Case "40"
                lblCasCStatus.Text = "ready"
            Case "07"
                lblCasAStatus.ForeColor = ArcaColor.errore
                lblCasAStatus.Text = "JAM"
            Case Else
                lblCasCStatus.Text = "-"
        End Select
        Select Case myMacchina.Status.CassetteD
            Case "04"
                lblCasDStatus.ForeColor = ArcaColor.attesa
                lblCasDStatus.Text = "FULL"
            Case "05"
                lblCasDStatus.ForeColor = ArcaColor.istruzione
                lblCasDStatus.Text = "EMPTY"
            Case "40"
                lblCasDStatus.Text = "ready"
            Case "07"
                lblCasAStatus.ForeColor = ArcaColor.errore
                lblCasAStatus.Text = "JAM"
            Case Else
                lblCasDStatus.Text = "-"
        End Select
        Select Case myMacchina.Status.CassetteE
            Case "04"
                lblCasEStatus.ForeColor = ArcaColor.attesa
                lblCasEStatus.Text = "FULL"
            Case "05"
                lblCasEStatus.ForeColor = ArcaColor.istruzione
                lblCasEStatus.Text = "EMPTY"
            Case "40"
                lblCasEStatus.Text = "ready"
            Case "07"
                lblCasAStatus.ForeColor = ArcaColor.errore
                lblCasAStatus.Text = "JAM"
            Case Else
                lblCasEStatus.Text = "-"
        End Select
        Select Case myMacchina.Status.CassetteF
            Case "04"
                lblCasFStatus.ForeColor = ArcaColor.attesa
                lblCasFStatus.Text = "FULL"
            Case "05"
                lblCasFStatus.ForeColor = ArcaColor.istruzione
                lblCasFStatus.Text = "EMPTY"
            Case "40"
                lblCasFStatus.Text = "ready"
            Case "07"
                lblCasAStatus.ForeColor = ArcaColor.errore
                lblCasAStatus.Text = "JAM"
            Case Else
                lblCasFStatus.Text = "-"
        End Select
        Select Case myMacchina.Status.CassetteG
            Case "04"
                lblCasGStatus.ForeColor = ArcaColor.attesa
                lblCasGStatus.Text = "FULL"
            Case "05"
                lblCasGStatus.ForeColor = ArcaColor.istruzione
                lblCasGStatus.Text = "EMPTY"
            Case "40"
                lblCasGStatus.Text = "ready"
            Case "07"
                lblCasAStatus.ForeColor = ArcaColor.errore
                lblCasAStatus.Text = "JAM"
            Case Else
                lblCasGStatus.Text = "-"
        End Select
        Select Case myMacchina.Status.CassetteH
            Case "04"
                lblCasHStatus.ForeColor = ArcaColor.attesa
                lblCasHStatus.Text = "FULL"
            Case "05"
                lblCasHStatus.ForeColor = ArcaColor.istruzione
                lblCasHStatus.Text = "EMPTY"
            Case "40"
                lblCasHStatus.Text = "ready"
            Case "07"
                lblCasAStatus.ForeColor = ArcaColor.errore
                lblCasAStatus.Text = "JAM"
            Case Else
                lblCasHStatus.Text = "-"
        End Select
        Select Case myMacchina.Status.CassetteI
            Case "04"
                lblCasIStatus.ForeColor = ArcaColor.attesa
                lblCasIStatus.Text = "FULL"
            Case "05"
                lblCasIStatus.ForeColor = ArcaColor.istruzione
                lblCasIStatus.Text = "EMPTY"
            Case "40"
                lblCasIStatus.Text = "ready"
            Case "07"
                lblCasAStatus.ForeColor = ArcaColor.errore
                lblCasAStatus.Text = "JAM"
            Case Else
                lblCasIStatus.Text = "-"
        End Select
        Select Case myMacchina.Status.CassetteJ
            Case "04"
                lblCasJStatus.ForeColor = ArcaColor.attesa
                lblCasJStatus.Text = "FULL"
            Case "05"
                lblCasJStatus.ForeColor = ArcaColor.istruzione
                lblCasJStatus.Text = "EMPTY"
            Case "40"
                lblCasJStatus.Text = "ready"
            Case "07"
                lblCasAStatus.ForeColor = ArcaColor.errore
                lblCasAStatus.Text = "JAM"
            Case Else
                lblCasJStatus.Text = "-"
        End Select
        Select Case myMacchina.Status.CassetteK
            Case "04"
                lblCasKStatus.ForeColor = ArcaColor.attesa
                lblCasKStatus.Text = "FULL"
            Case "05"
                lblCasKStatus.ForeColor = ArcaColor.istruzione
                lblCasKStatus.Text = "EMPTY"
            Case "40"
                lblCasKStatus.Text = "ready"
            Case "07"
                lblCasAStatus.ForeColor = ArcaColor.errore
                lblCasAStatus.Text = "JAM"
            Case Else
                lblCasKStatus.Text = "-"
        End Select
        Select Case myMacchina.Status.CassetteL
            Case "04"
                lblCasLStatus.ForeColor = ArcaColor.attesa
                lblCasLStatus.Text = "FULL"
            Case "05"
                lblCasLStatus.ForeColor = ArcaColor.istruzione
                lblCasLStatus.Text = "EMPTY"
            Case "40"
                lblCasLStatus.Text = "ready"
            Case "07"
                lblCasAStatus.ForeColor = ArcaColor.errore
                lblCasAStatus.Text = "JAM"
            Case Else
                lblCasLStatus.Text = "-"
        End Select

        If myMacchina.Info.Categoria = "CM18B" Or myMacchina.Info.Categoria = "OM61" Then
            Dim reply As String = cmdComandoSingolo("T,1,a,5", 4, 7000)
            If reply = "1" Or reply = "101" Then
                myMacchina.CashData.bag.status = comandoSingoloRisposta(6)
                Select Case myMacchina.CashData.bag.status
                    Case "04"
                        lblCasLStatus.ForeColor = ArcaColor.attesa
                        lblCasLStatus.Text = "FULL"
                    Case "05"
                        lblCasLStatus.ForeColor = ArcaColor.istruzione
                        lblCasLStatus.Text = "EMPTY"
                    Case "40"
                        lblCasLStatus.Text = "ready"
                    Case Else
                        lblCasLStatus.Text = "-"
                End Select
            Else
                ScriviMessaggio(String.Format(Messaggio(232), reply), lblMessaggio) 'Non è stato possibile leggere lo stato del modulo busta, risposta {0}. premere un tasto per ripetere, ESC per uscire
            End If
        End If

            Select Case myMacchina.Test.CassetteNumber
            Case 6
                myMacchina.CashData.totCassetti = myMacchina.CashData.A.bnNumber + myMacchina.CashData.B.bnNumber _
                    + myMacchina.CashData.C.bnNumber + myMacchina.CashData.D.bnNumber + myMacchina.CashData.E.bnNumber + myMacchina.CashData.F.bnNumber
            Case 8
                myMacchina.CashData.totCassetti = myMacchina.CashData.A.bnNumber + myMacchina.CashData.B.bnNumber _
                    + myMacchina.CashData.C.bnNumber + myMacchina.CashData.D.bnNumber + myMacchina.CashData.E.bnNumber _
                    + myMacchina.CashData.F.bnNumber + myMacchina.CashData.G.bnNumber + myMacchina.CashData.H.bnNumber
            Case 10
                myMacchina.CashData.totCassetti = myMacchina.CashData.A.bnNumber + myMacchina.CashData.B.bnNumber _
                    + myMacchina.CashData.C.bnNumber + myMacchina.CashData.D.bnNumber + myMacchina.CashData.E.bnNumber _
                    + myMacchina.CashData.F.bnNumber + myMacchina.CashData.G.bnNumber + myMacchina.CashData.H.bnNumber _
                    + myMacchina.CashData.I.bnNumber + myMacchina.CashData.J.bnNumber
            Case 12
                myMacchina.CashData.totCassetti = myMacchina.CashData.A.bnNumber + myMacchina.CashData.B.bnNumber _
                    + myMacchina.CashData.C.bnNumber + myMacchina.CashData.D.bnNumber + myMacchina.CashData.E.bnNumber _
                    + myMacchina.CashData.F.bnNumber + myMacchina.CashData.G.bnNumber + myMacchina.CashData.H.bnNumber _
                    + myMacchina.CashData.I.bnNumber + myMacchina.CashData.J.bnNumber + myMacchina.CashData.K.bnNumber + myMacchina.CashData.L.bnNumber
        End Select

        grpCashData.Visible = True
    End Function

    Private Sub AzzeraStati()
        lblStatusFeeder.Text = ""
        lblStatusController.Text = ""
        lblStatusReader.Text = ""
        lblStatusSafe.Text = ""
        lblStatusA.Text = ""
        lblStatusB.Text = ""
        lblStatusC.Text = ""
        lblStatusD.Text = ""
        lblStatusE.Text = ""
        lblStatusF.Text = ""
        lblStatusG.Text = ""
        lblStatusH.Text = ""
        lblStatusI.Text = ""
        lblStatusJ.Text = ""
        lblStatusK.Text = ""
        lblStatusL.Text = ""

        myToolTip.SetToolTip(lblStatusFeeder, "")
        myToolTip.SetToolTip(lblStatusController, "")
        myToolTip.SetToolTip(lblStatusReader, "")
        myToolTip.SetToolTip(lblStatusSafe, "")
        myToolTip.SetToolTip(lblStatusA, "")
        myToolTip.SetToolTip(lblStatusB, "")
        myToolTip.SetToolTip(lblStatusC, "")
        myToolTip.SetToolTip(lblStatusD, "")
        myToolTip.SetToolTip(lblStatusE, "")
        myToolTip.SetToolTip(lblStatusF, "")
        myToolTip.SetToolTip(lblStatusG, "")
        myToolTip.SetToolTip(lblStatusH, "")
        myToolTip.SetToolTip(lblStatusI, "")
        myToolTip.SetToolTip(lblStatusJ, "")
        myToolTip.SetToolTip(lblStatusK, "")
        myToolTip.SetToolTip(lblStatusL, "")
        For i = 0 To 12
            statusCassette(i) = ""
        Next
        statusFeeder = ""
        statusController = ""
        statusReader = ""
        statusSafe = ""
        statusEscrowA = ""
        statusDepositA = ""
        statusEscrowB = ""
        statusDepositB = ""
    End Sub


    Function DepositoCompleto(Optional ByVal specialClean As Boolean = False) As String
        Dim rCode As New INIFile(localFolder & "\TheRCs" & myMacchina.Info.replyCodeFile & ".ini")
        Dim bnOk, bnRej, bnUn As String
        Dim timeout As Integer = 60000
        DepositoCompleto = "KO"
        'Me.Text = messaggio(20) 'Deposito completo fino a riempimento di tutti i cassetti
        ScriviMessaggio(Messaggio(11), lblMessaggio) 'Fase di deposito completo. Inserire banconote fino a riempimento completo...
        If specialClean = True Then timeout = 400000

invioComando:
        Call GetCashData()
        ScriviMessaggio(Messaggio(12), lblMessaggio) 'Deposito completo in corso...

        DepositoCompleto = CMDeposit(side) '  cmdComandoSingolo("D,1," & side & ",0,0000", 4, timeout) 'Comando DEPOSIT
        If DepositoCompleto <> "OK" Then
            Select Case DepositoCompleto
                Case "NOTE ON FEEDER INPUT" 'note on feed input
                Case "DENOMINATION FULL" ' cassetto pieno
                    GoTo verificaCassetti
                Case "NOTE ON FRONT OUTPUT"
                    ScriviMessaggio(String.Format(Messaggio(13), DepositoCompleto), lblMessaggio) '. Togliere le banconote dalla bocchetta frontale per continuare
                    Aspetta(2000)
                    If Tasto = 27 Then
                        Exit Function
                    End If
                Case "NO INPUT NOTE"
                    ScriviMessaggio(String.Format(Messaggio(14), DepositoCompleto), lblMessaggio) '. Inserisci una mazzetta
                    Application.DoEvents()
                    Aspetta(2000)
                    If Tasto = 27 Then
                        Exit Function
                    End If

                Case "NOTE ON LEFT OUTPUT", "NOTE ON RIGHT OUTPUT"
                    ScriviMessaggio(String.Format(Messaggio(15), DepositoCompleto), lblMessaggio) '. Togliere le banconote dalla bocchetta di output per continuare
                    Aspetta(2000)
                    If Tasto = 27 Then
                        Exit Function
                    End If
                Case "MISFEEDING"
                    ScriviMessaggio(String.Format(Messaggio(233), rCode.ReadValue("RC", myDepositReply.rc), DepositoCompleto), lblMessaggio) 'Macchina in stato: /. Premere un tasto per ripetere, ESC per uscire
                    Select Case AttendiTasto(, Me)
                        Case "ESC"
                            Exit Function
                        Case Else
                            DepositoCompleto = ""
                            GoTo invioComando
                    End Select
                Case Else 'jam
                    ScriviMessaggio(String.Format(Messaggio(209), rCode.ReadValue("RC", myDepositReply.rc), DepositoCompleto), lblMessaggio) 'Macchina in stato: /. Premere un tasto per ripetere, ESC per uscire
                    Select Case AttendiTasto(, Me)
                        Case "ESC"
                            Exit Function
                        Case Else
                            GoTo invioComando
                    End Select
            End Select
            ScriviMessaggio(Messaggio(16), lblMessaggio) 'Inserire banconote fino a riempimento completo...
            stato = "ATTESA"
            Aspetta(1000)
            'If DepositoCompleto <> "OK" Then GoTo invioComando
            If tastoPremuto = 27 Then
                tastoPremuto = 0
                DepositoCompleto = "KO"
                Exit Function
            End If

        End If
verificaCassetti:
        bnOk = myDepositReply.NotesToSave
        bnUn = myDepositReply.NotesToOut
        bnRej = myDepositReply.NotesToRej
        totBnDep += bnOk
        totBnRej += bnRej

        If bnOk + bnRej + bnUn > 0 Then
            Me.Text = "Ultimo deposito effettuato: Banconote depositate: " & bnOk & ". Banconote UNFIT: " & bnUn & ". Banconote regettate: " & bnRej
            Aspetta(100)
        End If

        DepositoCompleto = "KO"
        CMExtendedStatus()
        AggiornaStati()

        Select Case numeroCassetti ' myMacchina.Test.CassetteNumber
            Case 6
                Select Case myMacchina.Info.CD80
                    Case 0
                        If myMacchina.Status.CassetteA = "04" And myMacchina.Status.CassetteB = "04" And myMacchina.Status.CassetteC = "04" _
                            And myMacchina.Status.CassetteD = "04" And myMacchina.Status.CassetteE = "04" And myMacchina.Status.CassetteF = "04" Then
                            DepositoCompleto = "OK"
                        End If
                    Case 2
                        If myMacchina.Status.CassetteC = "04" And myMacchina.Status.CassetteD = "04" And myMacchina.Status.CassetteE = "04" _
                            And myMacchina.Status.CassetteF = "04" Then
                            DepositoCompleto = "OK"
                        End If
                    Case 4
                        If myMacchina.Status.CassetteE = "04" And myMacchina.Status.CassetteF = "04" Then
                            DepositoCompleto = "OK"
                        End If
                End Select

            Case 8
                Select Case myMacchina.Info.CD80
                    Case 0
                        If myMacchina.Status.CassetteA = "04" And myMacchina.Status.CassetteB = "04" And myMacchina.Status.CassetteC = "04" _
                            And myMacchina.Status.CassetteD = "04" And myMacchina.Status.CassetteE = "04" And myMacchina.Status.CassetteF = "04" _
                            And myMacchina.Status.CassetteG = "04" And myMacchina.Status.CassetteH = "04" Then
                            DepositoCompleto = "OK"
                        End If
                    Case 2
                        If myMacchina.Status.CassetteC = "04" And myMacchina.Status.CassetteD = "04" And myMacchina.Status.CassetteE = "04" _
                            And myMacchina.Status.CassetteF = "04" And myMacchina.Status.CassetteG = "04" And myMacchina.Status.CassetteH = "04" Then
                            DepositoCompleto = "OK"
                        End If
                    Case 4
                        If myMacchina.Status.CassetteE = "04" And myMacchina.Status.CassetteF = "04" _
                            And myMacchina.Status.CassetteG = "04" And myMacchina.Status.CassetteH = "04" Then
                            DepositoCompleto = "OK"
                        End If
                End Select

            Case 10
                Select Case myMacchina.Info.CD80
                    Case 0
                        If myMacchina.Status.CassetteA = "04" And myMacchina.Status.CassetteB = "04" And myMacchina.Status.CassetteC = "04" _
                            And myMacchina.Status.CassetteD = "04" And myMacchina.Status.CassetteE = "04" And myMacchina.Status.CassetteF = "04" _
                            And myMacchina.Status.CassetteG = "04" And myMacchina.Status.CassetteH = "04" And myMacchina.Status.CassetteI = "04" _
                            And myMacchina.Status.CassetteJ = "04" Then
                            DepositoCompleto = "OK"
                        End If
                    Case 2
                        If myMacchina.Status.CassetteC = "04" And myMacchina.Status.CassetteD = "04" And myMacchina.Status.CassetteE = "04" _
                            And myMacchina.Status.CassetteF = "04" And myMacchina.Status.CassetteG = "04" And myMacchina.Status.CassetteH = "04" _
                            And myMacchina.Status.CassetteI = "04" And myMacchina.Status.CassetteJ = "04" Then
                            DepositoCompleto = "OK"
                        End If
                    Case 4
                        If myMacchina.Status.CassetteE = "04" And myMacchina.Status.CassetteF = "04" _
                            And myMacchina.Status.CassetteG = "04" And myMacchina.Status.CassetteH = "04" And myMacchina.Status.CassetteI = "04" _
                            And myMacchina.Status.CassetteJ = "04" Then
                            DepositoCompleto = "OK"
                        End If
                End Select

            Case 12
                Select Case myMacchina.Info.CD80
                    Case 0
                        If myMacchina.Status.CassetteA = "04" And myMacchina.Status.CassetteB = "04" And myMacchina.Status.CassetteC = "04" And
                            myMacchina.Status.CassetteD = "04" And myMacchina.Status.CassetteE = "04" And myMacchina.Status.CassetteF = "04" _
                            And myMacchina.Status.CassetteG = "04" And myMacchina.Status.CassetteH = "04" And myMacchina.Status.CassetteI = "04" _
                            And myMacchina.Status.CassetteJ = "04" And myMacchina.Status.CassetteK = "04" And myMacchina.Status.CassetteL = "04" Then
                            DepositoCompleto = "OK"
                        End If
                    Case 2
                        If myMacchina.Status.CassetteC = "04" And myMacchina.Status.CassetteD = "04" And myMacchina.Status.CassetteE = "04" _
                            And myMacchina.Status.CassetteF = "04" And myMacchina.Status.CassetteG = "04" And myMacchina.Status.CassetteH = "04" _
                            And myMacchina.Status.CassetteI = "04" And myMacchina.Status.CassetteJ = "04" And myMacchina.Status.CassetteK = "04" _
                            And myMacchina.Status.CassetteL = "04" Then
                            DepositoCompleto = "OK"
                        End If
                    Case 4
                        If myMacchina.Status.CassetteE = "04" And myMacchina.Status.CassetteF = "04" And myMacchina.Status.CassetteG = "04" _
                            And myMacchina.Status.CassetteH = "04" And myMacchina.Status.CassetteI = "04" And myMacchina.Status.CassetteJ = "04" _
                            And myMacchina.Status.CassetteK = "04" And myMacchina.Status.CassetteL = "04" Then
                            DepositoCompleto = "OK"
                        End If
                End Select

        End Select
        If DepositoCompleto <> "OK" Then
            GoTo invioComando
        End If

    End Function

    Function SvuotaUltimi() As String
        Dim rCode As New INIFile(localFolder & "\TheRCs" & myMacchina.Info.replyCodeFile & ".ini")
        Me.Text = Messaggio(18) 'Svuotamento ultimi cassetti
        SvuotaUltimi = ""
        Dim cassettiDaSvuotare As Integer
        Dim cassetto As String = ""
        Dim replyCode As String = ""
        If myMacchina.Test.CassetteNumber > 8 Then
            cassettiDaSvuotare = 4
        Else
            cassettiDaSvuotare = 2
        End If

        ScriviMessaggio(String.Format(Messaggio(21), cassettiDaSvuotare), lblMessaggio)

        For i = CInt(myMacchina.Test.CassetteNumber - 1) To CInt((myMacchina.Test.CassetteNumber - 1) - (cassettiDaSvuotare - 1)) Step -1

            Select Case i
                Case 11
                    cassetto = "LLLL"
                Case 10
                    cassetto = "KKKK"
                Case 9
                    cassetto = "JJJJ"
                Case 8
                    cassetto = "IIII"
                Case 7
                    cassetto = "HHHH"
                Case 6
                    cassetto = "GGGG"
                Case 5
                    cassetto = "FFFF"
                Case 4
                    cassetto = "EEEE"
                Case 3
                    cassetto = "DDDD"
                Case 2
                    cassetto = "CCCC"
                Case 1
                    cassetto = "BBBB"
                Case 0
                    cassetto = "AAAA"
            End Select


            Do Until SvuotaUltimi = "DENOMINATION EMPTY" '210
                ScriviMessaggio(String.Format(Messaggio(22), Mid(cassetto, 1, 1)), lblMessaggio) 'Svuotamento in corso cassetto: 
                SvuotaUltimi = cmdComandoSingolo("W,1,L," & cassetto & ",200", 3, 112000)
                If SvuotaUltimi <> "OK" Then
                    Select Case SvuotaUltimi
                        Case "NOTE ON FRONT OUTPUT" '206
                            ScriviMessaggio(String.Format(Messaggio(23), SvuotaUltimi), lblMessaggio) '. Togliere le banconote dalla bocchentta frontale per continuare
                            If Tasto = 27 Then
                                Exit Function
                            End If
                        Case "NO INPUT NOTE" '207
                            ScriviMessaggio(String.Format(Messaggio(24), SvuotaUltimi), lblMessaggio) '. Inserisci una mazzetta
                            If Tasto = 27 Then
                                Exit Function
                            End If
                        Case "NOTE ON LEFT OUTPUT", "NOTE ON RIGHT OUTPUT" ' 205
                            ScriviMessaggio(String.Format(Messaggio(25), SvuotaUltimi), lblMessaggio) '. Togliere le banconote dalla bocchetta di output per continuare
                            If Tasto = 27 Then
                                Exit Function
                            End If
                        Case "DENOMINATION EMPTY"
                            SvuotaUltimi = ""
                            Exit Do
                        Case Else
                            ScriviMessaggio(String.Format(Messaggio(26), SvuotaUltimi), lblMessaggio) '. Premere un tasto per continuare, ESC per uscire
                            AttendiTasto(, Me)
                            Select Case Tasto
                                Case 27
                                    SvuotaUltimi = False
                                    Exit Function
                            End Select
                    End Select
                    Aspetta(1000)

                Else
                    'Call InterpretaPrelievo()
                End If
                Call GetCashData()
                'SvuotaUltimi = ""
            Loop
        Next
        SvuotaUltimi = "OK"

    End Function


    Function PrelievoSingolo(ByVal numeroCicli As Integer) As String
        Dim rCode As New INIFile(localFolder & "\TheRCs" & myMacchina.Info.replyCodeFile & ".ini")
        Dim comando As String = "W,1,L"
        Dim replyCode As String = ""
        PrelievoSingolo = False
        Me.Text = Messaggio(27) ' Ciclo prelievo singola banconota da ogni cassetto
        'If myMacchina.Test.CRM = True Then

        '    Select Case myMacchina.Test.CassetteNumber
        '        Case 6
        '            comando = comando & ",AAAA,1,BBBB,1,CCCC,1,DDDD,1"
        '        Case 8, 10
        '            comando = comando & ",AAAA,1,BBBB,1,CCCC,1,DDDD,1,EEEE,1,FFFF,1"
        '        Case 12
        '            comando = comando & ",AAAA,1,BBBB,1,CCCC,1,DDDD,1,EEEE,1,FFFF,1,GGGG,1,HHHH,1"
        '    End Select
        'End If
        GetCashData()

        If myMacchina.Info.CD80 = 0 Then
            If myMacchina.CashData.A.bnNumber > numeroCicli Then
                comando &= ",AAAA,1"
            End If
            If myMacchina.CashData.B.bnNumber > numeroCicli Then
                comando &= ",BBBB,1"
            End If
            If myMacchina.CashData.C.bnNumber > numeroCicli Then
                comando &= ",CCCC,1"
            End If
            If myMacchina.CashData.D.bnNumber > numeroCicli Then
                comando &= ",DDDD,1"
            End If
        Else
            If myMacchina.Info.CD80 = 2 Then
                If myMacchina.CashData.C.bnNumber > numeroCicli Then
                    comando &= ",CCCC,1"
                End If
                If myMacchina.CashData.D.bnNumber > numeroCicli Then
                    comando &= ",DDDD,1"
                End If
            End If
        End If

        If myMacchina.CashData.E.bnNumber > numeroCicli Then
            comando &= ",EEEE,1"
        End If
        If myMacchina.CashData.F.bnNumber > numeroCicli Then
            comando &= ",FFFF,1"
        End If
        If myMacchina.CashData.G.bnNumber > numeroCicli Then
            comando &= ",GGGG,1"
        End If
        If myMacchina.CashData.H.bnNumber > numeroCicli Then
            comando &= ",HHHH,1"
        End If
        If myMacchina.CashData.I.bnNumber > numeroCicli Then
            comando &= ",IIII,1"
        End If
        If myMacchina.CashData.J.bnNumber > numeroCicli Then
            comando &= ",JJJJ,1"
        End If
        If myMacchina.CashData.K.bnNumber > numeroCicli Then
            comando &= ",KKKK,1"
        End If
        If myMacchina.CashData.L.bnNumber > numeroCicli Then
            comando &= ",LLLL,1"
        End If

        For i = 1 To numeroCicli
avvioPrelievo:
            ScriviMessaggio(String.Format(Messaggio(28), i, numeroCicli), lblMessaggio) 'Prelievo singolo a cascata in corso... ciclo n° 
            If comando = "W,1,L" Then Exit For
            cmdComandoSingolo(comando, 3, 112000)
            replyCode = rCode.ReadValue("RC", comandoSingoloRisposta(3))
            If replyCode <> "OK" Then
                Select Case replyCode
                    Case "NOTE ON FRONT OUTPUT" '206
                        ScriviMessaggio(String.Format(Messaggio(23), replyCode), lblMessaggio) '. Togliere le banconote dalla bocchetta anteriore per continuare
                        If Tasto = 27 Then
                            Exit Function
                        End If
                    Case "NO INPUT NOTE" '207
                        ScriviMessaggio(String.Format(Messaggio(24), replyCode), lblMessaggio) '. Inserisci una mazzetta
                        If Tasto = 27 Then
                            Exit Function
                        End If
                    Case "NOTE ON LEFT OUTPUT", "NOTE ON RIGHT OUTPUT" '205
                        ScriviMessaggio(String.Format(Messaggio(25), replyCode), lblMessaggio) '. Togliere le banconote dalla bocchetta di output per continuare
                        If Tasto = 27 Then
                            Exit Function
                        End If
                    Case "DENOMINATION EMPTY"

                    Case Else
                        ScriviMessaggio(String.Format(Messaggio(265), replyCode), lblMessaggio) 'Stato {0}. Premere un tasto per continuare, 'R' per ripetere o ESC per uscire.
                        Aspetta()
                        Select Case Tasto
                            Case 27
                                PrelievoSingolo = False
                                Exit Function
                            Case 82, 114
                                GoTo avvioPrelievo
                        End Select
                End Select
                Aspetta(1000)
                GetCashData()
                'GoTo avvioPrelievo
            Else
                'Call InterpretaDeposito()

            End If
            Aspetta(1000)
            Call GetCashData()
        Next
        PrelievoSingolo = replyCode

    End Function

    Function DepositoSingolo(Optional ByVal cicli As Integer = 0) As String
        Dim rCode As New INIFile(localFolder & "\TheRCs" & myMacchina.Info.replyCodeFile & ".ini")
        Dim depositiEffettuati As Integer = 0
        Dim replyCode As String = ""
        DepositoSingolo = False
        ScriviMessaggio(Messaggio(29), lblMessaggio) 'Inserire la mazzetta creata nell'ultima serie di prelievi 
        Aspetta(3000)

        Do Until replyCode = "OK"
            GetCashData()
singoloDeposito:
            ScriviMessaggio(Messaggio(30), lblMessaggio) 'Deposito in corso...
            cmdComandoSingolo("D,1," & side & ",0,0000", 3, 112000)
            replyCode = rCode.ReadValue("RC", comandoSingoloRisposta(3))
            DepositoSingolo = replyCode
            If comandoSingoloRisposta(3) = "1" Or comandoSingoloRisposta(3) = "101" Then
                If comandoSingoloRisposta(4) + comandoSingoloRisposta(5) + comandoSingoloRisposta(6) > 0 Then
                    depositiEffettuati += 1
                    totBnDep += comandoSingoloRisposta(4)
                    totBnRej += comandoSingoloRisposta(6)
                End If
            End If
            If replyCode <> "OK" Then
                Select Case replyCode
                    Case "NOTE ON FEEDER INPUT"

                    Case "NOTE ON FRONT OUTPUT" '206
                        ScriviMessaggio(String.Format(Messaggio(23), replyCode), lblMessaggio) '. Togliere le banconote dalla bocchentta frontale per continuare
                        If Tasto = 27 Then
                            Exit Function
                        End If
                    Case "NO INPUT NOTE" ' 207
                        ScriviMessaggio(String.Format(Messaggio(24), replyCode), lblMessaggio) '. Inserire la mazzetta creata nell'ultima serie di prelievi (1x30)
                        If Tasto = 27 Then
                            Exit Function
                        End If
                    Case "NOTE ON LEFT OUTPUT", "NOTE ON RIGHT OUTPUT" '205
                        ScriviMessaggio(String.Format(Messaggio(25), replyCode), lblMessaggio) '. Togliere le banconote dalla bocchetta di output per continuare
                        If Tasto = 27 Then
                            Exit Function
                        End If
                    Case "DENOMINATION FULL" '209 
                        replyCode = "OK"
                        DepositoSingolo = "OK"
                        GoTo verificaCicli
                    Case "FEEDER JAM"
                        ScriviMessaggio(Messaggio(271), lblMessaggio) 'FEEDER JAM. Aprire il feeder, rimuovere il jam e richiudere il feeder. Attendere il recovery e premere un tasto per continuare, ESC per uscire
                        AttendiTasto(, Me)
                        If Tasto = 27 Then
                            DepositoSingolo = "KO"
                            Exit Function
                        End If
                    Case Else
                        ScriviMessaggio(String.Format(Messaggio(26), replyCode), lblMessaggio) '{0}. Premere un tasto per continuare, ESC per uscire
                        AttendiTasto(, Me)
                        If Tasto = 27 Then
                            DepositoSingolo = "KO"
                            Exit Function
                        End If
                End Select
                GoTo singoloDeposito
            End If
            replyCode = ""
verificaCicli:
            If cicli > 0 And depositiEffettuati >= cicli Then
                GoTo cashData
            End If
            If cicli = 0 Then Exit Do
        Loop
        'DepositoSingolo = replyCode
cashData:
        DepositoSingolo = "OK"
        GetCashData()
    End Function

    Function DepositoInEscrow() As String
        Dim rCode As New INIFile(localFolder & "\TheRCs" & myMacchina.Info.replyCodeFile & ".ini")
        Dim depositiEffettuati As Integer = 0
        Dim replyCode As String = ""
        DepositoInEscrow = False
        ScriviMessaggio(Messaggio(49), lblMessaggio) 'Inserire una mazzetta 
        Aspetta(3000)

        Do Until replyCode = "OK"
            GetCashData()
singoloDeposito:
            ScriviMessaggio(Messaggio(30), lblMessaggio) 'Deposito in corso...
            cmdComandoSingolo("D,1," & side & ",0,0000",, 112000)
            replyCode = rCode.ReadValue("RC", comandoSingoloRisposta(4))
            DepositoInEscrow = replyCode
            If comandoSingoloRisposta(5) + comandoSingoloRisposta(6) + comandoSingoloRisposta(7) > 0 Then
                depositiEffettuati += 1
            End If
            If replyCode <> "OK" Then
                Select Case replyCode
                    Case "NOTE ON FRONT OUTPUT" '206
                        ScriviMessaggio(String.Format(Messaggio(23), replyCode), lblMessaggio) '. Togliere le banconote dalla bocchentta frontale per continuare
                        If Tasto = 27 Then
                            Exit Function
                        End If
                    Case "NO INPUT NOTE" ' 207
                        ScriviMessaggio(String.Format(Messaggio(24), replyCode), lblMessaggio) '. Inserire la mazzetta creata nell'ultima serie di prelievi (1x30)
                        If Tasto = 27 Then
                            Exit Function
                        End If
                    Case "NOTE ON LEFT OUTPUT", "NOTE ON RIGHT OUTPUT" '205
                        ScriviMessaggio(String.Format(Messaggio(25), replyCode), lblMessaggio) '. Togliere le banconote dalla bocchetta di output per continuare
                        If Tasto = 27 Then
                            Exit Function
                        End If
                    Case "DENOMINATION FULL" '209 
                        replyCode = "OK"
                        DepositoInEscrow = "OK"
                        Exit Do
                    Case Else
                        ScriviMessaggio(String.Format(Messaggio(26), replyCode), lblMessaggio) '. Premere un tasto per continuare, ESC per uscire
                        AttendiTasto(, Me)
                        Select Case Tasto
                            Case 27
                                'DepositoSingolo = False
                                Exit Function
                            Case Else

                        End Select
                End Select
                GoTo singoloDeposito
            End If
            replyCode = ""

        Loop
        'DepositoSingolo = replyCode
        GetCashData()
    End Function


    Function PrelievoDistribuito(ByVal numeroCicli As Integer) As String
        Dim rCode As New INIFile(localFolder & "\TheRCs" & myMacchina.Info.replyCodeFile & ".ini")
        Me.Text = Messaggio(115) & numeroCicli 'Prelievo di mazzette miste. n° cicli:
        Dim replyCode As String = ""
        PrelievoDistribuito = "KO"
        Dim comando As String = "W,1,L,"
        Select Case myMacchina.Test.CassetteNumber
            Case 6
                comando = comando & "AAAA,50,BBBB,50,CCCC,50,DDDD,50"
            Case 8, 10
                comando = comando & "AAAA,33,BBBB,33,CCCC,33,DDDD,33,EEEE,33,FFFF,33"
            Case 12
                comando = comando & "AAAA,20,BBBB,20,CCCC,20,DDDD,20,EEEE,20,FFFF,20,GGGG,20,HHHH,20"
        End Select

        For i = 1 To numeroCicli
            replyCode = ""
            Do Until replyCode = "OK"
                Call GetCashData()
                ScriviMessaggio(String.Format(Messaggio(31), i, numeroCicli), lblMessaggio) 'Prelievo in corso... ciclo n°:
                cmdComandoSingolo(comando,, 112000)
                replyCode = rCode.ReadValue("RC", comandoSingoloRisposta(3))
                If replyCode <> "OK" Then
                    Select Case replyCode
                        Case "NOTE ON FRONT OUTPUT" '206
                            ScriviMessaggio(String.Format(Messaggio(23), replyCode), lblMessaggio)  '. Togliere le banconote dalla bocchentta frontale per continuare
                            If Tasto = 27 Then
                                Exit Function
                            End If
                        Case "NOTE ON LEFT OUTPUT", "NOTE ON RIGHT OUTPUT" ' 205
                            ScriviMessaggio(String.Format(Messaggio(25), replyCode), lblMessaggio) '. Togliere le banconote dalla bocchetta di output per continuare
                            If Tasto = 27 Then
                                Exit Function
                            End If
                        Case "DENOMINATION EMPTY"
                            replyCode = "OK"
                            Exit Do
                        Case Else
                            ScriviMessaggio(String.Format(Messaggio(26), replyCode), lblMessaggio) '. Premere un tasto per continuare, ESC per uscire
                            AttendiTasto(, Me)
                            Select Case Tasto
                                Case 27
                                    PrelievoDistribuito = "KO"
                                    Exit Function
                            End Select
                    End Select
                    Aspetta(1000)
                End If
            Loop
            'replyCode = ""
            'Log.prelievoDistribuito = i
        Next
        grpCashData.Visible = False
        PrelievoDistribuito = replyCode
    End Function

    Function ControllaSerialeCassetto(ByVal seriale As String) As Boolean
        ControllaSerialeCassetto = True
        If Len(seriale) <> 12 Then
            ControllaSerialeCassetto = False
        Else
            For i = 1 To 12
                If Asc(Mid(seriale, i, 1)) < 48 Or Asc(Mid(seriale, i, 1)) > 57 Then
                    ControllaSerialeCassetto = False
                    Exit For
                End If
            Next
        End If
    End Function

    Private Sub txtSerial_KeyUp(sender As Object, e As KeyEventArgs) Handles txtSerialA.KeyUp, txtSerialB.KeyUp, txtSerialC.KeyUp, txtSerialD.KeyUp, txtSerialE.KeyUp, txtSerialF.KeyUp, txtSerialG.KeyUp, txtSerialH.KeyUp, txtSerialI.KeyUp, txtSerialJ.KeyUp, txtSerialK.KeyUp, txtSerialL.KeyUp
        If e.KeyCode = 13 Then
            If ControllaSerialeCassetto(sender.text) = True Then
                grpSerialNumber.Width = ((lblSerialA.Width + txtSerialA.Width) * 1.5) * 2
                Select Case sender.name.ToString
                    Case txtSerialA.Name.ToString
                        lblSerialA.Visible = True
                        serialCassetto(1) = txtSerialA.Text
                        txtSerialB.Focus()
                    Case txtSerialB.Name.ToString
                        lblSerialB.Visible = True
                        serialCassetto(2) = txtSerialB.Text
                        txtSerialC.Focus()
                    Case txtSerialC.Name.ToString
                        lblSerialC.Visible = True
                        serialCassetto(3) = txtSerialC.Text
                        txtSerialD.Focus()
                    Case txtSerialD.Name.ToString
                        lblSerialD.Visible = True
                        serialCassetto(4) = txtSerialD.Text
                        txtSerialE.Focus()
                    Case txtSerialE.Name.ToString
                        lblSerialE.Visible = True
                        serialCassetto(5) = txtSerialE.Text
                        txtSerialF.Focus()
                    Case txtSerialF.Name.ToString
                        lblSerialF.Visible = True
                        serialCassetto(6) = txtSerialF.Text
                        txtSerialG.Focus()
                    Case txtSerialG.Name.ToString
                        lblSerialG.Visible = True
                        serialCassetto(7) = txtSerialG.Text
                        txtSerialH.Focus()
                    Case txtSerialH.Name.ToString
                        lblSerialH.Visible = True
                        serialCassetto(8) = txtSerialH.Text
                        txtSerialI.Focus()
                    Case txtSerialI.Name.ToString
                        lblSerialI.Visible = True
                        serialCassetto(9) = txtSerialI.Text
                        txtSerialJ.Focus()
                    Case txtSerialJ.Name.ToString
                        lblSerialJ.Visible = True
                        serialCassetto(10) = txtSerialJ.Text
                        txtSerialK.Focus()
                    Case txtSerialK.Name.ToString
                        lblSerialK.Visible = True
                        serialCassetto(11) = txtSerialK.Text
                        txtSerialL.Focus()
                    Case txtSerialL.Name.ToString
                        lblSerialL.Visible = True
                        serialCassetto(12) = txtSerialL.Text
                End Select
                sender.enabled = False
                serialiVerificati += 1

            Else
                ScriviMessaggio("Serial number cassetto " & Mid(sender.name.ToString, 10, 1) & " errato. (" & sender.text & "). Reinserirlo!", lblMessaggio)
                sender.text = ""
                sender.focus()
                Aspetta(1000)
            End If
        End If
    End Sub

    Private Sub VisualizzaSerialNumber()
        grpSerialNumber.Visible = True
        For Each ctrl As Control In grpSerialNumber.Controls
            If TypeOf ctrl Is TextBox Then
                ctrl.Enabled = True
                ctrl.Text = ""
            End If
            ctrl.Visible = False
        Next

        'resetta controlli
        serialiVerificati = 0
        For i = 1 To 12
            serialCassetto(i) = ""
        Next
        grpSerialNumber.Width = 380
        If myMacchina.Test.CassetteNumber >= 2 Then
            LabelA.Text = "Cassetto A:"
            LabelA.Visible = True
            txtSerialA.Visible = True
            LabelB.Text = "Cassetto B:"
            LabelB.Visible = True
            txtSerialB.Visible = True
        End If
        If myMacchina.Test.CassetteNumber >= 4 Then
            LabelC.Text = "Cassetto C:"
            LabelC.Visible = True
            txtSerialC.Visible = True
            LabelD.Text = "Cassetto D:"
            LabelD.Visible = True
            txtSerialD.Visible = True
        End If
        If myMacchina.Test.CassetteNumber >= 6 Then
            LabelE.Text = "Cassetto E:"
            LabelE.Visible = True
            txtSerialE.Visible = True
            LabelF.Text = "Cassetto F:"
            LabelF.Visible = True
            txtSerialF.Visible = True
        End If
        If myMacchina.Test.CassetteNumber >= 8 Then
            LabelG.Text = "Cassetto G:"
            LabelG.Visible = True
            txtSerialG.Visible = True
            LabelH.Text = "Cassetto H:"
            LabelH.Visible = True
            txtSerialH.Visible = True
            grpSerialNumber.Width = 760
        End If
        If myMacchina.Test.CassetteNumber >= 10 Then
            LabelI.Text = "Cassetto I:"
            LabelI.Visible = True
            txtSerialI.Visible = True
            LabelJ.Text = "Cassetto J:"
            LabelJ.Visible = True
            txtSerialJ.Visible = True
        End If
        If myMacchina.Test.CassetteNumber = 12 Then
            LabelK.Text = "Cassetto K:"
            LabelK.Visible = True
            txtSerialK.Visible = True
            LabelL.Text = "Cassetto L:"
            LabelL.Visible = True
            txtSerialL.Visible = True
        End If
        txtSerialA.Focus()
    End Sub

    Function CassetteSerialNumber() As Boolean
        Dim replyCode As String = ""
        Dim comando As String = "Z,1"
        CassetteSerialNumber = False
        For i = 1 To CInt(myMacchina.Test.CassetteNumber)
invioComando:
            comando = comando & "," & Chr(i + 64) & ",0," & serialCassetto(i) & ",123456"
            ScriviMessaggio(String.Format(Messaggio(36), Chr(i + 64)), lblMessaggio) 'Scrittura Serial Number cassetto :
            replyCode = cmdComandoSingolo(comando, 4, 5000)
            myLog.AddTest("fillCasSN" & Chr(i + 64), "result", replyCode, "serial", serialCassetto(i))
            If replyCode <> "OK" Then
                Select Case replyCode
                    Case Else
                        ScriviMessaggio(String.Format(Messaggio(213), Chr(i + 64), replyCode), lblMessaggio) 'Scrittura Serial Number cassetto : //, premi un tasto per riprovare, ESC per uscire
                End Select
                AttendiTasto(, Me)
                Select Case Tasto
                    Case 27
                        CassetteSerialNumber = False
                        Exit Function
                    Case Else
                        GoTo invioComando
                End Select
            Else
                ScriviMessaggio(Messaggio(135) & Chr(i + 64) & " OK!", lblMessaggio)
                Select Case i
                    Case 1
                        myMacchina.Serials.CassettoA = serialCassetto(i)
                    Case 2
                        myMacchina.Serials.CassettoB = serialCassetto(i)
                    Case 3
                        myMacchina.Serials.CassettoC = serialCassetto(i)
                    Case 4
                        myMacchina.Serials.CassettoD = serialCassetto(i)
                    Case 5
                        myMacchina.Serials.CassettoE = serialCassetto(i)
                    Case 6
                        myMacchina.Serials.CassettoF = serialCassetto(i)
                    Case 7
                        myMacchina.Serials.CassettoG = serialCassetto(i)
                    Case 8
                        myMacchina.Serials.CassettoH = serialCassetto(i)
                    Case 9
                        myMacchina.Serials.CassettoI = serialCassetto(i)
                    Case 10
                        myMacchina.Serials.CassettoJ = serialCassetto(i)
                    Case 11
                        myMacchina.Serials.CassettoK = serialCassetto(i)
                    Case 12
                        myMacchina.Serials.CassettoL = serialCassetto(i)
                End Select
            End If
            comando = "Z,1"
        Next
        CassetteSerialNumber = True
    End Function

    Function AttendiReset() As String
        'Application.DoEvents()
        ScriviMessaggio(Messaggio(48), lblMessaggio) 'Recovery in corso 
        Application.DoEvents()
        Aspetta(20000)
attendi:
        AttendiReset = cmdComandoSingolo("O,1," & side & ",123456", 3, 5000)
        Do
            AttendiReset = cmdComandoSingolo("O,1," & side & ",123456", 3, 5000)
            Aspetta(500)
        Loop While comandoSingoloRisposta(3) = "102" Or comandoSingoloRisposta(3) = "4" Or AttendiReset = "BUSY"

    End Function

    Function InsertSerialNumber() As Boolean
inizio:
        Me.Text = Messaggio(32) 'Registrazione Serial Number Cassetti
        grpCashData.Visible = False
        InsertSerialNumber = False
        VisualizzaSerialNumber()
        Do Until InsertSerialNumber = True
            Application.DoEvents()
            If serialiVerificati = myMacchina.Test.CassetteNumber Then
                InsertSerialNumber = True
            End If
            ScriviMessaggio(Messaggio(33), lblMessaggio) 'Inserisci i serial number
        Loop
        If SerialiDoppi() = "SERIALDUPLICATE" Then
            InsertSerialNumber = False
            ScriviMessaggio(Messaggio(2222), lblMessaggio, ArcaColor.errore)
            Aspetta(2000)
            GoTo inizio
        End If



        ScriviMessaggio(Messaggio(47), lblMessaggio) 'Chiudi la cassaforte
        AttendiTasto(, Me)
        AttendiReset()

        If CassetteSerialNumber() = False Then
            InsertSerialNumber = False
        End If
        grpSerialNumber.Visible = False
    End Function

    Function SerialiDoppi() As String
        SerialiDoppi = ""
        For i = 1 To CInt(myMacchina.Test.CassetteNumber)
            For indice = i + 1 To CInt(myMacchina.Test.CassetteNumber)
                If serialCassetto(i).Length > 0 And serialCassetto(i) = serialCassetto(indice) Then
                    SerialiDoppi = "SERIALDUPLICATE"

                    Exit Function
                End If
            Next
        Next
    End Function

    Function PrelievoTotale() As String
        Dim rCode As New INIFile(localFolder & "\TheRCs" & myMacchina.Info.replyCodeFile & ".ini")
        Me.Text = Messaggio(35) 'Prelievo Completo di tutte le banconote da tutti i cassetti
        Dim replyCode As String = ""
        Dim comando As String
        PrelievoTotale = False

        For i = CInt(myMacchina.Info.MachineCassetteNumber) To 1 Step -1
            Do Until replyCode = "DENOMINATION EMPTY"
                Call GetCashData()
invioComando:
                comando = "W,1," & side & "," & Chr(i + 64) & Chr(i + 64) & Chr(i + 64) & Chr(i + 64) & ",200"
                ScriviMessaggio(String.Format(Messaggio(34), Chr(i + 64)), lblMessaggio) 'Prelievo in corso...
                cmdComandoSingolo(comando, 3, 112000)
                replyCode = rCode.ReadValue("RC", comandoSingoloRisposta(3))
                If replyCode <> "OK" Then
                    Select Case replyCode
                        Case "DENOMINATION EMPTY" ' 210 ' cassetto vuoto
                            Exit Do
                        Case "NOTE ON FRONT OUTPUT" ' 206
                            ScriviMessaggio(String.Format(Messaggio(23), replyCode), lblMessaggio) '. Togliere le banconote dalla bocchentta anteriore per continuare
                            Aspetta(1000)
                            GoTo invioComando
                            If Tasto = 27 Then
                                Exit Function
                            End If
                        Case "NOTE ON LEFT OUTPUT", "NOTE ON RIGHT OUTPUT" '205
                            ScriviMessaggio(String.Format(Messaggio(25), replyCode), lblMessaggio) '. Togliere le banconote dalla bocchetta di output per continuare
                            Aspetta(1000)
                            GoTo invioComando
                            If Tasto = 27 Then
                                Exit Function
                            End If
                        Case Else
                            ScriviMessaggio(String.Format(Messaggio(212), comandoSingoloRisposta(3), replyCode), lblMessaggio) '. Premere un tasto per continuare, ESC per uscire
                            AttendiTasto(, Me)
                            Select Case Tasto
                                Case 27
                                    PrelievoTotale = False
                                    Exit Function
                                Case Else
                                    GoTo invioComando
                            End Select
                    End Select

                    Aspetta(1000)
                End If
                If comandoSingoloRisposta(5) = 0 Then
                    replyCode = "DENOMINATION EMPTY"
                End If
            Loop
            replyCode = ""
        Next
        PrelievoTotale = "OK"
    End Function



    Function TransferCD80() As String
        Dim comando As String = ""
        TransferCD80 = ""
        Dim idModulo As String = ""

        GetCashData()

        For i = 1 To CInt(myMacchina.Info.CD80)
            Select Case i
                Case 1 : idModulo = "A"
                Case 2 : idModulo = "B"
                Case 3 : idModulo = "C"
                Case 4 : idModulo = "D"
                Case Else : idModulo = ""
            End Select

            Dim cassettoC As Integer
            Dim cassettoD As Integer
            Dim cassettoE As Integer
            Dim cassettoF As Integer
            Dim cassettoG As Integer
            Dim cassettoH As Integer
            Dim cassettoI As Integer
            Dim cassettoJ As Integer
            Dim cassettoK As Integer
            Dim cassettoL As Integer
            Dim freeCap As Integer = 0
            Dim totale As Integer = 0

            'comando = "w,1," & side & ",0," & idModulo & ",CPIA,66,CPEA,66,CPCA,66" 'specifici tagli (CPEA,10,EPCA,10) o target (EEEE,10,BBBB,10)

            Do Until TransferCD80 = "DENOMINATION FULL"
startDo:
                GetCashData()
                comando = "w,1," & side & ",0," & idModulo
                cassettoC = IIf(myMacchina.CashData.C.bnNumber > 50, myMacchina.CashData.C.bnNumber - 50, 0)
                cassettoD = IIf(myMacchina.CashData.D.bnNumber > 50, myMacchina.CashData.D.bnNumber - 50, 0)
                cassettoE = IIf(myMacchina.CashData.E.bnNumber > 50, myMacchina.CashData.E.bnNumber - 50, 0)
                cassettoF = IIf(myMacchina.CashData.F.bnNumber > 50, myMacchina.CashData.F.bnNumber - 50, 0)
                cassettoG = IIf(myMacchina.CashData.G.bnNumber > 50, myMacchina.CashData.G.bnNumber - 50, 0)
                cassettoH = IIf(myMacchina.CashData.H.bnNumber > 50, myMacchina.CashData.H.bnNumber - 50, 0)
                cassettoI = IIf(myMacchina.CashData.I.bnNumber > 50, myMacchina.CashData.I.bnNumber - 50, 0)
                cassettoJ = IIf(myMacchina.CashData.J.bnNumber > 50, myMacchina.CashData.J.bnNumber - 50, 0)
                cassettoK = IIf(myMacchina.CashData.K.bnNumber > 50, myMacchina.CashData.K.bnNumber - 50, 0)
                cassettoL = IIf(myMacchina.CashData.L.bnNumber > 50, myMacchina.CashData.L.bnNumber - 50, 0)
                Select Case idModulo
                    Case "A"
                        freeCap = IIf(myMacchina.CashData.A.freeCap >= 200, 200, myMacchina.CashData.A.freeCap)
                    Case "B"
                        freeCap = IIf(myMacchina.CashData.B.freeCap >= 200, 200, myMacchina.CashData.B.freeCap)
                    Case "C"
                        freeCap = IIf(myMacchina.CashData.C.freeCap >= 200, 200, myMacchina.CashData.C.freeCap)
                    Case "D"
                        freeCap = IIf(myMacchina.CashData.D.freeCap >= 200, 200, myMacchina.CashData.D.freeCap)
                End Select
                If freeCap = 0 Then
                    Exit Do
                End If

                If cassettoC > freeCap Then
                    comando &= ",CCCC," & freeCap
                    GoTo invioComando
                End If
                If cassettoC > 0 Then comando &= ",CCCC," & cassettoC

                If cassettoC + cassettoD > freeCap Then
                    comando &= ",DDDD," & freeCap - cassettoC
                    GoTo invioComando
                End If
                If cassettoD > 0 Then comando &= ",DDDD," & cassettoD

                If cassettoC + cassettoD + cassettoE > freeCap Then
                    comando &= ",EEEE," & freeCap - (cassettoC + cassettoD)
                    GoTo invioComando
                End If
                If cassettoE > 0 Then comando &= ",EEEE," & cassettoE

                If cassettoC + cassettoD + cassettoE + cassettoF > freeCap Then
                    comando &= ",FFFF," & freeCap - (cassettoC + cassettoD + cassettoE)
                    GoTo invioComando
                End If
                If cassettoF > 0 Then comando &= ",FFFF," & cassettoF

                If cassettoC + cassettoD + cassettoE + cassettoF + cassettoG > freeCap Then
                    comando &= ",GGGG," & freeCap - (cassettoC + cassettoD + cassettoE + cassettoF)
                    GoTo invioComando
                End If
                If cassettoG > 0 Then comando &= ",GGGG," & cassettoG

                If cassettoC + cassettoD + cassettoE + cassettoF + cassettoG + cassettoH > freeCap Then
                    comando &= ",HHHH," & freeCap - (cassettoC + cassettoD + cassettoE + cassettoF + cassettoG)
                    GoTo invioComando
                End If
                If cassettoH > 0 Then comando &= ",HHHH," & cassettoH

                If cassettoC + cassettoD + cassettoE + cassettoF + cassettoG + cassettoH + cassettoI > freeCap Then
                    comando &= ",IIII," & freeCap - (cassettoC + cassettoD + cassettoE + cassettoF + cassettoG + cassettoH)
                    GoTo invioComando
                End If
                If cassettoI > 0 Then comando &= ",IIII," & cassettoI

                If cassettoC + cassettoD + cassettoE + cassettoF + cassettoG + cassettoH + cassettoI + cassettoJ > freeCap Then
                    comando &= ",JJJJ," & freeCap - (cassettoC + cassettoD + cassettoE + cassettoF + cassettoG + cassettoH + cassettoI)
                    GoTo invioComando
                End If
                If cassettoJ > 0 Then comando &= ",JJJJ," & cassettoJ

                If cassettoC + cassettoD + cassettoE + cassettoF + cassettoG + cassettoH + cassettoI + cassettoJ + cassettoK > freeCap Then
                    comando &= ",KKKK," & freeCap - (cassettoC + cassettoD + cassettoE + cassettoF + cassettoG + cassettoH + cassettoI + cassettoJ)
                    GoTo invioComando
                End If
                If cassettoK > 0 Then comando &= ",KKKK," & cassettoK

                If cassettoC + cassettoD + cassettoE + cassettoF + cassettoG + cassettoH + cassettoI + cassettoJ + cassettoK + cassettoL > freeCap Then
                    comando &= ",LLLL," & freeCap - (cassettoC + cassettoD + cassettoE + cassettoF + cassettoH + cassettoG + cassettoI + cassettoJ + cassettoK)
                    GoTo invioComando
                End If
                If cassettoL > 0 Then comando &= ",LLLL," & cassettoL

invioComando:
                If comando.Length < 10 Then
                    ScriviMessaggio(Messaggio(275), lblMessaggio) 'Non ci sono sufficienti banconote da trasferire nei CD80. Depositarne tramite il comandi deposit e premere un tasto per ripetere o ESC per uscire
                    Select Case AttendiTasto()
                        Case "ESC"
                            myLog.AddTest("transferCd80", "result", TransferCD80)
                            Exit Function
                        Case Else
                            GoTo startDo
                    End Select
                End If
                ScriviMessaggio(Messaggio(64), lblMessaggio) 'Trasferimento banconote verso i CD80 in corso...
                TransferCD80 = cmdComandoSingolo(comando, 3, 800000)
                If TransferCD80 <> "OK" And TransferCD80 <> "DENOMINATION EMPTY" Then
                    Select Case TransferCD80
                        Case "DENOMINATION FULL"
                            TransferCD80 = "OK"
                            Exit Do
                            'Case "REQUEST OVER LIMIT"
                            '    Dim mancanti As Integer = 0
                            '    comando = "w,1," & side & ",0," & idModulo & ","
                            '    Select Case idModulo
                            '        Case "A"
                            '            mancanti = myMacchina.CashData.A.freeCap
                            '        Case "B"
                            '            mancanti = myMacchina.CashData.B.freeCap
                            '        Case "C"
                            '            mancanti = myMacchina.CashData.C.freeCap
                            '        Case "D"
                            '            mancanti = myMacchina.CashData.D.freeCap
                            '        Case "E"
                            '            mancanti = myMacchina.CashData.E.freeCap
                            '        Case "F"
                            '            mancanti = myMacchina.CashData.F.freeCap
                            '    End Select
                            '    If myMacchina.CashData.C.bnNumber = "CPIA" Or myMacchina.CashData.C.noteId = "CPEA" Or myMacchina.CashData.C.noteId = "CPCA" Then
                            '        If myMacchina.CashData.C.bnNumber >= mancanti Then
                            '            comando &= myMacchina.CashData.C.noteId & "," & mancanti
                            '            GoTo invioComando
                            '        Else
                            '            comando &= myMacchina.CashData.C.noteId & "," & myMacchina.CashData.C.bnNumber
                            '            mancanti -= myMacchina.CashData.C.bnNumber
                            '        End If
                            '    End If

                            '    If myMacchina.CashData.D.noteId = "CPIA" Or myMacchina.CashData.D.noteId = "CPEA" Or myMacchina.CashData.D.noteId = "CPCA" Then
                            '        If myMacchina.CashData.D.bnNumber >= mancanti Then
                            '            comando &= myMacchina.CashData.D.noteId & "," & mancanti
                            '            GoTo invioComando
                            '        Else
                            '            comando &= myMacchina.CashData.D.noteId & "," & myMacchina.CashData.D.bnNumber
                            '            mancanti -= myMacchina.CashData.D.bnNumber
                            '        End If
                            '    End If

                            '    If myMacchina.CashData.E.noteId = "CPIA" Or myMacchina.CashData.E.noteId = "CPEA" Or myMacchina.CashData.E.noteId = "CPCA" Then
                            '        If myMacchina.CashData.E.bnNumber >= mancanti Then
                            '            comando &= myMacchina.CashData.E.noteId & "," & mancanti
                            '            GoTo invioComando
                            '        Else
                            '            comando &= myMacchina.CashData.E.noteId & "," & myMacchina.CashData.E.bnNumber
                            '            mancanti -= myMacchina.CashData.E.bnNumber
                            '        End If
                            '    End If

                            '    If myMacchina.CashData.F.noteId = "CPIA" Or myMacchina.CashData.F.noteId = "CPEA" Or myMacchina.CashData.F.noteId = "CPCA" Then
                            '        If myMacchina.CashData.F.bnNumber >= mancanti Then
                            '            comando &= myMacchina.CashData.F.noteId & "," & mancanti
                            '            GoTo invioComando
                            '        Else
                            '            comando &= myMacchina.CashData.F.noteId & "," & myMacchina.CashData.F.bnNumber
                            '            mancanti -= myMacchina.CashData.F.bnNumber
                            '        End If
                            '    End If

                            '    If myMacchina.CashData.G.noteId = "CPIA" Or myMacchina.CashData.G.noteId = "CPEA" Or myMacchina.CashData.G.noteId = "CPCA" Then
                            '        If myMacchina.CashData.G.bnNumber >= mancanti Then
                            '            comando &= myMacchina.CashData.G.noteId & "," & mancanti
                            '            GoTo invioComando
                            '        Else
                            '            comando &= myMacchina.CashData.G.noteId & "," & myMacchina.CashData.G.bnNumber
                            '            mancanti -= myMacchina.CashData.G.bnNumber
                            '        End If
                            '    End If

                            '    If myMacchina.CashData.H.noteId = "CPIA" Or myMacchina.CashData.H.noteId = "CPEA" Or myMacchina.CashData.H.noteId = "CPCA" Then
                            '        If myMacchina.CashData.H.bnNumber >= mancanti Then
                            '            comando &= myMacchina.CashData.H.noteId & "," & mancanti
                            '            GoTo invioComando
                            '        Else
                            '            comando &= myMacchina.CashData.H.noteId & "," & myMacchina.CashData.H.bnNumber
                            '            mancanti -= myMacchina.CashData.H.bnNumber
                            '        End If
                            '    End If

                            '    If myMacchina.CashData.I.noteId = "CPIA" Or myMacchina.CashData.I.noteId = "CPEA" Or myMacchina.CashData.I.noteId = "CPCA" Then
                            '        If myMacchina.CashData.I.bnNumber >= mancanti Then
                            '            comando &= myMacchina.CashData.I.noteId & "," & mancanti
                            '            GoTo invioComando
                            '        Else
                            '            comando &= myMacchina.CashData.I.noteId & "," & myMacchina.CashData.I.bnNumber
                            '            mancanti -= myMacchina.CashData.I.bnNumber
                            '        End If
                            '    End If

                            '    If myMacchina.CashData.J.noteId = "CPIA" Or myMacchina.CashData.J.noteId = "CPEA" Or myMacchina.CashData.J.noteId = "CPCA" Then
                            '        If myMacchina.CashData.J.bnNumber >= mancanti Then
                            '            comando &= myMacchina.CashData.J.noteId & "," & mancanti
                            '            GoTo invioComando
                            '        Else
                            '            comando &= myMacchina.CashData.J.noteId & "," & myMacchina.CashData.J.bnNumber
                            '            mancanti -= myMacchina.CashData.J.bnNumber
                            '        End If
                            '    End If

                            '    If myMacchina.CashData.K.noteId = "CPIA" Or myMacchina.CashData.K.noteId = "CPEA" Or myMacchina.CashData.K.noteId = "CPCA" Then
                            '        If myMacchina.CashData.K.bnNumber >= mancanti Then
                            '            comando &= myMacchina.CashData.K.noteId & "," & mancanti
                            '            GoTo invioComando
                            '        Else
                            '            comando &= myMacchina.CashData.K.noteId & "," & myMacchina.CashData.K.bnNumber
                            '            mancanti -= myMacchina.CashData.K.bnNumber
                            '        End If
                            '    End If

                            '    If myMacchina.CashData.L.noteId = "CPIA" Or myMacchina.CashData.L.noteId = "CPEA" Or myMacchina.CashData.L.noteId = "CPCA" Then
                            '        If myMacchina.CashData.L.bnNumber >= mancanti Then
                            '            comando &= myMacchina.CashData.L.noteId & "," & mancanti
                            '            GoTo invioComando
                            '        Else
                            '            comando &= myMacchina.CashData.L.noteId & "," & myMacchina.CashData.L.bnNumber
                            '            mancanti -= myMacchina.CashData.L.bnNumber
                            '        End If
                            '    End If
                        Case Else
                            ScriviMessaggio(String.Format(Messaggio(212), comandoSingoloRisposta(3), TransferCD80), lblMessaggio, ArcaColor.errore)
                            Select Case AttendiTasto()
                                Case "ESC"
                                    myLog.AddTest("transferCd80", "result", TransferCD80)
                                    Exit Function
                                Case Else
                                    GoTo startDo
                            End Select
                    End Select
                End If
                'GetCashData()
            Loop
        Next
        GetCashData()
        myLog.AddTest("transferCd80", "result", TransferCD80)
    End Function

    Function HandleBarcodeCD80(Optional ByVal cassettePosition As String = "A", Optional ByVal barcode As String = "2043000108") As String
        Dim comando As String = ""
        Dim idModulo As String = ""
        HandleBarcodeCD80 = ""
        grpSerialNumber.Width = 385

        ScriviMessaggio(String.Format(Messaggio(43), barcode, cassettePosition), lblMessaggio)
        comando = "Z,1," & cassettePosition & ",4," & barcode
        HandleBarcodeCD80 = cmdComandoSingolo(comando, 4, 5000)
        'Select Case HandleBarcodeCD80
        '    Case "OK"
        '        HandleBarcodeCD80 = True
        '    Case Else
        '        HandleBarcodeCD80 = False
        '        Exit Function
        'End Select

        Select Case cassettePosition
            Case "A"
                'Log.barcodeCD80(1) = barcode
            Case "B"
                'Log.barcodeCD80(2) = barcode
        End Select

    End Function

    Function InserisciSeriale() As Boolean
        'Me.Text = "Serial number Prodotto"
        InserisciSeriale = False
        formatoSeriale = False
        ScriviMessaggio(Messaggio(9), lblMessaggio)
        txtMatricola.Visible = True
        txtMatricola.Focus()

        Do Until InserisciSeriale = True
            Application.DoEvents()
            'Aspetta(200)
            InserisciSeriale = formatoSeriale
        Loop
        txtMatricola.Visible = False
        myMacchina.Serials.System = txtMatricola.Text
        InserisciSeriale = True
    End Function

    Function DepositaInEscrow() As String
        Dim rCode As New INIFile(localFolder & "\TheRCs" & myMacchina.Info.replyCodeFile & ".ini")
        Dim replyCode As String = ""
        DepositaInEscrow = ""

deposita:
        ScriviMessaggio("Inserisci una mazzetta", lblMessaggio)
        GetCashData()
        Aspetta(1000)

        DepositaInEscrow = cmdComandoSingolo("D,1," & side & ",8,0000,q", 3, 400000)

        Do Until DepositaInEscrow = "GATE FULL"
            Select Case DepositaInEscrow
                Case "OK", "NOTE ON FEEDER INPUT", "NO INPUT NOTE"
                    If comandoSingoloRisposta(4) + comandoSingoloRisposta(6) > 0 Then
                        totBnDep += comandoSingoloRisposta(4)
                        totBnRej += comandoSingoloRisposta(6)
                    End If
                    GoTo deposita
                Case Else
                    ScriviMessaggio("macchina in stato: " & DepositaInEscrow & ". premere un tasto per ripetere, ESC per uscire", lblMessaggio)
                    Select Case AttendiTasto()
                        Case "ESC"
                            Exit Function
                        Case Else
                            GoTo deposita
                    End Select
            End Select
        Loop
        GetCashData()
    End Function

    Function TransferBag() As String
        ReDim comandoSingoloRisposta(4)
        comandoSingoloRisposta(4) = ""
        TransferBag = False
        grpCashData.Visible = True
        Dim comando As String = ""
        Do Until comandoSingoloRisposta(4) = "DENOMINA"
invioComando:
            GetCashData()
            If myMacchina.CashData.totCassetti = 0 Then
                DepositaInEscrow()
                GoTo invioComandoOpenGate
            End If
            ScriviMessaggio(Messaggio(44), lblMessaggio) 'Transfer in corso...
            Aspetta(1000)
            comando = "w,1," & side & ",0,q,AAAA," & CInt(myMacchina.CashData.A.bnNumber) & ",BBBB," _
                & CInt(myMacchina.CashData.B.bnNumber) & ",CCCC," & CInt(myMacchina.CashData.C.bnNumber) & ",DDDD," _
                & CInt(myMacchina.CashData.D.bnNumber) & ",EEEE," & CInt(myMacchina.CashData.E.bnNumber) & ",FFFF," & CInt(myMacchina.CashData.F.bnNumber)
comandosingolo:
            GetCashData()
            TransferBag = cmdComandoSingolo(comando, 3, 800000)
            If TransferBag <> "OK" Then
                Select Case TransferBag
                    Case "GATE FULL"
invioComandoOpenGate:
                        ScriviMessaggio(Messaggio(45), lblMessaggio) 'Deposito banconote in busta in corso
                        TransferBag = cmdComandoSingolo("M,1," & side & ",1,a", 3, 60000)
                        If TransferBag <> "OK" Then
                            Select Case TransferBag
                                Case "BAG FULL"
                                    GetCashData()
                                    TransferBag = "OK"
                                    Exit Do
                                Case "BAG NOT PRESENT"
                                    ScriviMessaggio(Messaggio(215), lblMessaggio, ArcaColor.errore)
                                    AttendiTasto(, Me)
                                    SetBagBarcode()
                                Case Else
                                    ScriviMessaggio(String.Format(Messaggio(214), comandoSingoloRisposta(3), TransferBag), lblMessaggio)
                                    AttendiTasto(, Me)
                                    Select Case Tasto
                                        Case 27
                                            TransferBag = False
                                            Exit Function
                                        Case Else
                                            GoTo invioComandoOpenGate
                                    End Select
                            End Select
                        End If
                        GetCashData()
                    Case "DENOMINATION FULL"
                        Exit Do
                    Case "REQUEST OVER LIMIT"
                        If cashData(0) < cashData(8) Then
                            cmdComandoSingolo("D,1," & side & ",8,0000,q", 3, 400000)
                        Else
                            comando = "w,1," & side & ",0,q"
                            If cashData(1) > cashData(8) Then
                                comando &= ",AAAA," & CInt(cashData(8))
                                GoTo componiComando
                            Else
                                comando &= ",AAAA," & CInt(cashData(1))
                                cashData(8) -= cashData(1)
                            End If

                            If cashData(2) > cashData(8) Then
                                comando &= ",BBBB," & CInt(cashData(8))
                                GoTo componiComando
                            Else
                                comando &= ",BBBB," & CInt(cashData(2))
                                cashData(8) -= cashData(2)
                            End If

                            If cashData(3) > cashData(8) Then
                                comando &= ",CCCC," & CInt(cashData(8))
                                GoTo componiComando
                            Else
                                comando &= ",CCCC," & CInt(cashData(3))
                                cashData(8) -= cashData(3)
                            End If

                            If cashData(4) > cashData(8) Then
                                comando &= ",DDDD," & CInt(cashData(8))
                                GoTo componiComando
                            Else
                                comando &= ",DDDD," & CInt(cashData(4))
                                cashData(8) -= cashData(4)
                            End If

                            If cashData(5) > cashData(8) Then
                                comando &= ",EEEE," & CInt(cashData(8))
                                GoTo componiComando
                            Else
                                comando &= ",EEEE," & CInt(cashData(5))
                                cashData(8) -= cashData(5)
                            End If

                            If cashData(6) > cashData(8) Then
                                comando &= ",FFFF," & CInt(cashData(8))
                                GoTo componiComando
                            Else
                                comando &= ",FFFF," & CInt(cashData(6))
                                cashData(8) -= cashData(6)
                            End If
                        End If
componiComando:
                        'comando = "w,1," & side & ",0,q,AAAA," & CInt(cashData(1)) & ",BBBB," & CInt(cashData(2)) & ",CCCC," & CInt(cashData(3)) & ",DDDD," & CInt(cashData(4)) & ",EEEE," & CInt(cashData(5)) & ",FFFF," & CInt(cashData(6))
                        GoTo comandosingolo

                    Case "DENOMINATION EMPTY"
                        DepositaInEscrow()
                        GoTo invioComandoOpenGate
                    Case Else
                        ScriviMessaggio(String.Format(Messaggio(214), comandoSingoloRisposta(3), TransferBag), lblMessaggio)
                        AttendiTasto(, Me)
                        Select Case Tasto
                            Case 27
                                TransferBag = False
                                Exit Function
                            Case Else
                                GoTo invioComando
                        End Select
                End Select
            End If

            GetCashData()
            Aspetta(1000)
        Loop

        grpCashData.Visible = False
        TransferBag = "OK"
    End Function

    Function SetBagBarcode() As String
        'Aprire la cassaforte
        'inviare B,1,7,0,2,0 per aprire il modulo busta -> rc=OK
        'sigillare la busta se presente e rimuoverla
        'inviare T,1,a,5 per il bag switches status rc=OK e stato 09 (T,1,a,101,40, 09 ,0000,0000*
        'Set barcode bag Z,1,a,2,"barcode"  rc=OK
        'inviare T,1,a,5 per il bag switches status rc=OK e stato 40 (T,1,a,101,40, 40 ,0000,0000*
        'chiudere il modulo busta
        'get barcode bag T,1,a,2 -> rc=OK e par="barcode"

        Dim bagBarcode As String = "a"
        SetBagBarcode = False
        grpCashData.Visible = False
        ScriviMessaggio(Messaggio(38), lblMessaggio) 'Aprire la cassaforte ed estrarre il modulo inferiore. premere un tasto per continuare, ESC per uscire
        AttendiTasto(, Me)
        If Tasto = 27 Then
            SetBagBarcode = "UNABLE TO OPEN SAFE"
            Exit Function
        End If

TemporaryBagOpenDoor:
        ScriviMessaggio(Messaggio(37), lblMessaggio)
        SetBagBarcode = cmdComandoSingolo("B,1,7,0,5,0", 2, 5000)
        If SetBagBarcode <> "OK" Then
            Select Case SetBagBarcode
                Case "UNEXPECTED COMMAND"
                    ScriviMessaggio(Messaggio(244), lblMessaggio) 'elettromagnete o ESC per uscire", lblMessaggio)
                    AttendiTasto(, Me)
                    Select Case tastoPremuto
                        'Case "e"
                            'elm on
                        Case 27
                            Exit Function
                        Case Else
                            GoTo TemporaryBagOpenDoor
                    End Select
                Case Else
                    ScriviMessaggio(String.Format(Messaggio(216), comandoSingoloRisposta(3), SetBagBarcode), lblMessaggio) 'elettromagnete o ESC per uscire", lblMessaggio)
                    AttendiTasto(, Me)
                    Select Case tastoPremuto
                        'Case "e"
                            'elm on
                        Case 27
                            Exit Function
                        Case Else
                            GoTo TemporaryBagOpenDoor
                    End Select
            End Select
        End If

        Aspetta(1000)

BagSwitchesStatus:
        SetBagBarcode = cmdComandoSingolo("T,1,a,5", 4, 7000)
        Try
            If SetBagBarcode <> "OK" Then
                Select Case SetBagBarcode
                    Case Else
                        ScriviMessaggio(String.Format(Messaggio(214), comandoSingoloRisposta(5), SetBagBarcode), lblMessaggio)
                        AttendiTasto(, Me)
                        Select Case tastoPremuto
                            Case 27
                                Exit Function
                            Case Else
                                GoTo BagSwitchesStatus
                        End Select
                End Select
            End If
        Catch
        End Try

SetBarcodeBag:
        ScriviMessaggio(Messaggio(39), lblMessaggio)
        txtBagBarcode.Visible = True
        txtBagBarcode.Text = ""
        txtBagBarcode.Focus()
        Do Until Len(txtBagBarcode.Text) = 12
            Application.DoEvents()
            Aspetta(100)
        Loop
        bagBarcode = txtBagBarcode.Text
        SetBagBarcode = cmdComandoSingolo("Z,1,a,2," & bagBarcode, 4, 5000)
        Try
            If SetBagBarcode <> "OK" Then
                Select Case SetBagBarcode
                    Case "SW ERROR"
                        ScriviMessaggio(String.Format(Messaggio(243), comandoSingoloRisposta(5), SetBagBarcode, txtBagBarcode.Text), lblMessaggio)
                        txtBagBarcode.Visible = False
                        AttendiTasto(, Me)
                        Select Case tastoPremuto
                            Case 27
                                Exit Function
                            Case Else
                                GoTo TemporaryBagOpenDoor
                        End Select
                    Case Else
                        ScriviMessaggio(String.Format(Messaggio(214), comandoSingoloRisposta(5), SetBagBarcode), lblMessaggio)
                        txtBagBarcode.Visible = False
                        AttendiTasto(, Me)
                        Select Case tastoPremuto
                            Case 27
                                Exit Function
                            Case Else
                                GoTo SetBarcodeBag
                        End Select
                End Select
            End If
        Catch
        End Try
        myMacchina.Serials.Bag = bagBarcode
        txtBagBarcode.Visible = False

        ScriviMessaggio(Messaggio(40), lblMessaggio)
        AttendiTasto(, Me)



BagSwitchesStatus2:
        SetBagBarcode = cmdComandoSingolo("T,1,a,5", 4, 7000)
        Try
            Select Case SetBagBarcode
                Case "OK"
                    If comandoSingoloRisposta(6) <> "40" Then
                        Select Case comandoSingoloRisposta(6)
                            Case "09"
                                ScriviMessaggio(Messaggio(218), lblMessaggio, ArcaColor.errore)
                            Case Else
                                ScriviMessaggio(String.Format(Messaggio(214), comandoSingoloRisposta(6)), lblMessaggio, ArcaColor.errore) 'Stato: {0}. Premere un tasto per ripetere, ESC per uscire.
                        End Select

                        AttendiTasto(, Me)
                        Select Case tastoPremuto
                            Case 27
                                Exit Function
                            Case Else
                                GoTo BagSwitchesStatus2
                        End Select
                    End If
                Case Else
                    ScriviMessaggio(String.Format(Messaggio(214), SetBagBarcode), lblMessaggio, ArcaColor.errore) 'Stato: {0}. Premere un tasto per ripetere, ESC per uscire.
                    AttendiTasto(, Me)
                    Select Case tastoPremuto
                        Case 27
                            Exit Function
                        Case Else
                            GoTo BagSwitchesStatus2
                    End Select
            End Select
        Catch
        End Try

getBarcode:
        SetBagBarcode = cmdComandoSingolo("T,1,a,2", 4, 5000)
        Try
            If SetBagBarcode <> "OK" Then
                Select Case SetBagBarcode
                    Case Else
                        ScriviMessaggio(String.Format(Messaggio(214), comandoSingoloRisposta(5), SetBagBarcode), lblMessaggio, ArcaColor.errore)
                        AttendiTasto(, Me)
                        Select Case tastoPremuto
                            Case 27
                                Exit Function
                            Case Else
                                GoTo getBarcode
                        End Select
                End Select
            End If
            If comandoSingoloRisposta(5) <> myMacchina.Serials.Bag Then
                ScriviMessaggio(Messaggio(256), lblMessaggio, ArcaColor.errore)
                AttendiTasto()
            End If
        Catch
        End Try
        ScriviMessaggio(Messaggio(42), lblMessaggio) 'Chiudere il trasporto inferiore, chiudere la cassaforte ed attendere la fine del recovery. Premere un tasto per continuare
        AttendiTasto(, Me)
        AttendiReset()
        SetBagBarcode = "OK"
    End Function

    Sub ComboNascondi()
        cboCliente.Visible = False
        cboIdProdotto.Visible = False
    End Sub

    Sub ComboMostra()
        cboCliente.Visible = True
        cboIdProdotto.Visible = True
    End Sub

    Function ValueCode(ByVal valueCodeIn As String) As String
        valueCodeIn = Mid(valueCodeIn, 3, 1)
        ValueCode = 0
        Select Case valueCodeIn
            Case "A"
                ValueCode = 1
            Case "B"
                ValueCode = 2
            Case "C"
                ValueCode = 5
            Case "D"
                ValueCode = 10
            Case "E"
                ValueCode = 20
            Case "F"
                ValueCode = 25
            Case "G"
                ValueCode = 50
            Case "H"
                ValueCode = 100
            Case "I"
                ValueCode = 200
            Case "J"
                ValueCode = 500
            Case "K"
                ValueCode = 1000
            Case "L"
                ValueCode = 2000
            Case "M"
                ValueCode = 5000
            Case "N"
                ValueCode = 10000
            Case "O"
                ValueCode = 20000
            Case "P"
                ValueCode = 50000
            Case "Q"
                ValueCode = 100000
            Case "R"
                ValueCode = 250
            Case "S"
                ValueCode = 200000
            Case "T"
                ValueCode = 250000
            Case "U"
                ValueCode = 500000
            Case "V"
                ValueCode = 1000000
            Case "W"
                ValueCode = 2000000
            Case "X"
                ValueCode = 5000000
            Case "Y"
                ValueCode = 10000000
            Case "*"
                ValueCode = "Multi"
            Case Else
                ValueCode = ""
        End Select
    End Function


    Private Sub cmdUtility_Click(sender As Object, e As EventArgs)
        cmdComandoSingolo(InputBox("Command to send", "Single command"))
    End Sub

    Private Sub mnuComParameter_Click(sender As Object, e As EventArgs) Handles mnuConnect.Click
        frmConnection.Show()
        Me.Enabled = False
    End Sub

    Private Sub DisconnectToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DisconnectToolStripMenuItem.Click
        ChiudiConnessione()
    End Sub

    Private Sub msnuComScopeShow_Click(sender As Object, e As EventArgs) Handles msnuComScopeShow.Click
        frmComScope.Show()
    End Sub

    Private Sub cboCasNum_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCasNum.SelectedIndexChanged
        For i = 0 To cboCasNum.Items.Count - 1
            If cboCasNum.Items(i) = cboCasNum.Text Then
                myMacchina.Test.CassetteNumber = cboCasNum.Text
                Exit For
            End If
        Next
    End Sub

    Private Sub mnuComScopeHide_Click(sender As Object, e As EventArgs) Handles mnuComScopeHide.Click
        frmComScope.Hide()
    End Sub

    Private Sub txtSerialA_TextChanged(sender As Object, e As EventArgs) Handles txtSerialA.TextChanged

    End Sub

    Private Sub mnuOpen_Click(sender As Object, e As EventArgs) Handles mnuOpen.Click
        Me.Enabled = False
        frmOpen.Visible = True
    End Sub

    Private Sub trvErrorLog_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles trvErrorLog.AfterSelect

    End Sub

    Private Sub mnuClose_Click(sender As Object, e As EventArgs) Handles mnuClose.Click
        Me.Enabled = False
        frmClose.Visible = True
    End Sub

    Private Sub btnEnd_Click(sender As Object, e As EventArgs) Handles btnEnd.Click
        btnEnd.Visible = False
        pulsantePremuto = True
    End Sub

    Private Sub grpExtendedStatus_Enter(sender As Object, e As EventArgs) Handles grpExtendedStatus.Enter

    End Sub

    Function AbilitaCashData(ByVal cassetto As String, Optional abilita As Boolean = True) As Boolean
        Select Case cassetto
            Case "A"
                lblCasA.Visible = True
                lblCasAFree.Visible = True
                txtCasABn.Visible = True
                txtCasADen.Visible = True
                txtCasAVal.Visible = True
                prbCasA.Visible = True
                lblCasAEnable.Visible = True
                lblCasAStatus.Visible = True
            Case "B"
                lblCasB.Visible = True
                lblCasBFree.Visible = True
                txtCasBBn.Visible = True
                txtCasBDen.Visible = True
                txtCasBVal.Visible = True
                prbCasB.Visible = True
                lblCasBEnable.Visible = True
                lblCasBStatus.Visible = True
            Case "C"
                lblCasC.Visible = True
                lblCasCFree.Visible = True
                txtCasCBn.Visible = True
                txtCasCDen.Visible = True
                txtCasCVal.Visible = True
                prbCasC.Visible = True
                lblCasCEnable.Visible = True
                lblCasCStatus.Visible = True
            Case "D"
                lblCasD.Visible = True
                lblCasDFree.Visible = True
                txtCasDBn.Visible = True
                txtCasDDen.Visible = True
                txtCasDVal.Visible = True
                prbCasD.Visible = True
                lblCasDEnable.Visible = True
                lblCasDStatus.Visible = True
            Case "E"
                lblCasE.Visible = True
                lblCasEFree.Visible = True
                txtCasEBn.Visible = True
                txtCasEDen.Visible = True
                txtCasEVal.Visible = True
                prbCasE.Visible = True
                lblCasEEnable.Visible = True
                lblCasEStatus.Visible = True
            Case "F"
                lblCasF.Visible = True
                lblCasFFree.Visible = True
                txtCasFBn.Visible = True
                txtCasFDen.Visible = True
                txtCasFVal.Visible = True
                prbCasF.Visible = True
                lblCasFEnable.Visible = True
                lblCasFStatus.Visible = True
            Case "G"
                lblCasG.Visible = True
                lblCasGFree.Visible = True
                txtCasGBn.Visible = True
                txtCasGDen.Visible = True
                txtCasGVal.Visible = True
                prbCasG.Visible = True
                lblCasGEnable.Visible = True
                lblCasGStatus.Visible = True
            Case "H"
                lblCasH.Visible = True
                lblCasHFree.Visible = True
                txtCasHBn.Visible = True
                txtCasHDen.Visible = True
                txtCasHVal.Visible = True
                prbCasH.Visible = True
                lblCasHEnable.Visible = True
                lblCasHStatus.Visible = True
            Case "I"
                lblCasI.Visible = True
                lblCasIFree.Visible = True
                txtCasIBn.Visible = True
                txtCasIDen.Visible = True
                txtCasIVal.Visible = True
                prbCasI.Visible = True
                lblCasIEnable.Visible = True
                lblCasIStatus.Visible = True
            Case "J"
                lblCasJ.Visible = True
                lblCasJFree.Visible = True
                txtCasJBn.Visible = True
                txtCasJDen.Visible = True
                txtCasJVal.Visible = True
                prbCasJ.Visible = True
                lblCasJEnable.Visible = True
                lblCasJStatus.Visible = True
            Case "K"
                lblCasK.Visible = True
                lblCasKFree.Visible = True
                txtCasKBn.Visible = True
                txtCasKDen.Visible = True
                txtCasKVal.Visible = True
                prbCasK.Visible = True
                lblCasKEnable.Visible = True
                lblCasKStatus.Visible = True
            Case "L"
                lblCasL.Visible = True
                lblCasLFree.Visible = True
                txtCasLBn.Visible = True
                txtCasLDen.Visible = True
                txtCasLVal.Visible = True
                prbCasL.Visible = True
                lblCasLEnable.Visible = True
                lblCasLStatus.Visible = True
        End Select
        grpCashData.Height = grpCashData.Height + 25
        AbilitaCashData = True
    End Function

    Sub GetDetailOfUnitClassificationOfNotes()
        Dim frmUnfitDetail As NotesClassification = New NotesClassification()
        frmUnfitDetail.Show()

    End Sub

    Private Sub GetDetailsOfNotesClassToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GetDetailsOfNotesClassToolStripMenuItem.Click
        ApriConnessione()
        GetDetailOfUnitClassificationOfNotes()
    End Sub

    Private Sub mnuExtStatus_Click(sender As Object, e As EventArgs) Handles mnuExtStatus.Click

        CmdExtendedStatus()
        cmdComandoSingolo("T,1,0,88")
        myMacchina.Status.Reply = comandoSingoloRisposta(4)
        myMacchina.Status.ProtocolCasseteNum = comandoSingoloRisposta(9)
        myMacchina.Status.ProtocoloDepositNum = comandoSingoloRisposta(11)

        Dim frmExtendedStatus As New ExtendedStatusForm()
        frmExtendedStatus.ShowDialog()
    End Sub

    Private Sub mnuDeposit_Click(sender As Object, e As EventArgs) Handles mnuDeposit.Click
        cmdComandoSingolo("D,1," & side & ",0,0000")
    End Sub

    Private Sub ErrorLOGToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ErrorLOGToolStripMenuItem.Click
        Dim frmErrorLog As New ErrorLogForm()
        frmErrorLog.ShowDialog()
    End Sub

    Private Sub ErrorLogAnalisysToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ErrorLogAnalisysToolStripMenuItem.Click
        AnalisiErrorLog()
    End Sub


    'Private Sub myTimer_Tick(sender As Object, e As EventArgs) Handles myTimer.Tick
    'myTimer.Interval = 1000

    'Select Case stato
    '    Case "ERRORE"
    '        If Me.BackColor = ArcaColor.sfondo Then
    '            Me.BackColor = ArcaColor.errore
    '        Else
    'Me.BackColor = ArcaColor.sfondo
    '        End If
    '    Case "ATTESA"
    '        If Me.BackColor = ArcaColor.sfondo Then
    '            Me.BackColor = ArcaColor.attesa
    '        Else
    '            Me.BackColor = ArcaColor.sfondo
    '        End If
    'End Select
    'End Sub


    Private Sub Combo_Id_prodotto_KeyUp(sender As Object, e As KeyEventArgs) Handles cboIdProdotto.KeyUp
        If Len(cboIdProdotto.Text) = 5 Then
            For i = 0 To cboIdProdotto.Items.Count - 1
                If cboIdProdotto.Text = cboIdProdotto.Items.Item(i) Then
                    cboIdProdotto.SelectedIndex = i
                    cboCliente.Focus()
                End If
            Next
        End If
    End Sub

    Private Sub txtMatricola_KeyUp(sender As Object, e As KeyEventArgs) Handles txtMatricola.KeyUp
        Dim seriale As String = txtMatricola.Text
        If e.KeyCode = 13 Then
            formatoSeriale = False
            If Len(txtMatricola.Text) = 12 Then
                formatoSeriale = True
                'If Mid(seriale, 6, 1) = "0" Or Mid(seriale, 6, 1) = "A" Then
                '    formatoSeriale = True
                '    'TO BE guardare controllare tutte le etichette 
                '    'Select Case myMacchina.Info.Categoria

                '    '    Case "CM18"
                '    '        If Mid(seriale, 1, 4) = "CM18" Or Mid(seriale, 1, 4) = "CP18" Then
                '    '            formatoSeriale = True
                '    '        Else
                '    '            ScriviMessaggio(Messaggio(245) & Chr(13) & Messaggio(247), lblMessaggio, ArcaColor.errore)
                '    '        End If
                '    '    Case "CM18T"
                '    '        If Mid(seriale, 1, 5) = "CM18T" Or Mid(seriale, 1, 4) = "CP18T" Then
                '    '            formatoSeriale = True
                '    '        Else
                '    '            ScriviMessaggio(Messaggio(245) & Chr(13) & Messaggio(248), lblMessaggio, ArcaColor.errore)
                '    '        End If
                '    '    Case "CM18B"
                '    '        If Mid(seriale, 1, 5) = "CM18B" Or Mid(seriale, 1, 4) = "CP18B" Then
                '    '            formatoSeriale = True
                '    '        Else
                '    '            ScriviMessaggio(Messaggio(245) & Chr(13) & Messaggio(249), lblMessaggio, ArcaColor.errore)
                '    '        End If
                '    '    Case "CM18SOLO"
                '    '        If Mid(seriale, 1, 5) = "18SOL" Then
                '    '            formatoSeriale = True
                '    '        Else
                '    '            ScriviMessaggio(Messaggio(245) & Chr(13) & Messaggio(250), lblMessaggio, ArcaColor.errore)
                '    '        End If
                '    '    Case "CM18SOLOT"
                '    '        If Mid(seriale, 1, 5) = "18SOT" Then
                '    '            formatoSeriale = True
                '    '        Else
                '    '            ScriviMessaggio(Messaggio(245) & Chr(13) & Messaggio(251), lblMessaggio, ArcaColor.errore)
                '    '        End If
                '    '    Case "CM18EVO"
                '    '        If Mid(seriale, 1, 4) = "18EV" Then
                '    '            formatoSeriale = True
                '    '        Else
                '    '            ScriviMessaggio(Messaggio(245) & Chr(13) & Messaggio(252), lblMessaggio, ArcaColor.errore)
                '    '        End If
                '    '    Case "CM18EVOT"
                '    '        If Mid(seriale, 1, 4) = "18ET" Then
                '    '            formatoSeriale = True
                '    '        Else
                '    '            ScriviMessaggio(Messaggio(245) & Chr(13) & Messaggio(253), lblMessaggio, ArcaColor.errore)
                '    '        End If
                '    '    Case "CM20"
                '    '        If Mid(seriale, 1, 5) = "CM200" Or Mid(seriale, 1, 5) = "CP200" Then
                '    '            formatoSeriale = True
                '    '        Else
                '    '            ScriviMessaggio(Messaggio(245) & Chr(13) & Messaggio(254), lblMessaggio, ArcaColor.errore)
                '    '        End If
                '    '    Case "CM20S"
                '    '        If Mid(seriale, 1, 5) = "CM20S" Or Mid(seriale, 1, 5) = "CP20S" Or Mid(seriale, 1, 5) = "CM200" Then
                '    '            formatoSeriale = True
                '    '        Else
                '    '            ScriviMessaggio(Messaggio(245) & Chr(13) & Messaggio(254), lblMessaggio, ArcaColor.errore)
                '    '        End If
                '    '    Case "CM20T"
                '    '        If Mid(seriale, 1, 5) = "CM20T" Or Mid(seriale, 1, 5) = "CP20T" Or Mid(seriale, 1, 5) = "CM200" Then
                '    '            formatoSeriale = True
                '    '        Else
                '    '            ScriviMessaggio(Messaggio(245) & Chr(13) & Messaggio(254), lblMessaggio, ArcaColor.errore)
                '    '        End If
                '    '    Case "OM61"
                '    '        If Mid(seriale, 1, 5) = "OM61" Then
                '    '            formatoSeriale = True
                '    '        Else
                '    '            ScriviMessaggio(Messaggio(245) & Chr(13) & Messaggio(255), lblMessaggio, ArcaColor.errore)
                '    '        End If
                '    'End Select
                'Else
                '    ScriviMessaggio(Messaggio(245) & Chr(13) & String.Format(Messaggio(246), Mid(seriale, 6, 1)), lblMessaggio, ArcaColor.errore)
                'End If
            End If
        End If

        'Log.macchinaMatricola = seriale
    End Sub

    Private Sub txtBagBarcode_KeyUp(sender As Object, e As KeyEventArgs) Handles txtBagBarcode.KeyUp
        Tasto = e.KeyCode
        Dim seriale As String = txtBagBarcode.Text
        If e.KeyCode = 13 Then
            formatoSeriale = False
            If Len(txtBagBarcode.Text) = 12 Then
                For i = 1 To 12
                    If Asc(Mid(seriale, i, 1)) < 48 Or Asc(Mid(seriale, i, 1)) > 57 Then
                        ScriviMessaggio("Barcode errato, reinserire il codice corretto", lblMessaggio)
                        txtBagBarcode.Text = ""
                        txtBagBarcode.Focus()
                    End If
                Next
            End If
        End If
        'Log.barcodeBag = seriale

    End Sub

    Private Sub frmPincipale_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        End
    End Sub

    Private Sub cboCasNum_KeyUp(sender As Object, e As KeyEventArgs) Handles cboCasNum.KeyUp
        If e.KeyCode = 13 Then
            numeroCassetti = cboCasNum.Text
        End If
    End Sub

    Private Sub lblSerial_Click(sender As Object, e As EventArgs) Handles lblSerialA.Click,
        lblSerialB.Click, lblSerialC.Click, lblSerialD.Click, lblSerialE.Click, lblSerialF.Click,
        lblSerialG.Click, lblSerialH.Click, lblSerialI.Click, lblSerialJ.Click, lblSerialK.Click, lblSerialL.Click

        If MsgBox("Vuoi sostituire il serial number?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            Select Case sender.name
                Case lblSerialA.Name
                    txtSerialA.Text = ""
                    txtSerialA.Enabled = True
                    txtSerialA.Focus()
                Case lblSerialB.Name
                    txtSerialB.Text = ""
                    txtSerialB.Enabled = True
                    txtSerialB.Focus()
                Case lblSerialC.Name
                    txtSerialC.Text = ""
                    txtSerialC.Enabled = True
                    txtSerialC.Focus()
                Case lblSerialD.Name
                    txtSerialD.Text = ""
                    txtSerialD.Enabled = True
                    txtSerialD.Focus()
                Case lblSerialE.Name
                    txtSerialE.Text = ""
                    txtSerialE.Enabled = True
                    txtSerialE.Focus()
                Case lblSerialF.Name
                    txtSerialF.Text = ""
                    txtSerialF.Enabled = True
                    txtSerialF.Focus()
                Case lblSerialG.Name
                    txtSerialG.Text = ""
                    txtSerialG.Enabled = True
                    txtSerialG.Focus()
                Case lblSerialH.Name
                    txtSerialH.Text = ""
                    txtSerialH.Enabled = True
                    txtSerialH.Focus()
                Case lblSerialI.Name
                    txtSerialI.Text = ""
                    txtSerialI.Enabled = True
                    txtSerialI.Focus()
                Case lblSerialJ.Name
                    txtSerialJ.Text = ""
                    txtSerialJ.Enabled = True
                    txtSerialJ.Focus()
                Case lblSerialK.Name
                    txtSerialK.Text = ""
                    txtSerialK.Enabled = True
                    txtSerialK.Focus()
                Case lblSerialL.Name
                    txtSerialL.Text = ""
                    txtSerialL.Enabled = True
                    txtSerialL.Focus()
            End Select
            sender.visible = False
        End If
    End Sub

    Private Sub trvErrorLog_DoubleClick(sender As Object, e As EventArgs) Handles trvErrorLog.DoubleClick
        Try
            Dim replyCode As New INIFile(localFolder & "\TheRCs" & myMacchina.Info.replyCodeFile & ".ini")
            Dim messaggio As String = ""
            Dim indice As Integer = CInt(trvErrorLog.SelectedNode.Name)
            With lstErrorLog(indice)
                messaggio &= "Record Number: " & Format(.line, "0000") & Chr(13) & Chr(13)
                messaggio &= "Reply Code: " & .replyCode & " - " & replyCode.ReadValue("RC", .replyCode) & Chr(13) & Chr(13)
                messaggio &= "Life In: " & .life & Chr(13)
                messaggio &= "Operator Side: " & .operatorSide & Chr(13)
                messaggio &= "Transaction ID: " & .transaction & Chr(13) & Chr(13)
                messaggio &= "Feeder Status (F1): " & .statusFeed & " - " & replyCode.ReadValue("Feeder", .statusFeed) & Chr(13)
                messaggio &= "Controller Status (F2): " & .statusController & " - " & replyCode.ReadValue("Controller", .statusController) & Chr(13)
                messaggio &= "Reader Status (F3): " & .statusReader & " - " & replyCode.ReadValue("Reader", .statusReader) & Chr(13)
                messaggio &= "Safe Status (F4): " & .statusSafe & " - " & replyCode.ReadValue("Safe", .statusSafe) & Chr(13) & Chr(13)
                messaggio &= "Cassetto A Status (FA): " & .statusCassA & " - " & replyCode.ReadValue("Cassettes", .statusCassA) & Chr(13)
                messaggio &= "Cassetto B Status (FB): " & .statusCassB & " - " & replyCode.ReadValue("Cassettes", .statusCassB) & Chr(13)
                messaggio &= "Cassetto C Status (FC): " & .statusCassC & " - " & replyCode.ReadValue("Cassettes", .statusCassC) & Chr(13)
                messaggio &= "Cassetto D Status (FD): " & .statusCassD & " - " & replyCode.ReadValue("Cassettes", .statusCassD) & Chr(13)
                messaggio &= "Cassetto E Status (FE): " & .statusCassE & " - " & replyCode.ReadValue("Cassettes", .statusCassE) & Chr(13)
                messaggio &= "Cassetto F Status (FF): " & .statusCassF & " - " & replyCode.ReadValue("Cassettes", .statusCassF) & Chr(13)
                messaggio &= "Cassetto G Status (FG): " & .statusCassG & " - " & replyCode.ReadValue("Cassettes", .statusCassG) & Chr(13)
                messaggio &= "Cassetto H Status (FH): " & .statusCassH & " - " & replyCode.ReadValue("Cassettes", .statusCassH) & Chr(13)
                messaggio &= "Cassetto I Status (FI): " & .statusCassI & " - " & replyCode.ReadValue("Cassettes", .statusCassI) & Chr(13)
                messaggio &= "Cassetto J Status (FJ): " & .statusCassJ & " - " & replyCode.ReadValue("Cassettes", .statusCassJ) & Chr(13)
                messaggio &= "Cassetto K Status (FK): " & .statusCassK & " - " & replyCode.ReadValue("Cassettes", .statusCassK) & Chr(13)
                messaggio &= "Cassetto L Status (FL): " & .statusCassL & " - " & replyCode.ReadValue("Cassettes", .statusCassL) & Chr(13)
                messaggio &= "Cassetto M Status (FM): " & .statusCassM & " - " & replyCode.ReadValue("Cassettes", .statusCassM) & Chr(13)
                messaggio &= "Cassetto N Status (FN): " & .statusCassN & " - " & replyCode.ReadValue("Cassettes", .statusCassN) & Chr(13)
                messaggio &= "Cassetto O Status (FO): " & .statusCassO & " - " & replyCode.ReadValue("Cassettes", .statusCassO) & Chr(13)
                messaggio &= "Cassetto P Status (FP): " & .statusCassP & " - " & replyCode.ReadValue("Cassettes", .statusCassP) & Chr(13)
            End With
            MsgBox(messaggio)
        Catch
        End Try
    End Sub

    Private Sub grpExtendedStatus_Click(sender As Object, e As EventArgs) Handles grpExtendedStatus.Click
        CMExtendedStatus()
        AggiornaStati()
    End Sub
End Class
