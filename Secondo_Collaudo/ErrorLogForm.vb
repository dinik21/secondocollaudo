﻿Public Class ErrorLogForm
    Dim iToLog As Integer
    Dim myErrorLog As List(Of SErrorLog)



    Private Sub ErrorLogForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            dgErrorLog.Rows.Clear()
            cmdComandoSingolo("H,1,UUUU")
            myErrorLog = New List(Of SErrorLog)
            iToLog = comandoSingoloRisposta(4)
            TraduciErrorLog(comandoSingoloRisposta)
            'dgErrorLog.Rows.Add(comandoSingoloRisposta(5), comandoSingoloRisposta(6), comandoSingoloRisposta(7), comandoSingoloRisposta(8),
            '                        comandoSingoloRisposta(9), comandoSingoloRisposta(10), comandoSingoloRisposta(11), comandoSingoloRisposta(12),
            '                        comandoSingoloRisposta(13), comandoSingoloRisposta(14), comandoSingoloRisposta(15), comandoSingoloRisposta(16),
            '                        comandoSingoloRisposta(17), comandoSingoloRisposta(18), comandoSingoloRisposta(19), comandoSingoloRisposta(20),
            '                        comandoSingoloRisposta(21), comandoSingoloRisposta(22), comandoSingoloRisposta(23), comandoSingoloRisposta(24),
            '                        comandoSingoloRisposta(25), comandoSingoloRisposta(26), comandoSingoloRisposta(27), comandoSingoloRisposta(28))
            'dgErrorLog.Rows(0).HeaderCell.Value = 1
            txtTo.Text = iToLog
            optGetFromTo.Checked = True
            If txtTo.Text > 5 Then
                txtFrom.Text = iToLog - 5
            End If
        Catch
        End Try
    End Sub

    Private Sub btnGetErrorLog_Click(sender As Object, e As EventArgs) Handles btnGetErrorLog.Click
        Try
            Dim iFromLog As Integer = 1
            myErrorLog = New List(Of SErrorLog)
            dgErrorLog.Rows.Clear()
            If optGetFromTo.Checked = True Then
                iFromLog = txtFrom.Text
                iToLog = txtTo.Text
            End If
            If optGetAll.Checked = True Then iFromLog = 1

            For i = iFromLog To iToLog
                cmdComandoSingolo("H,1," & i.ToString("0000"))
                TraduciErrorLog(comandoSingoloRisposta)
                'dgErrorLog.Rows.Add(comandoSingoloRisposta(5), comandoSingoloRisposta(6), comandoSingoloRisposta(7), comandoSingoloRisposta(8),
                '                    comandoSingoloRisposta(9), comandoSingoloRisposta(10), comandoSingoloRisposta(11).Substring(1, 2), comandoSingoloRisposta(12).Substring(1, 2),
                '                    comandoSingoloRisposta(13).Substring(1, 2), comandoSingoloRisposta(14).Substring(1, 2), comandoSingoloRisposta(15).Substring(1, 2), comandoSingoloRisposta(16).Substring(1, 2),
                '                    comandoSingoloRisposta(17).Substring(1, 2), comandoSingoloRisposta(18).Substring(1, 2), comandoSingoloRisposta(19).Substring(1, 2), comandoSingoloRisposta(20).Substring(1, 2),
                '                    comandoSingoloRisposta(21).Substring(1, 2), comandoSingoloRisposta(22).Substring(1, 2), comandoSingoloRisposta(23).Substring(1, 2), comandoSingoloRisposta(24).Substring(1, 2),
                '                    comandoSingoloRisposta(25).Substring(1, 2), comandoSingoloRisposta(26).Substring(1, 2), comandoSingoloRisposta(27).Substring(1, 2), comandoSingoloRisposta(28).Substring(1, 2))
                'dgErrorLog.Rows(dgErrorLog.RowCount - 1).HeaderCell.Value = i
            Next
        Catch
        End Try
    End Sub

    Sub TraduciErrorLog(risposta As String())
        Try
            Dim tempErrorLog As SErrorLog = New SErrorLog

            With tempErrorLog
                .replyCode = risposta(3)
                .line = risposta(4)
                If .line > 0 And .line < 8193 Then
                    .life = risposta(5)
                Else

                End If
                Select Case risposta(6) 'l
                    Case "R", "L"
                    'user
                    Case "O", "o"
                    'Power off
                    Case "+"
                    'Deposit
                    Case "-"
                    'Withdrawal
                    Case "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "M", "N", "P", "Q", "S", "T"
                        'Module identificator
                End Select
                Select Case risposta(7) 'op
                    Case "D", "W", "I"

                    Case "O", "o"

                End Select


            End With
            'dgErrorLog.Rows.Add(risposta(4), risposta(5), risposta(6), risposta(7), risposta(8), risposta(9).Substring(1, risposta(9).Length - 1), risposta(10).Substring(1, risposta(10).Length - 1),
            '                     risposta(11).Substring(1, risposta(11).Length - 1), risposta(12).Substring(1, risposta(12).Length - 1), risposta(13).Substring(1, risposta(13).Length - 1),
            '                     risposta(14).Substring(1, risposta(14).Length - 1), risposta(15).Substring(1, risposta(15).Length - 1), risposta(16).Substring(1, risposta(16).Length - 1),
            '                     risposta(17).Substring(1, risposta(17).Length - 1), risposta(18).Substring(1, risposta(18).Length - 1), risposta(19).Substring(1, risposta(19).Length - 1),
            '                     risposta(20).Substring(1, risposta(20).Length - 1), risposta(21).Substring(1, risposta(21).Length - 1), risposta(22).Substring(1, risposta(22).Length - 1),
            '                     risposta(23).Substring(1, risposta(23).Length - 1), risposta(24).Substring(1, risposta(24).Length - 1), risposta(25).Substring(1, risposta(25).Length - 1),
            '                     risposta(26).Substring(1, risposta(26).Length - 1), risposta(27).Substring(1, risposta(27).Length - 1), risposta(28).Substring(1, risposta(28).Length - 1))

            dgErrorLog.Rows.Add(risposta(4), risposta(5), risposta(6), risposta(7), risposta(8), risposta(9), risposta(10), risposta(11), risposta(12), risposta(13), risposta(14),
                                risposta(15), risposta(16), risposta(17), risposta(18), risposta(19), risposta(20), risposta(21), risposta(22), risposta(23), risposta(24),
                                risposta(25), risposta(26), risposta(27), risposta(28))
        Catch
        End Try
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class