; File da includere
	!include "MUI2.nsh"

; Dati applicazione
	!define APPNAME "Secondo Collaudo"
	Name "${APPNAME}"
	!define COMPANYNAME "ARCA Techologies"
	!define VERSIONMAJOR 1
	!define VERSIONMINOR 1
	!define VERSIONBUILD 8


	
; Impostazione cartelle di sorgente, output ed installazione
	!define INSTFOLDER "C:\ARCA\Secondo Collaudo"
	!define SOURCEFOLDER "D:\Progetti VBNET\CM18\SecondoCollaudo\Secondo_Collaudo\bin\Debug"
	OutFile "${APPNAME} V${VERSIONMAJOR}.${VERSIONMINOR}.${VERSIONBUILD}.exe"
	
; Impostazione Icona installazione/disinstallazione
	!define MUI_ICON "D:\Progetti VBNET\NSISimages\icona.ico"
	!define MUI_UNICON "D:\Progetti VBNET\NSISimages\icona.ico"
	
	!define MUI_ABORTWARNING
	
; Impostazione header (bmp 150x57 24bit) GIMP:EsportaComeBmp->'NonScrivereInfoColore'+24bitR8G8B8
	!define MUI_HEADERIMAGE_BITMAP "D:\Progetti VBNET\NSISimages\headerlogo.bmp"
	!define MUI_HEADERIMAGE
	
; Impostazione pagina di benvenuto (testo ed immagine bmp 164x314 24bit) GIMP:EsportaComeBmp->'NonScrivereInfoColore'+24bitR8G8B8
	!define txtMessageLine1 "Sw guidato per il test funzionale, o 'secondo collaudo'."
	!define txtMessageLine2 "E' necessario installare anche i driver USB ARCA"
	!define txtMessageLine3 "se si intende connettere il prodotto tramite USB"
	!define MUI_WELCOMEPAGE_TITLE "Installazione Secondo Collaudo "
	!define MUI_WELCOMEPAGE_TEXT "${txtMessageLine1}$\r$\n$\r$\n${txtMessageLine2}$\r$\n$\r$\n${txtMessageLine3}"
	!define MUI_WELCOMEFINISHPAGE_BITMAP "D:\Progetti VBNET\NSISimages\scWelcome.bmp"
	!insertmacro MUI_PAGE_WELCOME
	
; Impostazione pagina della licenza
	!insertmacro MUI_PAGE_LICENSE "D:\Progetti VBNET\NSISimages\ArcaLicense.txt"
	
; Personalizzazione cartella d'installazione
	;!insertmacro MUI_PAGE_DIRECTORY
	
	
	!insertmacro MUI_PAGE_COMPONENTS
	
; Visualizzazione avanzamento installazione file
	!insertmacro MUI_PAGE_INSTFILES
	
; Impostazione conferma di disinstallazione
	!insertmacro MUI_UNPAGE_CONFIRM
	
; Visualizzazione avanzamento disinstallazione file
	!insertmacro MUI_UNPAGE_INSTFILES
	
; Impostazione linguaggio d'installazione
	!insertmacro MUI_LANGUAGE "English"
	
; Visualizzazione pagin installazione terminata
	;!define MUI_FINISHPAGE_TITLE "Installazione DLL per il visualizzatore del DBFW"
	;!define MUI_FINISHPAGE_TEXT "Installazione DLL terminata correttamente"
	;!insertmacro MUI_PAGE_FINISH
	

	
Section "Secondo Collaudo"
	SetOutPath "${INSTFOLDER}"
	File "${SOURCEFOLDER}\Secondo_Collaudo.exe"
	File "${SOURCEFOLDER}\setup.ini"
	File "${SOURCEFOLDER}\italiano.ini"
	File "${SOURCEFOLDER}\TheRCsCM14_8.ini"
	File "${SOURCEFOLDER}\TheRCsCM18.ini"
	File "${SOURCEFOLDER}\TheRCsCM18B.ini"
	File "${SOURCEFOLDER}\TheRCsCM18SOLO.ini"
	File "${SOURCEFOLDER}\TheRCsCM18T.ini"
	File "${SOURCEFOLDER}\TheRCsCM20.ini"
	File "${SOURCEFOLDER}\TheRCsCM24_8.ini"
	File "${SOURCEFOLDER}\TheRCsCM24B.ini"
	File "${SOURCEFOLDER}\TheRCsEMU_T.ini"
	File "${SOURCEFOLDER}\TheRCs.ini"
	File "${SOURCEFOLDER}\CMCommand.dll"
	File "${SOURCEFOLDER}\CMLink.dll"
	File "${SOURCEFOLDER}\CMTrace.dll"
	File "D:\Progetti VBNET\XmlDll\bin\x86\Debug\XmlDll.dll"
	WriteUninstaller "${INSTFOLDER}\Uninstall.exe"
	
	CreateShortCut "$DESKTOP\${APPNAME}.lnk" "${INSTFOLDER}\Secondo_Collaudo.exe" ""
	
SectionEnd



Section "Uninstall"
	RMDir /r "${INSTFOLDER}\*.*"
	RMDir "${INSTFOLDER}"
	Delete "$DESKTOP\${APPNAME}.lnk"
SectionEnd