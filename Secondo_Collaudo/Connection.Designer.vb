﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConnection
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.optRS232 = New System.Windows.Forms.RadioButton()
        Me.optUSB = New System.Windows.Forms.RadioButton()
        Me.optLAN = New System.Windows.Forms.RadioButton()
        Me.grpRS232 = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cboComProtocol = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboComData = New System.Windows.Forms.ComboBox()
        Me.cboComBaudrate = New System.Windows.Forms.ComboBox()
        Me.cboComStop = New System.Windows.Forms.ComboBox()
        Me.cboComParity = New System.Windows.Forms.ComboBox()
        Me.cboComName = New System.Windows.Forms.ComboBox()
        Me.grpUSB = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cboUsbProtocol = New System.Windows.Forms.ComboBox()
        Me.grpLan = New System.Windows.Forms.GroupBox()
        Me.txtLanIp = New System.Windows.Forms.MaskedTextBox()
        Me.chkLanSsl = New System.Windows.Forms.CheckBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cboLanProtocol = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cboLanPort = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cmdOk = New System.Windows.Forms.Button()
        Me.cmdCancel = New System.Windows.Forms.Button()
        Me.grpRS232.SuspendLayout()
        Me.grpUSB.SuspendLayout()
        Me.grpLan.SuspendLayout()
        Me.SuspendLayout()
        '
        'optRS232
        '
        Me.optRS232.AutoSize = True
        Me.optRS232.Location = New System.Drawing.Point(50, 48)
        Me.optRS232.Name = "optRS232"
        Me.optRS232.Size = New System.Drawing.Size(58, 17)
        Me.optRS232.TabIndex = 0
        Me.optRS232.TabStop = True
        Me.optRS232.Text = "RS232"
        Me.optRS232.UseVisualStyleBackColor = True
        '
        'optUSB
        '
        Me.optUSB.AutoSize = True
        Me.optUSB.Location = New System.Drawing.Point(50, 137)
        Me.optUSB.Name = "optUSB"
        Me.optUSB.Size = New System.Drawing.Size(47, 17)
        Me.optUSB.TabIndex = 1
        Me.optUSB.TabStop = True
        Me.optUSB.Text = "USB"
        Me.optUSB.UseVisualStyleBackColor = True
        '
        'optLAN
        '
        Me.optLAN.AutoSize = True
        Me.optLAN.Location = New System.Drawing.Point(50, 223)
        Me.optLAN.Name = "optLAN"
        Me.optLAN.Size = New System.Drawing.Size(61, 17)
        Me.optLAN.TabIndex = 2
        Me.optLAN.TabStop = True
        Me.optLAN.Text = "TCP/IP"
        Me.optLAN.UseVisualStyleBackColor = True
        '
        'grpRS232
        '
        Me.grpRS232.Controls.Add(Me.Label6)
        Me.grpRS232.Controls.Add(Me.cboComProtocol)
        Me.grpRS232.Controls.Add(Me.Label5)
        Me.grpRS232.Controls.Add(Me.Label4)
        Me.grpRS232.Controls.Add(Me.Label3)
        Me.grpRS232.Controls.Add(Me.Label2)
        Me.grpRS232.Controls.Add(Me.Label1)
        Me.grpRS232.Controls.Add(Me.cboComData)
        Me.grpRS232.Controls.Add(Me.cboComBaudrate)
        Me.grpRS232.Controls.Add(Me.cboComStop)
        Me.grpRS232.Controls.Add(Me.cboComParity)
        Me.grpRS232.Controls.Add(Me.cboComName)
        Me.grpRS232.Enabled = False
        Me.grpRS232.Location = New System.Drawing.Point(125, 12)
        Me.grpRS232.Name = "grpRS232"
        Me.grpRS232.Size = New System.Drawing.Size(644, 72)
        Me.grpRS232.TabIndex = 3
        Me.grpRS232.TabStop = False
        Me.grpRS232.Text = "RS232 Configuration"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(522, 19)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(46, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Protocol"
        '
        'cboComProtocol
        '
        Me.cboComProtocol.FormattingEnabled = True
        Me.cboComProtocol.Location = New System.Drawing.Point(525, 35)
        Me.cboComProtocol.Name = "cboComProtocol"
        Me.cboComProtocol.Size = New System.Drawing.Size(98, 21)
        Me.cboComProtocol.TabIndex = 10
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(111, 19)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(50, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Baudrate"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(217, 19)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Stop bit"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(301, 19)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(33, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Parity"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(422, 19)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(49, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Data bits"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(82, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Available COMs"
        '
        'cboComData
        '
        Me.cboComData.FormattingEnabled = True
        Me.cboComData.Location = New System.Drawing.Point(425, 35)
        Me.cboComData.Name = "cboComData"
        Me.cboComData.Size = New System.Drawing.Size(86, 21)
        Me.cboComData.TabIndex = 4
        '
        'cboComBaudrate
        '
        Me.cboComBaudrate.FormattingEnabled = True
        Me.cboComBaudrate.Location = New System.Drawing.Point(114, 35)
        Me.cboComBaudrate.Name = "cboComBaudrate"
        Me.cboComBaudrate.Size = New System.Drawing.Size(81, 21)
        Me.cboComBaudrate.TabIndex = 3
        '
        'cboComStop
        '
        Me.cboComStop.FormattingEnabled = True
        Me.cboComStop.Location = New System.Drawing.Point(220, 35)
        Me.cboComStop.Name = "cboComStop"
        Me.cboComStop.Size = New System.Drawing.Size(61, 21)
        Me.cboComStop.TabIndex = 2
        '
        'cboComParity
        '
        Me.cboComParity.FormattingEnabled = True
        Me.cboComParity.Location = New System.Drawing.Point(304, 35)
        Me.cboComParity.Name = "cboComParity"
        Me.cboComParity.Size = New System.Drawing.Size(99, 21)
        Me.cboComParity.TabIndex = 1
        '
        'cboComName
        '
        Me.cboComName.FormattingEnabled = True
        Me.cboComName.Location = New System.Drawing.Point(9, 35)
        Me.cboComName.Name = "cboComName"
        Me.cboComName.Size = New System.Drawing.Size(79, 21)
        Me.cboComName.TabIndex = 0
        '
        'grpUSB
        '
        Me.grpUSB.Controls.Add(Me.Label7)
        Me.grpUSB.Controls.Add(Me.cboUsbProtocol)
        Me.grpUSB.Enabled = False
        Me.grpUSB.Location = New System.Drawing.Point(125, 99)
        Me.grpUSB.Name = "grpUSB"
        Me.grpUSB.Size = New System.Drawing.Size(642, 71)
        Me.grpUSB.TabIndex = 4
        Me.grpUSB.TabStop = False
        Me.grpUSB.Text = "USB Configuration"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 21)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(46, 13)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "Protocol"
        '
        'cboUsbProtocol
        '
        Me.cboUsbProtocol.FormattingEnabled = True
        Me.cboUsbProtocol.Location = New System.Drawing.Point(9, 37)
        Me.cboUsbProtocol.Name = "cboUsbProtocol"
        Me.cboUsbProtocol.Size = New System.Drawing.Size(124, 21)
        Me.cboUsbProtocol.TabIndex = 12
        '
        'grpLan
        '
        Me.grpLan.Controls.Add(Me.txtLanIp)
        Me.grpLan.Controls.Add(Me.chkLanSsl)
        Me.grpLan.Controls.Add(Me.Label10)
        Me.grpLan.Controls.Add(Me.cboLanProtocol)
        Me.grpLan.Controls.Add(Me.Label9)
        Me.grpLan.Controls.Add(Me.cboLanPort)
        Me.grpLan.Controls.Add(Me.Label8)
        Me.grpLan.Enabled = False
        Me.grpLan.Location = New System.Drawing.Point(125, 186)
        Me.grpLan.Name = "grpLan"
        Me.grpLan.Size = New System.Drawing.Size(642, 71)
        Me.grpLan.TabIndex = 14
        Me.grpLan.TabStop = False
        Me.grpLan.Text = "LAN Configuration"
        '
        'txtLanIp
        '
        Me.txtLanIp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLanIp.Location = New System.Drawing.Point(9, 36)
        Me.txtLanIp.Mask = "###,###,###,###"
        Me.txtLanIp.Name = "txtLanIp"
        Me.txtLanIp.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.txtLanIp.Size = New System.Drawing.Size(124, 22)
        Me.txtLanIp.TabIndex = 19
        '
        'chkLanSsl
        '
        Me.chkLanSsl.AutoSize = True
        Me.chkLanSsl.Location = New System.Drawing.Point(409, 38)
        Me.chkLanSsl.Name = "chkLanSsl"
        Me.chkLanSsl.Size = New System.Drawing.Size(46, 17)
        Me.chkLanSsl.TabIndex = 18
        Me.chkLanSsl.Text = "SSL"
        Me.chkLanSsl.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(266, 21)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(46, 13)
        Me.Label10.TabIndex = 17
        Me.Label10.Text = "Protocol"
        '
        'cboLanProtocol
        '
        Me.cboLanProtocol.FormattingEnabled = True
        Me.cboLanProtocol.Location = New System.Drawing.Point(269, 37)
        Me.cboLanProtocol.Name = "cboLanProtocol"
        Me.cboLanProtocol.Size = New System.Drawing.Size(124, 21)
        Me.cboLanProtocol.TabIndex = 16
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(136, 21)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(26, 13)
        Me.Label9.TabIndex = 15
        Me.Label9.Text = "Port"
        '
        'cboLanPort
        '
        Me.cboLanPort.FormattingEnabled = True
        Me.cboLanPort.Location = New System.Drawing.Point(139, 37)
        Me.cboLanPort.Name = "cboLanPort"
        Me.cboLanPort.Size = New System.Drawing.Size(124, 21)
        Me.cboLanPort.TabIndex = 14
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 21)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(58, 13)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "IP Address"
        '
        'cmdOk
        '
        Me.cmdOk.Location = New System.Drawing.Point(125, 283)
        Me.cmdOk.Name = "cmdOk"
        Me.cmdOk.Size = New System.Drawing.Size(75, 23)
        Me.cmdOk.TabIndex = 15
        Me.cmdOk.Text = "OK"
        Me.cmdOk.UseVisualStyleBackColor = True
        '
        'cmdCancel
        '
        Me.cmdCancel.Location = New System.Drawing.Point(212, 283)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 16
        Me.cmdCancel.Text = "Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'frmConnection
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(790, 318)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOk)
        Me.Controls.Add(Me.grpLan)
        Me.Controls.Add(Me.grpUSB)
        Me.Controls.Add(Me.grpRS232)
        Me.Controls.Add(Me.optLAN)
        Me.Controls.Add(Me.optUSB)
        Me.Controls.Add(Me.optRS232)
        Me.Name = "frmConnection"
        Me.Text = "Connection Parameters"
        Me.grpRS232.ResumeLayout(False)
        Me.grpRS232.PerformLayout()
        Me.grpUSB.ResumeLayout(False)
        Me.grpUSB.PerformLayout()
        Me.grpLan.ResumeLayout(False)
        Me.grpLan.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents optRS232 As RadioButton
    Friend WithEvents optUSB As RadioButton
    Friend WithEvents optLAN As RadioButton
    Friend WithEvents grpRS232 As GroupBox
    Friend WithEvents Label6 As Label
    Friend WithEvents cboComProtocol As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents cboComData As ComboBox
    Friend WithEvents cboComBaudrate As ComboBox
    Friend WithEvents cboComStop As ComboBox
    Friend WithEvents cboComParity As ComboBox
    Friend WithEvents cboComName As ComboBox
    Friend WithEvents grpUSB As GroupBox
    Friend WithEvents Label7 As Label
    Friend WithEvents cboUsbProtocol As ComboBox
    Friend WithEvents grpLan As GroupBox
    Friend WithEvents chkLanSsl As CheckBox
    Friend WithEvents Label10 As Label
    Friend WithEvents cboLanProtocol As ComboBox
    Friend WithEvents Label9 As Label
    Friend WithEvents cboLanPort As ComboBox
    Friend WithEvents Label8 As Label
    Friend WithEvents cmdOk As Button
    Friend WithEvents cmdCancel As Button
    Friend WithEvents txtLanIp As MaskedTextBox
End Class
