﻿'Imports SecondoCollaudo.frmPincipale

Public Class frmConnection
    Private Sub frmConnection_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Riempimento dei vari combobox
        For Each rsPort As String In My.Computer.Ports.SerialPortNames
            cboComName.Items.Add(rsPort)
        Next

        cboComBaudrate.Items.Add("9600")
        cboComBaudrate.Items.Add("19200")
        cboComBaudrate.Items.Add("38400")
        cboComBaudrate.Items.Add("57600")
        cboComBaudrate.Items.Add("115200")

        cboComStop.Items.Add("1")
        cboComStop.Items.Add("2")

        cboComParity.Items.Add("NONE")
        cboComParity.Items.Add("ODD")
        cboComParity.Items.Add("EVEN")

        cboComData.Items.Add("4")
        cboComData.Items.Add("5")
        cboComData.Items.Add("6")
        cboComData.Items.Add("7")
        cboComData.Items.Add("8")

        cboComProtocol.Items.Add("STANDARD")
        cboComProtocol.Items.Add("SIMPLIFIED")
        cboComProtocol.Items.Add("NONE")

        cboUsbProtocol.Items.Add("STANDARD")
        cboUsbProtocol.Items.Add("SIMPLIFIED")
        cboUsbProtocol.Items.Add("NONE")

        cboLanPort.Items.Add("8000")
        cboLanPort.Items.Add("8001")
        cboLanPort.Items.Add("8002")
        cboLanPort.Items.Add("8003")
        cboLanPort.Items.Add("8004")
        cboLanPort.Items.Add("8005")
        cboLanPort.Items.Add("8006")
        cboLanPort.Items.Add("8007")
        cboLanPort.Items.Add("8008")
        cboLanPort.Items.Add("8009")
        cboLanPort.Items.Add("8101")
        cboLanPort.Items.Add("8102")

        cboLanProtocol.Items.Add("SIMPLIFIED")
        cboLanProtocol.Items.Add("NONE")

        Select Case frmPrincipale.tipoConnessione
            Case "RS232"
                optRS232.Checked = True
            Case "USB"
                optUSB.Checked = True
            Case "LAN"
                optLAN.Checked = True
        End Select
        cboComName.Text = frmPrincipale.comPort
        cboComBaudrate.Text = frmPrincipale.comBaudRate
        cboComStop.Text = frmPrincipale.comStopbits
        cboComParity.Text = frmPrincipale.comParity
        cboComData.Text = frmPrincipale.comDataBits
        cboComProtocol.Text = frmPrincipale.comProtocol
        cboUsbProtocol.Text = frmPrincipale.usbProtocol
        txtLanIp.Text = frmPrincipale.lanIPAddress
        cboLanPort.Text = frmPrincipale.lanPort
        cboLanProtocol.Text = frmPrincipale.lanProtocol
        chkLanSsl.Checked = IIf(frmPrincipale.lanSSL = "N", False, True)
    End Sub

    Private Sub VisualizzaCampi(ByVal connessione As String)
        Select Case connessione
            Case "RS232"
                cboUsbProtocol.Enabled = False
                txtLanIp.Enabled = False
                cboLanPort.Enabled = False
                cboLanProtocol.Enabled = False
                chkLanSsl.Enabled = False

                cboComName.Enabled = True
                cboComBaudrate.Enabled = True
                cboComStop.Enabled = True
                cboComParity.Enabled = True
                cboComData.Enabled = True
                cboComProtocol.Enabled = True
            Case "USB"
                cboComName.Enabled = False
                cboComBaudrate.Enabled = False
                cboComStop.Enabled = False
                cboComParity.Enabled = False
                cboComData.Enabled = False
                cboComProtocol.Enabled = False
                txtLanIp.Enabled = False
                cboLanPort.Enabled = False
                cboLanProtocol.Enabled = False
                chkLanSsl.Enabled = False

                cboUsbProtocol.Enabled = True
            Case "LAN"
                cboComName.Enabled = False
                cboComBaudrate.Enabled = False
                cboComStop.Enabled = False
                cboComParity.Enabled = False
                cboComData.Enabled = False
                cboComProtocol.Enabled = False
                cboUsbProtocol.Enabled = False

                txtLanIp.Enabled = True
                cboLanPort.Enabled = True
                cboLanProtocol.Enabled = True
                chkLanSsl.Enabled = True
        End Select


    End Sub

    Private Sub cmdCancel_Click(sender As Object, e As EventArgs) Handles cmdCancel.Click
        Me.Close()
    End Sub

    Private Sub optRS232_CheckedChanged(sender As Object, e As EventArgs) Handles optRS232.CheckedChanged
        grpRS232.Enabled = optRS232.Checked
        If optRS232.Checked = True Then
            cboComName.Text = frmPrincipale.comPort
            cboComBaudrate.Text = frmPrincipale.comBaudRate
            cboComStop.Text = frmPrincipale.comStopbits
            cboComParity.Text = frmPrincipale.comParity
            cboComData.Text = frmPrincipale.comDataBits
            cboComProtocol.Text = frmPrincipale.comProtocol
            VisualizzaCampi("RS232")
        End If
    End Sub

    Private Sub optUSB_CheckedChanged(sender As Object, e As EventArgs) Handles optUSB.CheckedChanged
        grpUSB.Enabled = optUSB.Checked
        If optUSB.Checked = True Then
            cboUsbProtocol.Text = frmPrincipale.usbProtocol
            VisualizzaCampi("USB")
        End If
    End Sub

    Private Sub optLAN_CheckedChanged(sender As Object, e As EventArgs) Handles optLAN.CheckedChanged
        grpLan.Enabled = optLAN.Checked
        If optLAN.Checked = True Then
            txtLanIp.Text = frmPrincipale.lanIPAddress
            cboLanPort.Text = frmPrincipale.lanPort
            cboLanProtocol.Text = frmPrincipale.lanProtocol
            chkLanSsl.Checked = IIf(frmPrincipale.lanSSL = "N", False, True)
            VisualizzaCampi("LAN")
        End If
    End Sub

    Private Sub frmConnection_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        frmPrincipale.Enabled = True
    End Sub

    Private Sub cmdOk_Click(sender As Object, e As EventArgs) Handles cmdOk.Click
        With frmPrincipale
            If optRS232.Checked = True Then
                .comPort = cboComName.Text
                .comBaudRate = cboComBaudrate.Text
                .comStopbits = cboComStop.Text
                .comParity = cboComParity.Text
                .comDataBits = cboComData.Text
                .comProtocol = cboComProtocol.Text
                .configIni.WriteValue("RS232", "port", .comPort)
                .configIni.WriteValue("RS232", "Baudrate", .comBaudRate)
                .configIni.WriteValue("RS232", "Stopbit", .comStopbits)
                .configIni.WriteValue("RS232", "Parity", .comParity)
                .configIni.WriteValue("RS232", "Databits", .comDataBits)
                .configIni.WriteValue("RS232", "Protocol", .comProtocol)
            End If
            If optLAN.Checked = True Then
                .lanSSL = IIf(chkLanSsl.Checked = True, "Y", "N")
                .lanIPAddress = txtLanIp.Text
                .lanPort = cboLanPort.Text
                .lanProtocol = cboLanProtocol.Text
                .configIni.WriteValue("TCP-IP", "SSL", .lanSSL)
                .configIni.WriteValue("TCP-IP", "IPAddress", .lanIPAddress)
                .configIni.WriteValue("TCP-IP", "Port", .lanPort)
                .configIni.WriteValue("TCP-IP", "Protocol", .lanProtocol)
            End If
            If optUSB.Checked = True Then
                .usbProtocol = cboUsbProtocol.Text
                .configIni.WriteValue("USB", "Protocol", .usbProtocol)
            End If
        End With
        frmPrincipale.ConnectionSetup()
        ApriConnessione()
        cmdComandoSingolo("T,1,0,77", 4, 5000)
        'CmdExtendedStatus()
        Me.Close()
    End Sub
End Class