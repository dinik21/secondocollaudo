﻿Imports SecondoCollaudo.CMClass

Public Class ArcaClass

    Structure ArcaColor
        Public Shared sfondo As Color = Color.FromArgb(&HFF55166E)
        Public Shared errore As Color = Color.FromArgb(&HFFE45620)
        Public Shared istruzione As Color = Color.FromArgb(&HFF1A3281)
        Public Shared messaggio As Color = Color.FromArgb(&HFFFFFFFF)
        Public Shared OK As Color = Color.FromArgb(&HFFAEBF37)
        Public Shared KO As Color = Color.FromArgb(&HFFA00000)
    End Structure


    '''' <summary>
    '''' Ciclo di attesa pressione tasto(RISPOSTA : INVIO,R,ESC,OK=tastoSpecifico)
    '''' </summary>
    '''' <param name="tastoSpecifico">Codice asci decimale del tasto da attendere</param>
    '''' <returns></returns>
    'Public Shared Function AttendiTasto(Optional ByVal tastoSpecifico As String = "") As String
    '    Dim tasto As Integer
    '    AttendiTasto = ""
    '    tastoPremuto = 0
    '    Do Until Len(AttendiTasto) > 0
    '        If Len(tastoSpecifico) > 0 Then
    '            Select Case tastoSpecifico
    '                Case "INVIO", "invio", "ENTER", "enter"
    '                    tasto = 13
    '                Case "ESC", "esc"
    '                    tasto = 27
    '                Case Else
    '                    tasto = Asc(UCase(tastoSpecifico))
    '            End Select
    '            If tastoPremuto = tasto Then
    '                AttendiTasto = "OK"
    '            End If
    '        Else
    '            Select Case tastoPremuto
    '                Case 0
    '                Case 13
    '                    AttendiTasto = "INVIO"
    '                Case 82, 114
    '                    AttendiTasto = "R"
    '                Case 27
    '                    AttendiTasto = "ESC"
    '                Case Else
    '                    AttendiTasto = "OK"
    '            End Select
    '            'End If
    '        End If

    '        Application.DoEvents()
    '    Loop
    'End Function


    '''' <summary>
    '''' Scrittura del messaggio sulla riga principale
    '''' </summary>
    '''' <param name="testo">Testo da mostrare</param>
    '''' <param name="nomeObj">Nome dell'oggetto sulla quale scrivere il testo</param>
    '''' <param name="colore">Colore del testo da mostrare (default = messaggio)</param>
    'Public Shared Sub ScriviMessaggio(ByVal testo As String, ByVal nomeObj As Object, Optional ByVal colore As Color = Nothing)
    '    If colore = Nothing Then
    '        colore = ArcaColor.messaggio
    '    End If

    '    nomeObj.Text = testo
    '    nomeObj.ForeColor = colore
    'End Sub

    ''' <summary>
    ''' Caricamento messaggi che verranno visualizzati
    ''' </summary>
    ''' <param name="language">Nome del file della lingua da caricare senza l'estensione</param>
    Public Shared Sub CaricaMessaggi(ByVal language As String)
        Dim iMessaggio As Integer = 0
        Public linguaIni As New INIFile(localFolder & "\" & language & ".ini")
        Do
            messaggio.Add(linguaIni.ReadValue("messages", "msg" & iMessaggio))
            If messaggio(iMessaggio) = "!!END!!" Or messaggio(iMessaggio) = "Failed" Then Exit Do
            iMessaggio += 1
        Loop
        If iMessaggio = 0 Then
            MsgBox("LANGUAGE INI FILE PROBLEM.", vbCritical, "CRITICAL ERROR")
            End
        End If
    End Sub
End Class
