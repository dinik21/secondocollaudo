﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ErrorLogForm
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgErrorLog = New System.Windows.Forms.DataGridView()
        Me.optGetAll = New System.Windows.Forms.RadioButton()
        Me.optGetFromTo = New System.Windows.Forms.RadioButton()
        Me.txtFrom = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtTo = New System.Windows.Forms.TextBox()
        Me.btnGetErrorLog = New System.Windows.Forms.Button()
        Me.Column24 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column21 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column22 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column23 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column25 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnClose = New System.Windows.Forms.Button()
        CType(Me.dgErrorLog, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgErrorLog
        '
        Me.dgErrorLog.AllowUserToAddRows = False
        Me.dgErrorLog.AllowUserToDeleteRows = False
        Me.dgErrorLog.AllowUserToResizeColumns = False
        Me.dgErrorLog.AllowUserToResizeRows = False
        Me.dgErrorLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgErrorLog.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column24, Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.Column5, Me.Column6, Me.Column7, Me.Column8, Me.Column9, Me.Column10, Me.Column11, Me.Column12, Me.Column13, Me.Column14, Me.Column15, Me.Column16, Me.Column17, Me.Column18, Me.Column19, Me.Column20, Me.Column21, Me.Column22, Me.Column23, Me.Column25})
        Me.dgErrorLog.Location = New System.Drawing.Point(12, 58)
        Me.dgErrorLog.MultiSelect = False
        Me.dgErrorLog.Name = "dgErrorLog"
        Me.dgErrorLog.ReadOnly = True
        Me.dgErrorLog.RowHeadersVisible = False
        Me.dgErrorLog.Size = New System.Drawing.Size(915, 408)
        Me.dgErrorLog.TabIndex = 0
        '
        'optGetAll
        '
        Me.optGetAll.AutoSize = True
        Me.optGetAll.Location = New System.Drawing.Point(12, 35)
        Me.optGetAll.Name = "optGetAll"
        Me.optGetAll.Size = New System.Drawing.Size(81, 17)
        Me.optGetAll.TabIndex = 1
        Me.optGetAll.TabStop = True
        Me.optGetAll.Text = "Get ALL log"
        Me.optGetAll.UseVisualStyleBackColor = True
        '
        'optGetFromTo
        '
        Me.optGetFromTo.AutoSize = True
        Me.optGetFromTo.Checked = True
        Me.optGetFromTo.Location = New System.Drawing.Point(12, 12)
        Me.optGetFromTo.Name = "optGetFromTo"
        Me.optGetFromTo.Size = New System.Drawing.Size(82, 17)
        Me.optGetFromTo.TabIndex = 2
        Me.optGetFromTo.TabStop = True
        Me.optGetFromTo.Text = "Get log from"
        Me.optGetFromTo.UseVisualStyleBackColor = True
        '
        'txtFrom
        '
        Me.txtFrom.Location = New System.Drawing.Point(94, 9)
        Me.txtFrom.Name = "txtFrom"
        Me.txtFrom.Size = New System.Drawing.Size(61, 20)
        Me.txtFrom.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(167, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(16, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "to"
        '
        'txtTo
        '
        Me.txtTo.Location = New System.Drawing.Point(184, 9)
        Me.txtTo.Name = "txtTo"
        Me.txtTo.Size = New System.Drawing.Size(61, 20)
        Me.txtTo.TabIndex = 5
        '
        'btnGetErrorLog
        '
        Me.btnGetErrorLog.Location = New System.Drawing.Point(267, 9)
        Me.btnGetErrorLog.Name = "btnGetErrorLog"
        Me.btnGetErrorLog.Size = New System.Drawing.Size(98, 43)
        Me.btnGetErrorLog.TabIndex = 6
        Me.btnGetErrorLog.Text = "Get Error Log"
        Me.btnGetErrorLog.UseVisualStyleBackColor = True
        '
        'Column24
        '
        Me.Column24.HeaderText = "Row"
        Me.Column24.Name = "Column24"
        Me.Column24.ReadOnly = True
        Me.Column24.Width = 40
        '
        'Column1
        '
        Me.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.Column1.HeaderText = "Life/Time"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 77
        '
        'Column2
        '
        Me.Column2.HeaderText = "L"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Width = 20
        '
        'Column3
        '
        Me.Column3.HeaderText = "Op"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.Width = 25
        '
        'Column4
        '
        Me.Column4.HeaderText = "Rc1"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.Width = 30
        '
        'Column5
        '
        Me.Column5.HeaderText = "F1"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.Width = 35
        '
        'Column6
        '
        Me.Column6.HeaderText = "F2"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.Width = 35
        '
        'Column7
        '
        Me.Column7.HeaderText = "F3"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        Me.Column7.Width = 35
        '
        'Column8
        '
        Me.Column8.HeaderText = "F4"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        Me.Column8.Width = 35
        '
        'Column9
        '
        Me.Column9.HeaderText = "FA"
        Me.Column9.Name = "Column9"
        Me.Column9.ReadOnly = True
        Me.Column9.Width = 35
        '
        'Column10
        '
        Me.Column10.HeaderText = "FB"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        Me.Column10.Width = 35
        '
        'Column11
        '
        Me.Column11.HeaderText = "FC"
        Me.Column11.Name = "Column11"
        Me.Column11.ReadOnly = True
        Me.Column11.Width = 35
        '
        'Column12
        '
        Me.Column12.HeaderText = "FD"
        Me.Column12.Name = "Column12"
        Me.Column12.ReadOnly = True
        Me.Column12.Width = 35
        '
        'Column13
        '
        Me.Column13.HeaderText = "FE"
        Me.Column13.Name = "Column13"
        Me.Column13.ReadOnly = True
        Me.Column13.Width = 35
        '
        'Column14
        '
        Me.Column14.HeaderText = "FF"
        Me.Column14.Name = "Column14"
        Me.Column14.ReadOnly = True
        Me.Column14.Width = 35
        '
        'Column15
        '
        Me.Column15.HeaderText = "FG"
        Me.Column15.Name = "Column15"
        Me.Column15.ReadOnly = True
        Me.Column15.Width = 35
        '
        'Column16
        '
        Me.Column16.HeaderText = "FH"
        Me.Column16.Name = "Column16"
        Me.Column16.ReadOnly = True
        Me.Column16.Width = 35
        '
        'Column17
        '
        Me.Column17.HeaderText = "FI"
        Me.Column17.Name = "Column17"
        Me.Column17.ReadOnly = True
        Me.Column17.Width = 35
        '
        'Column18
        '
        Me.Column18.HeaderText = "FJ"
        Me.Column18.Name = "Column18"
        Me.Column18.ReadOnly = True
        Me.Column18.Width = 35
        '
        'Column19
        '
        Me.Column19.HeaderText = "FK"
        Me.Column19.Name = "Column19"
        Me.Column19.ReadOnly = True
        Me.Column19.Width = 35
        '
        'Column20
        '
        Me.Column20.HeaderText = "FL"
        Me.Column20.Name = "Column20"
        Me.Column20.ReadOnly = True
        Me.Column20.Width = 35
        '
        'Column21
        '
        Me.Column21.HeaderText = "FM"
        Me.Column21.Name = "Column21"
        Me.Column21.ReadOnly = True
        Me.Column21.Width = 35
        '
        'Column22
        '
        Me.Column22.HeaderText = "FN"
        Me.Column22.Name = "Column22"
        Me.Column22.ReadOnly = True
        Me.Column22.Width = 35
        '
        'Column23
        '
        Me.Column23.HeaderText = "FO"
        Me.Column23.Name = "Column23"
        Me.Column23.ReadOnly = True
        Me.Column23.Width = 35
        '
        'Column25
        '
        Me.Column25.HeaderText = "FP"
        Me.Column25.Name = "Column25"
        Me.Column25.ReadOnly = True
        Me.Column25.Width = 35
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(796, 485)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(130, 44)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'ErrorLogForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(939, 539)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnGetErrorLog)
        Me.Controls.Add(Me.txtTo)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtFrom)
        Me.Controls.Add(Me.optGetFromTo)
        Me.Controls.Add(Me.optGetAll)
        Me.Controls.Add(Me.dgErrorLog)
        Me.Name = "ErrorLogForm"
        Me.Text = "ErrorLogForm"
        CType(Me.dgErrorLog, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgErrorLog As DataGridView
    Friend WithEvents optGetAll As RadioButton
    Friend WithEvents optGetFromTo As RadioButton
    Friend WithEvents txtFrom As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtTo As TextBox
    Friend WithEvents btnGetErrorLog As Button
    Friend WithEvents Column24 As DataGridViewTextBoxColumn
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents Column5 As DataGridViewTextBoxColumn
    Friend WithEvents Column6 As DataGridViewTextBoxColumn
    Friend WithEvents Column7 As DataGridViewTextBoxColumn
    Friend WithEvents Column8 As DataGridViewTextBoxColumn
    Friend WithEvents Column9 As DataGridViewTextBoxColumn
    Friend WithEvents Column10 As DataGridViewTextBoxColumn
    Friend WithEvents Column11 As DataGridViewTextBoxColumn
    Friend WithEvents Column12 As DataGridViewTextBoxColumn
    Friend WithEvents Column13 As DataGridViewTextBoxColumn
    Friend WithEvents Column14 As DataGridViewTextBoxColumn
    Friend WithEvents Column15 As DataGridViewTextBoxColumn
    Friend WithEvents Column16 As DataGridViewTextBoxColumn
    Friend WithEvents Column17 As DataGridViewTextBoxColumn
    Friend WithEvents Column18 As DataGridViewTextBoxColumn
    Friend WithEvents Column19 As DataGridViewTextBoxColumn
    Friend WithEvents Column20 As DataGridViewTextBoxColumn
    Friend WithEvents Column21 As DataGridViewTextBoxColumn
    Friend WithEvents Column22 As DataGridViewTextBoxColumn
    Friend WithEvents Column23 As DataGridViewTextBoxColumn
    Friend WithEvents Column25 As DataGridViewTextBoxColumn
    Friend WithEvents btnClose As Button
End Class
