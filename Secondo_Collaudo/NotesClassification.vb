﻿Public Class NotesClassification
    Private Sub NotesClassification_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dgvDetail.Rows.Add("Denomination", "0000")
        dgvDetail.Rows.Add("Soliling test", "0")
        dgvDetail.Rows.Add("Grid check", "0")
        dgvDetail.Rows.Add("Tape check", "0")
        dgvDetail.Rows.Add("De-inked check with UV", "0")
        dgvDetail.Rows.Add("Dog ears corner check", "0")
        dgvDetail.Rows.Add("Format check", "0")
        dgvDetail.Rows.Add("Contrast check", "0")
        dgvDetail.Rows.Add("Closed tears check", "0")
        dgvDetail.Rows.Add("Graffiti check", "0")
        dgvDetail.Rows.Add("Stain check", "0")
        dgvDetail.Rows.Add("Dye check", "0")
        'dgvDetail.AutoSize = True
        dgvDetail.AllowUserToAddRows = False
        GetDetail()
    End Sub

    Sub GetDetail()
        cmdComandoSingolo("T,1,3,6," & txtNoteId.Value, 5, 5000)
        Try
            txtAnswer.Text = comandoSingoloRisposta(5)
            dgvDetail.Item(1, 0).Value = comandoSingoloRisposta(6)
            dgvDetail.Item(1, 1).Value = comandoSingoloRisposta(7)
            dgvDetail.Item(1, 2).Value = comandoSingoloRisposta(8)
            dgvDetail.Item(1, 3).Value = comandoSingoloRisposta(9)
            dgvDetail.Item(1, 4).Value = comandoSingoloRisposta(10)
            dgvDetail.Item(1, 5).Value = comandoSingoloRisposta(11)
            dgvDetail.Item(1, 6).Value = comandoSingoloRisposta(12)
            dgvDetail.Item(1, 7).Value = comandoSingoloRisposta(13)
            dgvDetail.Item(1, 8).Value = comandoSingoloRisposta(14)
            dgvDetail.Item(1, 9).Value = comandoSingoloRisposta(15)
            dgvDetail.Item(1, 10).Value = comandoSingoloRisposta(16)
            dgvDetail.Item(1, 11).Value = comandoSingoloRisposta(17)
        Catch e As Exception
        End Try
    End Sub

    Private Sub txtNoteId_ValueChanged(sender As Object, e As EventArgs) Handles txtNoteId.ValueChanged
        GetDetail()
    End Sub
End Class