﻿Public Class frmClose
    Private Sub Close_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cboUser.Items.Add("R")
        cboUser.Items.Add("L")
        cboUser.SelectedIndex = 0
    End Sub

    Private Sub cmdOk_Click(sender As Object, e As EventArgs) Handles cmdOk.Click
        CMClose(cboUser.Text)
        txtReplyCode.Text = myCloseReply.Rc
    End Sub

    Private Sub cmdCancel_Click(sender As Object, e As EventArgs) Handles cmdCancel.Click
        Me.Close()
    End Sub

    Private Sub frmClose_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        frmPrincipale.Enabled = True
    End Sub
End Class