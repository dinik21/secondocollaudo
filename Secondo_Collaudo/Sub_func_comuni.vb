﻿Imports System.Math
Imports System.IO

Structure sLog
    Dim esitoTestSecondoCollaudo As Boolean
    Dim stazioneCollaudo As String
    Dim versioneSoftware As String
    Dim dataInizio As String
    Dim dataFine As String
    Dim oraInizio As String
    Dim oraFine As String
    Dim macchinaCodice As String
    Dim macchinaMatricola As String
    Dim macchinaCliente As String
    Dim macchinaLettoreMatricola As String
    Dim macchinaMacAddress As String
    Dim macchinaCassetteNumber As String
    Dim macchinaSuite As String
    Dim primoDeposito As Boolean
    Dim transferCD80 As Boolean
    Dim transferBag As Boolean
    Dim barcodeCD80() As String
    Dim barcodeBag As String
    Dim svuotamentoUltimi As Boolean
    Dim prelievoSingolo As Boolean
    Dim depositoSingolo As Boolean
    Dim prelievoDistribuito As Boolean
    Dim serialNumberCassetto() As String
    Dim depositoCompleto As Boolean
    Dim prelievoTotale As Boolean
End Structure

Structure SBancCas
    Dim name As String
    Dim noteNumber As Integer
    Dim denomination As String
End Structure

Structure dep
    Dim replyCode As String
    Dim bnTot As Byte
    Dim bnRej As Byte
    Dim bnUnFit As Byte
    Dim bnCas() As SBancCas
End Structure

Structure _bottone
    Dim Titolo As String
    Dim Comando As String
End Structure

Structure limiti_safe

    Dim F_Cash_L_min As Integer
    Dim F_Cash_L_max As Integer
    Dim F_Cash_R_min As Integer
    Dim F_Cash_R_max As Integer

End Structure

Structure _test
    Dim test As Boolean
    Dim V_Off As Single
    Dim V_On_Mot_Off As Single
    Dim V_On_Mot_On 
    Dim Shift_cente_value As Byte
    Dim Foto_forchetta_FAIL As String
    Dim check_list As String
    Dim Bn_riconosciute As Integer
    Dim Bn_rifiutate As Integer

End Structure
Structure _Secondo
    Dim categoria As String
    Dim Matricola_Secondo_collaudo As String
    Dim Codice_cliente As Integer
    Dim Nome_cliente As String
    Dim Codice_Secondo_collaudo As String
    Dim Codice_Macchina As String
    Dim Matricola_Controller As String
    Dim Matricola_lettore As String
    Dim CRM As Boolean
    Dim CD80 As Byte
    Dim Data As String
    Dim Ora As String
    Dim Test As _test
End Structure

Module Sub_func_comuni
    Structure _colore
        Dim Normal As Color
        Dim Warning As Color
        Dim Ok As Color
        Dim Errore As Color
    End Structure

    Public Log As sLog
    Public Secondo_collaudo As _Secondo
    Public Colore As _colore
    Public Traspric As String
    Public Salva_com As Boolean
    Public Debug_test As Boolean
    Public Debug_pausa As Integer
    Public WorkDir As String
    Public No_Timeout As Boolean = False
    Public Tasto As Integer
    Public k_timeout As Integer
    Public Baudrate As Integer
    Public Const PURGE_RXCLEAR As Integer = &H8
    Public WithEvents moRS232 As Rs232
    Private Delegate Sub CommEventUpdate(ByVal source As Rs232, ByVal mask As Rs232.EventMasks)
    Public Stringa_vuota As Boolean
    Public Stringa_di_connessione As String
    Public no_timeout_USB As Boolean
    Public Tipo_Comunicazione As Byte
    Public Ricez As String
    Public mex() As String
    Public replyCodeMessage() As String
    Public replyCodeCassetteMessage() As String
    Public replyCodeFeederMessage() As String
    Public replyCodeControllerMessage() As String
    Public replyCodeReaderMessage() As String
    Public replyCodeSafeMessage() As String
    Public replyCodeDepositMessage() As String
    Public OKcomm As Boolean
    Public IP_Macchina As String
    Public IP_Old As String
    Public Database_path As String
    Public Database_name As String
    Public Primo_avvio As Boolean
    Public Fw_Path As String
    Public Transparent_ON As Boolean
    Public Zip_path As String
    Public Deposito As dep
    Public Comm_Port As Integer
    Public Errori_penalità() As Integer
    'Public photo_status As _foto_status

    Public Function stringa_invio(ByVal stringa As String) As String
        stringa_invio = ""
        For a = 0 To Len(stringa) - 1 Step 2
            stringa_invio = stringa_invio + Chr("&h" + stringa.Substring(a, 2))
        Next
    End Function

    Public Function stringa_ricezione(ByVal stringa As String) As String

        Dim aaa As String
        stringa_ricezione = ""
        If stringa <> "" Then
            For a = 1 To Len(stringa)
                aaa = Hex$(Asc(Mid$(stringa, a, 1)))
                If Len(aaa) = 1 Then aaa = "0" + aaa
                stringa_ricezione = stringa_ricezione + aaa
            Next
        End If

    End Function

    Public Sub Scrivi_N(ByVal Stringa As String)
        frmPincipale.Label1.ForeColor = Colore.Normal
        frmPincipale.Label1.Text = Stringa
        Application.DoEvents()
    End Sub

    Public Sub Scrivi_E(ByVal Stringa As String)
        frmPincipale.Label1.ForeColor = Colore.Errore
        frmPincipale.Label1.Text = Stringa
        Application.DoEvents()
    End Sub

    Public Sub Scrivi_O(ByVal Stringa As String)
        frmPincipale.Label1.ForeColor = Colore.Ok
        frmPincipale.Label1.Text = Stringa
        Application.DoEvents()
    End Sub

    Public Sub Scrivi_W(ByVal Stringa As String)
        frmPincipale.Label1.ForeColor = Colore.Warning
        frmPincipale.Label1.Text = Stringa
        Application.DoEvents()
    End Sub

    Public Sub MySleep(ByVal Millisec As Integer)

        Dim iIndex As Int16
        For iIndex = 1 To Millisec / 10

            Threading.Thread.Sleep(10)
            Application.DoEvents()

        Next

    End Sub

    Public Function METTE_0(ByVal pippo As String)
        Dim stringa As String
        stringa = ""

        For a = 1 To Len(pippo)

            stringa = stringa + "0" + Mid$(pippo, a, 1)

        Next

        METTE_0 = stringa

    End Function

    Function dec_hex(ByVal DEC As Byte) As String

        If DEC.ToString <> "" Then

            dec_hex = Hex(DEC) : If Len(dec_hex) = 1 Then dec_hex = "0" & dec_hex
            dec_hex = dec_hex
        Else
            dec_hex = ""

        End If

    End Function
    Function dec_hex(ByVal DEC As Int16) As String

        If DEC.ToString <> "" Then
            dec_hex = Hex(DEC)
            For a = Len(dec_hex) + 1 To 4
                dec_hex = "0" & dec_hex
            Next
        Else
            dec_hex = ""

        End If

    End Function
    Function dec_hex(ByVal DEC As UInt16) As String

        If DEC.ToString <> "" Then
            dec_hex = Hex(DEC)
            For a = Len(dec_hex) + 1 To 4
                dec_hex = "0" & dec_hex
            Next
        Else
            dec_hex = ""

        End If

    End Function

    Function dec_hex(ByVal DEC As Int32) As String

        If DEC.ToString <> "" Then
            dec_hex = Hex(DEC)
            For a = Len(dec_hex) + 1 To 8
                dec_hex = "0" & dec_hex
            Next
        Else
            dec_hex = ""

        End If

    End Function
    Function TOGLIE_0(ByVal pippo As String)
        Dim stringa As String = ""
        For a = 2 To Len(pippo) Step 2

            stringa = stringa + Mid$(pippo, a, 1)

        Next

        TOGLIE_0 = stringa

    End Function


    Public Function hex_bin(ByVal hex As String)
        Dim b As String
        hex_bin = ""
        For a = 1 To Len(hex)

            b = dec_bin("&h" + Mid$(hex, a, 1))
            b = Mid(b, 5)

            hex_bin = hex_bin + b
        Next
        Dim k As Integer
        Select Case hex.Length
            Case 1
                k = 4
            Case 2
                k = 8
            Case 3
                k = 16
            Case 4
                k = 16
        End Select

        For a = 1 To k - Len(hex_bin)

            hex_bin = "0" + Trim(hex_bin)
        Next

    End Function

    Public Function bin_dec(ByVal bin)
        Dim aa As Long = 0
        For a = 0 To Len(bin) - 1
            Application.DoEvents()
            aa = aa + Val(Mid$(bin, Len(bin) - a, 1) * 2 ^ a)
        Next

        bin_dec = aa

    End Function
    Public Function dec_bin(ByVal dec)
        Dim aa As Integer
        On Error Resume Next
        dec_bin = ""
        Do While Not dec = 0
            aa = dec \ 2

            If aa * 2 = Val(dec) Then
                dec_bin = "0" + dec_bin
            Else
                dec_bin = "1" + dec_bin
            End If
            dec = Int(dec / 2)
        Loop

        If dec_bin = "" Then dec_bin = "0"

        For a = 1 To 8 - Len(dec_bin)

            dec_bin = "0" + Trim(dec_bin)
        Next

    End Function

    Public Function int_offset(ByVal st As String) As Integer

        If Mid$(st, 2, 1) = "0" Then
            int_offset = -(Val("&h" + Mid$(st, 3, 2)))
        Else
            int_offset = (Val("&h" + Mid$(st, 3, 2)))
        End If

    End Function

    Function Carattere_pad(ByVal stringa As String, ByVal carattere As Char, ByVal num_tot_caratteri As Byte) As String

        Carattere_pad = stringa

        For a = stringa.Length To num_tot_caratteri - 1
            Carattere_pad = carattere & Carattere_pad
        Next

    End Function
    Public Function bin_hex(ByVal bin)
        Dim p
        bin_hex = ""
        If bin = "0" Then bin_hex = "0" : Exit Function
        Do While Not Len(bin) Mod 4 = 0
            bin = "0" + bin
            Application.DoEvents()
        Loop

        For a = 1 To Len(bin) Step 4
            Application.DoEvents()
            p = Hex(bin_dec(Mid$(bin, a, 4)))
            bin_hex = Trim((bin_hex + p))
        Next

    End Function
    Public Sub Attendi_Invio()

        frmPincipale.Focus()
        Tasto = 0
        Do While Not Tasto = 13
            Application.DoEvents()
            Threading.Thread.Sleep(50)
        Loop

    End Sub

    Sub Attendi_Tasto()

        frmPincipale.Focus()
        Tasto = 0
        Do While Not Tasto <> 0
            Application.DoEvents()
            Threading.Thread.Sleep(50)
        Loop

    End Sub

    Public Sub Traspsend(ByVal Out As String, ByVal NByte As Integer, ByVal Tout As Integer, Optional ByVal Hi_lev As Boolean = False)
        If Transparent_ON = True Then Aggiorna_list_com("SND: " & Out)
        If Transparent_ON = True Then
            If moRS232.IsOpen = False Then moRS232.Open(Comm_Port, 9600, 8, Rs232.DataParity.Parity_None, Rs232.DataStopBit.StopBit_1, 1024)
        End If

        Traspric = ""
        '----------------------
        '// Clear Tx/Rx Buffers
        If Hi_lev = False Then moRS232.PurgeBuffer(Rs232.PurgeBuffers.TxClear Or Rs232.PurgeBuffers.RXClear)
        moRS232.RxBufferThreshold = Int32.Parse(NByte)
        moRS232.Timeout = Val(Tout)
        If Transparent_ON = True Then NByte = NByte * 2
        moRS232.Write(Out)

        '// Clears Rx textbox
        Dim aa = moRS232.StopBit
        If NByte > 0 Then
            Ricezione(NByte)
        Else
            MySleep(Tout)
        End If

        If Transparent_ON = True Then
            If moRS232.IsOpen = True Then moRS232.Close()
        End If

        If Transparent_ON = True And Traspric <> "" Then Aggiorna_list_com("RCV: " & Traspric)
    End Sub

    Private Sub Ricezione(ByVal nbyte As Integer)

        Dim Risp As Integer = moRS232.Read(Int32.Parse(nbyte))

        If Risp = 0 Then
            Traspric = ""
        Else
            Traspric = moRS232.InputStreamString.ToString
        End If


    End Sub
    Private Sub moRS232_CommEvent(ByVal source As Rs232, ByVal Mask As Rs232.EventMasks) Handles moRS232.CommEvent
        '===================================================
        '												©2003 www.codeworks.it All rights reserved
        '
        '	Description	:	Events raised when a comunication event occurs
        '	Created			:	15/07/03 - 15:13:46
        '	Author			:	Corrado Cavalli
        '
        '						*Parameters Info*
        '
        '	Notes				:	
        '===================================================
        Debug.Assert(frmPincipale.InvokeRequired = False)

        Dim iPnt As Int32, Buffer() As Byte
        Debug.Assert(frmPincipale.InvokeRequired = False)

        If (Mask And Rs232.EventMasks.RxChar) > 0 Then

            Buffer = source.InputStream
            For iPnt = 0 To Buffer.Length - 1

            Next

        End If

    End Sub

#Region "UI update routine"
#End Region



    Sub attendi_per(ByVal stato As String)
        Traspric = Chr("&H" + stato)
        Do While Not Traspric <> Chr("&H" + stato)
            Call Traspsend("F3", 1, 1000)
            If Mid$(Traspric, 1, 1) = Chr(0) Then
                Traspric = Chr("&H" + stato)
            End If
            MySleep(100)

        Loop

    End Sub
    Function attendi_per_t(ByVal stato As String, ByVal lop As Long, ByVal t As Integer) As Boolean

        Traspric = Chr("&H" & stato)
        Dim limite As DateTime = DateTime.Now.AddMilliseconds(t)
        Do While Not Traspric <> Chr("&H" & stato)
            If DateTime.Now > limite Then
                attendi_per_t = False

                Exit Function
            End If
            Call Traspsend("F3", 1, 1000)
            If Mid$(Traspric, 1, 1) = Chr(0) Then
                Traspric = Chr("&H" & stato)
            End If
            MySleep(lop)
            Application.DoEvents()
        Loop
        If Traspric = "" Then attendi_per_t = False : Exit Function

        attendi_per_t = True

    End Function




    Sub Traspin()

        If Secondo_collaudo.categoria <> "CIMA" Then
ripeti:
            Transparent_ON = False
            Dim Send As String = "X,1"
            Call HiLevSend(Send)


            If OKcomm = False Then

            Else
                If Right(Ricez$, 2) = ",4" Or Right(Ricez$, 3) = "104" Then

                    OKcomm = False
                Else
                    OKcomm = True
                    Transparent_ON = True
                End If

            End If
        Else


        End If
    End Sub
    Sub Traspout()

        Transparent_ON = False
        Dim Send As String = "Y,1"

        Call HiLevSend(Send)

        If OKcomm = False Then
            Transparent_ON = True
            Exit Sub

        Else
            Transparent_ON = False
        End If

    End Sub


    Private Sub close_port()

        If moRS232.IsOpen = True Then moRS232.Close()

    End Sub

    Function USB_Array_to_String(ByVal buf As Byte()) As String
        USB_Array_to_String = ""
        If buf.Length > 0 Then
            For a = 0 To buf.Length - 1
                USB_Array_to_String = USB_Array_to_String & Chr(buf(a))
            Next
        End If
    End Function



    Sub Power_On()
        Traspsend("D233", 0, 500)
    End Sub

    Sub Power_Off()
        Traspsend("D234", 0, 500)
    End Sub

    Sub MSO_Imput_on()
        Traspsend("D210", 0, 200)
    End Sub

    Sub MSO_Output_on()
        Traspsend("D211", 0, 200)
    End Sub

    Sub Pressor_Home()
        Traspsend("D20B", 0, 500)
    End Sub

    Sub Pressor_Opened()
        Traspsend("D20C", 0, 500)
    End Sub

    Sub Pressor_Closed()
        Traspsend("D20D", 0, 500)
    End Sub

    Sub Prefeeding_On()
        Traspsend("D20E", 0, 200)
    End Sub

    Sub Prefeeding_Off()
        Traspsend("D20F", 0, 200)
    End Sub

    Sub Allign_Home()
        Traspsend("D21C", 0, 500)
    End Sub

    Sub Push_Home()
        Traspsend("D21D", 0, 500)
    End Sub

    Sub Push_Open()
        Traspsend("D21F", 0, 500)
    End Sub

    Function Preset() As String
        Scrivi_N("Preset in corso attendere..")
        Call Traspsend("83", 1, 10000)
        Preset = stringa_ricezione(Traspric)

    End Function

    'Function Deposito_ALL(Optional ByVal counting As Boolean = False) As Boolean

    '    Call Principale.Open_R(True)
    '    If counting = False Then
    '        Call HiLevSend("D,1,R,0,0000", "")
    '    Else
    '        Call HiLevSend("D,1,R,3,0000", "")
    '    End If

    '    Call Interpreta_deposito(Ricez$)
    '    Deposito_ALL = True

    'End Function

    'Sub Interpreta_deposito(ByVal stringa As String)
    '    ReDim Deposito.bnCas(12)
    '    For a = 0 To 11
    '        Deposito.bnCas(a).denomination = ""
    '        Deposito.bnCas(a).noteNumbernote = 0
    '    Next

    '    Dim aaa As String = Ricez$ & ","
    '    Dim prev As Integer = 1
    '    Dim act As Integer = 1
    '    Dim virgola As Integer = 0
    '    For a = 1 To Len(aaa)

    '        If Mid$(aaa, a, 1) = "," Then
    '            act = a
    '            virgola = virgola + 1
    '            Dim aa As String = Mid$(aaa, prev, act - prev)
    '            Select Case virgola
    '                Case 4
    '                    Deposito.RC = aa
    '                Case 5
    '                    Deposito.Banconote_tot = aa
    '                Case 6
    '                    Deposito.Banconote_UNFit = aa
    '                Case 7
    '                    Deposito.Banconote_Rif = aa
    '                Case 8
    '                    Deposito.Bn_cas(0).denomination = aa
    '                Case 9
    '                    Deposito.Bn_cas(0).N_note = aa
    '                Case 10
    '                    Deposito.Bn_cas(1).denomination = aa
    '                Case 11
    '                    Deposito.Bn_cas(1).N_note = aa
    '                Case 12
    '                    Deposito.Bn_cas(2).denomination = aa
    '                Case 13
    '                    Deposito.Bn_cas(2).N_note = aa
    '                Case 14
    '                    Deposito.Bn_cas(3).denomination = aa
    '                Case 15
    '                    Deposito.Bn_cas(3).N_note = aa
    '                Case 16
    '                    Deposito.Bn_cas(4).denomination = aa
    '                Case 17
    '                    Deposito.Bn_cas(4).N_note = aa
    '                Case 18
    '                    Deposito.Bn_cas(5).denomination = aa
    '                Case 19
    '                    Deposito.Bn_cas(5).N_note = aa
    '                Case 20
    '                    Deposito.Bn_cas(6).denomination = aa
    '                Case 21
    '                    Deposito.Bn_cas(6).N_note = aa
    '                Case 22
    '                    Deposito.Bn_cas(7).denomination = aa
    '                Case 23
    '                    Deposito.Bn_cas(7).N_note = aa
    '                Case 24
    '                    Deposito.Bn_cas(8).denomination = aa
    '                Case 25
    '                    Deposito.Bn_cas(8).N_note = aa
    '                Case 26
    '                    Deposito.Bn_cas(9).denomination = aa
    '                Case 27
    '                    Deposito.Bn_cas(9).N_note = aa
    '                Case 28
    '                    Deposito.Bn_cas(10).denomination = aa
    '                Case 29
    '                    Deposito.Bn_cas(10).N_note = aa
    '                Case 30
    '                    Deposito.Bn_cas(11).denomination = aa
    '                Case 31
    '                    Deposito.Bn_cas(11).N_note = aa

    '            End Select
    '            prev = act + 1
    '        End If

    '    Next

    'End Sub

    Public Sub Aggiorna_list_com(ByVal stringa As String)

        If frmPincipale.lstComScope.Items.Count = 10 Then
            For a = 1 To 9
                frmPincipale.lstComScope.Items.Item(a - 1) = frmPincipale.lstComScope.Items.Item(a)
            Next
            frmPincipale.lstComScope.Items.Item(9) = stringa
        Else
            frmPincipale.lstComScope.Items.Add(stringa)
        End If

    End Sub
End Module
