﻿Public Class frmOpen
    Private Sub Open_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        frmPrincipale.Enabled = False
        cboUser.Items.Add("R")
        cboUser.Items.Add("L")
        cboUser.SelectedIndex = 0
    End Sub

    Private Sub cboUser_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboUser.SelectedIndexChanged
        Select Case cboUser.Text
            Case "R"
                txtPassword.Text = frmPrincipale.configIni.ReadValue("Pwd", "UserR")
            Case "L"
                txtPassword.Text = frmPrincipale.configIni.ReadValue("Pwd", "UserL")
            Case Else
                txtPassword.Text = ""
        End Select
    End Sub

    Private Sub cmdCancel_Click(sender As Object, e As EventArgs) Handles cmdCancel.Click
        Me.Close()
    End Sub

    Private Sub Open_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        frmPrincipale.Enabled = True
    End Sub

    Private Sub cmdOk_Click(sender As Object, e As EventArgs) Handles cmdOk.Click
        CMOpen(cboUser.Text, txtPassword.Text)
        txtReplyCode.Text = myOpenReply.Rc
    End Sub
End Class