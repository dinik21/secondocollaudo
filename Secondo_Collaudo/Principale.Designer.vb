﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmPrincipale
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPrincipale))
        Me.cmdPrincipale = New System.Windows.Forms.Button()
        Me.lblMessaggio = New System.Windows.Forms.Label()
        Me.Grid1 = New System.Windows.Forms.DataGridView()
        Me.txtSerialNumber = New System.Windows.Forms.TextBox()
        Me.cboCliente = New System.Windows.Forms.ComboBox()
        Me.cboIdProdotto = New System.Windows.Forms.ComboBox()
        Me.grpCashData = New System.Windows.Forms.GroupBox()
        Me.lblCasLStatus = New System.Windows.Forms.Label()
        Me.lblCasLEnable = New System.Windows.Forms.Label()
        Me.lblCasLFree = New System.Windows.Forms.Label()
        Me.lblCasKStatus = New System.Windows.Forms.Label()
        Me.lblCasKEnable = New System.Windows.Forms.Label()
        Me.lblCasKFree = New System.Windows.Forms.Label()
        Me.lblCasJStatus = New System.Windows.Forms.Label()
        Me.lblCasJEnable = New System.Windows.Forms.Label()
        Me.lblCasJFree = New System.Windows.Forms.Label()
        Me.lblCasIStatus = New System.Windows.Forms.Label()
        Me.lblCasIEnable = New System.Windows.Forms.Label()
        Me.lblCasIFree = New System.Windows.Forms.Label()
        Me.lblCasHStatus = New System.Windows.Forms.Label()
        Me.lblCasHEnable = New System.Windows.Forms.Label()
        Me.lblCasHFree = New System.Windows.Forms.Label()
        Me.lblCasGStatus = New System.Windows.Forms.Label()
        Me.lblCasGEnable = New System.Windows.Forms.Label()
        Me.lblCasGFree = New System.Windows.Forms.Label()
        Me.lblCasFStatus = New System.Windows.Forms.Label()
        Me.lblCasFEnable = New System.Windows.Forms.Label()
        Me.lblCasFFree = New System.Windows.Forms.Label()
        Me.lblCasEStatus = New System.Windows.Forms.Label()
        Me.lblCasEEnable = New System.Windows.Forms.Label()
        Me.lblCasEFree = New System.Windows.Forms.Label()
        Me.lblCasDStatus = New System.Windows.Forms.Label()
        Me.lblCasDEnable = New System.Windows.Forms.Label()
        Me.lblCasDFree = New System.Windows.Forms.Label()
        Me.lblCasCStatus = New System.Windows.Forms.Label()
        Me.lblCasCEnable = New System.Windows.Forms.Label()
        Me.lblCasCFree = New System.Windows.Forms.Label()
        Me.lblCasBStatus = New System.Windows.Forms.Label()
        Me.lblCasBEnable = New System.Windows.Forms.Label()
        Me.lblCasBFree = New System.Windows.Forms.Label()
        Me.lblCasAStatus = New System.Windows.Forms.Label()
        Me.lblCasAEnable = New System.Windows.Forms.Label()
        Me.lblCasAFree = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.txtCasLBn = New System.Windows.Forms.Label()
        Me.txtCasKBn = New System.Windows.Forms.Label()
        Me.txtCasJBn = New System.Windows.Forms.Label()
        Me.txtCasIBn = New System.Windows.Forms.Label()
        Me.txtCasHBn = New System.Windows.Forms.Label()
        Me.txtCasGBn = New System.Windows.Forms.Label()
        Me.txtCasFBn = New System.Windows.Forms.Label()
        Me.txtCasEBn = New System.Windows.Forms.Label()
        Me.txtCasDBn = New System.Windows.Forms.Label()
        Me.txtCasCBn = New System.Windows.Forms.Label()
        Me.txtCasBBn = New System.Windows.Forms.Label()
        Me.txtCasABn = New System.Windows.Forms.Label()
        Me.txtCasLVal = New System.Windows.Forms.Label()
        Me.txtCasKVal = New System.Windows.Forms.Label()
        Me.txtCasJVal = New System.Windows.Forms.Label()
        Me.txtCasIVal = New System.Windows.Forms.Label()
        Me.txtCasHVal = New System.Windows.Forms.Label()
        Me.txtCasGVal = New System.Windows.Forms.Label()
        Me.txtCasFVal = New System.Windows.Forms.Label()
        Me.txtCasEVal = New System.Windows.Forms.Label()
        Me.txtCasDVal = New System.Windows.Forms.Label()
        Me.txtCasCVal = New System.Windows.Forms.Label()
        Me.txtCasBVal = New System.Windows.Forms.Label()
        Me.txtCasAVal = New System.Windows.Forms.Label()
        Me.txtCasLDen = New System.Windows.Forms.Label()
        Me.txtCasKDen = New System.Windows.Forms.Label()
        Me.txtCasJDen = New System.Windows.Forms.Label()
        Me.txtCasIDen = New System.Windows.Forms.Label()
        Me.txtCasHDen = New System.Windows.Forms.Label()
        Me.txtCasGDen = New System.Windows.Forms.Label()
        Me.txtCasFDen = New System.Windows.Forms.Label()
        Me.txtCasEDen = New System.Windows.Forms.Label()
        Me.txtCasDDen = New System.Windows.Forms.Label()
        Me.txtCasCDen = New System.Windows.Forms.Label()
        Me.txtCasBDen = New System.Windows.Forms.Label()
        Me.txtCasADen = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.lblCasL = New System.Windows.Forms.Label()
        Me.lblCasK = New System.Windows.Forms.Label()
        Me.lblCasJ = New System.Windows.Forms.Label()
        Me.lblCasI = New System.Windows.Forms.Label()
        Me.lblCasH = New System.Windows.Forms.Label()
        Me.lblCasG = New System.Windows.Forms.Label()
        Me.lblCasF = New System.Windows.Forms.Label()
        Me.lblCasE = New System.Windows.Forms.Label()
        Me.lblCasD = New System.Windows.Forms.Label()
        Me.lblCasC = New System.Windows.Forms.Label()
        Me.lblCasB = New System.Windows.Forms.Label()
        Me.prbCasL = New System.Windows.Forms.ProgressBar()
        Me.prbCasK = New System.Windows.Forms.ProgressBar()
        Me.prbCasJ = New System.Windows.Forms.ProgressBar()
        Me.prbCasI = New System.Windows.Forms.ProgressBar()
        Me.prbCasH = New System.Windows.Forms.ProgressBar()
        Me.prbCasG = New System.Windows.Forms.ProgressBar()
        Me.prbCasF = New System.Windows.Forms.ProgressBar()
        Me.prbCasE = New System.Windows.Forms.ProgressBar()
        Me.prbCasD = New System.Windows.Forms.ProgressBar()
        Me.prbCasC = New System.Windows.Forms.ProgressBar()
        Me.prbCasB = New System.Windows.Forms.ProgressBar()
        Me.lblCasA = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.prbCasA = New System.Windows.Forms.ProgressBar()
        Me.grpExtendedStatus = New System.Windows.Forms.GroupBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.lblStatusDescrDb = New System.Windows.Forms.Label()
        Me.lblStatusDescrEb = New System.Windows.Forms.Label()
        Me.lblStatusDescrL = New System.Windows.Forms.Label()
        Me.lblStatusDescrJ = New System.Windows.Forms.Label()
        Me.lblStatusDescrH = New System.Windows.Forms.Label()
        Me.lblStatusDescrF = New System.Windows.Forms.Label()
        Me.lblStatusDescrD = New System.Windows.Forms.Label()
        Me.lblStatusDescrB = New System.Windows.Forms.Label()
        Me.lblStatusDescrSafe = New System.Windows.Forms.Label()
        Me.lblStatusDescrReader = New System.Windows.Forms.Label()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.lblStatusDescrDa = New System.Windows.Forms.Label()
        Me.lblStatusDescrEa = New System.Windows.Forms.Label()
        Me.lblStatusDescrK = New System.Windows.Forms.Label()
        Me.lblStatusDescrI = New System.Windows.Forms.Label()
        Me.lblStatusDescrG = New System.Windows.Forms.Label()
        Me.lblStatusDescrE = New System.Windows.Forms.Label()
        Me.lblStatusDescrC = New System.Windows.Forms.Label()
        Me.lblStatusDescrA = New System.Windows.Forms.Label()
        Me.lblStatusDescrController = New System.Windows.Forms.Label()
        Me.lblStatusDescrFeeder = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.lblStatusDa = New System.Windows.Forms.Label()
        Me.lblStatusEa = New System.Windows.Forms.Label()
        Me.lblStatusDb = New System.Windows.Forms.Label()
        Me.lblStatusEb = New System.Windows.Forms.Label()
        Me.lblStatusL = New System.Windows.Forms.Label()
        Me.lblStatusK = New System.Windows.Forms.Label()
        Me.lblStatusJ = New System.Windows.Forms.Label()
        Me.lblStatusI = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.lblStatusH = New System.Windows.Forms.Label()
        Me.lblStatusG = New System.Windows.Forms.Label()
        Me.lblStatusF = New System.Windows.Forms.Label()
        Me.lblStatusE = New System.Windows.Forms.Label()
        Me.lblStatusD = New System.Windows.Forms.Label()
        Me.lblStatusC = New System.Windows.Forms.Label()
        Me.lblStatusB = New System.Windows.Forms.Label()
        Me.lblStatusA = New System.Windows.Forms.Label()
        Me.lblStatusSafe = New System.Windows.Forms.Label()
        Me.lblStatusReader = New System.Windows.Forms.Label()
        Me.lblStatusController = New System.Windows.Forms.Label()
        Me.lblStatusFeeder = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.grpSerialNumber = New System.Windows.Forms.GroupBox()
        Me.lblSerialL = New System.Windows.Forms.Label()
        Me.LabelL = New System.Windows.Forms.Label()
        Me.txtSerialL = New System.Windows.Forms.TextBox()
        Me.lblSerialK = New System.Windows.Forms.Label()
        Me.LabelK = New System.Windows.Forms.Label()
        Me.txtSerialK = New System.Windows.Forms.TextBox()
        Me.lblSerialJ = New System.Windows.Forms.Label()
        Me.LabelJ = New System.Windows.Forms.Label()
        Me.txtSerialJ = New System.Windows.Forms.TextBox()
        Me.lblSerialI = New System.Windows.Forms.Label()
        Me.LabelI = New System.Windows.Forms.Label()
        Me.txtSerialI = New System.Windows.Forms.TextBox()
        Me.lblSerialH = New System.Windows.Forms.Label()
        Me.LabelH = New System.Windows.Forms.Label()
        Me.txtSerialH = New System.Windows.Forms.TextBox()
        Me.lblSerialG = New System.Windows.Forms.Label()
        Me.LabelG = New System.Windows.Forms.Label()
        Me.txtSerialG = New System.Windows.Forms.TextBox()
        Me.lblSerialF = New System.Windows.Forms.Label()
        Me.LabelF = New System.Windows.Forms.Label()
        Me.txtSerialF = New System.Windows.Forms.TextBox()
        Me.lblSerialE = New System.Windows.Forms.Label()
        Me.LabelE = New System.Windows.Forms.Label()
        Me.txtSerialE = New System.Windows.Forms.TextBox()
        Me.lblSerialD = New System.Windows.Forms.Label()
        Me.LabelD = New System.Windows.Forms.Label()
        Me.txtSerialD = New System.Windows.Forms.TextBox()
        Me.lblSerialC = New System.Windows.Forms.Label()
        Me.LabelC = New System.Windows.Forms.Label()
        Me.txtSerialC = New System.Windows.Forms.TextBox()
        Me.lblSerialB = New System.Windows.Forms.Label()
        Me.LabelB = New System.Windows.Forms.Label()
        Me.txtSerialB = New System.Windows.Forms.TextBox()
        Me.lblSerialA = New System.Windows.Forms.Label()
        Me.LabelA = New System.Windows.Forms.Label()
        Me.txtSerialA = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.lblMacchina = New System.Windows.Forms.Label()
        Me.txtMatricola = New System.Windows.Forms.TextBox()
        Me.txtBagBarcode = New System.Windows.Forms.TextBox()
        Me.picLogo = New System.Windows.Forms.PictureBox()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ConnectionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuConnect = New System.Windows.Forms.ToolStripMenuItem()
        Me.DisconnectToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MainCommandsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOpen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDeposit = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuWithdrawal = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExtStatus = New System.Windows.Forms.ToolStripMenuItem()
        Me.ErrorLOGToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GetDetailsOfNotesClassToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ErrorLogAnalisysToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComScopeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.msnuComScopeShow = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuComScopeHide = New System.Windows.Forms.ToolStripMenuItem()
        Me.myTimer = New System.Windows.Forms.Timer(Me.components)
        Me.cboCasNum = New System.Windows.Forms.ComboBox()
        Me.trvErrorLog = New System.Windows.Forms.TreeView()
        Me.pgbErrorLog = New System.Windows.Forms.ProgressBar()
        Me.btnEnd = New System.Windows.Forms.Button()
        CType(Me.Grid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpCashData.SuspendLayout()
        Me.grpExtendedStatus.SuspendLayout()
        Me.grpSerialNumber.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.picLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdPrincipale
        '
        Me.cmdPrincipale.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdPrincipale.Location = New System.Drawing.Point(644, 66)
        Me.cmdPrincipale.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdPrincipale.Name = "cmdPrincipale"
        Me.cmdPrincipale.Size = New System.Drawing.Size(333, 89)
        Me.cmdPrincipale.TabIndex = 24
        Me.cmdPrincipale.Text = "Button1"
        Me.cmdPrincipale.UseVisualStyleBackColor = True
        Me.cmdPrincipale.Visible = False
        '
        'lblMessaggio
        '
        Me.lblMessaggio.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessaggio.ForeColor = System.Drawing.Color.White
        Me.lblMessaggio.Location = New System.Drawing.Point(11, 33)
        Me.lblMessaggio.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblMessaggio.Name = "lblMessaggio"
        Me.lblMessaggio.Size = New System.Drawing.Size(491, 117)
        Me.lblMessaggio.TabIndex = 23
        Me.lblMessaggio.Text = "Label1"
        '
        'Grid1
        '
        Me.Grid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Grid1.ColumnHeadersVisible = False
        Me.Grid1.Location = New System.Drawing.Point(644, 374)
        Me.Grid1.Margin = New System.Windows.Forms.Padding(4)
        Me.Grid1.Name = "Grid1"
        Me.Grid1.RowHeadersVisible = False
        Me.Grid1.Size = New System.Drawing.Size(249, 76)
        Me.Grid1.TabIndex = 26
        Me.Grid1.Visible = False
        '
        'txtSerialNumber
        '
        Me.txtSerialNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSerialNumber.Location = New System.Drawing.Point(361, 293)
        Me.txtSerialNumber.Margin = New System.Windows.Forms.Padding(4)
        Me.txtSerialNumber.Name = "txtSerialNumber"
        Me.txtSerialNumber.Size = New System.Drawing.Size(423, 26)
        Me.txtSerialNumber.TabIndex = 29
        Me.txtSerialNumber.Visible = False
        '
        'cboCliente
        '
        Me.cboCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCliente.FormattingEnabled = True
        Me.cboCliente.Location = New System.Drawing.Point(590, 253)
        Me.cboCliente.Margin = New System.Windows.Forms.Padding(4)
        Me.cboCliente.Name = "cboCliente"
        Me.cboCliente.Size = New System.Drawing.Size(272, 32)
        Me.cboCliente.TabIndex = 28
        Me.cboCliente.Visible = False
        '
        'cboIdProdotto
        '
        Me.cboIdProdotto.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIdProdotto.FormattingEnabled = True
        Me.cboIdProdotto.Location = New System.Drawing.Point(301, 253)
        Me.cboIdProdotto.Margin = New System.Windows.Forms.Padding(4)
        Me.cboIdProdotto.Name = "cboIdProdotto"
        Me.cboIdProdotto.Size = New System.Drawing.Size(272, 32)
        Me.cboIdProdotto.TabIndex = 27
        Me.cboIdProdotto.Visible = False
        '
        'grpCashData
        '
        Me.grpCashData.Controls.Add(Me.lblCasLStatus)
        Me.grpCashData.Controls.Add(Me.lblCasLEnable)
        Me.grpCashData.Controls.Add(Me.lblCasLFree)
        Me.grpCashData.Controls.Add(Me.lblCasKStatus)
        Me.grpCashData.Controls.Add(Me.lblCasKEnable)
        Me.grpCashData.Controls.Add(Me.lblCasKFree)
        Me.grpCashData.Controls.Add(Me.lblCasJStatus)
        Me.grpCashData.Controls.Add(Me.lblCasJEnable)
        Me.grpCashData.Controls.Add(Me.lblCasJFree)
        Me.grpCashData.Controls.Add(Me.lblCasIStatus)
        Me.grpCashData.Controls.Add(Me.lblCasIEnable)
        Me.grpCashData.Controls.Add(Me.lblCasIFree)
        Me.grpCashData.Controls.Add(Me.lblCasHStatus)
        Me.grpCashData.Controls.Add(Me.lblCasHEnable)
        Me.grpCashData.Controls.Add(Me.lblCasHFree)
        Me.grpCashData.Controls.Add(Me.lblCasGStatus)
        Me.grpCashData.Controls.Add(Me.lblCasGEnable)
        Me.grpCashData.Controls.Add(Me.lblCasGFree)
        Me.grpCashData.Controls.Add(Me.lblCasFStatus)
        Me.grpCashData.Controls.Add(Me.lblCasFEnable)
        Me.grpCashData.Controls.Add(Me.lblCasFFree)
        Me.grpCashData.Controls.Add(Me.lblCasEStatus)
        Me.grpCashData.Controls.Add(Me.lblCasEEnable)
        Me.grpCashData.Controls.Add(Me.lblCasEFree)
        Me.grpCashData.Controls.Add(Me.lblCasDStatus)
        Me.grpCashData.Controls.Add(Me.lblCasDEnable)
        Me.grpCashData.Controls.Add(Me.lblCasDFree)
        Me.grpCashData.Controls.Add(Me.lblCasCStatus)
        Me.grpCashData.Controls.Add(Me.lblCasCEnable)
        Me.grpCashData.Controls.Add(Me.lblCasCFree)
        Me.grpCashData.Controls.Add(Me.lblCasBStatus)
        Me.grpCashData.Controls.Add(Me.lblCasBEnable)
        Me.grpCashData.Controls.Add(Me.lblCasBFree)
        Me.grpCashData.Controls.Add(Me.lblCasAStatus)
        Me.grpCashData.Controls.Add(Me.lblCasAEnable)
        Me.grpCashData.Controls.Add(Me.lblCasAFree)
        Me.grpCashData.Controls.Add(Me.Label5)
        Me.grpCashData.Controls.Add(Me.Label1)
        Me.grpCashData.Controls.Add(Me.Label32)
        Me.grpCashData.Controls.Add(Me.txtCasLBn)
        Me.grpCashData.Controls.Add(Me.txtCasKBn)
        Me.grpCashData.Controls.Add(Me.txtCasJBn)
        Me.grpCashData.Controls.Add(Me.txtCasIBn)
        Me.grpCashData.Controls.Add(Me.txtCasHBn)
        Me.grpCashData.Controls.Add(Me.txtCasGBn)
        Me.grpCashData.Controls.Add(Me.txtCasFBn)
        Me.grpCashData.Controls.Add(Me.txtCasEBn)
        Me.grpCashData.Controls.Add(Me.txtCasDBn)
        Me.grpCashData.Controls.Add(Me.txtCasCBn)
        Me.grpCashData.Controls.Add(Me.txtCasBBn)
        Me.grpCashData.Controls.Add(Me.txtCasABn)
        Me.grpCashData.Controls.Add(Me.txtCasLVal)
        Me.grpCashData.Controls.Add(Me.txtCasKVal)
        Me.grpCashData.Controls.Add(Me.txtCasJVal)
        Me.grpCashData.Controls.Add(Me.txtCasIVal)
        Me.grpCashData.Controls.Add(Me.txtCasHVal)
        Me.grpCashData.Controls.Add(Me.txtCasGVal)
        Me.grpCashData.Controls.Add(Me.txtCasFVal)
        Me.grpCashData.Controls.Add(Me.txtCasEVal)
        Me.grpCashData.Controls.Add(Me.txtCasDVal)
        Me.grpCashData.Controls.Add(Me.txtCasCVal)
        Me.grpCashData.Controls.Add(Me.txtCasBVal)
        Me.grpCashData.Controls.Add(Me.txtCasAVal)
        Me.grpCashData.Controls.Add(Me.txtCasLDen)
        Me.grpCashData.Controls.Add(Me.txtCasKDen)
        Me.grpCashData.Controls.Add(Me.txtCasJDen)
        Me.grpCashData.Controls.Add(Me.txtCasIDen)
        Me.grpCashData.Controls.Add(Me.txtCasHDen)
        Me.grpCashData.Controls.Add(Me.txtCasGDen)
        Me.grpCashData.Controls.Add(Me.txtCasFDen)
        Me.grpCashData.Controls.Add(Me.txtCasEDen)
        Me.grpCashData.Controls.Add(Me.txtCasDDen)
        Me.grpCashData.Controls.Add(Me.txtCasCDen)
        Me.grpCashData.Controls.Add(Me.txtCasBDen)
        Me.grpCashData.Controls.Add(Me.txtCasADen)
        Me.grpCashData.Controls.Add(Me.Label18)
        Me.grpCashData.Controls.Add(Me.lblCasL)
        Me.grpCashData.Controls.Add(Me.lblCasK)
        Me.grpCashData.Controls.Add(Me.lblCasJ)
        Me.grpCashData.Controls.Add(Me.lblCasI)
        Me.grpCashData.Controls.Add(Me.lblCasH)
        Me.grpCashData.Controls.Add(Me.lblCasG)
        Me.grpCashData.Controls.Add(Me.lblCasF)
        Me.grpCashData.Controls.Add(Me.lblCasE)
        Me.grpCashData.Controls.Add(Me.lblCasD)
        Me.grpCashData.Controls.Add(Me.lblCasC)
        Me.grpCashData.Controls.Add(Me.lblCasB)
        Me.grpCashData.Controls.Add(Me.prbCasL)
        Me.grpCashData.Controls.Add(Me.prbCasK)
        Me.grpCashData.Controls.Add(Me.prbCasJ)
        Me.grpCashData.Controls.Add(Me.prbCasI)
        Me.grpCashData.Controls.Add(Me.prbCasH)
        Me.grpCashData.Controls.Add(Me.prbCasG)
        Me.grpCashData.Controls.Add(Me.prbCasF)
        Me.grpCashData.Controls.Add(Me.prbCasE)
        Me.grpCashData.Controls.Add(Me.prbCasD)
        Me.grpCashData.Controls.Add(Me.prbCasC)
        Me.grpCashData.Controls.Add(Me.prbCasB)
        Me.grpCashData.Controls.Add(Me.lblCasA)
        Me.grpCashData.Controls.Add(Me.Label4)
        Me.grpCashData.Controls.Add(Me.Label3)
        Me.grpCashData.Controls.Add(Me.Label2)
        Me.grpCashData.Controls.Add(Me.prbCasA)
        Me.grpCashData.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpCashData.ForeColor = System.Drawing.Color.White
        Me.grpCashData.Location = New System.Drawing.Point(72, 196)
        Me.grpCashData.Name = "grpCashData"
        Me.grpCashData.Size = New System.Drawing.Size(705, 320)
        Me.grpCashData.TabIndex = 31
        Me.grpCashData.TabStop = False
        Me.grpCashData.Text = "CASH DATA"
        Me.grpCashData.Visible = False
        '
        'lblCasLStatus
        '
        Me.lblCasLStatus.AutoSize = True
        Me.lblCasLStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasLStatus.Location = New System.Drawing.Point(218, 286)
        Me.lblCasLStatus.Name = "lblCasLStatus"
        Me.lblCasLStatus.Size = New System.Drawing.Size(16, 16)
        Me.lblCasLStatus.TabIndex = 101
        Me.lblCasLStatus.Text = "L"
        Me.lblCasLStatus.Visible = False
        '
        'lblCasLEnable
        '
        Me.lblCasLEnable.AutoSize = True
        Me.lblCasLEnable.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasLEnable.Location = New System.Drawing.Point(660, 286)
        Me.lblCasLEnable.Name = "lblCasLEnable"
        Me.lblCasLEnable.Size = New System.Drawing.Size(15, 16)
        Me.lblCasLEnable.TabIndex = 101
        Me.lblCasLEnable.Text = "L"
        Me.lblCasLEnable.Visible = False
        '
        'lblCasLFree
        '
        Me.lblCasLFree.AutoSize = True
        Me.lblCasLFree.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasLFree.Location = New System.Drawing.Point(616, 286)
        Me.lblCasLFree.Name = "lblCasLFree"
        Me.lblCasLFree.Size = New System.Drawing.Size(15, 16)
        Me.lblCasLFree.TabIndex = 101
        Me.lblCasLFree.Text = "L"
        Me.lblCasLFree.Visible = False
        '
        'lblCasKStatus
        '
        Me.lblCasKStatus.AutoSize = True
        Me.lblCasKStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasKStatus.Location = New System.Drawing.Point(218, 265)
        Me.lblCasKStatus.Name = "lblCasKStatus"
        Me.lblCasKStatus.Size = New System.Drawing.Size(17, 16)
        Me.lblCasKStatus.TabIndex = 100
        Me.lblCasKStatus.Text = "K"
        Me.lblCasKStatus.Visible = False
        '
        'lblCasKEnable
        '
        Me.lblCasKEnable.AutoSize = True
        Me.lblCasKEnable.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasKEnable.Location = New System.Drawing.Point(660, 265)
        Me.lblCasKEnable.Name = "lblCasKEnable"
        Me.lblCasKEnable.Size = New System.Drawing.Size(16, 16)
        Me.lblCasKEnable.TabIndex = 100
        Me.lblCasKEnable.Text = "K"
        Me.lblCasKEnable.Visible = False
        '
        'lblCasKFree
        '
        Me.lblCasKFree.AutoSize = True
        Me.lblCasKFree.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasKFree.Location = New System.Drawing.Point(616, 265)
        Me.lblCasKFree.Name = "lblCasKFree"
        Me.lblCasKFree.Size = New System.Drawing.Size(16, 16)
        Me.lblCasKFree.TabIndex = 100
        Me.lblCasKFree.Text = "K"
        Me.lblCasKFree.Visible = False
        '
        'lblCasJStatus
        '
        Me.lblCasJStatus.AutoSize = True
        Me.lblCasJStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasJStatus.Location = New System.Drawing.Point(218, 244)
        Me.lblCasJStatus.Name = "lblCasJStatus"
        Me.lblCasJStatus.Size = New System.Drawing.Size(16, 16)
        Me.lblCasJStatus.TabIndex = 99
        Me.lblCasJStatus.Text = "J"
        Me.lblCasJStatus.Visible = False
        '
        'lblCasJEnable
        '
        Me.lblCasJEnable.AutoSize = True
        Me.lblCasJEnable.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasJEnable.Location = New System.Drawing.Point(660, 244)
        Me.lblCasJEnable.Name = "lblCasJEnable"
        Me.lblCasJEnable.Size = New System.Drawing.Size(15, 16)
        Me.lblCasJEnable.TabIndex = 99
        Me.lblCasJEnable.Text = "J"
        Me.lblCasJEnable.Visible = False
        '
        'lblCasJFree
        '
        Me.lblCasJFree.AutoSize = True
        Me.lblCasJFree.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasJFree.Location = New System.Drawing.Point(616, 244)
        Me.lblCasJFree.Name = "lblCasJFree"
        Me.lblCasJFree.Size = New System.Drawing.Size(15, 16)
        Me.lblCasJFree.TabIndex = 99
        Me.lblCasJFree.Text = "J"
        Me.lblCasJFree.Visible = False
        '
        'lblCasIStatus
        '
        Me.lblCasIStatus.AutoSize = True
        Me.lblCasIStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasIStatus.Location = New System.Drawing.Point(218, 223)
        Me.lblCasIStatus.Name = "lblCasIStatus"
        Me.lblCasIStatus.Size = New System.Drawing.Size(12, 16)
        Me.lblCasIStatus.TabIndex = 98
        Me.lblCasIStatus.Text = "I"
        Me.lblCasIStatus.Visible = False
        '
        'lblCasIEnable
        '
        Me.lblCasIEnable.AutoSize = True
        Me.lblCasIEnable.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasIEnable.Location = New System.Drawing.Point(660, 223)
        Me.lblCasIEnable.Name = "lblCasIEnable"
        Me.lblCasIEnable.Size = New System.Drawing.Size(11, 16)
        Me.lblCasIEnable.TabIndex = 98
        Me.lblCasIEnable.Text = "I"
        Me.lblCasIEnable.Visible = False
        '
        'lblCasIFree
        '
        Me.lblCasIFree.AutoSize = True
        Me.lblCasIFree.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasIFree.Location = New System.Drawing.Point(616, 223)
        Me.lblCasIFree.Name = "lblCasIFree"
        Me.lblCasIFree.Size = New System.Drawing.Size(11, 16)
        Me.lblCasIFree.TabIndex = 98
        Me.lblCasIFree.Text = "I"
        Me.lblCasIFree.Visible = False
        '
        'lblCasHStatus
        '
        Me.lblCasHStatus.AutoSize = True
        Me.lblCasHStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasHStatus.Location = New System.Drawing.Point(218, 202)
        Me.lblCasHStatus.Name = "lblCasHStatus"
        Me.lblCasHStatus.Size = New System.Drawing.Size(19, 16)
        Me.lblCasHStatus.TabIndex = 97
        Me.lblCasHStatus.Text = "H"
        Me.lblCasHStatus.Visible = False
        '
        'lblCasHEnable
        '
        Me.lblCasHEnable.AutoSize = True
        Me.lblCasHEnable.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasHEnable.Location = New System.Drawing.Point(660, 202)
        Me.lblCasHEnable.Name = "lblCasHEnable"
        Me.lblCasHEnable.Size = New System.Drawing.Size(18, 16)
        Me.lblCasHEnable.TabIndex = 97
        Me.lblCasHEnable.Text = "H"
        Me.lblCasHEnable.Visible = False
        '
        'lblCasHFree
        '
        Me.lblCasHFree.AutoSize = True
        Me.lblCasHFree.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasHFree.Location = New System.Drawing.Point(616, 202)
        Me.lblCasHFree.Name = "lblCasHFree"
        Me.lblCasHFree.Size = New System.Drawing.Size(18, 16)
        Me.lblCasHFree.TabIndex = 97
        Me.lblCasHFree.Text = "H"
        Me.lblCasHFree.Visible = False
        '
        'lblCasGStatus
        '
        Me.lblCasGStatus.AutoSize = True
        Me.lblCasGStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasGStatus.Location = New System.Drawing.Point(218, 181)
        Me.lblCasGStatus.Name = "lblCasGStatus"
        Me.lblCasGStatus.Size = New System.Drawing.Size(19, 16)
        Me.lblCasGStatus.TabIndex = 96
        Me.lblCasGStatus.Text = "G"
        Me.lblCasGStatus.Visible = False
        '
        'lblCasGEnable
        '
        Me.lblCasGEnable.AutoSize = True
        Me.lblCasGEnable.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasGEnable.Location = New System.Drawing.Point(660, 181)
        Me.lblCasGEnable.Name = "lblCasGEnable"
        Me.lblCasGEnable.Size = New System.Drawing.Size(18, 16)
        Me.lblCasGEnable.TabIndex = 96
        Me.lblCasGEnable.Text = "G"
        Me.lblCasGEnable.Visible = False
        '
        'lblCasGFree
        '
        Me.lblCasGFree.AutoSize = True
        Me.lblCasGFree.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasGFree.Location = New System.Drawing.Point(616, 181)
        Me.lblCasGFree.Name = "lblCasGFree"
        Me.lblCasGFree.Size = New System.Drawing.Size(18, 16)
        Me.lblCasGFree.TabIndex = 96
        Me.lblCasGFree.Text = "G"
        Me.lblCasGFree.Visible = False
        '
        'lblCasFStatus
        '
        Me.lblCasFStatus.AutoSize = True
        Me.lblCasFStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasFStatus.Location = New System.Drawing.Point(218, 160)
        Me.lblCasFStatus.Name = "lblCasFStatus"
        Me.lblCasFStatus.Size = New System.Drawing.Size(17, 16)
        Me.lblCasFStatus.TabIndex = 95
        Me.lblCasFStatus.Text = "F"
        Me.lblCasFStatus.Visible = False
        '
        'lblCasFEnable
        '
        Me.lblCasFEnable.AutoSize = True
        Me.lblCasFEnable.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasFEnable.Location = New System.Drawing.Point(660, 160)
        Me.lblCasFEnable.Name = "lblCasFEnable"
        Me.lblCasFEnable.Size = New System.Drawing.Size(16, 16)
        Me.lblCasFEnable.TabIndex = 95
        Me.lblCasFEnable.Text = "F"
        Me.lblCasFEnable.Visible = False
        '
        'lblCasFFree
        '
        Me.lblCasFFree.AutoSize = True
        Me.lblCasFFree.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasFFree.Location = New System.Drawing.Point(616, 160)
        Me.lblCasFFree.Name = "lblCasFFree"
        Me.lblCasFFree.Size = New System.Drawing.Size(16, 16)
        Me.lblCasFFree.TabIndex = 95
        Me.lblCasFFree.Text = "F"
        Me.lblCasFFree.Visible = False
        '
        'lblCasEStatus
        '
        Me.lblCasEStatus.AutoSize = True
        Me.lblCasEStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasEStatus.Location = New System.Drawing.Point(218, 139)
        Me.lblCasEStatus.Name = "lblCasEStatus"
        Me.lblCasEStatus.Size = New System.Drawing.Size(18, 16)
        Me.lblCasEStatus.TabIndex = 94
        Me.lblCasEStatus.Text = "E"
        Me.lblCasEStatus.Visible = False
        '
        'lblCasEEnable
        '
        Me.lblCasEEnable.AutoSize = True
        Me.lblCasEEnable.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasEEnable.Location = New System.Drawing.Point(660, 139)
        Me.lblCasEEnable.Name = "lblCasEEnable"
        Me.lblCasEEnable.Size = New System.Drawing.Size(17, 16)
        Me.lblCasEEnable.TabIndex = 94
        Me.lblCasEEnable.Text = "E"
        Me.lblCasEEnable.Visible = False
        '
        'lblCasEFree
        '
        Me.lblCasEFree.AutoSize = True
        Me.lblCasEFree.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasEFree.Location = New System.Drawing.Point(616, 139)
        Me.lblCasEFree.Name = "lblCasEFree"
        Me.lblCasEFree.Size = New System.Drawing.Size(17, 16)
        Me.lblCasEFree.TabIndex = 94
        Me.lblCasEFree.Text = "E"
        Me.lblCasEFree.Visible = False
        '
        'lblCasDStatus
        '
        Me.lblCasDStatus.AutoSize = True
        Me.lblCasDStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasDStatus.Location = New System.Drawing.Point(218, 118)
        Me.lblCasDStatus.Name = "lblCasDStatus"
        Me.lblCasDStatus.Size = New System.Drawing.Size(19, 16)
        Me.lblCasDStatus.TabIndex = 93
        Me.lblCasDStatus.Text = "D"
        Me.lblCasDStatus.Visible = False
        '
        'lblCasDEnable
        '
        Me.lblCasDEnable.AutoSize = True
        Me.lblCasDEnable.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasDEnable.Location = New System.Drawing.Point(660, 118)
        Me.lblCasDEnable.Name = "lblCasDEnable"
        Me.lblCasDEnable.Size = New System.Drawing.Size(18, 16)
        Me.lblCasDEnable.TabIndex = 93
        Me.lblCasDEnable.Text = "D"
        Me.lblCasDEnable.Visible = False
        '
        'lblCasDFree
        '
        Me.lblCasDFree.AutoSize = True
        Me.lblCasDFree.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasDFree.Location = New System.Drawing.Point(616, 118)
        Me.lblCasDFree.Name = "lblCasDFree"
        Me.lblCasDFree.Size = New System.Drawing.Size(18, 16)
        Me.lblCasDFree.TabIndex = 93
        Me.lblCasDFree.Text = "D"
        Me.lblCasDFree.Visible = False
        '
        'lblCasCStatus
        '
        Me.lblCasCStatus.AutoSize = True
        Me.lblCasCStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasCStatus.Location = New System.Drawing.Point(218, 97)
        Me.lblCasCStatus.Name = "lblCasCStatus"
        Me.lblCasCStatus.Size = New System.Drawing.Size(18, 16)
        Me.lblCasCStatus.TabIndex = 92
        Me.lblCasCStatus.Text = "C"
        Me.lblCasCStatus.Visible = False
        '
        'lblCasCEnable
        '
        Me.lblCasCEnable.AutoSize = True
        Me.lblCasCEnable.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasCEnable.Location = New System.Drawing.Point(660, 97)
        Me.lblCasCEnable.Name = "lblCasCEnable"
        Me.lblCasCEnable.Size = New System.Drawing.Size(17, 16)
        Me.lblCasCEnable.TabIndex = 92
        Me.lblCasCEnable.Text = "C"
        Me.lblCasCEnable.Visible = False
        '
        'lblCasCFree
        '
        Me.lblCasCFree.AutoSize = True
        Me.lblCasCFree.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasCFree.Location = New System.Drawing.Point(616, 97)
        Me.lblCasCFree.Name = "lblCasCFree"
        Me.lblCasCFree.Size = New System.Drawing.Size(17, 16)
        Me.lblCasCFree.TabIndex = 92
        Me.lblCasCFree.Text = "C"
        Me.lblCasCFree.Visible = False
        '
        'lblCasBStatus
        '
        Me.lblCasBStatus.AutoSize = True
        Me.lblCasBStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasBStatus.Location = New System.Drawing.Point(218, 76)
        Me.lblCasBStatus.Name = "lblCasBStatus"
        Me.lblCasBStatus.Size = New System.Drawing.Size(18, 16)
        Me.lblCasBStatus.TabIndex = 91
        Me.lblCasBStatus.Text = "B"
        Me.lblCasBStatus.Visible = False
        '
        'lblCasBEnable
        '
        Me.lblCasBEnable.AutoSize = True
        Me.lblCasBEnable.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasBEnable.Location = New System.Drawing.Point(660, 76)
        Me.lblCasBEnable.Name = "lblCasBEnable"
        Me.lblCasBEnable.Size = New System.Drawing.Size(17, 16)
        Me.lblCasBEnable.TabIndex = 91
        Me.lblCasBEnable.Text = "B"
        Me.lblCasBEnable.Visible = False
        '
        'lblCasBFree
        '
        Me.lblCasBFree.AutoSize = True
        Me.lblCasBFree.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasBFree.Location = New System.Drawing.Point(616, 76)
        Me.lblCasBFree.Name = "lblCasBFree"
        Me.lblCasBFree.Size = New System.Drawing.Size(17, 16)
        Me.lblCasBFree.TabIndex = 91
        Me.lblCasBFree.Text = "B"
        Me.lblCasBFree.Visible = False
        '
        'lblCasAStatus
        '
        Me.lblCasAStatus.AutoSize = True
        Me.lblCasAStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasAStatus.Location = New System.Drawing.Point(218, 55)
        Me.lblCasAStatus.Name = "lblCasAStatus"
        Me.lblCasAStatus.Size = New System.Drawing.Size(18, 16)
        Me.lblCasAStatus.TabIndex = 90
        Me.lblCasAStatus.Text = "A"
        Me.lblCasAStatus.Visible = False
        '
        'lblCasAEnable
        '
        Me.lblCasAEnable.AutoSize = True
        Me.lblCasAEnable.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasAEnable.Location = New System.Drawing.Point(660, 55)
        Me.lblCasAEnable.Name = "lblCasAEnable"
        Me.lblCasAEnable.Size = New System.Drawing.Size(17, 16)
        Me.lblCasAEnable.TabIndex = 90
        Me.lblCasAEnable.Text = "A"
        Me.lblCasAEnable.Visible = False
        '
        'lblCasAFree
        '
        Me.lblCasAFree.AutoSize = True
        Me.lblCasAFree.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasAFree.Location = New System.Drawing.Point(616, 55)
        Me.lblCasAFree.Name = "lblCasAFree"
        Me.lblCasAFree.Size = New System.Drawing.Size(17, 16)
        Me.lblCasAFree.TabIndex = 90
        Me.lblCasAFree.Text = "A"
        Me.lblCasAFree.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(218, 33)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(63, 16)
        Me.Label5.TabIndex = 89
        Me.Label5.Text = "STATUS"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(660, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(27, 16)
        Me.Label1.TabIndex = 89
        Me.Label1.Text = "EN"
        '
        'Label32
        '
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(606, 16)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(44, 39)
        Me.Label32.TabIndex = 89
        Me.Label32.Text = "FREE CAP"
        '
        'txtCasLBn
        '
        Me.txtCasLBn.AutoSize = True
        Me.txtCasLBn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasLBn.Location = New System.Drawing.Point(118, 286)
        Me.txtCasLBn.Name = "txtCasLBn"
        Me.txtCasLBn.Size = New System.Drawing.Size(15, 16)
        Me.txtCasLBn.TabIndex = 88
        Me.txtCasLBn.Text = "L"
        Me.txtCasLBn.Visible = False
        '
        'txtCasKBn
        '
        Me.txtCasKBn.AutoSize = True
        Me.txtCasKBn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasKBn.Location = New System.Drawing.Point(118, 265)
        Me.txtCasKBn.Name = "txtCasKBn"
        Me.txtCasKBn.Size = New System.Drawing.Size(16, 16)
        Me.txtCasKBn.TabIndex = 87
        Me.txtCasKBn.Text = "K"
        Me.txtCasKBn.Visible = False
        '
        'txtCasJBn
        '
        Me.txtCasJBn.AutoSize = True
        Me.txtCasJBn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasJBn.Location = New System.Drawing.Point(118, 244)
        Me.txtCasJBn.Name = "txtCasJBn"
        Me.txtCasJBn.Size = New System.Drawing.Size(15, 16)
        Me.txtCasJBn.TabIndex = 86
        Me.txtCasJBn.Text = "J"
        Me.txtCasJBn.Visible = False
        '
        'txtCasIBn
        '
        Me.txtCasIBn.AutoSize = True
        Me.txtCasIBn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasIBn.Location = New System.Drawing.Point(118, 223)
        Me.txtCasIBn.Name = "txtCasIBn"
        Me.txtCasIBn.Size = New System.Drawing.Size(11, 16)
        Me.txtCasIBn.TabIndex = 85
        Me.txtCasIBn.Text = "I"
        Me.txtCasIBn.Visible = False
        '
        'txtCasHBn
        '
        Me.txtCasHBn.AutoSize = True
        Me.txtCasHBn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasHBn.Location = New System.Drawing.Point(118, 202)
        Me.txtCasHBn.Name = "txtCasHBn"
        Me.txtCasHBn.Size = New System.Drawing.Size(18, 16)
        Me.txtCasHBn.TabIndex = 84
        Me.txtCasHBn.Text = "H"
        Me.txtCasHBn.Visible = False
        '
        'txtCasGBn
        '
        Me.txtCasGBn.AutoSize = True
        Me.txtCasGBn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasGBn.Location = New System.Drawing.Point(118, 181)
        Me.txtCasGBn.Name = "txtCasGBn"
        Me.txtCasGBn.Size = New System.Drawing.Size(18, 16)
        Me.txtCasGBn.TabIndex = 83
        Me.txtCasGBn.Text = "G"
        Me.txtCasGBn.Visible = False
        '
        'txtCasFBn
        '
        Me.txtCasFBn.AutoSize = True
        Me.txtCasFBn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasFBn.Location = New System.Drawing.Point(118, 160)
        Me.txtCasFBn.Name = "txtCasFBn"
        Me.txtCasFBn.Size = New System.Drawing.Size(16, 16)
        Me.txtCasFBn.TabIndex = 82
        Me.txtCasFBn.Text = "F"
        Me.txtCasFBn.Visible = False
        '
        'txtCasEBn
        '
        Me.txtCasEBn.AutoSize = True
        Me.txtCasEBn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasEBn.Location = New System.Drawing.Point(118, 139)
        Me.txtCasEBn.Name = "txtCasEBn"
        Me.txtCasEBn.Size = New System.Drawing.Size(17, 16)
        Me.txtCasEBn.TabIndex = 81
        Me.txtCasEBn.Text = "E"
        Me.txtCasEBn.Visible = False
        '
        'txtCasDBn
        '
        Me.txtCasDBn.AutoSize = True
        Me.txtCasDBn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasDBn.Location = New System.Drawing.Point(118, 118)
        Me.txtCasDBn.Name = "txtCasDBn"
        Me.txtCasDBn.Size = New System.Drawing.Size(18, 16)
        Me.txtCasDBn.TabIndex = 80
        Me.txtCasDBn.Text = "D"
        Me.txtCasDBn.Visible = False
        '
        'txtCasCBn
        '
        Me.txtCasCBn.AutoSize = True
        Me.txtCasCBn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasCBn.Location = New System.Drawing.Point(118, 97)
        Me.txtCasCBn.Name = "txtCasCBn"
        Me.txtCasCBn.Size = New System.Drawing.Size(17, 16)
        Me.txtCasCBn.TabIndex = 79
        Me.txtCasCBn.Text = "C"
        Me.txtCasCBn.Visible = False
        '
        'txtCasBBn
        '
        Me.txtCasBBn.AutoSize = True
        Me.txtCasBBn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasBBn.Location = New System.Drawing.Point(118, 76)
        Me.txtCasBBn.Name = "txtCasBBn"
        Me.txtCasBBn.Size = New System.Drawing.Size(17, 16)
        Me.txtCasBBn.TabIndex = 78
        Me.txtCasBBn.Text = "B"
        Me.txtCasBBn.Visible = False
        '
        'txtCasABn
        '
        Me.txtCasABn.AutoSize = True
        Me.txtCasABn.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasABn.Location = New System.Drawing.Point(118, 55)
        Me.txtCasABn.Name = "txtCasABn"
        Me.txtCasABn.Size = New System.Drawing.Size(17, 16)
        Me.txtCasABn.TabIndex = 77
        Me.txtCasABn.Text = "A"
        Me.txtCasABn.Visible = False
        '
        'txtCasLVal
        '
        Me.txtCasLVal.AutoSize = True
        Me.txtCasLVal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasLVal.Location = New System.Drawing.Point(158, 286)
        Me.txtCasLVal.Name = "txtCasLVal"
        Me.txtCasLVal.Size = New System.Drawing.Size(16, 16)
        Me.txtCasLVal.TabIndex = 76
        Me.txtCasLVal.Text = "L"
        Me.txtCasLVal.Visible = False
        '
        'txtCasKVal
        '
        Me.txtCasKVal.AutoSize = True
        Me.txtCasKVal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasKVal.Location = New System.Drawing.Point(158, 265)
        Me.txtCasKVal.Name = "txtCasKVal"
        Me.txtCasKVal.Size = New System.Drawing.Size(17, 16)
        Me.txtCasKVal.TabIndex = 75
        Me.txtCasKVal.Text = "K"
        Me.txtCasKVal.Visible = False
        '
        'txtCasJVal
        '
        Me.txtCasJVal.AutoSize = True
        Me.txtCasJVal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasJVal.Location = New System.Drawing.Point(158, 244)
        Me.txtCasJVal.Name = "txtCasJVal"
        Me.txtCasJVal.Size = New System.Drawing.Size(16, 16)
        Me.txtCasJVal.TabIndex = 74
        Me.txtCasJVal.Text = "J"
        Me.txtCasJVal.Visible = False
        '
        'txtCasIVal
        '
        Me.txtCasIVal.AutoSize = True
        Me.txtCasIVal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasIVal.Location = New System.Drawing.Point(158, 223)
        Me.txtCasIVal.Name = "txtCasIVal"
        Me.txtCasIVal.Size = New System.Drawing.Size(12, 16)
        Me.txtCasIVal.TabIndex = 73
        Me.txtCasIVal.Text = "I"
        Me.txtCasIVal.Visible = False
        '
        'txtCasHVal
        '
        Me.txtCasHVal.AutoSize = True
        Me.txtCasHVal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasHVal.Location = New System.Drawing.Point(158, 202)
        Me.txtCasHVal.Name = "txtCasHVal"
        Me.txtCasHVal.Size = New System.Drawing.Size(19, 16)
        Me.txtCasHVal.TabIndex = 72
        Me.txtCasHVal.Text = "H"
        Me.txtCasHVal.Visible = False
        '
        'txtCasGVal
        '
        Me.txtCasGVal.AutoSize = True
        Me.txtCasGVal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasGVal.Location = New System.Drawing.Point(158, 181)
        Me.txtCasGVal.Name = "txtCasGVal"
        Me.txtCasGVal.Size = New System.Drawing.Size(19, 16)
        Me.txtCasGVal.TabIndex = 71
        Me.txtCasGVal.Text = "G"
        Me.txtCasGVal.Visible = False
        '
        'txtCasFVal
        '
        Me.txtCasFVal.AutoSize = True
        Me.txtCasFVal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasFVal.Location = New System.Drawing.Point(158, 160)
        Me.txtCasFVal.Name = "txtCasFVal"
        Me.txtCasFVal.Size = New System.Drawing.Size(17, 16)
        Me.txtCasFVal.TabIndex = 70
        Me.txtCasFVal.Text = "F"
        Me.txtCasFVal.Visible = False
        '
        'txtCasEVal
        '
        Me.txtCasEVal.AutoSize = True
        Me.txtCasEVal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasEVal.Location = New System.Drawing.Point(158, 139)
        Me.txtCasEVal.Name = "txtCasEVal"
        Me.txtCasEVal.Size = New System.Drawing.Size(18, 16)
        Me.txtCasEVal.TabIndex = 69
        Me.txtCasEVal.Text = "E"
        Me.txtCasEVal.Visible = False
        '
        'txtCasDVal
        '
        Me.txtCasDVal.AutoSize = True
        Me.txtCasDVal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasDVal.Location = New System.Drawing.Point(158, 118)
        Me.txtCasDVal.Name = "txtCasDVal"
        Me.txtCasDVal.Size = New System.Drawing.Size(19, 16)
        Me.txtCasDVal.TabIndex = 68
        Me.txtCasDVal.Text = "D"
        Me.txtCasDVal.Visible = False
        '
        'txtCasCVal
        '
        Me.txtCasCVal.AutoSize = True
        Me.txtCasCVal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasCVal.Location = New System.Drawing.Point(158, 97)
        Me.txtCasCVal.Name = "txtCasCVal"
        Me.txtCasCVal.Size = New System.Drawing.Size(18, 16)
        Me.txtCasCVal.TabIndex = 67
        Me.txtCasCVal.Text = "C"
        Me.txtCasCVal.Visible = False
        '
        'txtCasBVal
        '
        Me.txtCasBVal.AutoSize = True
        Me.txtCasBVal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasBVal.Location = New System.Drawing.Point(158, 76)
        Me.txtCasBVal.Name = "txtCasBVal"
        Me.txtCasBVal.Size = New System.Drawing.Size(18, 16)
        Me.txtCasBVal.TabIndex = 66
        Me.txtCasBVal.Text = "B"
        Me.txtCasBVal.Visible = False
        '
        'txtCasAVal
        '
        Me.txtCasAVal.AutoSize = True
        Me.txtCasAVal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasAVal.Location = New System.Drawing.Point(158, 55)
        Me.txtCasAVal.Name = "txtCasAVal"
        Me.txtCasAVal.Size = New System.Drawing.Size(18, 16)
        Me.txtCasAVal.TabIndex = 65
        Me.txtCasAVal.Text = "A"
        Me.txtCasAVal.Visible = False
        '
        'txtCasLDen
        '
        Me.txtCasLDen.AutoSize = True
        Me.txtCasLDen.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasLDen.Location = New System.Drawing.Point(60, 286)
        Me.txtCasLDen.Name = "txtCasLDen"
        Me.txtCasLDen.Size = New System.Drawing.Size(15, 16)
        Me.txtCasLDen.TabIndex = 64
        Me.txtCasLDen.Text = "L"
        Me.txtCasLDen.Visible = False
        '
        'txtCasKDen
        '
        Me.txtCasKDen.AutoSize = True
        Me.txtCasKDen.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasKDen.Location = New System.Drawing.Point(60, 265)
        Me.txtCasKDen.Name = "txtCasKDen"
        Me.txtCasKDen.Size = New System.Drawing.Size(16, 16)
        Me.txtCasKDen.TabIndex = 63
        Me.txtCasKDen.Text = "K"
        Me.txtCasKDen.Visible = False
        '
        'txtCasJDen
        '
        Me.txtCasJDen.AutoSize = True
        Me.txtCasJDen.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasJDen.Location = New System.Drawing.Point(60, 244)
        Me.txtCasJDen.Name = "txtCasJDen"
        Me.txtCasJDen.Size = New System.Drawing.Size(15, 16)
        Me.txtCasJDen.TabIndex = 62
        Me.txtCasJDen.Text = "J"
        Me.txtCasJDen.Visible = False
        '
        'txtCasIDen
        '
        Me.txtCasIDen.AutoSize = True
        Me.txtCasIDen.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasIDen.Location = New System.Drawing.Point(60, 223)
        Me.txtCasIDen.Name = "txtCasIDen"
        Me.txtCasIDen.Size = New System.Drawing.Size(11, 16)
        Me.txtCasIDen.TabIndex = 61
        Me.txtCasIDen.Text = "I"
        Me.txtCasIDen.Visible = False
        '
        'txtCasHDen
        '
        Me.txtCasHDen.AutoSize = True
        Me.txtCasHDen.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasHDen.Location = New System.Drawing.Point(60, 202)
        Me.txtCasHDen.Name = "txtCasHDen"
        Me.txtCasHDen.Size = New System.Drawing.Size(18, 16)
        Me.txtCasHDen.TabIndex = 60
        Me.txtCasHDen.Text = "H"
        Me.txtCasHDen.Visible = False
        '
        'txtCasGDen
        '
        Me.txtCasGDen.AutoSize = True
        Me.txtCasGDen.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasGDen.Location = New System.Drawing.Point(60, 181)
        Me.txtCasGDen.Name = "txtCasGDen"
        Me.txtCasGDen.Size = New System.Drawing.Size(18, 16)
        Me.txtCasGDen.TabIndex = 59
        Me.txtCasGDen.Text = "G"
        Me.txtCasGDen.Visible = False
        '
        'txtCasFDen
        '
        Me.txtCasFDen.AutoSize = True
        Me.txtCasFDen.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasFDen.Location = New System.Drawing.Point(60, 160)
        Me.txtCasFDen.Name = "txtCasFDen"
        Me.txtCasFDen.Size = New System.Drawing.Size(16, 16)
        Me.txtCasFDen.TabIndex = 58
        Me.txtCasFDen.Text = "F"
        Me.txtCasFDen.Visible = False
        '
        'txtCasEDen
        '
        Me.txtCasEDen.AutoSize = True
        Me.txtCasEDen.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasEDen.Location = New System.Drawing.Point(60, 139)
        Me.txtCasEDen.Name = "txtCasEDen"
        Me.txtCasEDen.Size = New System.Drawing.Size(17, 16)
        Me.txtCasEDen.TabIndex = 57
        Me.txtCasEDen.Text = "E"
        Me.txtCasEDen.Visible = False
        '
        'txtCasDDen
        '
        Me.txtCasDDen.AutoSize = True
        Me.txtCasDDen.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasDDen.Location = New System.Drawing.Point(60, 118)
        Me.txtCasDDen.Name = "txtCasDDen"
        Me.txtCasDDen.Size = New System.Drawing.Size(18, 16)
        Me.txtCasDDen.TabIndex = 56
        Me.txtCasDDen.Text = "D"
        Me.txtCasDDen.Visible = False
        '
        'txtCasCDen
        '
        Me.txtCasCDen.AutoSize = True
        Me.txtCasCDen.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasCDen.Location = New System.Drawing.Point(60, 97)
        Me.txtCasCDen.Name = "txtCasCDen"
        Me.txtCasCDen.Size = New System.Drawing.Size(17, 16)
        Me.txtCasCDen.TabIndex = 55
        Me.txtCasCDen.Text = "C"
        Me.txtCasCDen.Visible = False
        '
        'txtCasBDen
        '
        Me.txtCasBDen.AutoSize = True
        Me.txtCasBDen.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasBDen.Location = New System.Drawing.Point(60, 76)
        Me.txtCasBDen.Name = "txtCasBDen"
        Me.txtCasBDen.Size = New System.Drawing.Size(17, 16)
        Me.txtCasBDen.TabIndex = 54
        Me.txtCasBDen.Text = "B"
        Me.txtCasBDen.Visible = False
        '
        'txtCasADen
        '
        Me.txtCasADen.AutoSize = True
        Me.txtCasADen.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCasADen.Location = New System.Drawing.Point(60, 55)
        Me.txtCasADen.Name = "txtCasADen"
        Me.txtCasADen.Size = New System.Drawing.Size(17, 16)
        Me.txtCasADen.TabIndex = 53
        Me.txtCasADen.Text = "A"
        Me.txtCasADen.Visible = False
        '
        'Label18
        '
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(112, 16)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(36, 39)
        Me.Label18.TabIndex = 52
        Me.Label18.Text = "TOT BN"
        '
        'lblCasL
        '
        Me.lblCasL.AutoSize = True
        Me.lblCasL.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasL.Location = New System.Drawing.Point(23, 286)
        Me.lblCasL.Name = "lblCasL"
        Me.lblCasL.Size = New System.Drawing.Size(15, 16)
        Me.lblCasL.TabIndex = 51
        Me.lblCasL.Text = "L"
        Me.lblCasL.Visible = False
        '
        'lblCasK
        '
        Me.lblCasK.AutoSize = True
        Me.lblCasK.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasK.Location = New System.Drawing.Point(23, 265)
        Me.lblCasK.Name = "lblCasK"
        Me.lblCasK.Size = New System.Drawing.Size(16, 16)
        Me.lblCasK.TabIndex = 50
        Me.lblCasK.Text = "K"
        Me.lblCasK.Visible = False
        '
        'lblCasJ
        '
        Me.lblCasJ.AutoSize = True
        Me.lblCasJ.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasJ.Location = New System.Drawing.Point(23, 244)
        Me.lblCasJ.Name = "lblCasJ"
        Me.lblCasJ.Size = New System.Drawing.Size(15, 16)
        Me.lblCasJ.TabIndex = 49
        Me.lblCasJ.Text = "J"
        Me.lblCasJ.Visible = False
        '
        'lblCasI
        '
        Me.lblCasI.AutoSize = True
        Me.lblCasI.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasI.Location = New System.Drawing.Point(23, 223)
        Me.lblCasI.Name = "lblCasI"
        Me.lblCasI.Size = New System.Drawing.Size(11, 16)
        Me.lblCasI.TabIndex = 48
        Me.lblCasI.Text = "I"
        Me.lblCasI.Visible = False
        '
        'lblCasH
        '
        Me.lblCasH.AutoSize = True
        Me.lblCasH.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasH.Location = New System.Drawing.Point(23, 202)
        Me.lblCasH.Name = "lblCasH"
        Me.lblCasH.Size = New System.Drawing.Size(18, 16)
        Me.lblCasH.TabIndex = 47
        Me.lblCasH.Text = "H"
        Me.lblCasH.Visible = False
        '
        'lblCasG
        '
        Me.lblCasG.AutoSize = True
        Me.lblCasG.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasG.Location = New System.Drawing.Point(23, 181)
        Me.lblCasG.Name = "lblCasG"
        Me.lblCasG.Size = New System.Drawing.Size(18, 16)
        Me.lblCasG.TabIndex = 46
        Me.lblCasG.Text = "G"
        Me.lblCasG.Visible = False
        '
        'lblCasF
        '
        Me.lblCasF.AutoSize = True
        Me.lblCasF.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasF.Location = New System.Drawing.Point(23, 160)
        Me.lblCasF.Name = "lblCasF"
        Me.lblCasF.Size = New System.Drawing.Size(16, 16)
        Me.lblCasF.TabIndex = 45
        Me.lblCasF.Text = "F"
        Me.lblCasF.Visible = False
        '
        'lblCasE
        '
        Me.lblCasE.AutoSize = True
        Me.lblCasE.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasE.Location = New System.Drawing.Point(23, 139)
        Me.lblCasE.Name = "lblCasE"
        Me.lblCasE.Size = New System.Drawing.Size(17, 16)
        Me.lblCasE.TabIndex = 44
        Me.lblCasE.Text = "E"
        Me.lblCasE.Visible = False
        '
        'lblCasD
        '
        Me.lblCasD.AutoSize = True
        Me.lblCasD.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasD.Location = New System.Drawing.Point(23, 118)
        Me.lblCasD.Name = "lblCasD"
        Me.lblCasD.Size = New System.Drawing.Size(18, 16)
        Me.lblCasD.TabIndex = 43
        Me.lblCasD.Text = "D"
        Me.lblCasD.Visible = False
        '
        'lblCasC
        '
        Me.lblCasC.AutoSize = True
        Me.lblCasC.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasC.Location = New System.Drawing.Point(23, 97)
        Me.lblCasC.Name = "lblCasC"
        Me.lblCasC.Size = New System.Drawing.Size(17, 16)
        Me.lblCasC.TabIndex = 42
        Me.lblCasC.Text = "C"
        Me.lblCasC.Visible = False
        '
        'lblCasB
        '
        Me.lblCasB.AutoSize = True
        Me.lblCasB.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasB.Location = New System.Drawing.Point(23, 76)
        Me.lblCasB.Name = "lblCasB"
        Me.lblCasB.Size = New System.Drawing.Size(17, 16)
        Me.lblCasB.TabIndex = 41
        Me.lblCasB.Text = "B"
        Me.lblCasB.Visible = False
        '
        'prbCasL
        '
        Me.prbCasL.Location = New System.Drawing.Point(292, 286)
        Me.prbCasL.Name = "prbCasL"
        Me.prbCasL.Size = New System.Drawing.Size(313, 17)
        Me.prbCasL.TabIndex = 38
        Me.prbCasL.Visible = False
        '
        'prbCasK
        '
        Me.prbCasK.Location = New System.Drawing.Point(292, 265)
        Me.prbCasK.Name = "prbCasK"
        Me.prbCasK.Size = New System.Drawing.Size(313, 17)
        Me.prbCasK.TabIndex = 35
        Me.prbCasK.Visible = False
        '
        'prbCasJ
        '
        Me.prbCasJ.Location = New System.Drawing.Point(292, 244)
        Me.prbCasJ.Name = "prbCasJ"
        Me.prbCasJ.Size = New System.Drawing.Size(313, 17)
        Me.prbCasJ.TabIndex = 32
        Me.prbCasJ.Visible = False
        '
        'prbCasI
        '
        Me.prbCasI.Location = New System.Drawing.Point(292, 223)
        Me.prbCasI.Name = "prbCasI"
        Me.prbCasI.Size = New System.Drawing.Size(313, 17)
        Me.prbCasI.TabIndex = 29
        Me.prbCasI.Visible = False
        '
        'prbCasH
        '
        Me.prbCasH.Location = New System.Drawing.Point(292, 202)
        Me.prbCasH.Name = "prbCasH"
        Me.prbCasH.Size = New System.Drawing.Size(313, 17)
        Me.prbCasH.TabIndex = 26
        Me.prbCasH.Visible = False
        '
        'prbCasG
        '
        Me.prbCasG.Location = New System.Drawing.Point(292, 181)
        Me.prbCasG.Name = "prbCasG"
        Me.prbCasG.Size = New System.Drawing.Size(313, 17)
        Me.prbCasG.TabIndex = 23
        Me.prbCasG.Visible = False
        '
        'prbCasF
        '
        Me.prbCasF.Location = New System.Drawing.Point(292, 160)
        Me.prbCasF.Name = "prbCasF"
        Me.prbCasF.Size = New System.Drawing.Size(313, 17)
        Me.prbCasF.TabIndex = 20
        Me.prbCasF.Visible = False
        '
        'prbCasE
        '
        Me.prbCasE.Location = New System.Drawing.Point(292, 139)
        Me.prbCasE.Name = "prbCasE"
        Me.prbCasE.Size = New System.Drawing.Size(313, 17)
        Me.prbCasE.TabIndex = 17
        Me.prbCasE.Visible = False
        '
        'prbCasD
        '
        Me.prbCasD.Location = New System.Drawing.Point(292, 118)
        Me.prbCasD.Name = "prbCasD"
        Me.prbCasD.Size = New System.Drawing.Size(313, 17)
        Me.prbCasD.TabIndex = 14
        Me.prbCasD.Visible = False
        '
        'prbCasC
        '
        Me.prbCasC.Location = New System.Drawing.Point(292, 97)
        Me.prbCasC.Name = "prbCasC"
        Me.prbCasC.Size = New System.Drawing.Size(313, 17)
        Me.prbCasC.TabIndex = 11
        Me.prbCasC.Visible = False
        '
        'prbCasB
        '
        Me.prbCasB.Location = New System.Drawing.Point(292, 76)
        Me.prbCasB.Name = "prbCasB"
        Me.prbCasB.Size = New System.Drawing.Size(313, 17)
        Me.prbCasB.TabIndex = 8
        Me.prbCasB.Visible = False
        '
        'lblCasA
        '
        Me.lblCasA.AutoSize = True
        Me.lblCasA.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCasA.Location = New System.Drawing.Point(23, 55)
        Me.lblCasA.Name = "lblCasA"
        Me.lblCasA.Size = New System.Drawing.Size(17, 16)
        Me.lblCasA.TabIndex = 7
        Me.lblCasA.Text = "A"
        Me.lblCasA.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(158, 33)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(33, 16)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "VAL"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(60, 33)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(37, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "DEN"
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(22, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 39)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "CASSETTE ID"
        '
        'prbCasA
        '
        Me.prbCasA.Location = New System.Drawing.Point(292, 55)
        Me.prbCasA.Name = "prbCasA"
        Me.prbCasA.Size = New System.Drawing.Size(313, 17)
        Me.prbCasA.TabIndex = 0
        Me.prbCasA.Visible = False
        '
        'grpExtendedStatus
        '
        Me.grpExtendedStatus.Controls.Add(Me.Label25)
        Me.grpExtendedStatus.Controls.Add(Me.Label26)
        Me.grpExtendedStatus.Controls.Add(Me.Label27)
        Me.grpExtendedStatus.Controls.Add(Me.Label22)
        Me.grpExtendedStatus.Controls.Add(Me.Label23)
        Me.grpExtendedStatus.Controls.Add(Me.Label24)
        Me.grpExtendedStatus.Controls.Add(Me.Label19)
        Me.grpExtendedStatus.Controls.Add(Me.Label20)
        Me.grpExtendedStatus.Controls.Add(Me.Label21)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusDescrDb)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusDescrEb)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusDescrL)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusDescrJ)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusDescrH)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusDescrF)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusDescrD)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusDescrB)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusDescrSafe)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusDescrReader)
        Me.grpExtendedStatus.Controls.Add(Me.Label82)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusDescrDa)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusDescrEa)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusDescrK)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusDescrI)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusDescrG)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusDescrE)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusDescrC)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusDescrA)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusDescrController)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusDescrFeeder)
        Me.grpExtendedStatus.Controls.Add(Me.Label17)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusDa)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusEa)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusDb)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusEb)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusL)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusK)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusJ)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusI)
        Me.grpExtendedStatus.Controls.Add(Me.Label29)
        Me.grpExtendedStatus.Controls.Add(Me.Label34)
        Me.grpExtendedStatus.Controls.Add(Me.Label35)
        Me.grpExtendedStatus.Controls.Add(Me.Label36)
        Me.grpExtendedStatus.Controls.Add(Me.Label37)
        Me.grpExtendedStatus.Controls.Add(Me.Label38)
        Me.grpExtendedStatus.Controls.Add(Me.Label39)
        Me.grpExtendedStatus.Controls.Add(Me.Label40)
        Me.grpExtendedStatus.Controls.Add(Me.Label41)
        Me.grpExtendedStatus.Controls.Add(Me.Label42)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusH)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusG)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusF)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusE)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusD)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusC)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusB)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusA)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusSafe)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusReader)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusController)
        Me.grpExtendedStatus.Controls.Add(Me.lblStatusFeeder)
        Me.grpExtendedStatus.Controls.Add(Me.Label55)
        Me.grpExtendedStatus.Controls.Add(Me.Label56)
        Me.grpExtendedStatus.Controls.Add(Me.Label57)
        Me.grpExtendedStatus.Controls.Add(Me.Label58)
        Me.grpExtendedStatus.Controls.Add(Me.Label59)
        Me.grpExtendedStatus.Controls.Add(Me.Label60)
        Me.grpExtendedStatus.Controls.Add(Me.Label61)
        Me.grpExtendedStatus.Controls.Add(Me.Label62)
        Me.grpExtendedStatus.Controls.Add(Me.Label63)
        Me.grpExtendedStatus.Controls.Add(Me.Label64)
        Me.grpExtendedStatus.Controls.Add(Me.Label65)
        Me.grpExtendedStatus.Controls.Add(Me.Label66)
        Me.grpExtendedStatus.Controls.Add(Me.Label67)
        Me.grpExtendedStatus.Controls.Add(Me.Label68)
        Me.grpExtendedStatus.ForeColor = System.Drawing.Color.White
        Me.grpExtendedStatus.Location = New System.Drawing.Point(236, 27)
        Me.grpExtendedStatus.Name = "grpExtendedStatus"
        Me.grpExtendedStatus.Size = New System.Drawing.Size(549, 139)
        Me.grpExtendedStatus.TabIndex = 32
        Me.grpExtendedStatus.TabStop = False
        Me.grpExtendedStatus.Text = "EXTENDED STATUS"
        Me.grpExtendedStatus.Visible = False
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.ForeColor = System.Drawing.Color.White
        Me.Label25.Location = New System.Drawing.Point(692, 185)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(62, 13)
        Me.Label25.TabIndex = 156
        Me.Label25.Text = "Descrizione"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.ForeColor = System.Drawing.Color.White
        Me.Label26.Location = New System.Drawing.Point(509, 20)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(32, 13)
        Me.Label26.TabIndex = 155
        Me.Label26.Text = "Stato"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.ForeColor = System.Drawing.Color.White
        Me.Label27.Location = New System.Drawing.Point(449, 20)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(42, 13)
        Me.Label27.TabIndex = 154
        Me.Label27.Text = "Modulo"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.ForeColor = System.Drawing.Color.White
        Me.Label22.Location = New System.Drawing.Point(528, 185)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(62, 13)
        Me.Label22.TabIndex = 153
        Me.Label22.Text = "Descrizione"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.ForeColor = System.Drawing.Color.White
        Me.Label23.Location = New System.Drawing.Point(396, 20)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(32, 13)
        Me.Label23.TabIndex = 152
        Me.Label23.Text = "Stato"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.ForeColor = System.Drawing.Color.White
        Me.Label24.Location = New System.Drawing.Point(336, 20)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(42, 13)
        Me.Label24.TabIndex = 151
        Me.Label24.Text = "Modulo"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.ForeColor = System.Drawing.Color.White
        Me.Label19.Location = New System.Drawing.Point(360, 185)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(62, 13)
        Me.Label19.TabIndex = 150
        Me.Label19.Text = "Descrizione"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.ForeColor = System.Drawing.Color.White
        Me.Label20.Location = New System.Drawing.Point(283, 20)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(32, 13)
        Me.Label20.TabIndex = 149
        Me.Label20.Text = "Stato"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(223, 20)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(42, 13)
        Me.Label21.TabIndex = 148
        Me.Label21.Text = "Modulo"
        '
        'lblStatusDescrDb
        '
        Me.lblStatusDescrDb.AutoSize = True
        Me.lblStatusDescrDb.ForeColor = System.Drawing.Color.White
        Me.lblStatusDescrDb.Location = New System.Drawing.Point(692, 290)
        Me.lblStatusDescrDb.Name = "lblStatusDescrDb"
        Me.lblStatusDescrDb.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusDescrDb.TabIndex = 147
        Me.lblStatusDescrDb.Text = "xx"
        '
        'lblStatusDescrEb
        '
        Me.lblStatusDescrEb.AutoSize = True
        Me.lblStatusDescrEb.ForeColor = System.Drawing.Color.White
        Me.lblStatusDescrEb.Location = New System.Drawing.Point(692, 264)
        Me.lblStatusDescrEb.Name = "lblStatusDescrEb"
        Me.lblStatusDescrEb.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusDescrEb.TabIndex = 146
        Me.lblStatusDescrEb.Text = "xx"
        '
        'lblStatusDescrL
        '
        Me.lblStatusDescrL.AutoSize = True
        Me.lblStatusDescrL.ForeColor = System.Drawing.Color.White
        Me.lblStatusDescrL.Location = New System.Drawing.Point(692, 238)
        Me.lblStatusDescrL.Name = "lblStatusDescrL"
        Me.lblStatusDescrL.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusDescrL.TabIndex = 145
        Me.lblStatusDescrL.Text = "xx"
        '
        'lblStatusDescrJ
        '
        Me.lblStatusDescrJ.AutoSize = True
        Me.lblStatusDescrJ.ForeColor = System.Drawing.Color.White
        Me.lblStatusDescrJ.Location = New System.Drawing.Point(692, 212)
        Me.lblStatusDescrJ.Name = "lblStatusDescrJ"
        Me.lblStatusDescrJ.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusDescrJ.TabIndex = 144
        Me.lblStatusDescrJ.Text = "xx"
        '
        'lblStatusDescrH
        '
        Me.lblStatusDescrH.AutoSize = True
        Me.lblStatusDescrH.ForeColor = System.Drawing.Color.White
        Me.lblStatusDescrH.Location = New System.Drawing.Point(360, 290)
        Me.lblStatusDescrH.Name = "lblStatusDescrH"
        Me.lblStatusDescrH.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusDescrH.TabIndex = 143
        Me.lblStatusDescrH.Text = "xx"
        '
        'lblStatusDescrF
        '
        Me.lblStatusDescrF.AutoSize = True
        Me.lblStatusDescrF.ForeColor = System.Drawing.Color.White
        Me.lblStatusDescrF.Location = New System.Drawing.Point(360, 264)
        Me.lblStatusDescrF.Name = "lblStatusDescrF"
        Me.lblStatusDescrF.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusDescrF.TabIndex = 142
        Me.lblStatusDescrF.Text = "xx"
        '
        'lblStatusDescrD
        '
        Me.lblStatusDescrD.AutoSize = True
        Me.lblStatusDescrD.ForeColor = System.Drawing.Color.White
        Me.lblStatusDescrD.Location = New System.Drawing.Point(360, 238)
        Me.lblStatusDescrD.Name = "lblStatusDescrD"
        Me.lblStatusDescrD.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusDescrD.TabIndex = 141
        Me.lblStatusDescrD.Text = "xx"
        '
        'lblStatusDescrB
        '
        Me.lblStatusDescrB.AutoSize = True
        Me.lblStatusDescrB.ForeColor = System.Drawing.Color.White
        Me.lblStatusDescrB.Location = New System.Drawing.Point(360, 212)
        Me.lblStatusDescrB.Name = "lblStatusDescrB"
        Me.lblStatusDescrB.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusDescrB.TabIndex = 140
        Me.lblStatusDescrB.Text = "xx"
        '
        'lblStatusDescrSafe
        '
        Me.lblStatusDescrSafe.AutoSize = True
        Me.lblStatusDescrSafe.ForeColor = System.Drawing.Color.White
        Me.lblStatusDescrSafe.Location = New System.Drawing.Point(28, 290)
        Me.lblStatusDescrSafe.Name = "lblStatusDescrSafe"
        Me.lblStatusDescrSafe.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusDescrSafe.TabIndex = 139
        Me.lblStatusDescrSafe.Text = "xx"
        '
        'lblStatusDescrReader
        '
        Me.lblStatusDescrReader.AutoSize = True
        Me.lblStatusDescrReader.ForeColor = System.Drawing.Color.White
        Me.lblStatusDescrReader.Location = New System.Drawing.Point(28, 264)
        Me.lblStatusDescrReader.Name = "lblStatusDescrReader"
        Me.lblStatusDescrReader.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusDescrReader.TabIndex = 138
        Me.lblStatusDescrReader.Text = "xx"
        '
        'Label82
        '
        Me.Label82.AutoSize = True
        Me.Label82.ForeColor = System.Drawing.Color.White
        Me.Label82.Location = New System.Drawing.Point(194, 184)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(62, 13)
        Me.Label82.TabIndex = 137
        Me.Label82.Text = "Descrizione"
        '
        'lblStatusDescrDa
        '
        Me.lblStatusDescrDa.AutoSize = True
        Me.lblStatusDescrDa.ForeColor = System.Drawing.Color.White
        Me.lblStatusDescrDa.Location = New System.Drawing.Point(525, 290)
        Me.lblStatusDescrDa.Name = "lblStatusDescrDa"
        Me.lblStatusDescrDa.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusDescrDa.TabIndex = 136
        Me.lblStatusDescrDa.Text = "xx"
        '
        'lblStatusDescrEa
        '
        Me.lblStatusDescrEa.AutoSize = True
        Me.lblStatusDescrEa.ForeColor = System.Drawing.Color.White
        Me.lblStatusDescrEa.Location = New System.Drawing.Point(525, 264)
        Me.lblStatusDescrEa.Name = "lblStatusDescrEa"
        Me.lblStatusDescrEa.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusDescrEa.TabIndex = 135
        Me.lblStatusDescrEa.Text = "xx"
        '
        'lblStatusDescrK
        '
        Me.lblStatusDescrK.AutoSize = True
        Me.lblStatusDescrK.ForeColor = System.Drawing.Color.White
        Me.lblStatusDescrK.Location = New System.Drawing.Point(525, 238)
        Me.lblStatusDescrK.Name = "lblStatusDescrK"
        Me.lblStatusDescrK.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusDescrK.TabIndex = 134
        Me.lblStatusDescrK.Text = "xx"
        '
        'lblStatusDescrI
        '
        Me.lblStatusDescrI.AutoSize = True
        Me.lblStatusDescrI.ForeColor = System.Drawing.Color.White
        Me.lblStatusDescrI.Location = New System.Drawing.Point(525, 212)
        Me.lblStatusDescrI.Name = "lblStatusDescrI"
        Me.lblStatusDescrI.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusDescrI.TabIndex = 133
        Me.lblStatusDescrI.Text = "xx"
        '
        'lblStatusDescrG
        '
        Me.lblStatusDescrG.AutoSize = True
        Me.lblStatusDescrG.ForeColor = System.Drawing.Color.White
        Me.lblStatusDescrG.Location = New System.Drawing.Point(193, 290)
        Me.lblStatusDescrG.Name = "lblStatusDescrG"
        Me.lblStatusDescrG.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusDescrG.TabIndex = 132
        Me.lblStatusDescrG.Text = "xx"
        '
        'lblStatusDescrE
        '
        Me.lblStatusDescrE.AutoSize = True
        Me.lblStatusDescrE.ForeColor = System.Drawing.Color.White
        Me.lblStatusDescrE.Location = New System.Drawing.Point(193, 264)
        Me.lblStatusDescrE.Name = "lblStatusDescrE"
        Me.lblStatusDescrE.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusDescrE.TabIndex = 131
        Me.lblStatusDescrE.Text = "xx"
        '
        'lblStatusDescrC
        '
        Me.lblStatusDescrC.AutoSize = True
        Me.lblStatusDescrC.ForeColor = System.Drawing.Color.White
        Me.lblStatusDescrC.Location = New System.Drawing.Point(193, 238)
        Me.lblStatusDescrC.Name = "lblStatusDescrC"
        Me.lblStatusDescrC.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusDescrC.TabIndex = 130
        Me.lblStatusDescrC.Text = "xx"
        '
        'lblStatusDescrA
        '
        Me.lblStatusDescrA.AutoSize = True
        Me.lblStatusDescrA.ForeColor = System.Drawing.Color.White
        Me.lblStatusDescrA.Location = New System.Drawing.Point(193, 212)
        Me.lblStatusDescrA.Name = "lblStatusDescrA"
        Me.lblStatusDescrA.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusDescrA.TabIndex = 129
        Me.lblStatusDescrA.Text = "xx"
        '
        'lblStatusDescrController
        '
        Me.lblStatusDescrController.AutoSize = True
        Me.lblStatusDescrController.ForeColor = System.Drawing.Color.White
        Me.lblStatusDescrController.Location = New System.Drawing.Point(27, 238)
        Me.lblStatusDescrController.Name = "lblStatusDescrController"
        Me.lblStatusDescrController.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusDescrController.TabIndex = 128
        Me.lblStatusDescrController.Text = "xx"
        '
        'lblStatusDescrFeeder
        '
        Me.lblStatusDescrFeeder.AutoSize = True
        Me.lblStatusDescrFeeder.ForeColor = System.Drawing.Color.White
        Me.lblStatusDescrFeeder.Location = New System.Drawing.Point(27, 212)
        Me.lblStatusDescrFeeder.Name = "lblStatusDescrFeeder"
        Me.lblStatusDescrFeeder.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusDescrFeeder.TabIndex = 127
        Me.lblStatusDescrFeeder.Text = "xx"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.ForeColor = System.Drawing.Color.White
        Me.Label17.Location = New System.Drawing.Point(27, 184)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(62, 13)
        Me.Label17.TabIndex = 126
        Me.Label17.Text = "Descrizione"
        '
        'lblStatusDa
        '
        Me.lblStatusDa.AutoSize = True
        Me.lblStatusDa.ForeColor = System.Drawing.Color.White
        Me.lblStatusDa.Location = New System.Drawing.Point(396, 117)
        Me.lblStatusDa.Name = "lblStatusDa"
        Me.lblStatusDa.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusDa.TabIndex = 125
        Me.lblStatusDa.Text = "xx"
        '
        'lblStatusEa
        '
        Me.lblStatusEa.AutoSize = True
        Me.lblStatusEa.ForeColor = System.Drawing.Color.White
        Me.lblStatusEa.Location = New System.Drawing.Point(396, 91)
        Me.lblStatusEa.Name = "lblStatusEa"
        Me.lblStatusEa.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusEa.TabIndex = 124
        Me.lblStatusEa.Text = "xx"
        '
        'lblStatusDb
        '
        Me.lblStatusDb.AutoSize = True
        Me.lblStatusDb.Enabled = False
        Me.lblStatusDb.ForeColor = System.Drawing.Color.White
        Me.lblStatusDb.Location = New System.Drawing.Point(509, 117)
        Me.lblStatusDb.Name = "lblStatusDb"
        Me.lblStatusDb.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusDb.TabIndex = 123
        Me.lblStatusDb.Text = "xx"
        '
        'lblStatusEb
        '
        Me.lblStatusEb.AutoSize = True
        Me.lblStatusEb.Enabled = False
        Me.lblStatusEb.ForeColor = System.Drawing.Color.White
        Me.lblStatusEb.Location = New System.Drawing.Point(509, 91)
        Me.lblStatusEb.Name = "lblStatusEb"
        Me.lblStatusEb.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusEb.TabIndex = 122
        Me.lblStatusEb.Text = "xx"
        '
        'lblStatusL
        '
        Me.lblStatusL.AutoSize = True
        Me.lblStatusL.ForeColor = System.Drawing.Color.White
        Me.lblStatusL.Location = New System.Drawing.Point(509, 65)
        Me.lblStatusL.Name = "lblStatusL"
        Me.lblStatusL.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusL.TabIndex = 121
        Me.lblStatusL.Text = "xx"
        '
        'lblStatusK
        '
        Me.lblStatusK.AutoSize = True
        Me.lblStatusK.ForeColor = System.Drawing.Color.White
        Me.lblStatusK.Location = New System.Drawing.Point(396, 65)
        Me.lblStatusK.Name = "lblStatusK"
        Me.lblStatusK.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusK.TabIndex = 120
        Me.lblStatusK.Text = "xx"
        '
        'lblStatusJ
        '
        Me.lblStatusJ.AutoSize = True
        Me.lblStatusJ.ForeColor = System.Drawing.Color.White
        Me.lblStatusJ.Location = New System.Drawing.Point(509, 39)
        Me.lblStatusJ.Name = "lblStatusJ"
        Me.lblStatusJ.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusJ.TabIndex = 119
        Me.lblStatusJ.Text = "xx"
        '
        'lblStatusI
        '
        Me.lblStatusI.AutoSize = True
        Me.lblStatusI.ForeColor = System.Drawing.Color.White
        Me.lblStatusI.Location = New System.Drawing.Point(396, 39)
        Me.lblStatusI.Name = "lblStatusI"
        Me.lblStatusI.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusI.TabIndex = 118
        Me.lblStatusI.Text = "xx"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.ForeColor = System.Drawing.Color.White
        Me.Label29.Location = New System.Drawing.Point(180, 19)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(32, 13)
        Me.Label29.TabIndex = 117
        Me.Label29.Text = "Stato"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.ForeColor = System.Drawing.Color.White
        Me.Label34.Location = New System.Drawing.Point(336, 117)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(52, 13)
        Me.Label34.TabIndex = 112
        Me.Label34.Text = "Deposit a"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.ForeColor = System.Drawing.Color.White
        Me.Label35.Location = New System.Drawing.Point(336, 91)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(51, 13)
        Me.Label35.TabIndex = 111
        Me.Label35.Text = "Escrow a"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Enabled = False
        Me.Label36.ForeColor = System.Drawing.Color.White
        Me.Label36.Location = New System.Drawing.Point(449, 117)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(52, 13)
        Me.Label36.TabIndex = 110
        Me.Label36.Text = "Deposit b"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Enabled = False
        Me.Label37.ForeColor = System.Drawing.Color.White
        Me.Label37.Location = New System.Drawing.Point(449, 91)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(51, 13)
        Me.Label37.TabIndex = 109
        Me.Label37.Text = "Escrow b"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.ForeColor = System.Drawing.Color.White
        Me.Label38.Location = New System.Drawing.Point(449, 65)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(57, 13)
        Me.Label38.TabIndex = 108
        Me.Label38.Text = "Cassette L"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.ForeColor = System.Drawing.Color.White
        Me.Label39.Location = New System.Drawing.Point(336, 65)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(58, 13)
        Me.Label39.TabIndex = 107
        Me.Label39.Text = "Cassette K"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.ForeColor = System.Drawing.Color.White
        Me.Label40.Location = New System.Drawing.Point(449, 39)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(56, 13)
        Me.Label40.TabIndex = 106
        Me.Label40.Text = "Cassette J"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.ForeColor = System.Drawing.Color.White
        Me.Label41.Location = New System.Drawing.Point(336, 39)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(54, 13)
        Me.Label41.TabIndex = 105
        Me.Label41.Text = "Cassette I"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.ForeColor = System.Drawing.Color.White
        Me.Label42.Location = New System.Drawing.Point(117, 19)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(42, 13)
        Me.Label42.TabIndex = 104
        Me.Label42.Text = "Modulo"
        '
        'lblStatusH
        '
        Me.lblStatusH.AutoSize = True
        Me.lblStatusH.ForeColor = System.Drawing.Color.White
        Me.lblStatusH.Location = New System.Drawing.Point(283, 117)
        Me.lblStatusH.Name = "lblStatusH"
        Me.lblStatusH.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusH.TabIndex = 103
        Me.lblStatusH.Text = "xx"
        '
        'lblStatusG
        '
        Me.lblStatusG.AutoSize = True
        Me.lblStatusG.ForeColor = System.Drawing.Color.White
        Me.lblStatusG.Location = New System.Drawing.Point(180, 117)
        Me.lblStatusG.Name = "lblStatusG"
        Me.lblStatusG.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusG.TabIndex = 102
        Me.lblStatusG.Text = "xx"
        '
        'lblStatusF
        '
        Me.lblStatusF.AutoSize = True
        Me.lblStatusF.ForeColor = System.Drawing.Color.White
        Me.lblStatusF.Location = New System.Drawing.Point(283, 91)
        Me.lblStatusF.Name = "lblStatusF"
        Me.lblStatusF.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusF.TabIndex = 101
        Me.lblStatusF.Text = "xx"
        '
        'lblStatusE
        '
        Me.lblStatusE.AutoSize = True
        Me.lblStatusE.ForeColor = System.Drawing.Color.White
        Me.lblStatusE.Location = New System.Drawing.Point(180, 91)
        Me.lblStatusE.Name = "lblStatusE"
        Me.lblStatusE.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusE.TabIndex = 100
        Me.lblStatusE.Text = "xx"
        '
        'lblStatusD
        '
        Me.lblStatusD.AutoSize = True
        Me.lblStatusD.ForeColor = System.Drawing.Color.White
        Me.lblStatusD.Location = New System.Drawing.Point(283, 65)
        Me.lblStatusD.Name = "lblStatusD"
        Me.lblStatusD.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusD.TabIndex = 99
        Me.lblStatusD.Text = "xx"
        '
        'lblStatusC
        '
        Me.lblStatusC.AutoSize = True
        Me.lblStatusC.ForeColor = System.Drawing.Color.White
        Me.lblStatusC.Location = New System.Drawing.Point(180, 65)
        Me.lblStatusC.Name = "lblStatusC"
        Me.lblStatusC.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusC.TabIndex = 98
        Me.lblStatusC.Text = "xx"
        '
        'lblStatusB
        '
        Me.lblStatusB.AutoSize = True
        Me.lblStatusB.ForeColor = System.Drawing.Color.White
        Me.lblStatusB.Location = New System.Drawing.Point(283, 39)
        Me.lblStatusB.Name = "lblStatusB"
        Me.lblStatusB.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusB.TabIndex = 97
        Me.lblStatusB.Text = "xx"
        '
        'lblStatusA
        '
        Me.lblStatusA.AutoSize = True
        Me.lblStatusA.ForeColor = System.Drawing.Color.White
        Me.lblStatusA.Location = New System.Drawing.Point(180, 39)
        Me.lblStatusA.Name = "lblStatusA"
        Me.lblStatusA.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusA.TabIndex = 96
        Me.lblStatusA.Text = "xx"
        '
        'lblStatusSafe
        '
        Me.lblStatusSafe.AutoSize = True
        Me.lblStatusSafe.ForeColor = System.Drawing.Color.White
        Me.lblStatusSafe.Location = New System.Drawing.Point(67, 117)
        Me.lblStatusSafe.Name = "lblStatusSafe"
        Me.lblStatusSafe.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusSafe.TabIndex = 95
        Me.lblStatusSafe.Text = "xx"
        '
        'lblStatusReader
        '
        Me.lblStatusReader.AutoSize = True
        Me.lblStatusReader.ForeColor = System.Drawing.Color.White
        Me.lblStatusReader.Location = New System.Drawing.Point(67, 91)
        Me.lblStatusReader.Name = "lblStatusReader"
        Me.lblStatusReader.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusReader.TabIndex = 94
        Me.lblStatusReader.Text = "xx"
        '
        'lblStatusController
        '
        Me.lblStatusController.AutoSize = True
        Me.lblStatusController.ForeColor = System.Drawing.Color.White
        Me.lblStatusController.Location = New System.Drawing.Point(67, 65)
        Me.lblStatusController.Name = "lblStatusController"
        Me.lblStatusController.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusController.TabIndex = 93
        Me.lblStatusController.Text = "xx"
        '
        'lblStatusFeeder
        '
        Me.lblStatusFeeder.AutoSize = True
        Me.lblStatusFeeder.ForeColor = System.Drawing.Color.White
        Me.lblStatusFeeder.Location = New System.Drawing.Point(67, 39)
        Me.lblStatusFeeder.Name = "lblStatusFeeder"
        Me.lblStatusFeeder.Size = New System.Drawing.Size(17, 13)
        Me.lblStatusFeeder.TabIndex = 92
        Me.lblStatusFeeder.Text = "xx"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.ForeColor = System.Drawing.Color.White
        Me.Label55.Location = New System.Drawing.Point(67, 19)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(32, 13)
        Me.Label55.TabIndex = 91
        Me.Label55.Text = "Stato"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.ForeColor = System.Drawing.Color.White
        Me.Label56.Location = New System.Drawing.Point(223, 117)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(59, 13)
        Me.Label56.TabIndex = 90
        Me.Label56.Text = "Cassette H"
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.ForeColor = System.Drawing.Color.White
        Me.Label57.Location = New System.Drawing.Point(117, 117)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(59, 13)
        Me.Label57.TabIndex = 89
        Me.Label57.Text = "Cassette G"
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.ForeColor = System.Drawing.Color.White
        Me.Label58.Location = New System.Drawing.Point(223, 91)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(57, 13)
        Me.Label58.TabIndex = 88
        Me.Label58.Text = "Cassette F"
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.ForeColor = System.Drawing.Color.White
        Me.Label59.Location = New System.Drawing.Point(117, 91)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(58, 13)
        Me.Label59.TabIndex = 87
        Me.Label59.Text = "Cassette E"
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.ForeColor = System.Drawing.Color.White
        Me.Label60.Location = New System.Drawing.Point(223, 65)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(59, 13)
        Me.Label60.TabIndex = 86
        Me.Label60.Text = "Cassette D"
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.ForeColor = System.Drawing.Color.White
        Me.Label61.Location = New System.Drawing.Point(117, 65)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(58, 13)
        Me.Label61.TabIndex = 85
        Me.Label61.Text = "Cassette C"
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.ForeColor = System.Drawing.Color.White
        Me.Label62.Location = New System.Drawing.Point(223, 39)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(58, 13)
        Me.Label62.TabIndex = 84
        Me.Label62.Text = "Cassette B"
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.ForeColor = System.Drawing.Color.White
        Me.Label63.Location = New System.Drawing.Point(117, 39)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(58, 13)
        Me.Label63.TabIndex = 83
        Me.Label63.Text = "Cassette A"
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.ForeColor = System.Drawing.Color.White
        Me.Label64.Location = New System.Drawing.Point(7, 117)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(29, 13)
        Me.Label64.TabIndex = 82
        Me.Label64.Text = "Safe"
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.ForeColor = System.Drawing.Color.White
        Me.Label65.Location = New System.Drawing.Point(7, 91)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(42, 13)
        Me.Label65.TabIndex = 81
        Me.Label65.Text = "Reader"
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.ForeColor = System.Drawing.Color.White
        Me.Label66.Location = New System.Drawing.Point(7, 65)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(51, 13)
        Me.Label66.TabIndex = 80
        Me.Label66.Text = "Controller"
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.ForeColor = System.Drawing.Color.White
        Me.Label67.Location = New System.Drawing.Point(7, 39)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(40, 13)
        Me.Label67.TabIndex = 79
        Me.Label67.Text = "Feeder"
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.ForeColor = System.Drawing.Color.White
        Me.Label68.Location = New System.Drawing.Point(7, 19)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(42, 13)
        Me.Label68.TabIndex = 78
        Me.Label68.Text = "Modulo"
        '
        'grpSerialNumber
        '
        Me.grpSerialNumber.Controls.Add(Me.lblSerialL)
        Me.grpSerialNumber.Controls.Add(Me.LabelL)
        Me.grpSerialNumber.Controls.Add(Me.txtSerialL)
        Me.grpSerialNumber.Controls.Add(Me.lblSerialK)
        Me.grpSerialNumber.Controls.Add(Me.LabelK)
        Me.grpSerialNumber.Controls.Add(Me.txtSerialK)
        Me.grpSerialNumber.Controls.Add(Me.lblSerialJ)
        Me.grpSerialNumber.Controls.Add(Me.LabelJ)
        Me.grpSerialNumber.Controls.Add(Me.txtSerialJ)
        Me.grpSerialNumber.Controls.Add(Me.lblSerialI)
        Me.grpSerialNumber.Controls.Add(Me.LabelI)
        Me.grpSerialNumber.Controls.Add(Me.txtSerialI)
        Me.grpSerialNumber.Controls.Add(Me.lblSerialH)
        Me.grpSerialNumber.Controls.Add(Me.LabelH)
        Me.grpSerialNumber.Controls.Add(Me.txtSerialH)
        Me.grpSerialNumber.Controls.Add(Me.lblSerialG)
        Me.grpSerialNumber.Controls.Add(Me.LabelG)
        Me.grpSerialNumber.Controls.Add(Me.txtSerialG)
        Me.grpSerialNumber.Controls.Add(Me.lblSerialF)
        Me.grpSerialNumber.Controls.Add(Me.LabelF)
        Me.grpSerialNumber.Controls.Add(Me.txtSerialF)
        Me.grpSerialNumber.Controls.Add(Me.lblSerialE)
        Me.grpSerialNumber.Controls.Add(Me.LabelE)
        Me.grpSerialNumber.Controls.Add(Me.txtSerialE)
        Me.grpSerialNumber.Controls.Add(Me.lblSerialD)
        Me.grpSerialNumber.Controls.Add(Me.LabelD)
        Me.grpSerialNumber.Controls.Add(Me.txtSerialD)
        Me.grpSerialNumber.Controls.Add(Me.lblSerialC)
        Me.grpSerialNumber.Controls.Add(Me.LabelC)
        Me.grpSerialNumber.Controls.Add(Me.txtSerialC)
        Me.grpSerialNumber.Controls.Add(Me.lblSerialB)
        Me.grpSerialNumber.Controls.Add(Me.LabelB)
        Me.grpSerialNumber.Controls.Add(Me.txtSerialB)
        Me.grpSerialNumber.Controls.Add(Me.lblSerialA)
        Me.grpSerialNumber.Controls.Add(Me.LabelA)
        Me.grpSerialNumber.Controls.Add(Me.txtSerialA)
        Me.grpSerialNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpSerialNumber.ForeColor = System.Drawing.Color.White
        Me.grpSerialNumber.Location = New System.Drawing.Point(900, 430)
        Me.grpSerialNumber.Name = "grpSerialNumber"
        Me.grpSerialNumber.Size = New System.Drawing.Size(751, 244)
        Me.grpSerialNumber.TabIndex = 33
        Me.grpSerialNumber.TabStop = False
        Me.grpSerialNumber.Text = "CASSETTES SERIAL NUMBER"
        Me.grpSerialNumber.Visible = False
        '
        'lblSerialL
        '
        Me.lblSerialL.AutoSize = True
        Me.lblSerialL.Font = New System.Drawing.Font("Symbol", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.lblSerialL.ForeColor = System.Drawing.Color.Lime
        Me.lblSerialL.Location = New System.Drawing.Point(726, 187)
        Me.lblSerialL.Name = "lblSerialL"
        Me.lblSerialL.Size = New System.Drawing.Size(26, 30)
        Me.lblSerialL.TabIndex = 35
        Me.lblSerialL.Text = "Ö"
        Me.lblSerialL.Visible = False
        '
        'LabelL
        '
        Me.LabelL.AutoSize = True
        Me.LabelL.Location = New System.Drawing.Point(397, 194)
        Me.LabelL.Name = "LabelL"
        Me.LabelL.Size = New System.Drawing.Size(90, 20)
        Me.LabelL.TabIndex = 34
        Me.LabelL.Text = "Cassetto L:"
        '
        'txtSerialL
        '
        Me.txtSerialL.Location = New System.Drawing.Point(495, 191)
        Me.txtSerialL.MaxLength = 12
        Me.txtSerialL.Name = "txtSerialL"
        Me.txtSerialL.Size = New System.Drawing.Size(225, 26)
        Me.txtSerialL.TabIndex = 33
        '
        'lblSerialK
        '
        Me.lblSerialK.AutoSize = True
        Me.lblSerialK.Font = New System.Drawing.Font("Symbol", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.lblSerialK.ForeColor = System.Drawing.Color.Lime
        Me.lblSerialK.Location = New System.Drawing.Point(726, 155)
        Me.lblSerialK.Name = "lblSerialK"
        Me.lblSerialK.Size = New System.Drawing.Size(26, 30)
        Me.lblSerialK.TabIndex = 32
        Me.lblSerialK.Text = "Ö"
        Me.lblSerialK.Visible = False
        '
        'LabelK
        '
        Me.LabelK.AutoSize = True
        Me.LabelK.Location = New System.Drawing.Point(397, 162)
        Me.LabelK.Name = "LabelK"
        Me.LabelK.Size = New System.Drawing.Size(91, 20)
        Me.LabelK.TabIndex = 31
        Me.LabelK.Text = "Cassetto K:"
        '
        'txtSerialK
        '
        Me.txtSerialK.Location = New System.Drawing.Point(495, 159)
        Me.txtSerialK.MaxLength = 12
        Me.txtSerialK.Name = "txtSerialK"
        Me.txtSerialK.Size = New System.Drawing.Size(225, 26)
        Me.txtSerialK.TabIndex = 30
        '
        'lblSerialJ
        '
        Me.lblSerialJ.AutoSize = True
        Me.lblSerialJ.Font = New System.Drawing.Font("Symbol", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.lblSerialJ.ForeColor = System.Drawing.Color.Lime
        Me.lblSerialJ.Location = New System.Drawing.Point(726, 123)
        Me.lblSerialJ.Name = "lblSerialJ"
        Me.lblSerialJ.Size = New System.Drawing.Size(26, 30)
        Me.lblSerialJ.TabIndex = 29
        Me.lblSerialJ.Text = "Ö"
        Me.lblSerialJ.Visible = False
        '
        'LabelJ
        '
        Me.LabelJ.AutoSize = True
        Me.LabelJ.Location = New System.Drawing.Point(397, 130)
        Me.LabelJ.Name = "LabelJ"
        Me.LabelJ.Size = New System.Drawing.Size(89, 20)
        Me.LabelJ.TabIndex = 28
        Me.LabelJ.Text = "Cassetto J:"
        '
        'txtSerialJ
        '
        Me.txtSerialJ.Location = New System.Drawing.Point(495, 127)
        Me.txtSerialJ.MaxLength = 12
        Me.txtSerialJ.Name = "txtSerialJ"
        Me.txtSerialJ.Size = New System.Drawing.Size(225, 26)
        Me.txtSerialJ.TabIndex = 27
        '
        'lblSerialI
        '
        Me.lblSerialI.AutoSize = True
        Me.lblSerialI.Font = New System.Drawing.Font("Symbol", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.lblSerialI.ForeColor = System.Drawing.Color.Lime
        Me.lblSerialI.Location = New System.Drawing.Point(726, 91)
        Me.lblSerialI.Name = "lblSerialI"
        Me.lblSerialI.Size = New System.Drawing.Size(26, 30)
        Me.lblSerialI.TabIndex = 26
        Me.lblSerialI.Text = "Ö"
        Me.lblSerialI.Visible = False
        '
        'LabelI
        '
        Me.LabelI.AutoSize = True
        Me.LabelI.Location = New System.Drawing.Point(397, 98)
        Me.LabelI.Name = "LabelI"
        Me.LabelI.Size = New System.Drawing.Size(86, 20)
        Me.LabelI.TabIndex = 25
        Me.LabelI.Text = "Cassetto I:"
        '
        'txtSerialI
        '
        Me.txtSerialI.Location = New System.Drawing.Point(495, 95)
        Me.txtSerialI.MaxLength = 12
        Me.txtSerialI.Name = "txtSerialI"
        Me.txtSerialI.Size = New System.Drawing.Size(225, 26)
        Me.txtSerialI.TabIndex = 24
        '
        'lblSerialH
        '
        Me.lblSerialH.AutoSize = True
        Me.lblSerialH.Font = New System.Drawing.Font("Symbol", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.lblSerialH.ForeColor = System.Drawing.Color.Lime
        Me.lblSerialH.Location = New System.Drawing.Point(726, 59)
        Me.lblSerialH.Name = "lblSerialH"
        Me.lblSerialH.Size = New System.Drawing.Size(26, 30)
        Me.lblSerialH.TabIndex = 23
        Me.lblSerialH.Text = "Ö"
        Me.lblSerialH.Visible = False
        '
        'LabelH
        '
        Me.LabelH.AutoSize = True
        Me.LabelH.Location = New System.Drawing.Point(397, 66)
        Me.LabelH.Name = "LabelH"
        Me.LabelH.Size = New System.Drawing.Size(93, 20)
        Me.LabelH.TabIndex = 22
        Me.LabelH.Text = "Cassetto H:"
        '
        'txtSerialH
        '
        Me.txtSerialH.Location = New System.Drawing.Point(495, 63)
        Me.txtSerialH.MaxLength = 12
        Me.txtSerialH.Name = "txtSerialH"
        Me.txtSerialH.Size = New System.Drawing.Size(225, 26)
        Me.txtSerialH.TabIndex = 21
        '
        'lblSerialG
        '
        Me.lblSerialG.AutoSize = True
        Me.lblSerialG.Font = New System.Drawing.Font("Symbol", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.lblSerialG.ForeColor = System.Drawing.Color.Lime
        Me.lblSerialG.Location = New System.Drawing.Point(726, 27)
        Me.lblSerialG.Name = "lblSerialG"
        Me.lblSerialG.Size = New System.Drawing.Size(26, 30)
        Me.lblSerialG.TabIndex = 20
        Me.lblSerialG.Text = "Ö"
        Me.lblSerialG.Visible = False
        '
        'LabelG
        '
        Me.LabelG.AutoSize = True
        Me.LabelG.Location = New System.Drawing.Point(397, 34)
        Me.LabelG.Name = "LabelG"
        Me.LabelG.Size = New System.Drawing.Size(94, 20)
        Me.LabelG.TabIndex = 19
        Me.LabelG.Text = "Cassetto G:"
        '
        'txtSerialG
        '
        Me.txtSerialG.Location = New System.Drawing.Point(495, 31)
        Me.txtSerialG.MaxLength = 12
        Me.txtSerialG.Name = "txtSerialG"
        Me.txtSerialG.Size = New System.Drawing.Size(225, 26)
        Me.txtSerialG.TabIndex = 18
        '
        'lblSerialF
        '
        Me.lblSerialF.AutoSize = True
        Me.lblSerialF.Font = New System.Drawing.Font("Symbol", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.lblSerialF.ForeColor = System.Drawing.Color.Lime
        Me.lblSerialF.Location = New System.Drawing.Point(341, 187)
        Me.lblSerialF.Name = "lblSerialF"
        Me.lblSerialF.Size = New System.Drawing.Size(26, 30)
        Me.lblSerialF.TabIndex = 17
        Me.lblSerialF.Text = "Ö"
        Me.lblSerialF.Visible = False
        '
        'LabelF
        '
        Me.LabelF.AutoSize = True
        Me.LabelF.Location = New System.Drawing.Point(12, 194)
        Me.LabelF.Name = "LabelF"
        Me.LabelF.Size = New System.Drawing.Size(91, 20)
        Me.LabelF.TabIndex = 16
        Me.LabelF.Text = "Cassetto F:"
        '
        'txtSerialF
        '
        Me.txtSerialF.Location = New System.Drawing.Point(110, 191)
        Me.txtSerialF.MaxLength = 12
        Me.txtSerialF.Name = "txtSerialF"
        Me.txtSerialF.Size = New System.Drawing.Size(225, 26)
        Me.txtSerialF.TabIndex = 15
        '
        'lblSerialE
        '
        Me.lblSerialE.AutoSize = True
        Me.lblSerialE.Font = New System.Drawing.Font("Symbol", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.lblSerialE.ForeColor = System.Drawing.Color.Lime
        Me.lblSerialE.Location = New System.Drawing.Point(341, 155)
        Me.lblSerialE.Name = "lblSerialE"
        Me.lblSerialE.Size = New System.Drawing.Size(26, 30)
        Me.lblSerialE.TabIndex = 14
        Me.lblSerialE.Text = "Ö"
        Me.lblSerialE.Visible = False
        '
        'LabelE
        '
        Me.LabelE.AutoSize = True
        Me.LabelE.Location = New System.Drawing.Point(12, 162)
        Me.LabelE.Name = "LabelE"
        Me.LabelE.Size = New System.Drawing.Size(92, 20)
        Me.LabelE.TabIndex = 13
        Me.LabelE.Text = "Cassetto E:"
        '
        'txtSerialE
        '
        Me.txtSerialE.Location = New System.Drawing.Point(110, 159)
        Me.txtSerialE.MaxLength = 12
        Me.txtSerialE.Name = "txtSerialE"
        Me.txtSerialE.Size = New System.Drawing.Size(225, 26)
        Me.txtSerialE.TabIndex = 12
        '
        'lblSerialD
        '
        Me.lblSerialD.AutoSize = True
        Me.lblSerialD.Font = New System.Drawing.Font("Symbol", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.lblSerialD.ForeColor = System.Drawing.Color.Lime
        Me.lblSerialD.Location = New System.Drawing.Point(341, 123)
        Me.lblSerialD.Name = "lblSerialD"
        Me.lblSerialD.Size = New System.Drawing.Size(26, 30)
        Me.lblSerialD.TabIndex = 11
        Me.lblSerialD.Text = "Ö"
        Me.lblSerialD.Visible = False
        '
        'LabelD
        '
        Me.LabelD.AutoSize = True
        Me.LabelD.Location = New System.Drawing.Point(12, 130)
        Me.LabelD.Name = "LabelD"
        Me.LabelD.Size = New System.Drawing.Size(93, 20)
        Me.LabelD.TabIndex = 10
        Me.LabelD.Text = "Cassetto D:"
        '
        'txtSerialD
        '
        Me.txtSerialD.Location = New System.Drawing.Point(110, 127)
        Me.txtSerialD.MaxLength = 12
        Me.txtSerialD.Name = "txtSerialD"
        Me.txtSerialD.Size = New System.Drawing.Size(225, 26)
        Me.txtSerialD.TabIndex = 9
        '
        'lblSerialC
        '
        Me.lblSerialC.AutoSize = True
        Me.lblSerialC.Font = New System.Drawing.Font("Symbol", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.lblSerialC.ForeColor = System.Drawing.Color.Lime
        Me.lblSerialC.Location = New System.Drawing.Point(341, 91)
        Me.lblSerialC.Name = "lblSerialC"
        Me.lblSerialC.Size = New System.Drawing.Size(26, 30)
        Me.lblSerialC.TabIndex = 8
        Me.lblSerialC.Text = "Ö"
        Me.lblSerialC.Visible = False
        '
        'LabelC
        '
        Me.LabelC.AutoSize = True
        Me.LabelC.Location = New System.Drawing.Point(12, 98)
        Me.LabelC.Name = "LabelC"
        Me.LabelC.Size = New System.Drawing.Size(92, 20)
        Me.LabelC.TabIndex = 7
        Me.LabelC.Text = "Cassetto C:"
        '
        'txtSerialC
        '
        Me.txtSerialC.Location = New System.Drawing.Point(110, 95)
        Me.txtSerialC.MaxLength = 12
        Me.txtSerialC.Name = "txtSerialC"
        Me.txtSerialC.Size = New System.Drawing.Size(225, 26)
        Me.txtSerialC.TabIndex = 6
        '
        'lblSerialB
        '
        Me.lblSerialB.AutoSize = True
        Me.lblSerialB.Font = New System.Drawing.Font("Symbol", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.lblSerialB.ForeColor = System.Drawing.Color.Lime
        Me.lblSerialB.Location = New System.Drawing.Point(341, 59)
        Me.lblSerialB.Name = "lblSerialB"
        Me.lblSerialB.Size = New System.Drawing.Size(26, 30)
        Me.lblSerialB.TabIndex = 5
        Me.lblSerialB.Text = "Ö"
        Me.lblSerialB.Visible = False
        '
        'LabelB
        '
        Me.LabelB.AutoSize = True
        Me.LabelB.Location = New System.Drawing.Point(12, 66)
        Me.LabelB.Name = "LabelB"
        Me.LabelB.Size = New System.Drawing.Size(92, 20)
        Me.LabelB.TabIndex = 4
        Me.LabelB.Text = "Cassetto B:"
        '
        'txtSerialB
        '
        Me.txtSerialB.Location = New System.Drawing.Point(110, 63)
        Me.txtSerialB.MaxLength = 12
        Me.txtSerialB.Name = "txtSerialB"
        Me.txtSerialB.Size = New System.Drawing.Size(225, 26)
        Me.txtSerialB.TabIndex = 3
        '
        'lblSerialA
        '
        Me.lblSerialA.AutoSize = True
        Me.lblSerialA.Font = New System.Drawing.Font("Symbol", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.lblSerialA.ForeColor = System.Drawing.Color.Lime
        Me.lblSerialA.Location = New System.Drawing.Point(341, 27)
        Me.lblSerialA.Name = "lblSerialA"
        Me.lblSerialA.Size = New System.Drawing.Size(26, 30)
        Me.lblSerialA.TabIndex = 2
        Me.lblSerialA.Text = "Ö"
        Me.lblSerialA.Visible = False
        '
        'LabelA
        '
        Me.LabelA.AutoSize = True
        Me.LabelA.Location = New System.Drawing.Point(12, 34)
        Me.LabelA.Name = "LabelA"
        Me.LabelA.Size = New System.Drawing.Size(92, 20)
        Me.LabelA.TabIndex = 1
        Me.LabelA.Text = "Cassetto A:"
        '
        'txtSerialA
        '
        Me.txtSerialA.Location = New System.Drawing.Point(110, 31)
        Me.txtSerialA.MaxLength = 12
        Me.txtSerialA.Name = "txtSerialA"
        Me.txtSerialA.Size = New System.Drawing.Size(225, 26)
        Me.txtSerialA.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label28)
        Me.GroupBox1.Controls.Add(Me.Label33)
        Me.GroupBox1.Controls.Add(Me.Label45)
        Me.GroupBox1.Controls.Add(Me.Label48)
        Me.GroupBox1.Controls.Add(Me.Label49)
        Me.GroupBox1.Controls.Add(Me.Label50)
        Me.GroupBox1.Controls.Add(Me.Label51)
        Me.GroupBox1.Controls.Add(Me.Label52)
        Me.GroupBox1.Controls.Add(Me.Label53)
        Me.GroupBox1.Controls.Add(Me.Label54)
        Me.GroupBox1.Controls.Add(Me.Label69)
        Me.GroupBox1.Controls.Add(Me.Label70)
        Me.GroupBox1.Controls.Add(Me.Label71)
        Me.GroupBox1.Controls.Add(Me.Label72)
        Me.GroupBox1.Controls.Add(Me.Label73)
        Me.GroupBox1.Controls.Add(Me.Label74)
        Me.GroupBox1.Controls.Add(Me.Label75)
        Me.GroupBox1.Controls.Add(Me.Label76)
        Me.GroupBox1.Controls.Add(Me.Label77)
        Me.GroupBox1.Controls.Add(Me.Label78)
        Me.GroupBox1.Controls.Add(Me.Label79)
        Me.GroupBox1.Controls.Add(Me.Label80)
        Me.GroupBox1.Controls.Add(Me.Label81)
        Me.GroupBox1.Controls.Add(Me.Label83)
        Me.GroupBox1.Controls.Add(Me.Label84)
        Me.GroupBox1.ForeColor = System.Drawing.Color.White
        Me.GroupBox1.Location = New System.Drawing.Point(1095, 476)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(307, 139)
        Me.GroupBox1.TabIndex = 34
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "LAST OPERATION"
        Me.GroupBox1.Visible = False
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.ForeColor = System.Drawing.Color.White
        Me.Label28.Location = New System.Drawing.Point(692, 185)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(62, 13)
        Me.Label28.TabIndex = 156
        Me.Label28.Text = "Descrizione"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.ForeColor = System.Drawing.Color.White
        Me.Label33.Location = New System.Drawing.Point(528, 185)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(62, 13)
        Me.Label33.TabIndex = 153
        Me.Label33.Text = "Descrizione"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.ForeColor = System.Drawing.Color.White
        Me.Label45.Location = New System.Drawing.Point(360, 185)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(62, 13)
        Me.Label45.TabIndex = 150
        Me.Label45.Text = "Descrizione"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.ForeColor = System.Drawing.Color.White
        Me.Label48.Location = New System.Drawing.Point(692, 290)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(17, 13)
        Me.Label48.TabIndex = 147
        Me.Label48.Text = "xx"
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.ForeColor = System.Drawing.Color.White
        Me.Label49.Location = New System.Drawing.Point(692, 264)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(17, 13)
        Me.Label49.TabIndex = 146
        Me.Label49.Text = "xx"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.ForeColor = System.Drawing.Color.White
        Me.Label50.Location = New System.Drawing.Point(692, 238)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(17, 13)
        Me.Label50.TabIndex = 145
        Me.Label50.Text = "xx"
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.ForeColor = System.Drawing.Color.White
        Me.Label51.Location = New System.Drawing.Point(692, 212)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(17, 13)
        Me.Label51.TabIndex = 144
        Me.Label51.Text = "xx"
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.ForeColor = System.Drawing.Color.White
        Me.Label52.Location = New System.Drawing.Point(360, 290)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(17, 13)
        Me.Label52.TabIndex = 143
        Me.Label52.Text = "xx"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.ForeColor = System.Drawing.Color.White
        Me.Label53.Location = New System.Drawing.Point(360, 264)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(17, 13)
        Me.Label53.TabIndex = 142
        Me.Label53.Text = "xx"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.ForeColor = System.Drawing.Color.White
        Me.Label54.Location = New System.Drawing.Point(360, 238)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(17, 13)
        Me.Label54.TabIndex = 141
        Me.Label54.Text = "xx"
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.ForeColor = System.Drawing.Color.White
        Me.Label69.Location = New System.Drawing.Point(360, 212)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(17, 13)
        Me.Label69.TabIndex = 140
        Me.Label69.Text = "xx"
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.ForeColor = System.Drawing.Color.White
        Me.Label70.Location = New System.Drawing.Point(28, 290)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(17, 13)
        Me.Label70.TabIndex = 139
        Me.Label70.Text = "xx"
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.ForeColor = System.Drawing.Color.White
        Me.Label71.Location = New System.Drawing.Point(28, 264)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(17, 13)
        Me.Label71.TabIndex = 138
        Me.Label71.Text = "xx"
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.ForeColor = System.Drawing.Color.White
        Me.Label72.Location = New System.Drawing.Point(194, 184)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(62, 13)
        Me.Label72.TabIndex = 137
        Me.Label72.Text = "Descrizione"
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.ForeColor = System.Drawing.Color.White
        Me.Label73.Location = New System.Drawing.Point(525, 290)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(17, 13)
        Me.Label73.TabIndex = 136
        Me.Label73.Text = "xx"
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.ForeColor = System.Drawing.Color.White
        Me.Label74.Location = New System.Drawing.Point(525, 264)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(17, 13)
        Me.Label74.TabIndex = 135
        Me.Label74.Text = "xx"
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.ForeColor = System.Drawing.Color.White
        Me.Label75.Location = New System.Drawing.Point(525, 238)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(17, 13)
        Me.Label75.TabIndex = 134
        Me.Label75.Text = "xx"
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.ForeColor = System.Drawing.Color.White
        Me.Label76.Location = New System.Drawing.Point(525, 212)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(17, 13)
        Me.Label76.TabIndex = 133
        Me.Label76.Text = "xx"
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.ForeColor = System.Drawing.Color.White
        Me.Label77.Location = New System.Drawing.Point(193, 290)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(17, 13)
        Me.Label77.TabIndex = 132
        Me.Label77.Text = "xx"
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.ForeColor = System.Drawing.Color.White
        Me.Label78.Location = New System.Drawing.Point(193, 264)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(17, 13)
        Me.Label78.TabIndex = 131
        Me.Label78.Text = "xx"
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.ForeColor = System.Drawing.Color.White
        Me.Label79.Location = New System.Drawing.Point(193, 238)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(17, 13)
        Me.Label79.TabIndex = 130
        Me.Label79.Text = "xx"
        '
        'Label80
        '
        Me.Label80.AutoSize = True
        Me.Label80.ForeColor = System.Drawing.Color.White
        Me.Label80.Location = New System.Drawing.Point(193, 212)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(17, 13)
        Me.Label80.TabIndex = 129
        Me.Label80.Text = "xx"
        '
        'Label81
        '
        Me.Label81.AutoSize = True
        Me.Label81.ForeColor = System.Drawing.Color.White
        Me.Label81.Location = New System.Drawing.Point(27, 238)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(17, 13)
        Me.Label81.TabIndex = 128
        Me.Label81.Text = "xx"
        '
        'Label83
        '
        Me.Label83.AutoSize = True
        Me.Label83.ForeColor = System.Drawing.Color.White
        Me.Label83.Location = New System.Drawing.Point(27, 212)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(17, 13)
        Me.Label83.TabIndex = 127
        Me.Label83.Text = "xx"
        '
        'Label84
        '
        Me.Label84.AutoSize = True
        Me.Label84.ForeColor = System.Drawing.Color.White
        Me.Label84.Location = New System.Drawing.Point(27, 184)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(62, 13)
        Me.Label84.TabIndex = 126
        Me.Label84.Text = "Descrizione"
        '
        'lblMacchina
        '
        Me.lblMacchina.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMacchina.ForeColor = System.Drawing.Color.White
        Me.lblMacchina.Location = New System.Drawing.Point(443, 380)
        Me.lblMacchina.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblMacchina.Name = "lblMacchina"
        Me.lblMacchina.Size = New System.Drawing.Size(806, 30)
        Me.lblMacchina.TabIndex = 35
        Me.lblMacchina.Text = "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234" &
    "567890 1234567890 1234567890   1234567890 1234567890 1234567890 1234567890 12345" &
    "67890 1234567890"
        Me.lblMacchina.Visible = False
        '
        'txtMatricola
        '
        Me.txtMatricola.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold)
        Me.txtMatricola.Location = New System.Drawing.Point(821, 294)
        Me.txtMatricola.MaxLength = 12
        Me.txtMatricola.Name = "txtMatricola"
        Me.txtMatricola.Size = New System.Drawing.Size(324, 29)
        Me.txtMatricola.TabIndex = 36
        Me.txtMatricola.Visible = False
        '
        'txtBagBarcode
        '
        Me.txtBagBarcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold)
        Me.txtBagBarcode.Location = New System.Drawing.Point(821, 329)
        Me.txtBagBarcode.MaxLength = 12
        Me.txtBagBarcode.Name = "txtBagBarcode"
        Me.txtBagBarcode.Size = New System.Drawing.Size(324, 29)
        Me.txtBagBarcode.TabIndex = 37
        Me.txtBagBarcode.Visible = False
        '
        'picLogo
        '
        Me.picLogo.Image = CType(resources.GetObject("picLogo.Image"), System.Drawing.Image)
        Me.picLogo.Location = New System.Drawing.Point(1109, 94)
        Me.picLogo.Name = "picLogo"
        Me.picLogo.Size = New System.Drawing.Size(75, 85)
        Me.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picLogo.TabIndex = 30
        Me.picLogo.TabStop = False
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ConnectionToolStripMenuItem, Me.MainCommandsToolStripMenuItem, Me.ComScopeToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1466, 24)
        Me.MenuStrip1.TabIndex = 38
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ConnectionToolStripMenuItem
        '
        Me.ConnectionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuConnect, Me.DisconnectToolStripMenuItem})
        Me.ConnectionToolStripMenuItem.Name = "ConnectionToolStripMenuItem"
        Me.ConnectionToolStripMenuItem.Size = New System.Drawing.Size(81, 20)
        Me.ConnectionToolStripMenuItem.Text = "Connection"
        '
        'mnuConnect
        '
        Me.mnuConnect.Name = "mnuConnect"
        Me.mnuConnect.Size = New System.Drawing.Size(133, 22)
        Me.mnuConnect.Text = "Connect"
        '
        'DisconnectToolStripMenuItem
        '
        Me.DisconnectToolStripMenuItem.Name = "DisconnectToolStripMenuItem"
        Me.DisconnectToolStripMenuItem.Size = New System.Drawing.Size(133, 22)
        Me.DisconnectToolStripMenuItem.Text = "Disconnect"
        '
        'MainCommandsToolStripMenuItem
        '
        Me.MainCommandsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuOpen, Me.mnuClose, Me.mnuDeposit, Me.mnuWithdrawal, Me.mnuExtStatus, Me.ErrorLOGToolStripMenuItem, Me.GetDetailsOfNotesClassToolStripMenuItem, Me.ErrorLogAnalisysToolStripMenuItem})
        Me.MainCommandsToolStripMenuItem.Name = "MainCommandsToolStripMenuItem"
        Me.MainCommandsToolStripMenuItem.Size = New System.Drawing.Size(111, 20)
        Me.MainCommandsToolStripMenuItem.Text = "Main Commands"
        '
        'mnuOpen
        '
        Me.mnuOpen.Name = "mnuOpen"
        Me.mnuOpen.Size = New System.Drawing.Size(246, 22)
        Me.mnuOpen.Text = "Open"
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        Me.mnuClose.Size = New System.Drawing.Size(246, 22)
        Me.mnuClose.Text = "Close"
        '
        'mnuDeposit
        '
        Me.mnuDeposit.Name = "mnuDeposit"
        Me.mnuDeposit.Size = New System.Drawing.Size(246, 22)
        Me.mnuDeposit.Text = "Deposit"
        '
        'mnuWithdrawal
        '
        Me.mnuWithdrawal.Enabled = False
        Me.mnuWithdrawal.Name = "mnuWithdrawal"
        Me.mnuWithdrawal.Size = New System.Drawing.Size(246, 22)
        Me.mnuWithdrawal.Text = "Withdrawal"
        '
        'mnuExtStatus
        '
        Me.mnuExtStatus.Name = "mnuExtStatus"
        Me.mnuExtStatus.Size = New System.Drawing.Size(246, 22)
        Me.mnuExtStatus.Text = "Extended Status"
        '
        'ErrorLOGToolStripMenuItem
        '
        Me.ErrorLOGToolStripMenuItem.Name = "ErrorLOGToolStripMenuItem"
        Me.ErrorLOGToolStripMenuItem.Size = New System.Drawing.Size(246, 22)
        Me.ErrorLOGToolStripMenuItem.Text = "Error LOG"
        '
        'GetDetailsOfNotesClassToolStripMenuItem
        '
        Me.GetDetailsOfNotesClassToolStripMenuItem.Name = "GetDetailsOfNotesClassToolStripMenuItem"
        Me.GetDetailsOfNotesClassToolStripMenuItem.Size = New System.Drawing.Size(246, 22)
        Me.GetDetailsOfNotesClassToolStripMenuItem.Text = "Get details of notes classification"
        '
        'ErrorLogAnalisysToolStripMenuItem
        '
        Me.ErrorLogAnalisysToolStripMenuItem.Name = "ErrorLogAnalisysToolStripMenuItem"
        Me.ErrorLogAnalisysToolStripMenuItem.Size = New System.Drawing.Size(246, 22)
        Me.ErrorLogAnalisysToolStripMenuItem.Text = "ErrorLog Analisys"
        '
        'ComScopeToolStripMenuItem
        '
        Me.ComScopeToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.msnuComScopeShow, Me.mnuComScopeHide})
        Me.ComScopeToolStripMenuItem.Name = "ComScopeToolStripMenuItem"
        Me.ComScopeToolStripMenuItem.Size = New System.Drawing.Size(77, 20)
        Me.ComScopeToolStripMenuItem.Text = "ComScope"
        '
        'msnuComScopeShow
        '
        Me.msnuComScopeShow.Name = "msnuComScopeShow"
        Me.msnuComScopeShow.Size = New System.Drawing.Size(124, 22)
        Me.msnuComScopeShow.Text = "Visualizza"
        '
        'mnuComScopeHide
        '
        Me.mnuComScopeHide.Name = "mnuComScopeHide"
        Me.mnuComScopeHide.Size = New System.Drawing.Size(124, 22)
        Me.mnuComScopeHide.Text = "Nascondi"
        '
        'myTimer
        '
        Me.myTimer.Enabled = True
        Me.myTimer.Interval = 500
        '
        'cboCasNum
        '
        Me.cboCasNum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCasNum.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCasNum.FormattingEnabled = True
        Me.cboCasNum.Location = New System.Drawing.Point(301, 200)
        Me.cboCasNum.Margin = New System.Windows.Forms.Padding(4)
        Me.cboCasNum.Name = "cboCasNum"
        Me.cboCasNum.Size = New System.Drawing.Size(136, 45)
        Me.cboCasNum.TabIndex = 39
        Me.cboCasNum.Visible = False
        '
        'trvErrorLog
        '
        Me.trvErrorLog.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!)
        Me.trvErrorLog.Location = New System.Drawing.Point(15, 340)
        Me.trvErrorLog.Name = "trvErrorLog"
        Me.trvErrorLog.Size = New System.Drawing.Size(547, 238)
        Me.trvErrorLog.TabIndex = 40
        Me.trvErrorLog.Visible = False
        '
        'pgbErrorLog
        '
        Me.pgbErrorLog.Location = New System.Drawing.Point(15, 106)
        Me.pgbErrorLog.Maximum = 10000
        Me.pgbErrorLog.Name = "pgbErrorLog"
        Me.pgbErrorLog.Size = New System.Drawing.Size(547, 23)
        Me.pgbErrorLog.TabIndex = 41
        Me.pgbErrorLog.Visible = False
        '
        'btnEnd
        '
        Me.btnEnd.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEnd.Location = New System.Drawing.Point(644, 156)
        Me.btnEnd.Margin = New System.Windows.Forms.Padding(4)
        Me.btnEnd.Name = "btnEnd"
        Me.btnEnd.Size = New System.Drawing.Size(333, 89)
        Me.btnEnd.TabIndex = 42
        Me.btnEnd.Text = "btnEnd"
        Me.btnEnd.UseVisualStyleBackColor = True
        Me.btnEnd.Visible = False
        '
        'frmPrincipale
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(155, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(155, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1466, 837)
        Me.Controls.Add(Me.btnEnd)
        Me.Controls.Add(Me.pgbErrorLog)
        Me.Controls.Add(Me.trvErrorLog)
        Me.Controls.Add(Me.cboCasNum)
        Me.Controls.Add(Me.txtBagBarcode)
        Me.Controls.Add(Me.txtMatricola)
        Me.Controls.Add(Me.lblMacchina)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.grpSerialNumber)
        Me.Controls.Add(Me.grpExtendedStatus)
        Me.Controls.Add(Me.grpCashData)
        Me.Controls.Add(Me.picLogo)
        Me.Controls.Add(Me.txtSerialNumber)
        Me.Controls.Add(Me.cboCliente)
        Me.Controls.Add(Me.cboIdProdotto)
        Me.Controls.Add(Me.Grid1)
        Me.Controls.Add(Me.cmdPrincipale)
        Me.Controls.Add(Me.lblMessaggio)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmPrincipale"
        Me.Text = "0"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.Grid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCashData.ResumeLayout(False)
        Me.grpCashData.PerformLayout()
        Me.grpExtendedStatus.ResumeLayout(False)
        Me.grpExtendedStatus.PerformLayout()
        Me.grpSerialNumber.ResumeLayout(False)
        Me.grpSerialNumber.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.picLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdPrincipale As System.Windows.Forms.Button
    Friend WithEvents lblMessaggio As System.Windows.Forms.Label
    Friend WithEvents Grid1 As System.Windows.Forms.DataGridView
    Friend WithEvents txtSerialNumber As System.Windows.Forms.TextBox
    Friend WithEvents cboCliente As System.Windows.Forms.ComboBox
    Friend WithEvents cboIdProdotto As System.Windows.Forms.ComboBox
    Friend WithEvents grpCashData As GroupBox
    Friend WithEvents lblCasC As Label
    Friend WithEvents lblCasB As Label
    Friend WithEvents prbCasL As ProgressBar
    Friend WithEvents prbCasK As ProgressBar
    Friend WithEvents prbCasJ As ProgressBar
    Friend WithEvents prbCasI As ProgressBar
    Friend WithEvents prbCasH As ProgressBar
    Friend WithEvents prbCasG As ProgressBar
    Friend WithEvents prbCasF As ProgressBar
    Friend WithEvents prbCasE As ProgressBar
    Friend WithEvents prbCasD As ProgressBar
    Friend WithEvents prbCasC As ProgressBar
    Friend WithEvents prbCasB As ProgressBar
    Friend WithEvents lblCasA As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents prbCasA As ProgressBar
    Friend WithEvents lblCasL As Label
    Friend WithEvents lblCasK As Label
    Friend WithEvents lblCasJ As Label
    Friend WithEvents lblCasI As Label
    Friend WithEvents lblCasH As Label
    Friend WithEvents lblCasG As Label
    Friend WithEvents lblCasF As Label
    Friend WithEvents lblCasE As Label
    Friend WithEvents lblCasD As Label
    Friend WithEvents grpExtendedStatus As GroupBox
    Friend WithEvents lblStatusDescrDb As Label
    Friend WithEvents lblStatusDescrEb As Label
    Friend WithEvents lblStatusDescrL As Label
    Friend WithEvents lblStatusDescrJ As Label
    Friend WithEvents lblStatusDescrH As Label
    Friend WithEvents lblStatusDescrF As Label
    Friend WithEvents lblStatusDescrD As Label
    Friend WithEvents lblStatusDescrB As Label
    Friend WithEvents lblStatusDescrSafe As Label
    Friend WithEvents lblStatusDescrReader As Label
    Friend WithEvents Label82 As Label
    Friend WithEvents lblStatusDescrDa As Label
    Friend WithEvents lblStatusDescrEa As Label
    Friend WithEvents lblStatusDescrK As Label
    Friend WithEvents lblStatusDescrI As Label
    Friend WithEvents lblStatusDescrG As Label
    Friend WithEvents lblStatusDescrE As Label
    Friend WithEvents lblStatusDescrC As Label
    Friend WithEvents lblStatusDescrA As Label
    Friend WithEvents lblStatusDescrController As Label
    Friend WithEvents lblStatusDescrFeeder As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents lblStatusDa As Label
    Friend WithEvents lblStatusEa As Label
    Friend WithEvents lblStatusDb As Label
    Friend WithEvents lblStatusEb As Label
    Friend WithEvents lblStatusL As Label
    Friend WithEvents lblStatusK As Label
    Friend WithEvents lblStatusJ As Label
    Friend WithEvents lblStatusI As Label
    Friend WithEvents Label29 As Label
    Friend WithEvents Label34 As Label
    Friend WithEvents Label35 As Label
    Friend WithEvents Label36 As Label
    Friend WithEvents Label37 As Label
    Friend WithEvents Label38 As Label
    Friend WithEvents Label39 As Label
    Friend WithEvents Label40 As Label
    Friend WithEvents Label41 As Label
    Friend WithEvents Label42 As Label
    Friend WithEvents lblStatusH As Label
    Friend WithEvents lblStatusG As Label
    Friend WithEvents lblStatusF As Label
    Friend WithEvents lblStatusE As Label
    Friend WithEvents lblStatusD As Label
    Friend WithEvents lblStatusC As Label
    Friend WithEvents lblStatusB As Label
    Friend WithEvents lblStatusA As Label
    Friend WithEvents lblStatusSafe As Label
    Friend WithEvents lblStatusReader As Label
    Friend WithEvents lblStatusController As Label
    Friend WithEvents lblStatusFeeder As Label
    Friend WithEvents Label55 As Label
    Friend WithEvents Label56 As Label
    Friend WithEvents Label57 As Label
    Friend WithEvents Label58 As Label
    Friend WithEvents Label59 As Label
    Friend WithEvents Label60 As Label
    Friend WithEvents Label61 As Label
    Friend WithEvents Label62 As Label
    Friend WithEvents Label63 As Label
    Friend WithEvents Label64 As Label
    Friend WithEvents Label65 As Label
    Friend WithEvents Label66 As Label
    Friend WithEvents Label67 As Label
    Friend WithEvents Label68 As Label
    Friend WithEvents txtCasLBn As Label
    Friend WithEvents txtCasKBn As Label
    Friend WithEvents txtCasJBn As Label
    Friend WithEvents txtCasIBn As Label
    Friend WithEvents txtCasHBn As Label
    Friend WithEvents txtCasGBn As Label
    Friend WithEvents txtCasFBn As Label
    Friend WithEvents txtCasEBn As Label
    Friend WithEvents txtCasDBn As Label
    Friend WithEvents txtCasCBn As Label
    Friend WithEvents txtCasBBn As Label
    Friend WithEvents txtCasABn As Label
    Friend WithEvents txtCasLVal As Label
    Friend WithEvents txtCasKVal As Label
    Friend WithEvents txtCasJVal As Label
    Friend WithEvents txtCasIVal As Label
    Friend WithEvents txtCasHVal As Label
    Friend WithEvents txtCasGVal As Label
    Friend WithEvents txtCasFVal As Label
    Friend WithEvents txtCasEVal As Label
    Friend WithEvents txtCasDVal As Label
    Friend WithEvents txtCasCVal As Label
    Friend WithEvents txtCasBVal As Label
    Friend WithEvents txtCasAVal As Label
    Friend WithEvents txtCasLDen As Label
    Friend WithEvents txtCasKDen As Label
    Friend WithEvents txtCasJDen As Label
    Friend WithEvents txtCasIDen As Label
    Friend WithEvents txtCasHDen As Label
    Friend WithEvents txtCasGDen As Label
    Friend WithEvents txtCasFDen As Label
    Friend WithEvents txtCasEDen As Label
    Friend WithEvents txtCasDDen As Label
    Friend WithEvents txtCasCDen As Label
    Friend WithEvents txtCasBDen As Label
    Friend WithEvents txtCasADen As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents lblCasLFree As Label
    Friend WithEvents lblCasKFree As Label
    Friend WithEvents lblCasJFree As Label
    Friend WithEvents lblCasIFree As Label
    Friend WithEvents lblCasHFree As Label
    Friend WithEvents lblCasGFree As Label
    Friend WithEvents lblCasFFree As Label
    Friend WithEvents lblCasEFree As Label
    Friend WithEvents lblCasDFree As Label
    Friend WithEvents lblCasCFree As Label
    Friend WithEvents lblCasBFree As Label
    Friend WithEvents lblCasAFree As Label
    Friend WithEvents Label32 As Label
    Friend WithEvents grpSerialNumber As GroupBox
    Friend WithEvents LabelA As Label
    Friend WithEvents txtSerialA As TextBox
    Friend WithEvents lblSerialL As Label
    Friend WithEvents LabelL As Label
    Friend WithEvents txtSerialL As TextBox
    Friend WithEvents lblSerialK As Label
    Friend WithEvents LabelK As Label
    Friend WithEvents txtSerialK As TextBox
    Friend WithEvents lblSerialJ As Label
    Friend WithEvents LabelJ As Label
    Friend WithEvents txtSerialJ As TextBox
    Friend WithEvents lblSerialI As Label
    Friend WithEvents LabelI As Label
    Friend WithEvents txtSerialI As TextBox
    Friend WithEvents lblSerialH As Label
    Friend WithEvents LabelH As Label
    Friend WithEvents txtSerialH As TextBox
    Friend WithEvents lblSerialG As Label
    Friend WithEvents LabelG As Label
    Friend WithEvents txtSerialG As TextBox
    Friend WithEvents lblSerialF As Label
    Friend WithEvents LabelF As Label
    Friend WithEvents txtSerialF As TextBox
    Friend WithEvents lblSerialE As Label
    Friend WithEvents LabelE As Label
    Friend WithEvents txtSerialE As TextBox
    Friend WithEvents lblSerialD As Label
    Friend WithEvents LabelD As Label
    Friend WithEvents txtSerialD As TextBox
    Friend WithEvents lblSerialC As Label
    Friend WithEvents LabelC As Label
    Friend WithEvents txtSerialC As TextBox
    Friend WithEvents lblSerialB As Label
    Friend WithEvents LabelB As Label
    Friend WithEvents txtSerialB As TextBox
    Friend WithEvents lblSerialA As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents Label26 As Label
    Friend WithEvents Label27 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label21 As Label
    Private WithEvents picLogo As PictureBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label28 As Label
    Friend WithEvents Label33 As Label
    Friend WithEvents Label45 As Label
    Friend WithEvents Label48 As Label
    Friend WithEvents Label49 As Label
    Friend WithEvents Label50 As Label
    Friend WithEvents Label51 As Label
    Friend WithEvents Label52 As Label
    Friend WithEvents Label53 As Label
    Friend WithEvents Label54 As Label
    Friend WithEvents Label69 As Label
    Friend WithEvents Label70 As Label
    Friend WithEvents Label71 As Label
    Friend WithEvents Label72 As Label
    Friend WithEvents Label73 As Label
    Friend WithEvents Label74 As Label
    Friend WithEvents Label75 As Label
    Friend WithEvents Label76 As Label
    Friend WithEvents Label77 As Label
    Friend WithEvents Label78 As Label
    Friend WithEvents Label79 As Label
    Friend WithEvents Label80 As Label
    Friend WithEvents Label81 As Label
    Friend WithEvents Label83 As Label
    Friend WithEvents Label84 As Label
    Friend WithEvents lblMacchina As Label
    Friend WithEvents txtMatricola As TextBox
    Friend WithEvents txtBagBarcode As TextBox
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents MainCommandsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents mnuOpen As ToolStripMenuItem
    Friend WithEvents mnuClose As ToolStripMenuItem
    Friend WithEvents mnuDeposit As ToolStripMenuItem
    Friend WithEvents mnuWithdrawal As ToolStripMenuItem
    Friend WithEvents myTimer As Timer
    Friend WithEvents mnuExtStatus As ToolStripMenuItem
    Friend WithEvents lblCasLStatus As Label
    Friend WithEvents lblCasLEnable As Label
    Friend WithEvents lblCasKStatus As Label
    Friend WithEvents lblCasKEnable As Label
    Friend WithEvents lblCasJStatus As Label
    Friend WithEvents lblCasJEnable As Label
    Friend WithEvents lblCasIStatus As Label
    Friend WithEvents lblCasIEnable As Label
    Friend WithEvents lblCasHStatus As Label
    Friend WithEvents lblCasHEnable As Label
    Friend WithEvents lblCasGStatus As Label
    Friend WithEvents lblCasGEnable As Label
    Friend WithEvents lblCasFStatus As Label
    Friend WithEvents lblCasFEnable As Label
    Friend WithEvents lblCasEStatus As Label
    Friend WithEvents lblCasEEnable As Label
    Friend WithEvents lblCasDStatus As Label
    Friend WithEvents lblCasDEnable As Label
    Friend WithEvents lblCasCStatus As Label
    Friend WithEvents lblCasCEnable As Label
    Friend WithEvents lblCasBStatus As Label
    Friend WithEvents lblCasBEnable As Label
    Friend WithEvents lblCasAStatus As Label
    Friend WithEvents lblCasAEnable As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents ConnectionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents mnuConnect As ToolStripMenuItem
    Friend WithEvents DisconnectToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ComScopeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents msnuComScopeShow As ToolStripMenuItem
    Friend WithEvents mnuComScopeHide As ToolStripMenuItem
    Friend WithEvents cboCasNum As ComboBox
    Friend WithEvents trvErrorLog As TreeView
    Friend WithEvents pgbErrorLog As ProgressBar
    Friend WithEvents btnEnd As Button
    Friend WithEvents GetDetailsOfNotesClassToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ErrorLOGToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ErrorLogAnalisysToolStripMenuItem As ToolStripMenuItem
End Class
